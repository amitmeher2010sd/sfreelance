package com.devfriendsgroup.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devfriendsgroup.model.Career;
@Repository
public interface CareerRepository extends JpaRepository<Career, Long> { 

	@Query("FROM Career WHERE Id=:id") 
	Career findCareerById(@Param("id")Long id);
	@Query("FROM Career") 
	List<Career> findAllPublishedCareer();
} 
	