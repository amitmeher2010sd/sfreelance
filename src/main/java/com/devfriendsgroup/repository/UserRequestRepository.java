package com.devfriendsgroup.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devfriendsgroup.model.User;
import com.devfriendsgroup.model.UserRequest;
@Repository
public interface UserRequestRepository extends JpaRepository<UserRequest, Long> { 

	@Query("FROM UserRequest ORDER BY Id DESC") 
	List<UserRequest> findAllUserRequest();
	
} 
	