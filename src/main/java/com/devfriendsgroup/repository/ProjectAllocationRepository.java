package com.devfriendsgroup.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devfriendsgroup.model.ProjectAllocation;

public interface ProjectAllocationRepository extends JpaRepository<ProjectAllocation, Long> { 

	@Query(value = "FROM ProjectAllocation WHERE project.Id=:projectId ") 
	List<ProjectAllocation> findAllProjectAllocationByProjectId(@Param("projectId")Long projectId);
	
	@Query(value = "FROM ProjectAllocation WHERE project.Id=:projectId and projectStatus='INPROGRESS' ") 
	List<ProjectAllocation> findAllProjectAllocationByActiveAndProjectId(@Param("projectId")Long projectId);
	
	@Query(value = "FROM ProjectAllocation WHERE Id=:Id") 
	ProjectAllocation findProjectAllocationById(@Param("Id")Long Id);
	
	@Query(value = "FROM ProjectAllocation WHERE project.projectCodeInternal=:projectCodeInternal ") 
	List<ProjectAllocation> findAllProjectAllocationsByProjectCode(@Param("projectCodeInternal")String projectCodeInternal);
	
	@Query(value = "FROM ProjectAllocation WHERE createdBy.userId=:userId and projectStatus=:projectStatus ") 
	List<ProjectAllocation> findAllProjectAllocationByCreatedBy(@Param("userId")Long userId,@Param("projectStatus")String projectStatus);

	@Query(value = "SELECT COUNT(*)  as cnt FROM project_allocation WHERE project_status='INPROGRESS' and created_by=:created_by AND status='ACTIVE'", nativeQuery = true) 
	Long totalActiveProjectAllocation(@Param("created_by")Long created_by);

	@Query(value = "FROM ProjectAllocation WHERE createdBy.userId=:userId and projectStatus=:projectStatus and status=:status ") 
	List<ProjectAllocation> findAllProjectAllocationByCreatedByAndAllocationStatusAndStatus(@Param("userId")Long userId,@Param("projectStatus")String projectStatus,@Param("status")String status);
	  
	@Query(value = "SELECT ui.user_id FROM project_allocation pa INNER JOIN user_info ui  ON ui.user_id=pa.freelancer_user_id INNER JOIN user_role ur ON ur.user_id=ui.user_id   WHERE pa.project_status IN (:projectStatus)  AND pa.status='ACTIVE' AND pa.created_by=:userId ",nativeQuery = true) 
	Long[] findAllAllocatedFreelancer(@Param("userId")Long userId, @Param("projectStatus")String projectStatus);
	
	@Query(value = "SELECT p.client_user_id FROM project_allocation pa INNER JOIN project p ON p.id=pa.project_id  INNER JOIN user_info ui  ON ui.user_id=p.client_user_id INNER JOIN user_role ur ON ur.user_id=ui.user_id   WHERE pa.project_status IN (:projectStatus)  AND pa.status='ACTIVE'  ",nativeQuery = true)//AND pa.created_by=:userId 
	Long[] findAllAllocatedClient( @Param("projectStatus")String projectStatus);//@Param("userId")Long userId,
	
	@Query(value = "SELECT COUNT(*)  as cnt FROM project_allocation", nativeQuery = true) 
	Integer  totalProjectAllocation();

} 
	