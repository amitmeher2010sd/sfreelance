package com.devfriendsgroup.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devfriendsgroup.model.Post;
import com.devfriendsgroup.model.User;
import com.devfriendsgroup.model.UserRequest;
@Repository
public interface PostRepository extends JpaRepository<Post, Long> { 

	@Query("FROM Post WHERE status='ACTIVE' and posted=true and skills like '%:skill%' ORDER BY createdOn DESC") 
	List<Post> findAllPublishedPost(@Param("skill")String skill);
	@Query("FROM Post WHERE status='ACTIVE' and posted=true ORDER BY createdOn DESC") 
	List<Post> findAllPublishedPost();
} 
	