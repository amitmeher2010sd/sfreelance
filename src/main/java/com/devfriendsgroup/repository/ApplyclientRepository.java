package com.devfriendsgroup.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devfriendsgroup.model.Applyclient;

public interface ApplyclientRepository extends JpaRepository<Applyclient, Long> { 

	@Query("FROM Applyclient WHERE createdBy.userId=:createdById ORDER BY Id DESC") 
	List<Applyclient> findAllApplyclientByUserId(@Param("createdById")Long createdById);
	
	@Query("FROM Applyclient WHERE createdBy.userId=:createdById and demostatus=:demostatus ORDER BY Id DESC") 
	List<Applyclient> findAllApplyclientByUserIdAndDemoStatus(@Param("createdById")Long createdById,@Param("demostatus")String demostatus);

	

} 
	