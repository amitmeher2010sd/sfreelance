package com.devfriendsgroup.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devfriendsgroup.model.Project;
import com.devfriendsgroup.model.ProjectAllocation;

public interface ProjectRepository extends JpaRepository<Project, Long> { 

	@Query("FROM Project WHERE userRoleAgent.userRoleId=:userRoleId and projectStatus=:projectStatus") 
	List<Project> findAllProjectByUserRoleId(@Param("userRoleId")Long userRoleId, @Param("projectStatus")String projectStatus);
	@Query("FROM Project WHERE userRoleAgent.userRoleId=:userRoleId ") 
	List<Project> findAllProjectByUserRoleId(@Param("userRoleId")Long userRoleId);
	@Query("FROM Project WHERE Id=:Id") 
	Project findProjectById(@Param("Id")Long Id);
		
	@Query(value = "SELECT COUNT(*)  as cnt FROM project WHERE   created_by=:created_by AND status='ACTIVE'", nativeQuery = true) 
	Long  totalActiveProject(@Param("created_by")Long created_by);
	
	Project findByProjectCodeInternal(String projectcode);
	
	@Query("FROM Project WHERE userClient.userId=:userId ") 
	List<Project> findAllProjectByClientUserId(@Param("userId")Long userId);
	
	
	@Query("FROM Project WHERE projectCodeInternal IN (:projectcodes) ") 
	List<Project> findAllProjectByProjectCodes(@Param("projectcodes")String projectcodes);
	
	
	@Query(value = "SELECT COUNT(*)  as cnt FROM project", nativeQuery = true) 
	Integer  totalProject();

	

	
	
	
} 
	