package com.devfriendsgroup.repository;


import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devfriendsgroup.model.ProjectPayment;


public interface ProjectPaymentRepository extends JpaRepository<ProjectPayment, Long> { 

	
	@Query(value = "FROM ProjectPayment WHERE projectAllocation.project.Id=:projectId")
	  List<ProjectPayment> findAllProjectPaymentsByProjectId(@Param("projectId")Long projectId);
	@Query(value = "FROM ProjectPayment WHERE projectAllocation.project.projectCodeInternal=:projectCodeInternal ")
	List<ProjectPayment> findAllProjectPaymentsByProjectCode(String projectCodeInternal);
	
	@Query(value = "FROM ProjectPayment WHERE createdBy.userId=:userId   ORDER BY paymentDate ASC")
	List<ProjectPayment> findAllProjectPaymentsByUser(@Param("userId")Long userId);
	
	@Query(value = "FROM ProjectPayment WHERE createdBy.userId=:userId and paymentStatus=:paymentStatus  ")
	List<ProjectPayment> findAllProjectPaymentsBySearch(@Param("userId")Long userId,@Param("paymentStatus")String paymentStatus);
	
	@Query(value = "FROM ProjectPayment WHERE projectAllocation.project.Id=:projectId and projectAllocation.projectStatus=:projectAlloStatus  ")
	List<ProjectPayment> findAllProjectPaymentsByProjectIdAndStatus(@Param("projectId")Long projectId,@Param("projectAlloStatus")String projectAlloStatus);
	
	
	@Query(value = "FROM ProjectPayment WHERE projectAllocation.project.userClient.userId=:userId  ")	
	List<ProjectPayment> findAllProjectPaymentsByRoleAndClientUserId(@Param("userId")Long userId);
	@Query(value = "FROM ProjectPayment WHERE projectAllocation.userFreelancer.userId=:userId  ")	
	List<ProjectPayment> findAllProjectPaymentsByRoleAndFreelancerUserId(@Param("userId")Long userId);
	
	/*
	 * startDate endDate
	 * 
	 * @Query(value =
	 * "FROM ProjectPayment WHERE startDate BETWEEN :stDate AND :edDate AND paymentStatus='CREDIT'"
	 * ) List<ProjectPayment>
	 * findAllProjectPaymentsByRoleAndFreelancerUserId(@Param("userId")Long userId);
	 */
	
	@Query(value = "FROM ProjectPayment WHERE paymentStatus IN ('PENDING') AND createdBy.userId=:createdBy AND projectAllocation.projectStatus=:projectStatus   ")	
	List<ProjectPayment> findAllPendingPaymentsByUserId(@Param("createdBy")Long createdBy,@Param("projectStatus")String projectStatus);
	@Query(value = "FROM ProjectPayment WHERE createdBy.userId=:userId AND paymentStatus=:paymentStatus and paymentDate between :startDate and :endDate  ")	
	List<ProjectPayment> findAllProjectPaymentSearchByStatusAndDate(@Param("paymentStatus")String paymentStatus,@Param("startDate")Date startDate,@Param("endDate")Date endDate,@Param("userId")Long userId);
	
	@Query(value = "FROM ProjectPayment WHERE createdBy.userId=:userId AND paymentDate between :startDate and :endDate ")	
	List<ProjectPayment> findAllProjectPaymentSearchByDate(@Param("startDate")Date startDate,@Param("endDate")Date endDate,@Param("userId")Long userId);
	
	
	@Query(value = "FROM ProjectPayment WHERE createdBy.userId=:userId  ORDER BY paymentDate ASC,projectAllocation.project.projectCode ASC")	
	List<ProjectPayment> findAllProjectPaymentSearchByAllStatus(@Param("userId")Long userId);
	@Query(value = "FROM ProjectPayment WHERE createdBy.userId=:userId AND projectAllocation.projectStatus=:projectStatus  ORDER BY paymentDate ASC,projectAllocation.project.projectCode ASC")	
	List<ProjectPayment> findAllProjectPaymentSearchByAllStatusAndProjectStatus(@Param("userId")Long userId,@Param("projectStatus")String projectStatus);
	
	
	
	@Query(value = "FROM ProjectPayment WHERE paymentStatus=:paymentStatus AND createdBy.userId=:userId   ")	
	List<ProjectPayment> findAllProjectPaymentSearchByStatus(@Param("userId")Long userId,@Param("paymentStatus")String paymentStatus);

	
	
	
	@Query(value = "FROM ProjectPayment WHERE createdBy.userId=:userId and paymentfor=:paymentfor  ORDER BY paymentDate ASC,projectAllocation.project.projectCode ASC")	
	List<ProjectPayment> findAllProjectPaymentSearchByAllStatus(@Param("userId")Long userId,@Param("paymentfor")String paymentfor);
	@Query(value = "FROM ProjectPayment WHERE paymentStatus=:paymentStatus and paymentfor=:paymentfor AND createdBy.userId=:userId  ORDER BY paymentDate ASC,projectAllocation.project.projectCode ASC")	
	List<ProjectPayment> findAllProjectPaymentSearchByStatus(@Param("userId")Long userId,@Param("paymentStatus")String paymentStatus,@Param("paymentfor")String paymentfor);
	@Query(value = "FROM ProjectPayment WHERE createdBy.userId=:userId and paymentfor=:paymentfor AND paymentDate between :startDate and :endDate   ORDER BY paymentDate ASC,projectAllocation.project.projectCode ASC")	
	List<ProjectPayment> findAllProjectPaymentSearchByDate(@Param("startDate")Date startDate,@Param("endDate")Date endDate,@Param("userId")Long userId,@Param("paymentfor")String paymentfor);
	@Query(value = "FROM ProjectPayment WHERE createdBy.userId=:userId AND paymentStatus=:paymentStatus and paymentfor=:paymentfor and paymentDate between :startDate and :endDate  ORDER BY paymentDate ASC,projectAllocation.project.projectCode ASC")	
	List<ProjectPayment> findAllProjectPaymentSearchByStatusAndDate(@Param("paymentStatus")String paymentStatus,@Param("startDate")Date startDate,@Param("endDate")Date endDate,@Param("userId")Long userId,@Param("paymentfor")String paymentfor);
	
	
	
	@Query(value = "FROM ProjectPayment WHERE projectAllocation.project.projectCodeInternal=:projectCodeInternal and paymentStatus=:paymentStatus ")
	List<ProjectPayment> findAllProjectPaymentsByProjectCodeAndStatus(@Param("projectCodeInternal")String projectCodeInternal,@Param("paymentStatus")String paymentStatus);
	
	
	
	 
	
	@Query(value = "FROM ProjectPayment WHERE projectAllocation.userFreelancer.userName like %:anysearch% or projectAllocation.userFreelancer.mobileNo like %:anysearch% or projectAllocation.userFreelancer.name like %:anysearch% or projectAllocation.project.projectName like %:anysearch% or projectAllocation.project.projectCode like %:anysearch%")
	List<ProjectPayment> findAllProjectPaymentAnysearch(@Param("anysearch")String anysearch);
	
	@Query(value = "FROM ProjectPayment WHERE (projectAllocation.userFreelancer.userName like %:anysearch% or projectAllocation.userFreelancer.mobileNo like %:anysearch% or projectAllocation.userFreelancer.name like %:anysearch% or projectAllocation.project.projectName like %:anysearch% or projectAllocation.project.projectCode like %:anysearch%)  and paymentStatus=:paystatus ")
	List<ProjectPayment> findAllProjectPaymentAnysearchAndStatus(@Param("anysearch")String anysearch,@Param("paystatus")String paystatus);
	

	
	@Query(value = "FROM ProjectPayment WHERE createdBy.userId=:userId AND paymentStatus=:paymentStatus AND projectAllocation.projectStatus=:projectStatus ")	
	List<ProjectPayment> findAllProjectPaymentSearchByStatusAndProjectStatus(@Param("userId")Long userId, @Param("paymentStatus")String paymentStatus,@Param("projectStatus")String projectStatus);
	
	
	@Query(value = "FROM ProjectPayment WHERE paymentDate between :startDate and :endDate AND createdBy.userId=:userId AND projectAllocation.projectStatus=:projectStatus ")	
	List<ProjectPayment> findAllProjectPaymentSearchByDateAndProjectStatus(@Param("startDate")Date startDate,@Param("endDate")Date endDate,@Param("userId")Long userId,@Param("projectStatus")String projectStatus);
	

	@Query(value = "FROM ProjectPayment WHERE createdBy.userId=:userId AND paymentStatus=:paymentStatus and paymentDate between :startDate and :endDate  AND projectAllocation.projectStatus=:projectStatus ")	
	List<ProjectPayment> findAllProjectPaymentSearchByStatusAndDateAndProjectStatus(@Param("paymentStatus")String paymentStatus,@Param("startDate")Date startDate,@Param("endDate")Date endDate,@Param("userId")Long userId,@Param("projectStatus")String projectStatus);
	
	
} 
	