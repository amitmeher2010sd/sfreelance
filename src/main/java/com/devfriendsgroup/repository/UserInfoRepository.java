package com.devfriendsgroup.repository;

import java.util.List;
import java.util.StringJoiner;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devfriendsgroup.model.UserInfo;

public interface UserInfoRepository extends JpaRepository<UserInfo, Long> { 

	@Query(value = "SELECT ui.* FROM user_info ui WHERE ui.user_id IN (SELECT u.user_id FROM user u, user_role ur, role r WHERE ur.role_id=r.role_id  AND ur.user_id=u.user_id AND r.role_code=:rolecode  AND ur.created_by=:user_id) ", nativeQuery = true) 
	List<UserInfo> getAllUsersByUserIdAndRole(@Param("user_id")Long user_id,@Param("rolecode")String rolecode);
	
	@Query(value = "SELECT * FROM user_info ui WHERE user_id IN (SELECT u.user_id FROM user u, user_role ur, role r WHERE ur.role_id=r.role_id  AND ur.user_id=u.user_id AND r.role_code=:rolecode  AND ur.created_by=:user_id and ur.status=:status) and status=:status ", nativeQuery = true) 
	List<UserInfo> getAllUsersByUserIdAndRole(@Param("user_id")Long user_id,@Param("rolecode")String rolecode,@Param("status")String status);
	@Query(value = "SELECT * FROM user_info WHERE user_id IN (SELECT u.user_id FROM user u, user_role ur, role r WHERE ur.role_id=r.role_id  AND ur.user_id=u.user_id AND r.role_code=:rolecode  AND ur.created_by=:user_id and ur.status=:status) and status=:status and  skills like %:skill%  ", nativeQuery = true) 
	List<UserInfo> getAllUsersByUserIdStatusAndRole(@Param("user_id")Long user_id,@Param("rolecode")String rolecode,@Param("status")String status,@Param("skill")String skill);
	
	@Query(value = "SELECT * FROM user_info WHERE user_id IN (SELECT u.user_id FROM user u, user_role ur, role r WHERE ur.role_id=r.role_id  AND ur.user_id=u.user_id AND r.role_code=:rolecode  AND ur.created_by=:user_id)  and skills like %:skill%  ", nativeQuery = true) 
	List<UserInfo> getAllUsersByUserIdRoleAndSkill(@Param("user_id")Long user_id,@Param("rolecode")String rolecode,@Param("skill")String skill);


	@Query(value = "FROM UserInfo WHERE contractors.contractorId=:contractorId ") 	
	List<UserInfo> findAllUsersByContractorsId(@Param("contractorId")Long contractorId);
	@Query(value = "FROM UserInfo WHERE user.userId=:userId ") 	
	UserInfo findUserInfoByUserId(@Param("userId")Long userId);

	@Query(value = "FROM UserInfo WHERE user.userId IN (:userIds) ") 	
	List<UserInfo> findAllUserByIds(@Param("userIds")Long[] userIds);
	
	@Query(value = "FROM UserInfo WHERE user.userId IN (:userIds) and skills like %:skill% ") 	
	List<UserInfo> findAllUserByIds(@Param("userIds")Long[] userIds,@Param("skill")String skill);
} 
	