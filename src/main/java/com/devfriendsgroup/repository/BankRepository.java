package com.devfriendsgroup.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devfriendsgroup.model.Bank;

public interface BankRepository extends JpaRepository<Bank, Long> { 

	@Query("FROM Bank WHERE status=:status ORDER BY Id DESC") 
	List<Bank> findAllBankByStatus(@Param("status")String status);
	@Query("FROM Bank WHERE  Id=:id and status=:status") 
	Bank findBankByIdAndStatus(@Param("id")Long id,@Param("status")String status);
} 
	