package com.devfriendsgroup.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devfriendsgroup.model.User;
@Repository
public interface UserRepository extends JpaRepository<User, Long> { 

	@Query("FROM User WHERE userName=:userName AND password=:password AND status IS 'ACTIVE'") 
	User getUserForLogin(@Param("userName")String userName, @Param("password")String password);

	@Query("FROM User WHERE userId=:userId")    
	User findByUserId(@Param("userId")Long userId);

	@Query("FROM User WHERE userName=:userName") 
	User getUserByUserName(@Param("userName")String userName);

	@Query("FROM User WHERE userName=:unameOrmob or mobileNo=:unameOrmob")  
	User getUserByUserNameOrMobileNumber(@Param("unameOrmob")String unameOrmob);

	@Query("FROM User WHERE otp=:otp") 
	User matchOtp(@Param("otp")String otp);

	@Query("FROM User WHERE mobileNo=:mobileNo")  
	User getUserByMobile(@Param("mobileNo")String mobileNo);
	
} 
	