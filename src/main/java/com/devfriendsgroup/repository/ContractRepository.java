package com.devfriendsgroup.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devfriendsgroup.model.Contract;
import com.devfriendsgroup.model.Project;
import com.devfriendsgroup.model.ProjectAllocation;

public interface ContractRepository extends JpaRepository<Contract, Long> { 

	@Query("FROM Contract WHERE client.userId =:userId OR freelancer.userId=:userId OR createdBy.userId=:userId ORDER BY Id DESC") 
	List<Contract> findAllContractBySenderOrReceiver(@Param("userId")Long userId);
	
	@Query("FROM Contract WHERE Id=:Id") 
	Contract findContractById(@Param("Id")Long Id);
	
	Contract findByContarctCode(String contarctCode);
	
	@Query(value = "SELECT COUNT(*)  as cnt FROM Contract WHERE createdBy.userId=:created_by AND status=:status") 
	Long totalActiveContracts(@Param("created_by")Long created_by,@Param("status")String status);

	
} 
	