package com.devfriendsgroup.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.devfriendsgroup.model.Notification;

public interface NotificationRepository extends JpaRepository<Notification, Long> { 

	@Query("FROM Notification  ORDER BY Id DESC") 
	List<Notification> findAllNotification();
	
} 
	