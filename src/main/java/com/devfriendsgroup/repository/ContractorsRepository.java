package com.devfriendsgroup.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devfriendsgroup.model.Contract;
import com.devfriendsgroup.model.Contractors;
import com.devfriendsgroup.model.Project;
import com.devfriendsgroup.model.ProjectAllocation;

public interface ContractorsRepository extends JpaRepository<Contractors, Long> { 

	//@Query("FROM Contractors WHERE createdBy.userId=:createdById ORDER BY Id DESC") 
	//List<Contractors> findAllByUserId(@Param("createdById")Long createdById);
	@Query("FROM Contractors WHERE createdBy.userId=:createdById AND status=:status ORDER BY Id DESC") 
	List<Contractors> findAllByUserIdAndStatus(@Param("createdById")Long createdById,@Param("status")String status);
	
	@Query("SELECT count(*) as count FROM Contractors WHERE createdBy.userId=:createdById AND status=:status ORDER BY Id DESC") 
	Long totalActiveContractor(@Param("createdById")Long createdById,@Param("status")String status);
	

} 
	