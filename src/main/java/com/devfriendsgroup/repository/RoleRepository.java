package com.devfriendsgroup.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devfriendsgroup.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {

	@Query("FROM Role WHERE roleId=:roleId")     
	Role findRoleByRoleId(@Param("roleId")Long roleId);

	@Query("FROM Role WHERE status IS 'ACTIVE'")     
	List<Role> findAllActiveRole(); 
	
	@Query("FROM Role WHERE roleName=:roleName AND status IS 'ACTIVE'")     
	Role findActiveRoleByRoleCode(@Param("roleName")String roleName);
	
} 
	