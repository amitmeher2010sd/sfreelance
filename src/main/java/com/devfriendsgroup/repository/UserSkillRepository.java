package com.devfriendsgroup.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devfriendsgroup.model.Role;
import com.devfriendsgroup.model.Skill;
import com.devfriendsgroup.model.UserSkills;

public interface UserSkillRepository extends JpaRepository<UserSkills, Long> {


	@Query("FROM UserSkills WHERE user.userId=:userId")     
	List<UserSkills> findUserSkillsByUserId(@Param("userId")Long userId); 
	
	
} 
	