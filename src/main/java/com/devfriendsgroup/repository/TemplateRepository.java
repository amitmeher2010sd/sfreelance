package com.devfriendsgroup.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devfriendsgroup.model.Template;

public interface TemplateRepository extends JpaRepository<Template, Long> {
	@Query("FROM Template WHERE type =:type") 
	List<Template> findAllTemplate(@Param("type")String type);

	@Query("FROM Template WHERE (type ='DEFAULT'  and status='ACTIVE') or (createdBy.userId=:userId and type ='CUSTOM')") 
	List<Template> findAllCustomTemplateByUserId(@Param("userId")Long userId);
	
	@Query("FROM Template WHERE type ='DEFAULT'  and status='ACTIVE'") 
	List<Template> findAllDefaultTemplate();
	
	@Query("FROM Template WHERE Id =:Id") 
	Template findTemplateById(@Param("Id")Long Id);
	
} 
	