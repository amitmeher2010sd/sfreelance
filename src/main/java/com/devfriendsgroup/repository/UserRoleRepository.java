package com.devfriendsgroup.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import com.devfriendsgroup.model.UserRole;
@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, Long>{

	@Query("FROM UserRole WHERE userId.userId=:userId")  
	UserRole getUserRoleByUserId(@Param("userId")Long userId);
	@Query("FROM UserRole WHERE userId.userName=:userName")  
	UserRole getUserRoleByUserName(@Param("userName")String userName);
	@Query("FROM UserRole WHERE userRoleId=:userRoleId")  
	UserRole getUserRoleById(@Param("userRoleId")Long userRoleId);
	
	@Query(value = "SELECT u.user_id FROM user u, user_role ur, role r WHERE ur.role_id=r.role_id AND ur.user_id=u.user_id AND r.role_code='ADMIN'", nativeQuery = true)  
	List<BigInteger> getAllAdminUserBySuperAdmin();

	@Query(value = "SELECT u.user_id FROM user u, user_role ur, role r WHERE ur.role_id=r.role_id AND ur.user_id=u.user_id AND r.role_code IN ('CLIENT_AGENT','FREELANCER_AGENT') AND ur.admin_id=:userId", nativeQuery = true)  
	List<BigInteger> getAllAgentUserByAdmin(@Param("userId")Long userId);

	@Query(value = "SELECT u.user_id FROM user u, user_role ur, role r WHERE ur.role_id=r.role_id AND ur.user_id=u.user_id AND r.role_code='CLIENT' AND ur.agent_id=:userId AND ur.admin_id=:adminUserId", nativeQuery = true)  
	List<BigInteger> getAllClientUserByClientAgent(@Param("userId")Long userId, @Param("adminUserId")Long adminUserId);

	@Query("FROM UserRole WHERE adminId.userId=:userId")  
	List<UserRole> getUserRolesByAdminId(@Param("userId")Long userId);

	@Query("FROM UserRole WHERE agentId.userId=:userId")  
	List<UserRole> getUserRolesByAgentId(@Param("userId")Long userId);
	 
	@Query("FROM UserRole WHERE userId.usercode=:usercode")  
	UserRole getUserRoleByUserCode(@Param("usercode")String usercode);
	
	
	@Query("FROM UserRole u WHERE u.createdBy.userId=:userId AND u.roleId.roleName=:roleName") 
	List<UserRole> findAllUserRoleByUserAndRole(@Param("userId")Long userId,@Param("roleName")String roleName);
	
	@Query("FROM UserRole WHERE roleId.roleName=:roleName")  
	UserRole getUserRoleByRoleCode(@Param("roleName")String roleName);
	
	@Query(value = "FROM UserRole WHERE createdBy.userId=:createdBy") 
	List<UserRole>  findAllUserRoleByCreatedBy(@Param("createdBy")Long createdBy);
	@Query("FROM UserRole u WHERE u.roleId.roleName IN (:roleName)") 
	List<UserRole> findAllUserRoleByRole(@Param("roleName")String roleName);
	
	@Query("FROM UserRole u WHERE u.roleId.roleName IN ('ADMIN','CONSULTANCY')") 
	List<UserRole> findAllUserRoleByRole();
	
	
	
}
	