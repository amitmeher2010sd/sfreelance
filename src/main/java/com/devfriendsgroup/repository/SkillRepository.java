package com.devfriendsgroup.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devfriendsgroup.model.Role;
import com.devfriendsgroup.model.Skill;

public interface SkillRepository extends JpaRepository<Skill, Long> {


	@Query("FROM Skill WHERE status IS 'ACTIVE'")     
	List<Skill> findAllActiveSkill(); 
	
	
} 
	