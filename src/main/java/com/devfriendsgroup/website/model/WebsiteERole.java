package com.devfriendsgroup.website.model;

public enum WebsiteERole {
	ROLE_DEVELOPER,
	ROLE_DEVELOPERCONSULTANCY
}
