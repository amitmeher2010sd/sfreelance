package com.devfriendsgroup.website.model;

import javax.persistence.*;

import lombok.Data;
@Entity
@Table(name = "amyohm_website_roles")
public class WebsiteRole {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Enumerated(EnumType.STRING)
  @Column(length = 20)
  private WebsiteERole name;

public Long getId() {
	return id;
}

public void setId(Long id) {
	this.id = id;
}

public WebsiteERole getName() {
	return name;
}

public void setName(WebsiteERole name) {
	this.name = name;
}
  
}