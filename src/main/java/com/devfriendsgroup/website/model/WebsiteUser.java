package com.devfriendsgroup.website.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
@Entity
@Table(name = "amyohm_website_users",
       uniqueConstraints = {
           @UniqueConstraint(columnNames = "username"),
           @UniqueConstraint(columnNames = "email")
       })
public class WebsiteUser {
  @Id 
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String name;
 
  private String username;
  @JsonIgnore
  private String password;
  private String email;
  private String phone;
  

@JsonIgnore
  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(name = "amyohm_website_user_roles", 
             joinColumns = @JoinColumn(name = "user_id"),
             inverseJoinColumns = @JoinColumn(name = "role_id"))
  private Set<WebsiteRole> roles = new HashSet<>();
@Transient
private String rolename;
private String status;

private String activationcode;
@JsonIgnore
private String secretkey;
private boolean verificationmailsent;
public WebsiteUser() {
	
}


public WebsiteUser(Long id, String name, String username, String password, String email, String phone,
		Set<WebsiteRole> roles, String rolename, String status, String activationcode, String secretkey,
		boolean verificationmailsent) {
	super();
	this.id = id;
	this.name = name;
	this.username = username;
	this.password = password;
	this.email = email;
	this.phone = phone;
	this.roles = roles;
	this.rolename = rolename;
	this.status = status;
	this.activationcode = activationcode;
	this.secretkey = secretkey;
	this.verificationmailsent = verificationmailsent;
}


@Override
public String toString() {
	return "User [id=" + id + ", name=" + name + ", username=" + username + ", password=" + password + ", email="
			+ email + ", phone=" + phone + ", roles=" + roles + ", rolename=" + rolename + ", status=" + status
			+ ", activationcode=" + activationcode + ", secretkey=" + secretkey + "]";
}

public Long getId() {
	return id;
}

public void setId(Long id) {
	this.id = id;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public String getUsername() {
	return username;
}

public void setUsername(String username) {
	this.username = username;
}

public String getPassword() {
	return password;
}

public void setPassword(String password) {
	this.password = password;
}

public String getEmail() {
	return email;
}

public void setEmail(String email) {
	this.email = email;
}

public String getPhone() {
	return phone;
}

public void setPhone(String phone) {
	this.phone = phone;
}

public Set<WebsiteRole> getRoles() {
	return roles;
}

public void setRoles(Set<WebsiteRole> roles) {
	this.roles = roles;
}

public String getRolename() {
	return rolename;
}

public void setRolename(String rolename) {
	this.rolename = rolename;
}

public String getStatus() {
	return status;
}

public void setStatus(String status) {
	this.status = status;
}

public String getActivationcode() {
	return activationcode;
}

public void setActivationcode(String activationcode) {
	this.activationcode = activationcode;
}

public String getSecretkey() {
	return secretkey;
}

public void setSecretkey(String secretkey) {
	this.secretkey = secretkey;
}

public boolean isVerificationmailsent() {
	return verificationmailsent;
}

public void setVerificationmailsent(boolean verificationmailsent) {
	this.verificationmailsent = verificationmailsent;
}


  
}
