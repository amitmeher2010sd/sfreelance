package com.devfriendsgroup.website.model;
import java.util.Arrays;
import java.util.Date;
import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;

@Entity
@Table(name = "amyohm_website_user_info")
public class WebsiteUserInfo {
	@Override
	public String toString() {
		return "WebsiteUserInfo [id=" + id + ", user=" + user + ", userCode=" + userCode + ", dateofbirth="
				+ dateofbirth + ", companyName=" + companyName + ", dateofFounded=" + dateofFounded + ", address="
				+ address + ", description=" + description + ", secondaryphone=" + secondaryphone + ", website="
				+ website + ", facebook=" + facebook + ", twitter=" + twitter + ", linkedin=" + linkedin
				+ ", designation=" + designation + ", primaryskills=" + primaryskills + ", secondaryskills="
				+ secondaryskills + ", experience=" + experience + ", createdBy=" + createdBy + ", createdOn="
				+ createdOn + ", updatedBy=" + updatedBy + ", updatedOn=" + updatedOn + ", status=" + status
				+ ", message=" + message + ", gender=" + gender + ", profilephoto=" + profilephoto + ", cvfiles="
				+ cvfiles + ", idsfiles=" + idsfiles + ", syllabusfiles=" + syllabusfiles + ", ids=" + ids + ", cvs="
				+ cvs + ", primarySkillArray=" + Arrays.toString(primarySkillArray) + ", secondaryskillsArray="
				+ Arrays.toString(secondaryskillsArray) + ", parentUsercode=" + parentUsercode + ", supportmode="
				+ supportmode + ", supporttype=" + supporttype + ", supporthour=" + supporthour
				+ ", jobsupporttimeslot=" + jobsupporttimeslot + ", othertimeslot=" + othertimeslot + ", bankmodetype="
				+ bankmodetype + ", bankmodenumber=" + bankmodenumber + ", accountsholdername=" + accountsholdername
				+ ", accountsifsc=" + accountsifsc + ", accountsbankname=" + accountsbankname + ", accountsaccno="
				+ accountsaccno + ", packagename=" + packagename + ", otherskills=" + otherskills
				+ ", profileverification=" + profileverification + ", name=" + name + ", username=" + username
				+ ", password=" + password + ", email=" + email + ", phone=" + phone + "]";
	}
	public WebsiteUserInfo() {
		// TODO Auto-generated constructor stub
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name="user_id")
	private WebsiteUser user;

	@Column(name="usercode")
	public String userCode;
	@Column(name="dateofbirth")
	public String dateofbirth;
	@Column(name="companyname")
	public String companyName;
	@Column(name="dateoffounded")
	public String dateofFounded;

	@Column(name="address")
	public String address;
	@Column(name="description")
	public String description;
	@Column(name="secondaryphone")
	public String secondaryphone; 
	@Column(name="website")
	public String website;
	@Column(name="facebook")
	public String facebook;
	@Column(name="twitter")
	public String twitter;
	@Column(name="linkedin")
	public String linkedin;
	@Column(name="designation")
	public String designation;
	@Column(name="primaryskills")
	public String primaryskills;
	@Column(name="secondaryskills")
	public String secondaryskills;
	@Column(name="experience")
	public String experience;
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="created_by")
	private WebsiteUser createdBy;
	@JsonIgnore
	@Column(name="created_on")
	private Date createdOn;
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="updated_by")
	private WebsiteUser updatedBy;
	@JsonIgnore
	@Column(name="updated_on")
	private Date updatedOn;
	@JsonIgnore
	@Column(name="status")
	private String status;

	@Transient
	private String message;

	@Column(name="gender")
	private String gender;

	@Column(name="profilephoto")
	private String profilephoto;

	@Column(name="cv_files")
	private String cvfiles;
	@Column(name="ids_files")
	private String idsfiles;

	@Column(name="syllabus_files")
	private String syllabusfiles;


	@Transient
	private String ids;
	@Transient
	private String cvs;

	@Transient
	private String[] primarySkillArray;
	@Transient
	private String[] secondaryskillsArray;

	@Column(name="parent_usercode")
	private String parentUsercode;

	@Column(name="supportmode")
	private String supportmode;
	@Column(name="supporttype")
	private String supporttype;
	@Column(name="supporthour")
	private String supporthour;
	@Column(name="jobsupporttimeslot")
	private String jobsupporttimeslot;
	@Column(name="othertimeslot")
	private String othertimeslot;
	@Column(name="bankmodetype")
	private String bankmodetype;
	@Column(name="bankmodenumber")
	private String bankmodenumber;

	@Column(name="accountsholdername")
	private String accountsholdername;
	@Column(name="accounts_ifsc")
	private String accountsifsc;
	@Column(name="accounts_bankname")
	private String accountsbankname;
	@Column(name="accounts_accno")
	private String accountsaccno;

	@Column(name="packagename")
	private String packagename;
	@Column(name="otherskills")
	private String otherskills;
	@Column(name="profileverification")
	private String profileverification;
	
	
	@Transient
	private String name;
	@Transient
	private String username;
	@Transient
	private String password;
	@Transient
	private String email;
	@Transient
	private String phone;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public WebsiteUser getUser() {
		return user;
	}
	public void setUser(WebsiteUser user) {
		this.user = user;
	}
	public String getUserCode() {
		return userCode;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	public String getDateofbirth() {
		return dateofbirth;
	}
	public void setDateofbirth(String dateofbirth) {
		this.dateofbirth = dateofbirth;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getDateofFounded() {
		return dateofFounded;
	}
	public void setDateofFounded(String dateofFounded) {
		this.dateofFounded = dateofFounded;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getSecondaryphone() {
		return secondaryphone;
	}
	public void setSecondaryphone(String secondaryphone) {
		this.secondaryphone = secondaryphone;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public String getFacebook() {
		return facebook;
	}
	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}
	public String getTwitter() {
		return twitter;
	}
	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}
	public String getLinkedin() {
		return linkedin;
	}
	public void setLinkedin(String linkedin) {
		this.linkedin = linkedin;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getPrimaryskills() {
		return primaryskills;
	}
	public void setPrimaryskills(String primaryskills) {
		this.primaryskills = primaryskills;
	}
	public String getSecondaryskills() {
		return secondaryskills;
	}
	public void setSecondaryskills(String secondaryskills) {
		this.secondaryskills = secondaryskills;
	}
	public String getExperience() {
		return experience;
	}
	public void setExperience(String experience) {
		this.experience = experience;
	}
	public WebsiteUser getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(WebsiteUser createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public WebsiteUser getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(WebsiteUser updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getProfilephoto() {
		return profilephoto;
	}
	public void setProfilephoto(String profilephoto) {
		this.profilephoto = profilephoto;
	}
	public String getCvfiles() {
		return cvfiles;
	}
	public void setCvfiles(String cvfiles) {
		this.cvfiles = cvfiles;
	}
	public String getIdsfiles() {
		return idsfiles;
	}
	public void setIdsfiles(String idsfiles) {
		this.idsfiles = idsfiles;
	}
	public String getSyllabusfiles() {
		return syllabusfiles;
	}
	public void setSyllabusfiles(String syllabusfiles) {
		this.syllabusfiles = syllabusfiles;
	}
	public String getIds() {
		return ids;
	}
	public void setIds(String ids) {
		this.ids = ids;
	}
	public String getCvs() {
		return cvs;
	}
	public void setCvs(String cvs) {
		this.cvs = cvs;
	}
	public String[] getPrimarySkillArray() {
		return primarySkillArray;
	}
	public void setPrimarySkillArray(String[] primarySkillArray) {
		this.primarySkillArray = primarySkillArray;
	}
	public String[] getSecondaryskillsArray() {
		return secondaryskillsArray;
	}
	public void setSecondaryskillsArray(String[] secondaryskillsArray) {
		this.secondaryskillsArray = secondaryskillsArray;
	}
	public String getParentUsercode() {
		return parentUsercode;
	}
	public void setParentUsercode(String parentUsercode) {
		this.parentUsercode = parentUsercode;
	}
	public String getSupportmode() {
		return supportmode;
	}
	public void setSupportmode(String supportmode) {
		this.supportmode = supportmode;
	}
	public String getSupporttype() {
		return supporttype;
	}
	public void setSupporttype(String supporttype) {
		this.supporttype = supporttype;
	}
	public String getSupporthour() {
		return supporthour;
	}
	public void setSupporthour(String supporthour) {
		this.supporthour = supporthour;
	}
	public String getJobsupporttimeslot() {
		return jobsupporttimeslot;
	}
	public void setJobsupporttimeslot(String jobsupporttimeslot) {
		this.jobsupporttimeslot = jobsupporttimeslot;
	}
	public String getOthertimeslot() {
		return othertimeslot;
	}
	public void setOthertimeslot(String othertimeslot) {
		this.othertimeslot = othertimeslot;
	}
	public String getBankmodetype() {
		return bankmodetype;
	}
	public void setBankmodetype(String bankmodetype) {
		this.bankmodetype = bankmodetype;
	}
	public String getBankmodenumber() {
		return bankmodenumber;
	}
	public void setBankmodenumber(String bankmodenumber) {
		this.bankmodenumber = bankmodenumber;
	}
	public String getAccountsholdername() {
		return accountsholdername;
	}
	public void setAccountsholdername(String accountsholdername) {
		this.accountsholdername = accountsholdername;
	}
	public String getAccountsifsc() {
		return accountsifsc;
	}
	public void setAccountsifsc(String accountsifsc) {
		this.accountsifsc = accountsifsc;
	}
	public String getAccountsbankname() {
		return accountsbankname;
	}
	public void setAccountsbankname(String accountsbankname) {
		this.accountsbankname = accountsbankname;
	}
	public String getAccountsaccno() {
		return accountsaccno;
	}
	public void setAccountsaccno(String accountsaccno) {
		this.accountsaccno = accountsaccno;
	}
	public String getPackagename() {
		return packagename;
	}
	public void setPackagename(String packagename) {
		this.packagename = packagename;
	}
	public String getOtherskills() {
		return otherskills;
	}
	public void setOtherskills(String otherskills) {
		this.otherskills = otherskills;
	}
	public String getProfileverification() {
		return profileverification;
	}
	public void setProfileverification(String profileverification) {
		this.profileverification = profileverification;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	
	
}
