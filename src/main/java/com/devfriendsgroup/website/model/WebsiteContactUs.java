package com.devfriendsgroup.website.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.*;
@Data
@Entity
@Table(name="amyohm_website_contactus", schema = "public")
public class WebsiteContactUs implements Serializable{
	
	private static final long serialVersionUID = 285701719160134651L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="email")
	private String email; 
	@Column(name="mobno")
	private String mobno;
	
	@Column(name="message")
	private String message; 
	
	@Column(name="subject")
	private String subject; 
	
	
	@Column(name="created_on") 
	private Date createdOn; 

} 
	