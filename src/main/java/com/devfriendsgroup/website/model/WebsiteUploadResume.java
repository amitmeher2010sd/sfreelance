package com.devfriendsgroup.website.model;
import java.util.Date;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
@Data
@AllArgsConstructor
@Entity
@Table(name = "amyohm_website_upload_resume")
public class WebsiteUploadResume {
	public WebsiteUploadResume() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	
	@JsonIgnore
	@Column(name="created_on")
	private Date createdOn;
	
	@JsonIgnore
	@Column(name="status")
	private String status;

	@Transient
	private String message;


	@Column(name="uploadresume")
	private String uploadresume;

	@Column(name="subject")
	private String subject;
}
