package com.devfriendsgroup.website.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devfriendsgroup.website.model.WebsiteUploadResume;

@Repository
public interface WebsiteUploadResumeRepository extends JpaRepository<WebsiteUploadResume, Long> {
}
