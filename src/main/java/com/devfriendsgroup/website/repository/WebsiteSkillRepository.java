package com.devfriendsgroup.website.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.devfriendsgroup.website.model.WebsiteSkill;



public interface WebsiteSkillRepository extends JpaRepository<WebsiteSkill, Long> {


	@Query("FROM WebsiteSkill WHERE status IS 'ACTIVE'")     
	List<WebsiteSkill> findAllActiveSkill(); 
	
	
} 
	