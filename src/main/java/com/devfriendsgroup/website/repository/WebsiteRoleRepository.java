package com.devfriendsgroup.website.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devfriendsgroup.website.model.WebsiteERole;
import com.devfriendsgroup.website.model.WebsiteRole;

@Repository
public interface WebsiteRoleRepository extends JpaRepository<WebsiteRole, Long> {
  Optional<WebsiteRole> findByName(WebsiteERole name);
}
