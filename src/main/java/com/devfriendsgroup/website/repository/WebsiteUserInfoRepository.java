package com.devfriendsgroup.website.repository;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devfriendsgroup.website.model.WebsiteUserInfo;


public interface WebsiteUserInfoRepository extends JpaRepository<WebsiteUserInfo, Long> {

	
	@Query("from WebsiteUserInfo where user.username =:username and status='ACTIVE'")
	WebsiteUserInfo findUserInfoByUsername(@Param("username")String username); 
	@Query("from WebsiteUserInfo where user.id =:userId and status='ACTIVE'")
	WebsiteUserInfo findUserInfoByUserId(@Param("userId")Long userId);
	
	@Query("from WebsiteUserInfo where user.username =:username")
	WebsiteUserInfo findUserInfosByUsername(@Param("username")String username); 
	
	@Query("from WebsiteUserInfo where userCode =:userCode and status='ACTIVE'")
	Optional<WebsiteUserInfo> getUserInfoByUserCode(@Param("userCode")String userCode);
	
	@Query("from WebsiteUserInfo where parentUsercode =:parentUsercode and status='ACTIVE'")
	Optional<List<WebsiteUserInfo>> getAllUserInfoByParentCode(@Param("parentUsercode")String parentUsercode);
	
	@Query("SELECT user.username as uname from WebsiteUserInfo where parentUsercode =:parentUsercode and status='ACTIVE'")
	List<String> getAllUserNameByParentCode(@Param("parentUsercode")String parentUsercode);
	

} 
	