package com.devfriendsgroup.website.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devfriendsgroup.website.model.WebsiteUser;



@Repository
public interface WebsiteUserRepository extends JpaRepository<WebsiteUser, Long> {
  Optional<WebsiteUser> findByUsername(String username);

  Boolean existsByUsername(String username);

  Boolean existsByEmail(String email);

@Query("FROM WebsiteUser where username =:username")
Optional<WebsiteUser> findWebsiteUserByUsername(@Param("username")String username); 

@Query("FROM WebsiteUser where username =:username and status =:status")
Optional<WebsiteUser> findWebsiteUserByUsernameAndStatus(@Param("username")String username,@Param("status")String status);

@Query("FROM WebsiteUser where activationcode =:activationcode")
Optional<WebsiteUser> findWebsiteUserByActivationCode(@Param("activationcode")String activationcode);

@Query("FROM WebsiteUser where username =:data or email =:data")
Optional<WebsiteUser> findWebsiteUserByUserNameOrEmail(@Param("data")String data); 
}
