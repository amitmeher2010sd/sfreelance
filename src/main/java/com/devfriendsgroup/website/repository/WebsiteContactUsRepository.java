package com.devfriendsgroup.website.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.devfriendsgroup.website.model.WebsiteContactUs;
@Repository
public interface WebsiteContactUsRepository extends JpaRepository<WebsiteContactUs,Long> {
 
}
