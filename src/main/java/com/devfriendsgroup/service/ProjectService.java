package com.devfriendsgroup.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.devfriendsgroup.model.PaymentCalenderResponse;
import com.devfriendsgroup.model.Project;
import com.devfriendsgroup.model.ProjectAllocation;

public interface ProjectService {

	
	Project addOrUpdateProject(Project project);	
	List<Project> findAllProjectByUserRoleId(Long userRoleId, String projectStatus);
	Project getProjectById(Long project);
	boolean deleteProject(Long project);
	Project findProject(Long id_edit);
	Project changeProjectStatus(Long pid, String status);
	
	
	
	ProjectAllocation addOrUpdateProjectAllocation(ProjectAllocation projectAllocation);	
	List<ProjectAllocation> findAllProjectAllocationByProjectId(Long projectId);
	ProjectAllocation getProjectAllocationById(Long projectId);
	void deleteProjectAllocation(Long project);
	ProjectAllocation findProjectAllocation(Long id_edit);
	ProjectAllocation changeProjectAllocationStatus(Long pid, String status);
	List<ProjectAllocation> findAllProjectAllocationsByProjectCode(String projectcodeInternal);
	List<Project> findAllProjectByUserRoleId(Long userRoleId);
	List<ProjectAllocation> findAllProjectAllocationByCreatedBy(Long userId, String projectAllocationStatus);
	Project findByProjectCodeInternal(String projectcode);
	
	List<ProjectAllocation> findAllProjectAllocationByCreatedByAndAllocationStatusAndStatus(Long userId, String projectAllocationStatus,String status);
	public List<PaymentCalenderResponse> getPaymentsCalenders(Long UserId);
	public List<PaymentCalenderResponse> getPayCalenders(Long UserId);
	List<Project> findAllProjectByProjectCodes(String projectcodes);
	public String  getNewProjectCode();
	

} 
	