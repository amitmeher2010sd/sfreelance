package com.devfriendsgroup.service;

import java.util.List;

import com.devfriendsgroup.model.ProjectPayment;
import com.devfriendsgroup.model.Contract;
import com.devfriendsgroup.model.Project;
import com.devfriendsgroup.model.ProjectAllocation;
import com.devfriendsgroup.model.Role;
import com.devfriendsgroup.model.Template;
import com.devfriendsgroup.model.User;
import com.devfriendsgroup.model.UserInfo;
import com.devfriendsgroup.model.UserRole;

public interface TemplateService {


	public List<Template> findAllCustomTemplateByUserId(Long userId) ;
	public List<Template> findAllDefaultTemplate() ;
	void deleteTemplate(Long id_del);
	Template findTemplate(Long id_edit);
	Template addOrUpdateTemplate(Template template);
	
	
   
} 
	