package com.devfriendsgroup.service;

import java.util.List;

import org.springframework.data.repository.query.Param;

import com.devfriendsgroup.model.ProjectPayment;
import com.devfriendsgroup.model.Contract;
import com.devfriendsgroup.model.Contractors;
import com.devfriendsgroup.model.DashBoard;
import com.devfriendsgroup.model.Post;
import com.devfriendsgroup.model.Project;
import com.devfriendsgroup.model.ProjectAllocation;
import com.devfriendsgroup.model.Role;
import com.devfriendsgroup.model.SearchRequest;
import com.devfriendsgroup.model.SearchResponse;
import com.devfriendsgroup.model.Skill;
import com.devfriendsgroup.model.Template;
import com.devfriendsgroup.model.User;
import com.devfriendsgroup.model.UserInfo;
import com.devfriendsgroup.model.UserRequest;
import com.devfriendsgroup.model.UserRole;

public interface SearchAndReportService {

	SearchResponse getSearchDetails(SearchRequest searchRequest);
	public List<UserInfo> getNonAllocatedUser(Long userId,String status,String type,String skills);
	public List<String> revenuegraph(String data) throws Exception ;

} 
	