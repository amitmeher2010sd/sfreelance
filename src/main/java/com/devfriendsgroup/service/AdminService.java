package com.devfriendsgroup.service;

import java.util.List;

import com.devfriendsgroup.model.Applyclient;
import com.devfriendsgroup.model.Bank;
import com.devfriendsgroup.model.Contractors;
import com.devfriendsgroup.model.DashBoard;
import com.devfriendsgroup.model.Notification;
import com.devfriendsgroup.model.Post;
import com.devfriendsgroup.model.Role;
import com.devfriendsgroup.model.Skill;
import com.devfriendsgroup.model.User;
import com.devfriendsgroup.model.UserInfo;
import com.devfriendsgroup.model.UserRequest;
import com.devfriendsgroup.model.UserRole;

public interface AdminService {

	Role addOrUpdateRole(Role role);	
	List<Role> findAllRole();
	Role getRoleById(Long roleId);
	void deleteRole(Long id_del, String status);
		
	UserInfo addUserInfo(UserInfo userInfo); 
	
	User getUserByUserName(String userName);

	List<Role> findAllActiveRole();

	List<User> findAllUser();

	User getUserById(Long userIdForEnableUser);

	public User disableUser(Long userId);
	public User enableUser(Long userId);

	Role findActiveRoleByRoleCode(String roleCode);     
	
	List<UserInfo> getAllAdminUserBySuperAdmin();

	

	List<UserInfo> getAllClientUserByClientAgent(Long userId);
	public List<UserInfo> getAllUsers(Long userId,String rtype,String status,String skill,String isActive);

	UserInfo findUserInfoById(Long id_edit);
		
	
	List<UserRole> findAllUserRoleByUserAndRole(Long userId, String rolename);
	Skill addOrUpdateSkill(Skill skill);
	List<Skill> findAllSkills();	
	Skill getSkillById(Long id_edit);
	void deleteSkill(Long id_del, String status);
	List<Skill> findAllActiveSkills();	
	List<UserInfo> getAllUsersByUserIdAndRole(Long userId, String role);
	
	public DashBoard loadDashBoardData(Long userId);
	
	
	
	Applyclient addOrUpdateApplyclient(Applyclient applyclient);
	List<Applyclient> findAllApplyclientByUserId(Long createdId);
	Applyclient findApplyclientById(Long id_edit);
	
	Contractors addOrUpdateContractors(Contractors contractors);
	List<Contractors> findAllByUserId(Long createdId, String status);
	List<Contractors> findAllByUserIdAndStatus(Long createdId,String status);
	boolean deleteContractor(Long id_del,String status);
	Contractors findContractorsById(Long id_edit);
	
	List<UserRequest> findAllUserRequest();
	UserRequest saveUserRequest(UserRequest userRequest);
	
	
	List<Post> findAllPublishedPost(String skill);
	public List<Post> findAllPost();
	Post addOrUpdatePost(Post post);
	void deletePost(Long id_del, String status);
	Post findPostById(Long id_edit);
	void publishPost(Long publish_id, boolean publish_flag);
	String validateUser(String data);
	public List<Applyclient>  findAllApplyclientByUserIdAndDemoStatus(Long userId, String demostatus);
	
	
	List<Bank> findAllBankByStatus(String status);
	Bank findBankByIdAndStatus(Long id,String status);
	
	List<Notification> findAllNotification();

} 
	