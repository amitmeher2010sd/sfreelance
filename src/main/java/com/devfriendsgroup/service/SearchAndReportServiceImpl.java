package com.devfriendsgroup.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.devfriendsgroup.model.ProjectPayment;
import com.devfriendsgroup.model.Contract;
import com.devfriendsgroup.model.Contractors;
import com.devfriendsgroup.model.DashBoard;
import com.devfriendsgroup.model.Post;
import com.devfriendsgroup.model.Project;
import com.devfriendsgroup.model.ProjectAllocation;
import com.devfriendsgroup.model.Role;
import com.devfriendsgroup.model.SearchRequest;
import com.devfriendsgroup.model.SearchResponse;
import com.devfriendsgroup.model.Skill;
import com.devfriendsgroup.model.Template;
import com.devfriendsgroup.model.User;
import com.devfriendsgroup.model.UserInfo;
import com.devfriendsgroup.model.UserRequest;
import com.devfriendsgroup.model.UserRole;
import com.devfriendsgroup.model.UserSkills;
import com.devfriendsgroup.repository.ContractRepository;
import com.devfriendsgroup.repository.ContractorsRepository;
import com.devfriendsgroup.repository.PostRepository;
import com.devfriendsgroup.repository.ProjectAllocationRepository;
import com.devfriendsgroup.repository.ProjectPaymentRepository;
import com.devfriendsgroup.repository.ProjectRepository;
import com.devfriendsgroup.repository.RoleRepository;
import com.devfriendsgroup.repository.SkillRepository;
import com.devfriendsgroup.repository.TemplateRepository;
import com.devfriendsgroup.repository.UserInfoRepository;
import com.devfriendsgroup.repository.UserRepository;
import com.devfriendsgroup.repository.UserRequestRepository;
import com.devfriendsgroup.repository.UserRoleRepository;
import com.devfriendsgroup.repository.UserSkillRepository;

import com.devfriendsgroup.util.DateUtil;
import com.devfriendsgroup.util.MasterData;
import com.devfriendsgroup.util.SecurityHelper;

@Service
public class SearchAndReportServiceImpl implements SearchAndReportService {

	@Autowired
	UserInfoRepository userInfoRepository; 
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	ProjectAllocationRepository allocationRepository;

	@Autowired
	ProjectPaymentRepository projectPaymentRepository;
	@Autowired
	ContractRepository contractRepository;
	@Autowired
	TemplateRepository templateRepository;
	@Autowired
	SkillRepository skillRepository;
	
	@Autowired
	ContractorsRepository contractorsRepository;
	@Autowired
	UserRequestRepository userRequestRepository;
	
	@Autowired
	ProjectService projectService;
	@Autowired
	AdminService adminService;
	@Autowired
	UserSkillRepository userSkillRepository;
	@Override
	public SearchResponse getSearchDetails(SearchRequest searchRequest) {
		SearchResponse response=new SearchResponse();
		if("USER".equals(searchRequest.getRdSearchBy())) {
			response.setUserInfoList(this.getUsersByUserTypeAndStatus(searchRequest));
		}
		else if("PAYMENT".equals(searchRequest.getRdSearchBy())) {
			response.setPaymentsList(getPaymentDetails(searchRequest));
		}	
		else if("PROJECT".equals(searchRequest.getRdSearchBy())) {
			response.setPaymentsList(null);
		}	
		return response;
	}	
	
	public List<UserInfo> getUsersByUserTypeAndStatus(SearchRequest searchRequest) {
		List<UserInfo> userInfoList=null;
		String status="";
		User user = SecurityHelper.getCurrentUser(); 
		if(searchRequest.getUser().equals("ALL_FREELANCER"))
		{
			userInfoList = adminService.getAllUsers(user.getUserId(),"f","",searchRequest.getSkills(),""); 
		
		} else if(searchRequest.getUser().equals("ALLOCATED_FREELANCER"))
		{
			status="INPROGRESS";
			Long[] flist=allocationRepository.findAllAllocatedFreelancer(user.getUserId(),status);
			if(flist.length>0) {	
				
				if(searchRequest.getSkills()!=null && !"".equals(searchRequest.getSkills()))
				{
					userInfoList=userInfoRepository.findAllUserByIds(flist,searchRequest.getSkills()); 
				}
				else {
					userInfoList=userInfoRepository.findAllUserByIds(flist);
				}
				
			
			}
			
		}
		else if(searchRequest.getUser().equals("NONE_ALLOCATED_FREELANCER"))
		{
			status="ACTIVE";
			userInfoList=getNonAllocatedUser(user.getUserId(),status,"f",searchRequest.getSkills());
		}
		else if(searchRequest.getUser().equals("ALLOCATED_CLIENT"))
		{
			status="INPROGRESS";
			Long[] flist=allocationRepository.findAllAllocatedClient(status);//user.getUserId(),
			if(flist.length>0) {	
				if(searchRequest.getSkills()!=null && !"".equals(searchRequest.getSkills()))
				{
					userInfoList=userInfoRepository.findAllUserByIds(flist,searchRequest.getSkills()); 
				}
				else {
					userInfoList=userInfoRepository.findAllUserByIds(flist);
				}
			
			}
		}
		else if(searchRequest.getUser().equals("NONE_ALLOCATED_CLIENT"))
		{
			status="ACTIVE";
			userInfoList=getNonAllocatedUser(user.getUserId(),status,"c",searchRequest.getSkills());
		}
		else if(searchRequest.getUser().equals("ALL_CLIENT"))
		{
			userInfoList = adminService.getAllUsers(user.getUserId(),"c","",searchRequest.getSkills(),""); 
		}
		
		return userInfoList;
	}
	@Override
	public List<UserInfo> getNonAllocatedUser(Long userId,String status,String type,String skills) {
		List<UserInfo> userInfoList = adminService.getAllUsers(userId,type,status,skills,""); 
		
			
			Long[] flist=null;
			if(type.equals("f"))
			{
			flist=allocationRepository.findAllAllocatedFreelancer(userId,"INPROGRESS");
			}else {
			flist=allocationRepository.findAllAllocatedClient("INPROGRESS");//userId,
			}
			
			if(flist.length>0) {			
			userInfoList.removeAll(userInfoRepository.findAllUserByIds(flist));
			
			}
		
		return userInfoList;
	}

	public List<ProjectPayment> getPaymentDetails(SearchRequest searchRequest) {
		List<ProjectPayment> paylist=null;
		User user = SecurityHelper.getCurrentUser(); 
		
		
		 if(searchRequest.getAnysearch()!=null && !"".equals(searchRequest.getAnysearch())) {
			 
			 if(searchRequest.getPaystatus().equals("ALL"))
				{		
				 paylist=projectPaymentRepository.findAllProjectPaymentAnysearch(searchRequest.getAnysearch());
					
				}
			 else {
				 paylist=projectPaymentRepository.findAllProjectPaymentAnysearchAndStatus(searchRequest.getAnysearch(),searchRequest.getPaystatus());				
			 }
			 return paylist;
		 }
		
		 if(searchRequest.getPayType().equals("ALL")) {
		
		
	  if(searchRequest.getPaystatus().equals("ALL") & (searchRequest.getStartDateStr()==null || "".equals(searchRequest.getStartDateStr()) || searchRequest.getEndDateStr()==null || "".equals(searchRequest.getEndDateStr())))
		{			
			
			if(searchRequest.getProjectstatus()!=null  & !"".equals(searchRequest.getProjectstatus()) & !"ALL".equals(searchRequest.getProjectstatus())){
				paylist=projectPaymentRepository.findAllProjectPaymentSearchByAllStatusAndProjectStatus(user.getUserId(),searchRequest.getProjectstatus());
			}
			else
			{
				paylist=projectPaymentRepository.findAllProjectPaymentSearchByAllStatus(user.getUserId());
			}
		}
	  else if(!searchRequest.getPaystatus().equals("ALL") & (searchRequest.getStartDateStr()==null || "".equals(searchRequest.getStartDateStr()) || searchRequest.getEndDateStr()==null || "".equals(searchRequest.getEndDateStr())))
		{			
			
			if(searchRequest.getProjectstatus()!=null  & !"".equals(searchRequest.getProjectstatus()) & !"ALL".equals(searchRequest.getProjectstatus())){
				paylist=projectPaymentRepository.findAllProjectPaymentSearchByStatusAndProjectStatus(user.getUserId(),searchRequest.getPaystatus(),searchRequest.getProjectstatus());
			}
			else
			{
				paylist=projectPaymentRepository.findAllProjectPaymentSearchByStatus(user.getUserId(),searchRequest.getPaystatus());
			}
		
		}
	  else {
		Date start_=DateUtil.formatDDMMYYYY(searchRequest.getStartDateStr());
		Date end_=DateUtil.formatDDMMYYYY(searchRequest.getEndDateStr());
		if(searchRequest.getPaystatus().equals("ALL"))
		{			
			
			if(searchRequest.getProjectstatus()!=null  & !"".equals(searchRequest.getProjectstatus()) & !"ALL".equals(searchRequest.getProjectstatus())){
				paylist=projectPaymentRepository.findAllProjectPaymentSearchByDateAndProjectStatus(start_,end_,user.getUserId(),searchRequest.getProjectstatus());
			}
			else
			{
				paylist=projectPaymentRepository.findAllProjectPaymentSearchByDate(start_,end_,user.getUserId());
			}
		}
		
		else
		{
			if(searchRequest.getProjectstatus()!=null  & !"".equals(searchRequest.getProjectstatus()) & !"ALL".equals(searchRequest.getProjectstatus())){
				paylist=projectPaymentRepository.findAllProjectPaymentSearchByStatusAndDateAndProjectStatus(searchRequest.getPaystatus(),start_,end_,user.getUserId(),searchRequest.getProjectstatus());
			}
			else
			{
				paylist=projectPaymentRepository.findAllProjectPaymentSearchByStatusAndDate(searchRequest.getPaystatus(),start_,end_,user.getUserId());
			}
			
		}
	  }
	}
		 else 
		 {
			  if(searchRequest.getPaystatus().equals("ALL") & (searchRequest.getStartDateStr()==null || "".equals(searchRequest.getStartDateStr()) || searchRequest.getEndDateStr()==null || "".equals(searchRequest.getEndDateStr())))
				{			
					paylist=projectPaymentRepository.findAllProjectPaymentSearchByAllStatus(user.getUserId(),searchRequest.getPayType());
				}
			  else if(!searchRequest.getPaystatus().equals("ALL") & (searchRequest.getStartDateStr()==null || "".equals(searchRequest.getStartDateStr()) || searchRequest.getEndDateStr()==null || "".equals(searchRequest.getEndDateStr())))
				{			
					paylist=projectPaymentRepository.findAllProjectPaymentSearchByStatus(user.getUserId(),searchRequest.getPaystatus(),searchRequest.getPayType());
				}
			  else {
				Date start_=DateUtil.formatDDMMYYYY(searchRequest.getStartDateStr());
				Date end_=DateUtil.formatDDMMYYYY(searchRequest.getEndDateStr());
				if(searchRequest.getPaystatus().equals("ALL"))
				{			
					paylist=projectPaymentRepository.findAllProjectPaymentSearchByDate(start_,end_,user.getUserId(),searchRequest.getPayType());
				}
				
				else
				{
					
					paylist=projectPaymentRepository.findAllProjectPaymentSearchByStatusAndDate(searchRequest.getPaystatus(),start_,end_,user.getUserId(),searchRequest.getPayType());
				}
			  }
		 }
		
		
		
		
		
		return paylist;
	}
	@Override
	public List<String> revenuegraph(String year) throws Exception {
		
		year=MasterData.CalendarData("YEAR");
		String month=MasterData.CalendarData("MONTH");
		
		User user = SecurityHelper.getCurrentUser(); 
		
		List<String> responseList =new ArrayList<>();
		for (int i = 1; i <= 12; i++) {
			String StartDateStr="01-"+i+"-"+year;
			String EndDateStr="31-"+i+"-"+year;
			Date start_=DateUtil.formatDDMMYYYY(StartDateStr);
			Date end_=DateUtil.formatDDMMYYYY(EndDateStr);
		   List<ProjectPayment> paylist=projectPaymentRepository.findAllProjectPaymentSearchByStatusAndDate("PAID",start_,end_,user.getUserId());
		   Double credit=0d; 
		   Double debit=0d;
			for (ProjectPayment payment : paylist) {
				Double inr=1d;
				if("USD".equals(payment.getCurrency())) {
					inr=MasterData.USD_TO_INR;
				}
				else if("CAD".equals(payment.getCurrency())) {
					inr=MasterData.CAD_TO_INR;
				}
				
				if("CREDIT".equals(payment.getPaymentfor()))
				{
					credit=credit+(payment.getPaymentAmount()*inr);
					
				}
				else if("DEBIT".equals(payment.getPaymentfor()))
				{
				  debit=debit+(payment.getPaymentAmount()*inr);									
				}
			}
			
			
			
			responseList.add(Math.round(credit)+"-"+Math.round(debit)+"-"+Math.round(credit-debit));	
			
		}		
		
		
		String currentMonth="";
		for (int i = 1; i <= 31;i=(i+7)) {
		String StartDateStr1=i+"-"+month+"-"+year;
		String EndDateStr1=(i+7-1)+"-"+month+"-"+year;
		Date start_1=DateUtil.formatDDMMYYYY(StartDateStr1);
		Date end_1=DateUtil.formatDDMMYYYY(EndDateStr1);
	   List<ProjectPayment> paylist=projectPaymentRepository.findAllProjectPaymentSearchByStatusAndDate("PAID",start_1,end_1,user.getUserId());
	   Double credit_week=0d; 
	   Double debit_week=0d;
	   for (ProjectPayment payment : paylist) {
			
			Double inr=1d;
			if("USD".equals(payment.getCurrency())) {
				inr=MasterData.USD_TO_INR;
			}
			else if("CAD".equals(payment.getCurrency())) {
				inr=MasterData.CAD_TO_INR;
			}
			
			if("CREDIT".equals(payment.getPaymentfor()))
			{
				credit_week=credit_week+(payment.getPaymentAmount()*inr);
				
			}
			else if("DEBIT".equals(payment.getPaymentfor()))
			{
			  debit_week=debit_week+(payment.getPaymentAmount()*inr);									
			}
		}	
	   System.out.println("Date D: "+StartDateStr1+" to "+EndDateStr1+"====="+Math.round(credit_week-debit_week));
		if(i==1)
		{
			currentMonth=Math.round(credit_week)+"S"+Math.round(debit_week)+"S"+Math.round(credit_week-debit_week);
		}
		else
		{
			currentMonth=currentMonth+"--"+Math.round(credit_week)+"S"+Math.round(debit_week)+"S"+Math.round(credit_week-debit_week);
		}		
		
		}
		responseList.add(currentMonth);
		
		return responseList;
	}

} 
	