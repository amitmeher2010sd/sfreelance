package com.devfriendsgroup.service;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.devfriendsgroup.model.ProjectPayment;
import com.devfriendsgroup.model.Applyclient;
import com.devfriendsgroup.model.Bank;
import com.devfriendsgroup.model.Contract;
import com.devfriendsgroup.model.Contractors;
import com.devfriendsgroup.model.DashBoard;
import com.devfriendsgroup.model.Notification;
import com.devfriendsgroup.model.Post;
import com.devfriendsgroup.model.Project;
import com.devfriendsgroup.model.ProjectAllocation;
import com.devfriendsgroup.model.Role;
import com.devfriendsgroup.model.Skill;
import com.devfriendsgroup.model.Template;
import com.devfriendsgroup.model.User;
import com.devfriendsgroup.model.UserInfo;
import com.devfriendsgroup.model.UserRequest;
import com.devfriendsgroup.model.UserRole;
import com.devfriendsgroup.model.UserSkills;
import com.devfriendsgroup.repository.ApplyclientRepository;
import com.devfriendsgroup.repository.BankRepository;
import com.devfriendsgroup.repository.ContractRepository;
import com.devfriendsgroup.repository.ContractorsRepository;
import com.devfriendsgroup.repository.NotificationRepository;
import com.devfriendsgroup.repository.PostRepository;
import com.devfriendsgroup.repository.ProjectAllocationRepository;
import com.devfriendsgroup.repository.ProjectPaymentRepository;
import com.devfriendsgroup.repository.ProjectRepository;
import com.devfriendsgroup.repository.RoleRepository;
import com.devfriendsgroup.repository.SkillRepository;
import com.devfriendsgroup.repository.TemplateRepository;
import com.devfriendsgroup.repository.UserInfoRepository;
import com.devfriendsgroup.repository.UserRepository;
import com.devfriendsgroup.repository.UserRequestRepository;
import com.devfriendsgroup.repository.UserRoleRepository;
import com.devfriendsgroup.repository.UserSkillRepository;
import com.devfriendsgroup.util.CodeGenerator;
import com.devfriendsgroup.util.DateUtil;
import com.devfriendsgroup.util.SecurityHelper;

@Service
public class AdminServiceImpl implements AdminService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private UserRoleRepository userRoleRepository;
	
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder; 
	@Autowired
	UserInfoRepository userInfoRepository; 
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	ProjectAllocationRepository allocationRepository;

	@Autowired
	ProjectPaymentRepository projectPaymentRepository;
	@Autowired
	ContractRepository contractRepository;
	@Autowired
	TemplateRepository templateRepository;
	@Autowired
	SkillRepository skillRepository;
	
	@Autowired
	ContractorsRepository contractorsRepository;
	@Autowired
	UserRequestRepository userRequestRepository;
	
	@Autowired
	PostRepository postRepository;
	@Autowired
	UserSkillRepository userSkillRepository;
	
	@Autowired
	ApplyclientRepository applyclientRepository;
	@Autowired
	BankRepository bankRepository;
	@Autowired
	NotificationRepository notificationRepository;
	
	
	
	@Transactional
	@Override
	public Role addOrUpdateRole(Role role) {
		User user = SecurityHelper.getCurrentUser(); 
		Role roleRes =role;
		if(roleRes.getRoleId()!=null)
		{
			 roleRes = roleRepository.findRoleByRoleId(role.getRoleId()); 			
			 roleRes.setRoleDisplayName(role.getRoleDisplayName());
			 roleRes.setUpdatedOn(new Date());  
			 roleRes.setUpdatedBy(userRepository.findByUserId(user.getUserId())); 		
		}
		else
		{
			role.setCreatedOn(new Date()); 
			role.setCreatedBy(userRepository.findByUserId(user.getUserId())); 
			role.setStatus("ACTIVE");		  
		
		}		
		  	
		return roleRepository.save(roleRes); 
	}

	@Override
	public List<Role> findAllRole() {
		return roleRepository.findAll(); 
	}

	@Override
	public Role getRoleById(Long roleIdForEdit) {
		return roleRepository.findById(roleIdForEdit).get();  
	}

	@Override
	public void deleteRole(Long id,String status) {
		Role role=roleRepository.findById(id).get();
		role.setStatus(status);
		roleRepository.save(role); 
	}

	@Override
	public User getUserByUserName(String userName) {
		return userRepository.getUserByUserName(userName);   
	}

	@Override
	public List<Role> findAllActiveRole() {
		return roleRepository.findAllActiveRole(); 
	}
	@Transactional
	@Override
	public UserInfo addUserInfo(UserInfo userInfoInput) {
		System.out.println("userInfoInput=========================="+userInfoInput);
		User currentUser = SecurityHelper.getCurrentUser(); 
		UserInfo userInfoResponse=null;
		User loggedinUser =userRepository.findByUserId(currentUser.getUserId());		
		if(userInfoInput.getId()==null)
		{
			
			System.out.println("getId===================NULL========");
			Integer count=userValidation(userInfoInput);
			if(count>0) {
				return null;
			}
			//SAVE USERINFO
		
			User user = new User();			
			user.setName(userInfoInput.getName());
			user.setUserName(userInfoInput.getUserName());
			user.setPassword(bCryptPasswordEncoder.encode("123456"));  
			user.setMobileNo(userInfoInput.getCountryCode()+"-"+userInfoInput.getMobileNo());
			user.setEmail(userInfoInput.getEmail());
			user.setGender(userInfoInput.getGender()); 
			user.setAddress(userInfoInput.getAddress()); 
			user.setRemark(userInfoInput.getRemark());   
			user.setCreatedOn(new Date()); 
			user.setCreatedBy(loggedinUser);
			user.setStatus("ACTIVE");
			user.setIsblocked(1);	//SET THE USER ACCOUNT BLOCKED 
			user.setUsercode(CodeGenerator.randomCode()+new Date().hashCode());
			User savedUser = userRepository.save(user);  //save user			
			if(savedUser!=null) {
				
			
			UserRole userRole = new UserRole(); 			
			Role role = roleRepository.findRoleByRoleId(userInfoInput.getRoleId()); 
			userRole.setUserId(savedUser); 
			userRole.setRoleId(role); 
			
			
			if(role.getRoleName().equals("CONSULTANCY")) {
				userRole.setAdminId(loggedinUser); 
			}
			if(role.getRoleName().equals("CLIENT") || role.getRoleName().equals("FREELANCER")) {
				userRole.setAdminId(userRoleRepository.getUserRoleByRoleCode("ADMIN").getUserId()); 
				userRole.setAgentId(loggedinUser); 
			}
			userRole.setCreatedOn(new Date()); 
			userRole.setCreatedBy(loggedinUser); 
			userRole.setStatus("ACTIVE"); 
			UserRole userRolel=userRoleRepository.save(userRole); //save user role map			
			if(savedUser!=null && userRolel!=null)
			{
				userInfoInput.setStatus("ACTIVE");
				userInfoInput.setUser(savedUser);
				userInfoInput.setCreatedOn(new Date()); 
				userInfoInput.setCreatedBy(loggedinUser);
				Bank bank=userInfoInput.getBankInfo();
				 if(bank!=null && bank.getId()!=null)
				 {
					 
					 bank.setUpdatedOn(new Date()); 
					 bank.setUpdatedBy(loggedinUser);
				 }
				 else {
					
					 bank.setCreatedOn(new Date()); 
					 bank.setCreatedBy(loggedinUser);
				 }
				 bank.setStatus("ACTIVE");
				 bank=bankRepository.save(bank);
				 userInfoInput.setBankInfo(bank);
				 if(userInfoInput.getContractorId()!=null){
				  userInfoInput.setContractors(contractorsRepository.findById(userInfoInput.
				  getContractorId()).get()); 
				  }
				 
				 
				userInfoResponse=userInfoRepository.save(userInfoInput); //save user info			
							
			}
			}			
		}
		else
		{
			System.out.println("getId=================NOT==NULL========");
			Optional<UserInfo> userInfod=userInfoRepository.findById(userInfoInput.getId());
			if(userInfod.isPresent())
			{
				UserInfo userInfoReq=userInfod.get();
				User userExist = userRepository.findByUserId(userInfoReq.getUser().getUserId());
				userExist.setAddress(userInfoInput.getAddress());	
				userExist.setGender(userInfoInput.getGender());
				userExist.setUpdatedBy(loggedinUser);
				userExist.setUpdatedOn(new Date());
				userExist.setMobileNo(userInfoInput.getCountryCode()+"-"+userInfoInput.getMobileNo());
				userExist=userRepository.save(userExist); //update user				
				userInfoReq.setFbLink(userInfoInput.getFbLink());
				userInfoReq.setInstaLink(userInfoInput.getInstaLink());
				userInfoReq.setLinkdeanLink(userInfoInput.getLinkdeanLink());
				userInfoReq.setWebsiteLink(userInfoInput.getWebsiteLink());
				userInfoReq.setStandardPayment(userInfoInput.getStandardPayment());
				userInfoReq.setOrganizationDetails(userInfoInput.getOrganizationDetails());				
				userInfoReq.setIdProofDocument(userInfoInput.getIdProofDocument());
				userInfoReq.setIdProofName(userInfoInput.getIdProofName());
				userInfoReq.setIdProofNumber(userInfoInput.getIdProofNumber());
				userInfoReq.setUpdatedBy(loggedinUser);
				userInfoReq.setUpdatedOn(new Date());
				userInfoReq.setSupportType(userInfoInput.getSupportType());
				userInfoReq.setSkills(userInfoInput.getSkills());
				userInfoReq.setCountryCode(userInfoInput.getCountryCode());
				userInfoReq.setExperience(userInfoInput.getExperience());
				userInfoReq.setSkillsOther(userInfoInput.getSkillsOther());
				userInfoReq.setProfession(userInfoInput.getProfession());
				  if(userInfoInput.getContractorId()!=null){
				  userInfoReq.setContractors(contractorsRepository.findById(userInfoInput.
				  getContractorId()).get()); 
				  }
				  Bank bank=userInfoInput.getBankInfo();
					 if(bank!=null && bank.getId()!=null)
					 {
						 
						 bank.setUpdatedOn(new Date()); 
						 bank.setUpdatedBy(loggedinUser);
					 }
					 else {
						
						 bank.setCreatedOn(new Date()); 
						 bank.setCreatedBy(loggedinUser);
					 }
					 bank.setStatus("ACTIVE");
					 bank=bankRepository.save(bank);
					 userInfoReq.setBankInfo(bank);
				userInfoResponse=userInfoRepository.save(userInfoReq);//update user info				
				
				
			}
		}
		return userInfoResponse;
	}
	
	public Integer userValidation(UserInfo userInfoInput) {
		User user_unameValid =userRepository.getUserByUserName(userInfoInput.getUserName());
		if(user_unameValid!=null) {
			System.out.println("user_unameValid======================");
			return 1;
		}
		User user_unameMob =userRepository.getUserByMobile(userInfoInput.getMobileNo());
		if(user_unameMob!=null) {
			System.out.println("user_unameMob======================");
			return 1;
		}
		Role role = roleRepository.findRoleByRoleId(userInfoInput.getRoleId()); 
		if(role!=null && "ADMIN".equals(role.getRoleName())) {
			System.out.println("role======================");
			return 1;
		}
		return 0;
	}
	
	/*
	 * public void userSkillsUpdate(UserInfo userInfo,User user) {
	 * System.out.println("=============UserSkills==== START=====================");
	 * System.out.println("userInfo================="+user.getUserId());
	 * List<UserSkills>
	 * userSkills=userSkillRepository.findUserSkillsByUserId(user.getUserId());
	 * System.out.println("userSkills================="+userSkills.size()); for
	 * (UserSkills uskill : userSkills) {
	 * System.out.println("userInfo================="+uskill.getSkill().getSkillName
	 * ()); userSkillRepository.delete(uskill); }
	 * 
	 * for (Long skilId : userInfo.getSkillsArr()) {
	 * System.out.println("p==getSkillsArr==============="+skilId);
	 * userSkillRepository.save(new
	 * UserSkills(user,skillRepository.findById(skilId).get())); }
	 * System.out.println("=============UserSkills==== END====================="); }
	 */
	
	
	@Override
	public List<User> findAllUser() {
		return userRepository.findAll();  
	}

	@Override
	public User getUserById(Long userIdForEnableUser) {
		return userRepository.getOne(userIdForEnableUser); 
	}

	@Override
	public User enableUser(Long userId) {
	
		User user=userRepository.findByUserId(userId);
		if(user!=null) {
		user.setIsblocked(0);
		user.setStatus("ACTIVE");
		userRepository.save(user);  
	
		
		UserInfo userInfo=userInfoRepository.findUserInfoByUserId(user.getUserId());
		if(userInfo!=null) {
			userInfo.setStatus("ACTIVE");
			userInfo.setUpdatedOn(new Date());
			userInfoRepository.save(userInfo);
			
			UserRole userRole = userRoleRepository.getUserRoleByUserId(user.getUserId());  
			userRole.setStatus("ACTIVE"); 
			userRoleRepository.save(userRole); 
			return user;
		}
		
		}
		return null;
		
	}
	@Override
	public User disableUser(Long userId) {
		
		User user=userRepository.findByUserId(userId);
		if(user!=null) {
		user.setIsblocked(1);
		user.setStatus("INACTIVE");
		userRepository.save(user);  
	
		
		UserInfo userInfo=userInfoRepository.findUserInfoByUserId(user.getUserId());
		if(userInfo!=null) {
			userInfo.setStatus("INACTIVE");
			userInfo.setUpdatedOn(new Date());
			userInfoRepository.save(userInfo);
			
			UserRole userRole = userRoleRepository.getUserRoleByUserId(user.getUserId());  
			userRole.setStatus("INACTIVE"); 
			userRoleRepository.save(userRole); 
			return user;
		}
		}
		return null;
	}
	

	@Override
	public Role findActiveRoleByRoleCode(String roleCode) {
		return roleRepository.findActiveRoleByRoleCode(roleCode); 
	} 
	
	@Override
	public List<UserInfo> getAllAdminUserBySuperAdmin() {
		List<BigInteger> userIds = userRoleRepository.getAllAdminUserBySuperAdmin(); 
		List<UserInfo> userList = new ArrayList<UserInfo>(); 
		if(userIds != null && userIds.size() > 0) {
			for(BigInteger currentUserId:userIds) {
				Optional<UserInfo> userInfo=userInfoRepository.findById(currentUserId.longValue());
				if(userInfo.isPresent()) {
					userList.add(userInfo.get());  
				}
				
			}
		}
		return userList; 
	}
	@Override
	public List<UserInfo> getAllUsersByUserIdAndRole(Long userId,String rolename) {		
		return userInfoRepository.getAllUsersByUserIdAndRole(userId,rolename); 
	
	}

	@Override
	public List<UserInfo> getAllClientUserByClientAgent(Long userId) {
		User createdUser = userRepository.findByUserId(userId); 
		UserRole createdUserDetails = userRoleRepository.getUserRoleByUserId(createdUser.getUserId()); 
		Long adminUserId = createdUserDetails.getAdminId().getUserId(); 
		List<BigInteger> userIds = userRoleRepository.getAllClientUserByClientAgent(userId, adminUserId); 
		List<UserInfo> userList = new ArrayList<UserInfo>(); 
		if(userIds != null && userIds.size() > 0) {
			
			Optional<UserInfo> userInfo=userInfoRepository.findById(userId.longValue());
			if(userInfo.isPresent()) {
				userList.add(userInfo.get());  
			}
		}
		return userList; 
	}
	
	@Override
	public List<UserInfo> getAllUsers(Long userId,String rtype,String status,String skill,String isActive) {
		
		String rolename="";
		if(rtype!=null & !rtype.equals("") & "c".equals(rtype)) {
			rolename="CLIENT";
		}else if(rtype!=null & !rtype.equals("") & "f".equals(rtype)) {
			rolename="FREELANCER";;
		}
		System.out.println("isActive:  "+isActive);
		if(isActive!=null && !"".equals(isActive)) {
			status=isActive;
		}
		System.out.println("status:  "+status);
		if(!"".equals(status))
		{
			if(skill!=null && !"".equals(skill))
			{
				System.out.println("getAllUsersByUserIdAndRole====1======"+skill);
				return userInfoRepository.getAllUsersByUserIdStatusAndRole(userId, rolename,status,skill); 
			}
			else {
				System.out.println("getAllUsersByUserIdAndRole===2======="+status);
			return userInfoRepository.getAllUsersByUserIdAndRole(userId, rolename,status); 
			}
		}
		else {
			if(skill!=null && !"".equals(skill))
			{
				System.out.println("getAllUsersByUserIdAndRole====3======"+skill);
				return userInfoRepository.getAllUsersByUserIdRoleAndSkill(userId, rolename,skill); 
			}
			else {
				System.out.println("getAllUsersByUserIdAndRole====4=====");
				return userInfoRepository.getAllUsersByUserIdAndRole(userId, rolename); 
			}
		
		}
		 
	}

	@Override
	public UserInfo findUserInfoById(Long id_edit) {
		Optional<UserInfo> userinfo=userInfoRepository.findById(id_edit);
		return userinfo.get();
	}


	
	
		@Override
		public List<UserRole> findAllUserRoleByUserAndRole(Long userId, String rolename) {
			return userRoleRepository.findAllUserRoleByUserAndRole(userId, rolename);
		}

		@Override
		public Skill addOrUpdateSkill(Skill skill) {
			User user = SecurityHelper.getCurrentUser(); 
			System.out.println("user====="+user.getUserId());
			//User cuser=userRepository.getOne(user.getUserId());
			//System.out.println("cuser====="+cuser);
			if(skill.getSkillId()!=null)
			{
				Skill skill_ = skillRepository.findById(skill.getSkillId()).get(); 			
				skill.setStatus(skill_.getStatus());
				skill.setUpdatedOn(new Date());  
				skill.setUpdatedBy(null); 		
			}
			else
			{
				skill.setCreatedOn(new Date()); 
				skill.setCreatedBy(null); 
				skill.setStatus("ACTIVE");		  
			
			}		
			  	
			return skillRepository.save(skill); 
		}

		@Override
		public List<Skill> findAllSkills() {
			// TODO Auto-generated method stub
			return skillRepository.findAll();
		}

		@Override
		public void deleteSkill(Long id_del,String status) {
			Skill skill=skillRepository.findById(id_del).get();
			skill.setStatus(status);
			skillRepository.save(skill);			
		}

		@Override
		public Skill getSkillById(Long id_edit) {
			return skillRepository.findById(id_edit).get();
		}
		//@Cacheable("dashboardcache")
		public DashBoard loadDashBoardData(Long userId) {
			DashBoard dashBoard=new DashBoard();
			//HashMap<K, V> map=new HashMap<>();
			//List<UserInfo> userList=userInfoRepository.totalUserByUserId(userId);
			//totalUserByUserId
			Long totalProject=projectRepository.totalActiveProject(userId);
			Long totalTotalActiveProjectAllocatio=allocationRepository.totalActiveProjectAllocation(userId);
			List<UserRole> userRoleList=userRoleRepository.findAllUserRoleByCreatedBy(userId);
			List<UserRole> userRoleList_=userRoleList.stream().filter(p->p.getStatus().equals("ACTIVE")).collect(Collectors.toList());
			dashBoard.setTotalUser(userRoleList_.stream().count());
			dashBoard.setTotalFreelancerUser(userRoleList_.stream().filter(p->p.getRoleId().getRoleName().equals("FREELANCER")).count());
			dashBoard.setTotalClientUser(userRoleList_.stream().filter(p->p.getRoleId().getRoleName().equals("CLIENT")).count());
			
			dashBoard.setTotalProject(totalProject);			
			dashBoard.setTotalActiveProjectAllocation(totalTotalActiveProjectAllocatio);
			dashBoard.setTotalContractor(contractorsRepository.totalActiveContractor(userId,"ACTIVE"));
			dashBoard.setTotalContract(contractRepository.totalActiveContracts(userId,"ACTIVE"));
			dashBoard.setTotalClosedProject(totalProject-totalTotalActiveProjectAllocatio);
			dashBoard.setNotifications(notificationRepository.findAllNotification());
			return dashBoard;
		}
		public DashBoard loadDashBoardDataOrg(Long userId) {
			DashBoard dashBoard=new DashBoard();
			//HashMap<K, V> map=new HashMap<>();
			//List<UserInfo> userList=userInfoRepository.totalUserByUserId(userId);
			//totalUserByUserId
			Long totalProject=projectRepository.totalActiveProject(userId);
			Long totalTotalActiveProjectAllocatio=allocationRepository.totalActiveProjectAllocation(userId);
			List<UserRole> userRoleList=userRoleRepository.findAllUserRoleByCreatedBy(userId);
			List<UserRole> userRoleList_=userRoleList.stream().filter(p->p.getStatus().equals("ACTIVE")).collect(Collectors.toList());
			dashBoard.setTotalUser(userRoleList_.stream().count());
			dashBoard.setTotalFreelancerUser(userRoleList_.stream().filter(p->p.getRoleId().getRoleName().equals("FREELANCER")).count());
			dashBoard.setTotalClientUser(userRoleList_.stream().filter(p->p.getRoleId().getRoleName().equals("CLIENT")).count());
			
			dashBoard.setTotalProject(totalProject);			
			dashBoard.setTotalActiveProjectAllocation(totalTotalActiveProjectAllocatio);
			dashBoard.setTotalContractor(contractorsRepository.totalActiveContractor(userId,"ACTIVE"));
			dashBoard.setTotalContract(contractRepository.totalActiveContracts(userId,"ACTIVE"));
			dashBoard.setTotalClosedProject(totalProject-totalTotalActiveProjectAllocatio);
			System.out.println("dashBoard=================="+dashBoard);
			return dashBoard;
		}
		@Override
		public Contractors addOrUpdateContractors(Contractors contractors) {
			User user = SecurityHelper.getCurrentUser(); 		
			contractors.setMobile(contractors.getCountryCode()+"-"+contractors.getMobile());
			
			if(contractors.getContractorId()!=null)
			{
				Contractors con = contractorsRepository.findById(contractors.getContractorId()).get(); 			
				contractors.setStatus(con.getStatus());
				contractors.setCreatedBy(con.getCreatedBy());
				contractors.setUpdatedOn(new Date());  
				contractors.setUpdatedBy(user); 
				contractors.setCreatedOn(con.getCreatedOn());
				
				
			}
			else
			{
				contractors.setCreatedOn(new Date()); 
				contractors.setCreatedBy(user); 
				contractors.setStatus("ACTIVE");		  
			
			}		
			  	
			
			return contractorsRepository.save(contractors);
		}

		@Override
		public List<Contractors> findAllByUserId(Long createdId,String status) {			
			return contractorsRepository.findAllByUserIdAndStatus(createdId,status);
		}

		@Override
		public boolean deleteContractor(Long id_del,String status) {
			
			List<UserInfo> userInfoList=userInfoRepository.findAllUsersByContractorsId(id_del);
			if(userInfoList!=null && userInfoList.size()>0) {
				return false;
			}
			Contractors con = contractorsRepository.findById(id_del).get(); 	
			con.setStatus(status);
			con.setUpdatedOn(new Date());
			contractorsRepository.save(con);	
			return true;
		}

		@Override
		public Contractors findContractorsById(Long id) {			
			return contractorsRepository.findById(id).get(); 	
		}

		@Override
		public List<Contractors> findAllByUserIdAndStatus(Long createdId, String status) {			
			return contractorsRepository.findAllByUserIdAndStatus(createdId,status);
		}

		@Override
		public List<UserRequest> findAllUserRequest() {			
			return userRequestRepository.findAllUserRequest();
		}

		@Override
		public UserRequest saveUserRequest(UserRequest userRequest) {
			userRequest.setCreatedOn(new Date());
			userRequest.setStatus("ACTIVE");
			return userRequestRepository.save(userRequest);
		}

		@Override
		public List<Post> findAllPublishedPost(String skill) {	
			
			
					if("latest".equals(skill)) {
						return postRepository.findAllPublishedPost();
					}
			
			return postRepository.findAllPublishedPost(skill);
		}
		@Override
		public List<Post> findAllPost() {			
			return postRepository.findAll();
		}

		@Override
		public Post addOrUpdatePost(Post post) {
			User user = SecurityHelper.getCurrentUser(); 
			User loggedinUser =userRepository.findByUserId(user.getUserId());
			post.setPosteddate(DateUtil.formatDDMMYYYY(post.getPosteddateStr()));			
			if(post.getId()!=null)
			{
				Post post_ = postRepository.findById(post.getId()).get(); 			
				post.setStatus(post_.getStatus());
				post.setUpdatedOn(new Date());  
				post.setUpdatedBy(loggedinUser); 		
			}
			else
			{
				post.setCreatedOn(new Date()); 
				post.setCreatedBy(loggedinUser); 
				post.setStatus("ACTIVE");		  
			
			}		
			return postRepository.save(post); 
		}

		@Override
		public void deletePost(Long id_del, String status) {
			Post con = postRepository.findById(id_del).get(); 	
			con.setStatus(status);
			con.setUpdatedOn(new Date());
			postRepository.save(con);
		}

		@Override
		public Post findPostById(Long id_edit) {
			return postRepository.findById(id_edit).get(); 	
		}

		@Override
		public void publishPost(Long publish_id, boolean publish_flag) {
			Post con = postRepository.findById(publish_id).get(); 	
			con.setPosted(publish_flag);
			con.setUpdatedOn(new Date());
			postRepository.save(con);
		}

		@Override
		public List<Skill> findAllActiveSkills() {
			// TODO Auto-generated method stub
			return skillRepository.findAllActiveSkill();
		}

		@Override
		public String validateUser(String data) {
			System.out.println("data===="+data);
			String code=null;
			User  userUname= userRepository.getUserByUserName(data);
			if(userUname!=null) {
				code= "UserNameExist";
			}
			User  userMob= userRepository.getUserByMobile("+"+data.trim());
			if(userMob!=null) {
				code= "ContactExist";
			}
			System.out.println("code===="+code);
			return code;
		}

		@Override
		public Applyclient addOrUpdateApplyclient(Applyclient applyclient) {
			User user = SecurityHelper.getCurrentUser(); 		
			applyclient.setStatus(applyclient.getStatus());		
			if(applyclient.getFreelancerId()!=null)
			{
				applyclient.setFreelancer(userRepository.findByUserId(applyclient.getFreelancerId()));
			}
			if(applyclient.getId()!=null)
			{
				Applyclient con = applyclientRepository.findById(applyclient.getId()).get(); 		
				
				applyclient.setCreatedBy(con.getCreatedBy());
				applyclient.setUpdatedOn(new Date());  
				applyclient.setUpdatedBy(user); 
				applyclient.setCreatedOn(con.getCreatedOn());
				applyclient.setApplycode(con.getApplycode());
				
				
			}
			else
			{
				applyclient.setCreatedOn(new Date()); 
				applyclient.setCreatedBy(user); 
				applyclient.setApplycode(CodeGenerator.randomCode());
			
			}		
			  	
			
			return applyclientRepository.save(applyclient);
		}

		@Override
		public List<Applyclient> findAllApplyclientByUserId(Long createdId) {
			return applyclientRepository.findAllApplyclientByUserId(createdId);
		}

		

		@Override
		public Applyclient findApplyclientById(Long id_edit) {
			return applyclientRepository.findById(id_edit).get(); 
		}

		@Override
		public List<Applyclient> findAllApplyclientByUserIdAndDemoStatus(Long userId, String demostatus) {
			if("ALL".equals(demostatus)) {
				return applyclientRepository.findAllApplyclientByUserId(userId);
			}
			return applyclientRepository.findAllApplyclientByUserIdAndDemoStatus(userId,demostatus);
		}

		@Override
		public List<Bank> findAllBankByStatus(String status) {
			return bankRepository.findAllBankByStatus(status);
		}

		@Override
		public Bank findBankByIdAndStatus(Long id, String status) {
			return bankRepository.findBankByIdAndStatus(id, status);
		}

		@Override
		public List<Notification> findAllNotification() {
			
			List<Notification>  list=notificationRepository.findAllNotification();
			list.stream()
		    .map(p -> {
		        // Create a SimpleDateFormat instance with the desired format
		        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM, yyyy");
		        
		        // Format the createdOn date
		        String formattedDate = formatter.format(p.getCreatedOn());
		        
		        // Set the formatted date into createdBy (or modify the desired field)
		        p.setCreatedBy(formattedDate);
		        
		        // Return the updated object
		        return p;
		    })
		    .collect(Collectors.toList());
			
			return list;
		}

		

		
} 
	