package com.devfriendsgroup.service;

import java.util.List;

import com.devfriendsgroup.model.User;
import com.devfriendsgroup.model.UserDTO;
import com.devfriendsgroup.model.UserInfo;
import com.devfriendsgroup.model.UserRole;

public interface UserService {

	User getUserForLogin(String userName, String password);

	UserRole getUserRoleByUserId(Long userId);

	User changePassword(Long userId, String oldPassword, String newPassword);
	public User changePwd(Long userId, String pwd);

	public UserInfo editProfile(UserInfo userInfo);
	List<UserRole> findAllUserRoleByRole(String roleName);

	User saveOtpForForgotPassword(User existingUser, String otp);

	User getUserById(Long userId);

	User matchOtp(String otp);

	User changePasswordForForgotCasePost(User existingUser, String password);

	User registerUser(String name, String userName, String password, String mobileNumber, String emailId, String remark, String address, String userType, String gender);

	User getUserByUserNameOrMobileNumber(String unameOrmob);

	UserRole getUserRoleByUserName(String name);         
	UserRole getUserRoleById(Long id);

	UserDTO getUserDTOById(Long userId);

	UserInfo geMyProfile(Long userId); 
}
	