package com.devfriendsgroup.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.devfriendsgroup.model.LoggedInUser;
import com.devfriendsgroup.model.User;
import com.devfriendsgroup.model.UserRole;
import com.devfriendsgroup.repository.UserRepository;
import com.devfriendsgroup.repository.UserRoleRepository;
import com.devfriendsgroup.util.MasterData;
import com.devfriendsgroup.util.SecurityHelper;
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private UserRoleRepository userRoleRepository;
	
	@Value("${spring.profiles.active}")
	public String dbname;
	@Value("${app.version}")
	public String appVersion;

	 private static final Logger logger = LoggerFactory.getLogger(UserDetailsServiceImpl.class);
	public UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException {

		User user = userRepository.getUserByUserName(userId); 
		
		if(user!=null && !user.getStatus().equals(MasterData.STATUS_ACTIVE)) {
			return null;
		}
		Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
		UserRole userRole = userRoleRepository.getUserRoleByUserId(user.getUserId()); 
		String roleType = userRole.getRoleId().getRoleName(); 
		grantedAuthorities.add(new SimpleGrantedAuthority(roleType)); 
		boolean mybool = (user.getIsblocked() == 0); 

		LoggedInUser liu = new LoggedInUser(user.getUserName(), user.getPassword(), mybool, mybool, mybool, mybool,
				grantedAuthorities, user);

		return liu;

	}
	public String portalRedirectUrl(HttpSession httpSession, String switchuser) {	
		logger.info("----portalRedirectUrl-------switchuser-----BY--"+switchuser);
		
		String url="";
		String roleCode="";
		User user = SecurityHelper.getCurrentUser(); 
		if(switchuser!=null && !"".equals(switchuser)) {
			User userDb = userRepository.getUserByUserName(switchuser);
			user=SecurityHelper.setCurrentUser(userDb);		
			httpSession.setAttribute("switchuser",switchuser);
			logger.info("----portalRedirectUrl-------autologin-----SUCCESS-----BY--"+switchuser);
			
		}
		
		UserRole userRole = userRoleRepository.getUserRoleByUserId(user.getUserId());  	
		
		roleCode=userRole.getRoleId().getRoleName();
		if (roleCode != null && (roleCode.equals("ADMIN") || roleCode.equals("CONSULTANCY"))) {
			httpSession.setAttribute("userName", userRole.getUserId().getUserName());	
			httpSession.setAttribute("roleName", userRole.getRoleId().getRoleDisplayName());	
			httpSession.setAttribute("roleCode", userRole.getRoleId().getRoleName());
			httpSession.setAttribute("gender", userRole.getUserId().getGender());	
			httpSession.setAttribute("userSession", user);	
			httpSession.setAttribute("name", user.getName());	
			httpSession.setAttribute("dbname",dbname);		
			httpSession.setAttribute("appVersion",appVersion);	
			httpSession.setAttribute("appVersion",appVersion);	
			
			List<String> usernameList= userRoleRepository.findAllUserRoleByRole().stream().map(p->p.getUserId().getUserName()).distinct().collect(Collectors.toList());
			httpSession.setAttribute("usernameList",usernameList);	
			logger.info("-------------------User Info Hold in session--------STORE SOME DATA--------");
			url= "redirect:/home"; 
		}
		else {
			url= "redirect:/access-denied";
		}
		return url;   

	} 

}
