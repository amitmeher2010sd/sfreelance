package com.devfriendsgroup.service;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.devfriendsgroup.model.ProjectPayment;
import com.devfriendsgroup.model.Contract;
import com.devfriendsgroup.model.PaymentCalenderResponse;
import com.devfriendsgroup.model.Project;
import com.devfriendsgroup.model.ProjectAllocation;
import com.devfriendsgroup.model.Role;
import com.devfriendsgroup.model.Template;
import com.devfriendsgroup.model.User;
import com.devfriendsgroup.model.UserInfo;
import com.devfriendsgroup.model.UserRole;
import com.devfriendsgroup.repository.ContractRepository;
import com.devfriendsgroup.repository.ProjectAllocationRepository;
import com.devfriendsgroup.repository.ProjectPaymentRepository;
import com.devfriendsgroup.repository.ProjectRepository;
import com.devfriendsgroup.repository.RoleRepository;
import com.devfriendsgroup.repository.TemplateRepository;
import com.devfriendsgroup.repository.UserInfoRepository;
import com.devfriendsgroup.repository.UserRepository;
import com.devfriendsgroup.repository.UserRoleRepository;
import com.devfriendsgroup.util.CodeGenerator;
import com.devfriendsgroup.util.DateUtil;
import com.devfriendsgroup.util.SecurityHelper;



@Service
public class ProjectServiceImpl implements ProjectService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private UserRoleRepository userRoleRepository;
	
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder; 
	@Autowired
	UserInfoRepository userInfoRepository; 
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	ProjectAllocationRepository projectAllocationRepository;

	@Autowired
	ProjectPaymentRepository projectPaymentRepository;
	@Autowired
	ContractRepository contractRepository;
	@Autowired
	TemplateRepository templateRepository;
	
	@Value("${patment_url}")
	String paymentUrl;
	

	@Override
	public Project addOrUpdateProject(Project project) {
		User currentUser = SecurityHelper.getCurrentUser(); 
		User loggedinUser =userRepository.findByUserId(currentUser.getUserId());
		project.setStartDate(DateUtil.formatDDMMYYYY(project.getStartDateStr()));
		project.setEndDate(DateUtil.formatDDMMYYYY(project.getEndDateStr()));
		if(project.getId()==null)
		{
			//SAVE 			   
			project.setCreatedOn(new Date()); 
			project.setCreatedBy(loggedinUser);
			project.setStatus("ACTIVE");
			project.setProjectStatus("PENDING");
			project.setProjectCodeInternal(CodeGenerator.randomCode());
			project.setUserRoleAgent(userRoleRepository.getUserRoleByUserId(currentUser.getUserId()));
		}
		else
		{
			
			Optional<Project> projectOp=projectRepository.findById(project.getId());
			if(projectOp.isPresent())
			{
				//UPDATE
				Project projectin=projectOp.get();
				project.setUpdatedBy(loggedinUser);
				project.setUpdatedOn(new Date());
				project.setCreatedBy(projectin.getCreatedBy());
				project.setCreatedOn(projectin.getCreatedOn());
				project.setProjectCode(projectin.getProjectCode());
				project.setProjectStatus(projectin.getProjectStatus());
				project.setStatus(projectin.getStatus());
				project.setUserRoleAgent(projectin.getUserRoleAgent());
				project.setProjectCodeInternal(projectin.getProjectCodeInternal());
				project.setProjectAllocation(projectin.getProjectAllocation());
				project.setUserClient(projectin.getUserClient());
			}
		}
		return projectRepository.save(project);
	}

	@Override
	public List<Project> findAllProjectByUserRoleId(Long userRoleId,String projectStatus) {
		List<Project> projects=new ArrayList<>();
		if("INPROGRESS".equals(projectStatus)) {
			//projects.addAll(projectRepository.findAllProjectByUserRoleId(userRoleId,"HOLD"));
		}
		
		projects.addAll( projectRepository.findAllProjectByUserRoleId(userRoleId,projectStatus));
		return projects;
	}

	@Override
	public Project getProjectById(Long projectId) {
		return projectRepository.findProjectById(projectId);
	}

	@Override
	public boolean deleteProject(Long projectId) {
		
		List<ProjectAllocation> projectAllocationList=projectAllocationRepository.findAllProjectAllocationByProjectId(projectId);
		if(projectAllocationList!=null && projectAllocationList.size()>0) {
			return false;
		}
		Project project=projectRepository.findProjectById(projectId);
		if(project!=null) {
			if("INACTIVE".equals(project.getStatus())){
				project.setStatus("ACTIVE");
			}
			else {
				project.setStatus("INACTIVE");
			}
			
			projectRepository.save(project); 
		}	
		
		return true;
		
	}

	@Override
	public Project findProject(Long id_edit) {
		return projectRepository.findById(id_edit).get();
	}

	@Override
	public Project changeProjectStatus(Long pid, String status) {
		Project project=projectRepository.findProjectById(pid);
		if(project!=null) {
			//allocation close if pending
			if(project.getProjectAllocation()!=null) {
				
				//for all allocation
				if(status.equals("COMPLETED") || status.equals("CANCELLED")) {
					List<ProjectAllocation> prevAllocationList=projectAllocationRepository.findAllProjectAllocationByProjectId(project.getId());
					for (ProjectAllocation allocation : prevAllocationList) {
								if(allocation.getProjectStatus().equals("PENDING") || allocation.getProjectStatus().equals("INPROGRESS"))
								{
									allocation.setReviseEndDate(new Date());	
									allocation.setProjectStatus(status);
									projectAllocationRepository.save(allocation);
								}
						
						
					}
					
				}else
				{
					//for current allocation
					ProjectAllocation allocation1= projectAllocationRepository.findProjectAllocationById(project.getProjectAllocation().getId());
					if(allocation1!=null) {
					allocation1.setReviseEndDate(allocation1.getEndDate());
					allocation1.setProjectStatus(status);
					allocation1=projectAllocationRepository.save(allocation1);
					}
				
				}				
				}
			project.setProjectStatus(status);
			project= projectRepository.save(project); 	
			return project;
		}	
		return null;
	}
	
	
	public void addOrUpdateProjectPayment(ProjectAllocation allocation,Boolean isExecutePayment,String pstatus) {
		System.out.println("allocation.getId()=========="+allocation.getId());
		if(isExecutePayment==true) {
		
		String paymentFrequency=allocation.getPaymentFrequencyCredit();
		Integer freNo=0; 
		Integer days=0; 
		if(paymentFrequency.equals("Weekly")) {
			freNo=4;
			days=7; 
		}else if(paymentFrequency.equals("Bi-Weekly")) {
			freNo=2;
			days=15; 
		}
		else if(paymentFrequency.equals("Monthly")) {
			freNo=1;
			days=30; 
		}
	
		Date nextPaymentDt=allocation.getStartDate();
		
        for (int i = 0; i < freNo; i++) {			
			ProjectPayment projectPayment=new ProjectPayment();
			projectPayment.setStartDate(nextPaymentDt);
		System.out.println("freNo==="+i+"============"+freNo);
		Double installmentAmt=allocation.getFreelancerCost()/freNo;
		Calendar cal = Calendar.getInstance();
		cal.setTime(nextPaymentDt);		
		cal.add(Calendar.DATE, days); 
		nextPaymentDt = cal.getTime();
		
		projectPayment.setEndDate(nextPaymentDt);
		
		System.out.println("installmentAmt==="+i+"============="+installmentAmt);
		
		User currentUser = SecurityHelper.getCurrentUser();
			projectPayment.setCreatedOn(new Date()); 
			projectPayment.setCreatedBy(userRepository.findByUserId(currentUser.getUserId()));
			projectPayment.setStatus("ACTIVE");
			projectPayment.setProjectAllocation(allocation);
			projectPayment.setPaymentAmount(installmentAmt);			
			
			projectPayment.setRemarks("AUTO PAYMENT GENERATED ON DATE: "+new Date());
			projectPayment.setPaymentDate(nextPaymentDt);
			if("CLOSED".equals(pstatus)) {
				projectPayment.setPaymentStatus("CANCELLED");
			}{
			projectPayment.setPaymentStatus("PENDING");
			}
			System.out.println("payDate==="+i+"============"+nextPaymentDt);
		    projectPaymentRepository.save(projectPayment);
		    
		}        
        
        
        List<ProjectPayment> projectPaymentList=projectPaymentRepository.findAllProjectPaymentsByProjectIdAndStatus(allocation.getProject().getId(),"CLOSED");
        for (ProjectPayment pobj : projectPaymentList) {
			if(!pobj.equals("PAID")) {
				pobj.setPaymentStatus("CANCELLED");
				pobj.setRemarks("PROJECT ALLOCATION CHANGED ON DATE: "+new Date());
				projectPaymentRepository.save(pobj);
			}
		}
        
		}
	}

	//======================project allocation========================
		@Override
		public ProjectAllocation addOrUpdateProjectAllocation(ProjectAllocation projectAllocation) {
			User currentUser = SecurityHelper.getCurrentUser(); 
			User loggedinUser =userRepository.findByUserId(currentUser.getUserId());
			projectAllocation.setStartDate(DateUtil.formatDDMMYYYY(projectAllocation.getStartDateStr()));
			projectAllocation.setEndDate(DateUtil.formatDDMMYYYY(projectAllocation.getEndDateStr()));
			String pstatus="PENDING";
			if(projectAllocation.getId()==null)
			{
				//SAVE 			   
				projectAllocation.setCreatedOn(new Date()); 
				projectAllocation.setCreatedBy(loggedinUser);
				projectAllocation.setStatus("ACTIVE");
				projectAllocation.setProjectStatus("PENDING");
				projectAllocation.setProject(projectRepository.findProjectById(projectAllocation.getProjectId()));
				//projectAllocation.setUserRoleFreelancer(projectAllocation.get);
				
			}
			else
			{
				pstatus="INPROGRESS";
				Optional<ProjectAllocation> projectAllocationOp=projectAllocationRepository.findById(projectAllocation.getId());
				if(projectAllocationOp.isPresent())
				{
					//UPDATE
					ProjectAllocation projectin=projectAllocationOp.get();
					projectAllocation.setUpdatedBy(loggedinUser);
					projectAllocation.setUpdatedOn(new Date());
					projectAllocation.setCreatedBy(projectin.getCreatedBy());
					projectAllocation.setCreatedOn(projectin.getCreatedOn());
					projectAllocation.setProject(projectin.getProject());
					projectAllocation.setProjectStatus(projectin.getProjectStatus());
					projectAllocation.setStatus(projectin.getStatus());
					projectAllocation.setPaymentFrequencyDebit(projectin.getPaymentFrequencyDebit());
					projectAllocation.setPaymentFrequencyCredit(projectin.getPaymentFrequencyCredit());
					
				}
			}
			List<ProjectAllocation> allocationList= projectAllocationRepository.findAllProjectAllocationByActiveAndProjectId(projectAllocation.getProjectId());
			
			String prestatus="";
			
			ProjectAllocation allocation= projectAllocationRepository.save(projectAllocation);//NEW ALLOCATION
			if(allocation!=null)
			{
				System.out.println("<<<<<<<New Project allocation done");
				
				Project project=projectRepository.findProjectById(allocation.getProject().getId());
				if(project!=null) {				
					
					
					if(allocationList!=null && allocationList.size()>0) {
						allocationList.get(0).setReviseEndDate(new Date());
						prestatus="CLOSED";
						allocationList.get(0).setProjectStatus(prestatus);
					    projectAllocationRepository.save(allocationList.get(0));
					   System.out.println("<<<<<<<Previous Project allocation end date update");
					   pstatus="INPROGRESS";
					}
					
					project.setProjectAllocation(allocation);
					
					project.setProjectStatus(pstatus);
					projectRepository.save(project); 
					System.out.println("<<<<<<<Project allocation details update");
					//Update Payment 
					/*
					 * if(allocation.getProjectStatus().equals("INPROGRESS") &&
					 * project.getProjectStatus().equals("INPROGRESS")) {
					 * addOrUpdateProjectPayment(allocation,true,prestatus); }
					 */
					allocation.setProjectStatus(pstatus); //set inprogress for new allocation
					projectAllocationRepository.save(allocation);
					System.out.println("<<<<<<<ProjectPayment details update");
					}
					
					
				}	
			
			return allocation;
		}

		@Override
		public List<ProjectAllocation> findAllProjectAllocationByProjectId(Long projectId) {		
			return projectAllocationRepository.findAllProjectAllocationByProjectId(projectId);
		}

		@Override
		public ProjectAllocation getProjectAllocationById(Long projectId) {
			return projectAllocationRepository.findProjectAllocationById(projectId);
		}

		@Override
		public void deleteProjectAllocation(Long projectId) {
			Project project=projectRepository.findProjectById(projectId);
			if(project!=null) {
				project.setStatus("INACTIVE");
				projectRepository.save(project); 
			}	
			
		}

		@Override
		public ProjectAllocation findProjectAllocation(Long id_edit) {
			return projectAllocationRepository.findById(id_edit).get();
		}

		@Override
		public ProjectAllocation changeProjectAllocationStatus(Long pid, String status) {
			ProjectAllocation projectAllocation=projectAllocationRepository.findProjectAllocationById(pid);
			if(projectAllocation!=null) {
				projectAllocation.setProjectStatus(status);
				return projectAllocationRepository.save(projectAllocation); 
			}	
			return null;
		}

		@Override
		public List<ProjectAllocation> findAllProjectAllocationsByProjectCode(String projectcodeInternal) {
			return projectAllocationRepository.findAllProjectAllocationsByProjectCode(projectcodeInternal);
		}

		@Override
		public List<Project> findAllProjectByUserRoleId(Long userRoleId) {
			return projectRepository.findAllProjectByUserRoleId(userRoleId);
		}

		@Override
		public List<ProjectAllocation> findAllProjectAllocationByCreatedBy(Long userId,String projectAllocationStatus) {
			return projectAllocationRepository.findAllProjectAllocationByCreatedBy(userId,projectAllocationStatus);
		}

		@Override
		public Project findByProjectCodeInternal(String projectcode) {
			System.out.println("projectcode==========="+projectcode);
			return projectRepository.findByProjectCodeInternal(projectcode);
		}

		@Override
		public List<ProjectAllocation> findAllProjectAllocationByCreatedByAndAllocationStatusAndStatus(Long userId,
				String projectAllocationStatus, String status) {
			return projectAllocationRepository.findAllProjectAllocationByCreatedByAndAllocationStatusAndStatus(userId,projectAllocationStatus,status);
			
		}
		//SHOW PAYMENT FOR 1 MONTH
		@Override
		public List<PaymentCalenderResponse> getPaymentsCalenders(Long UserId) {
			
			//Map<Long,List<Date>> paymentCalMap=new HashMap<Long,List<Date>>();
			List<PaymentCalenderResponse> payResList=new ArrayList<PaymentCalenderResponse>();
			List<ProjectAllocation> projAllList=projectAllocationRepository.findAllProjectAllocationByCreatedByAndAllocationStatusAndStatus(UserId,"INPROGRESS","ACTIVE");
			//Calendar currentCal = Calendar.getInstance();
			
			for (ProjectAllocation allocation : projAllList) {
				
				//if(currentCal.getTime().getMonth()<=allocation.getStartDate().getMonth()) {
				
				//System.out.println("currentCal.getTime().getMonth()-------allocation-------"+currentCal.getTime().getMonth());
				//System.out.println("currentCal.getTime().getMonth()------allocation--------"+allocation.getStartDate().getMonth());
			//System.out.println("projectAllocation==PayFrequency: "+allocation.getPaymentFrequency()+"======Start:==="+allocation.getStartDate()+"=End:==="+allocation.getEndDate());
				
			String paymentFrequency=allocation.getPaymentFrequencyCredit();
			Integer freNo=0; 
			Integer days=0; 
			if(paymentFrequency.equals("Weekly")) {
				freNo=4;
				days=7; 
			}else if(paymentFrequency.equals("Bi-Weekly")) {
				freNo=2;
				days=15; 
			}
			else if(paymentFrequency.equals("Monthly")) {
				freNo=1;
				days=30; 
			}
			
			
			
			
			//long dateBeforeInMs = allocation.getStartDate().getTime();
			
			//Calendar calNext = Calendar.getInstance();
			//calNext.setTime(new Date());		
			//calNext.add(Calendar.MONTH, 1); 			
			//long dateAfterInMs = calNext.getTime().getTime();//current month 
			//long dateAfterInMs = allocation.getEndDate().getTime();

			//long timeDiff = Math.abs(dateAfterInMs - dateBeforeInMs);
			
			
			/*
			 * int result=DateUtil.checkGreater(allocation.getStartDate(), new Date()); if
			 * (result < 0) { System.out.println("Date1 is before Date2"); }
			 * System.out.println("result-----------------: "+result);
			 */
			//long daysDiff = TimeUnit.DAYS.convert(timeDiff, TimeUnit.MILLISECONDS);
			// Alternatevly: 
			// int daysDiff = (int) (timeDiff / (1000 * 60 * 60 * 24));
			//System.out.println(" The number of days between dates: " + daysDiff);
			
			//freNo=(int) (daysDiff/days);
			System.out.println("freNo============"+freNo);
			Date startPaymentDt=allocation.getStartDate();
			
	        for (int i = 0; i < freNo; i++) {			
				
				
			Double installmentAmt=allocation.getFreelancerCost()/freNo;
			Calendar cal = Calendar.getInstance();
			cal.setTime(startPaymentDt);		
			cal.add(Calendar.DATE, days); 			
			Date nextPaymentDt = cal.getTime();
		
			Double clientinstallmentAmt=allocation.getProject().getProjectCost()/freNo;
			
			String clientDetails=allocation.getProject().getUserClient().getName();
			String freelanceDetails=allocation.getUserFreelancer().getName();
			if(i==0){
				nextPaymentDt=startPaymentDt;				
				}
					
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(nextPaymentDt);
			int year = calendar.get(Calendar.YEAR);
			//Add one to month {0 - 11}
			int month = calendar.get(Calendar.MONTH);
			int day = calendar.get(Calendar.DAY_OF_MONTH);
			
			//check paid or not
			/*
			 * SimpleDateFormat sf=new SimpleDateFormat("dd-MM-YYYY"); String fromDate=null;
			 * String toDate=null; fromDate=sf.format(startDate); toDate=sf.format(endDate);
			 */
			
			
			//System.out.println("Calendar================="+year+","+month+", "+day+"");
			//System.out.println("StartDate============"+nextPaymentDt+"=====installmentAmt:="+installmentAmt);
			String desc=freelanceDetails+"(DEBIT-AMOUNT-"+allocation.getPaymentFrequencyPayment()+" "+allocation.getCurrency()+") ::"+allocation.getUserFreelancer().getMobileNo()+"||"+clientDetails+"(CREDIT-AMOUNT-"+clientinstallmentAmt+" "+allocation.getProject().getCurrency()+") ::"+allocation.getProject().getUserClient().getMobileNo()+"||"+allocation.getProject().getProjectName();
	    	payResList.add(new PaymentCalenderResponse(allocation.getProject().getProjectName(),""+year+","+month+", "+day+"","false",paymentUrl+allocation.getProject().getProjectCodeInternal(),desc));
	    	startPaymentDt=nextPaymentDt;
	        }
			//}
			
		}
				
			
			return payResList;
		}
		//SHOW PAYMENT FOR 1 MONTH
				@Override
				public List<PaymentCalenderResponse> getPayCalenders(Long UserId) {
					List<ProjectPayment> projectPaymentList=projectPaymentRepository.findAllPendingPaymentsByUserId(UserId,"INPROGRESS");
					List<PaymentCalenderResponse> payResList=new ArrayList<PaymentCalenderResponse>();
						for (ProjectPayment payment : projectPaymentList) {
							String desc="";						
						Calendar calendar = new GregorianCalendar();
						calendar.setTime(payment.getPaymentDate());
						int year = calendar.get(Calendar.YEAR);
						//Add one to month {0 - 11}
						int month = calendar.get(Calendar.MONTH);
						int day = calendar.get(Calendar.DAY_OF_MONTH);
							ProjectAllocation allocation =payment.getProjectAllocation();
					payResList.add(new PaymentCalenderResponse(payment.getPaymentfor()+"::"+allocation.getProject().getProjectName(),""+year+","+month+", "+day+"","false",paymentUrl+allocation.getProject().getProjectCodeInternal(),desc));
			    	
			        }
					
					return payResList;
				}

				@Override
				public List<Project> findAllProjectByProjectCodes(String projectcodes) {
					return projectRepository.findAllProjectByProjectCodes(projectcodes);
				}
				@Override
				public String  getNewProjectCode() {
					Integer total=projectRepository.totalProject();
					String projectCode ="PC000"+(total+1);
					return projectCode;
				}
				
				
		
} 
	