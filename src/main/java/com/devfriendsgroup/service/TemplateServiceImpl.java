package com.devfriendsgroup.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devfriendsgroup.model.Template;
import com.devfriendsgroup.model.User;
import com.devfriendsgroup.model.UserRole;
import com.devfriendsgroup.repository.TemplateRepository;
import com.devfriendsgroup.repository.UserInfoRepository;
import com.devfriendsgroup.repository.UserRoleRepository;
import com.devfriendsgroup.util.SecurityHelper;

@Service
public class TemplateServiceImpl implements TemplateService {

	
	@Autowired
	private UserRoleRepository userRoleRepository;
	

	@Autowired
	UserInfoRepository userInfoRepository; 

	@Autowired
	TemplateRepository templateRepository;
	
	
		@Override
		public List<Template> findAllCustomTemplateByUserId(Long userId) {			
			return templateRepository.findAllCustomTemplateByUserId(userId);
		}

		@Override
		public List<Template> findAllDefaultTemplate() {
			return templateRepository.findAllDefaultTemplate();
		}

		@Override
		public void deleteTemplate(Long id_del) {
			Template template=templateRepository.findTemplateById(id_del);
			if(template!=null) {
				template.setStatus("INACTIVE");
				templateRepository.save(template); 
			}	
			
		}

		@Override
		public Template findTemplate(Long id_edit) {
			return templateRepository.findTemplateById(id_edit);
		}

		
		@Override
		public Template addOrUpdateTemplate(Template template) {
			User currentUser = SecurityHelper.getCurrentUser(); 
			UserRole currentUserDetails = userRoleRepository.getUserRoleByUserId(currentUser.getUserId()); 
		
			if(template.getId()==null)
			{
				//SAVE 			   
				template.setCreatedOn(new Date()); 
				template.setCreatedBy(currentUserDetails.getUserId());
				template.setStatus("ACTIVE");
				if(currentUserDetails.getRoleId().getRoleName().equals("ADMIN") || currentUserDetails.getRoleId().getRoleName().equals("SUPER_ADMIN")) {
					template.setType("DEFAULT");
				}
				else
				{
					template.setType("CUSTOM");
				}
				
				
			}
			else
			{
				
				Optional<Template> templateOp=templateRepository.findById(template.getId());
				if(templateOp.isPresent())
				{
					//UPDATE
					Template contractin=templateOp.get();
					template.setUpdatedBy(currentUserDetails.getUserId());
					template.setUpdatedOn(new Date());
					template.setCreatedBy(contractin.getCreatedBy());
					template.setCreatedOn(contractin.getCreatedOn());					
					template.setStatus(contractin.getStatus());
					template.setType(contractin.getType());
				}
			}
			return templateRepository.save(template);
		}

} 
	