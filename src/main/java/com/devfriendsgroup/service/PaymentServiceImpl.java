package com.devfriendsgroup.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devfriendsgroup.model.PaymentstatuschangeDTP;
import com.devfriendsgroup.model.Project;
import com.devfriendsgroup.model.ProjectPayment;
import com.devfriendsgroup.model.User;
import com.devfriendsgroup.repository.ProjectPaymentRepository;
import com.devfriendsgroup.repository.ProjectRepository;
import com.devfriendsgroup.repository.UserRepository;
import com.devfriendsgroup.util.DateUtil;
import com.devfriendsgroup.util.SecurityHelper;

@Service
public class PaymentServiceImpl implements PaymentService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	ProjectRepository projectRepository;

	@Autowired
	ProjectPaymentRepository projectPaymentRepository;

	@Override
	public List<ProjectPayment> findAllProjectPaymentsByProjectCode(String projectCode) {
		return projectPaymentRepository.findAllProjectPaymentsByProjectCode(projectCode);

	}
	@Override
	public ProjectPayment addOrUpdateProjectPayment(ProjectPayment projectPayment) {
		System.out.println("projectPayment============="+projectPayment);
		User currentUser = SecurityHelper.getCurrentUser();
		User loggedinUser =userRepository.findByUserId(currentUser.getUserId());
		projectPayment.setStartDate(DateUtil.formatDDMMYYYY(projectPayment.getStartDateStr()));
		projectPayment.setEndDate(DateUtil.formatDDMMYYYY(projectPayment.getEndDateStr()));
		projectPayment.setPaymentDate(DateUtil.formatDDMMYYYY(projectPayment.getPaymentDateStr()));
		if(projectPayment.getId()==null)
		{
			//SAVE 			
			projectPayment.setCreatedOn(new Date()); 
			projectPayment.setCreatedBy(loggedinUser);
			projectPayment.setStatus("ACTIVE");
			projectPayment.setProjectAllocation(projectRepository.findByProjectCodeInternal(projectPayment.getProjectCode()).getProjectAllocation());
		}
		else
		{
			Optional<ProjectPayment> projectPaymentOp=projectPaymentRepository.findById(projectPayment.getId());
			if(projectPaymentOp.isPresent())
			{
				//UPDATE
				ProjectPayment projectTaggin=projectPaymentOp.get();
				projectPayment.setUpdatedBy(loggedinUser);
				projectPayment.setUpdatedOn(new Date());
				projectPayment.setRemarks(projectTaggin.getRemarks());
				//projectPayment.setProject(projectTaggin.getProject());
			}
		}
		return projectPaymentRepository.save(projectPayment);
	}

	
	@Override
	public void deleteProjectPayment(Long id_del) {
		ProjectPayment projectPayment=projectPaymentRepository.getOne(id_del);
		if(projectPayment!=null) {
			projectPayment.setStatus("INACTIVE");
			projectPaymentRepository.save(projectPayment); 
		}	
		
	}
	@Override
	public ProjectPayment findProjectPayment(Long id_edit) {
		return projectPaymentRepository.getOne(id_edit);
	}

	@Override
	public List<ProjectPayment> findAllPendingPaymentsByUserId(Long UserId) {		
		return projectPaymentRepository.findAllPendingPaymentsByUserId(UserId,"INPROGRESS");

	}

	@Override
	public List<ProjectPayment> findAllProjectPaymentsBySearch(Long userId, String projectStatus) {
		// TODO Auto-generated method stub
		return projectPaymentRepository.findAllProjectPaymentsBySearch(userId,projectStatus);
	}

	@Override
	public ProjectPayment changePaymentStatus(PaymentstatuschangeDTP paymentstatuschangeDTP) {
		
		ProjectPayment projectPayment=projectPaymentRepository.findById(paymentstatuschangeDTP.getPaymentId()).orElseThrow();
		User currentUser = SecurityHelper.getCurrentUser();
		System.out.println("projectPayment======0====="+projectPayment);
		System.out.println("paymentDateStr=======1======"+paymentstatuschangeDTP.getPaymentDateStrM());
		System.out.println("paymentAmount========2====="+paymentstatuschangeDTP.getPaymentAmount_());
		
		if(projectPayment!=null) {			
			projectPayment.setPaymentStatus(paymentstatuschangeDTP.getPstatus());
			projectPayment.setRemarks(projectPayment.getRemarks()+" || "+paymentstatuschangeDTP.getRemarks());
			projectPayment.setUpdatedOn(new Date());
			projectPayment.setUpdatedBy(userRepository.findByUserId(currentUser.getUserId()));
			/*
			 * if(paymentstatuschangeDTP.getPaymentDateStrM()!=null &&
			 * !"".equals(paymentstatuschangeDTP.getPaymentDateStrM())) {
			 * projectPayment.setPaymentDate(DateUtil.formatDDMMYYYY(paymentstatuschangeDTP.
			 * getPaymentDateStrM())); }
			 * 
			 * 
			 * if(paymentstatuschangeDTP.getPaymentAmount_()!=null) {
			 * projectPayment.setPaymentAmount(paymentstatuschangeDTP.getPaymentAmount_());
			 * }
			 */
				
			System.out.println("projectPayment========3============"+projectPayment);
			return null;//projectPaymentRepository.save(projectPayment); 
		}	
		System.out.println("projectPayment===========4========="+paymentstatuschangeDTP);
		return null;
	}
	
	@Override
	public ProjectPayment updatePaymentStatus(PaymentstatuschangeDTP paymentstatuschangeDTP) {
		System.out.println("projectPayment======0====="+paymentstatuschangeDTP.getPaymentId());
		ProjectPayment projectPayment=projectPaymentRepository.findById(paymentstatuschangeDTP.getPaymentId()).orElseThrow();
		User currentUser = SecurityHelper.getCurrentUser();
		if(projectPayment!=null) {			
			projectPayment.setPaymentStatus(paymentstatuschangeDTP.getPstatus());
			projectPayment.setRemarks(projectPayment.getRemarks()+" || "+paymentstatuschangeDTP.getRemarks());
			projectPayment.setUpdatedOn(new Date());
			projectPayment.setUpdatedBy(userRepository.findByUserId(currentUser.getUserId()));
			
			
			 if(paymentstatuschangeDTP.getPaymentDateStrM()!=null &&
			  !"".equals(paymentstatuschangeDTP.getPaymentDateStrM())) {
			  projectPayment.setPaymentDate(DateUtil.formatDDMMYYYY(paymentstatuschangeDTP.
			  getPaymentDateStrM())); 
			  }
			 if(paymentstatuschangeDTP.getStartDateStr_()!=null &&
					  !"".equals(paymentstatuschangeDTP.getStartDateStr_())) {
					  projectPayment.setStartDate(DateUtil.formatDDMMYYYY(paymentstatuschangeDTP.
							  getStartDateStr_())); 
					  }
			 if(paymentstatuschangeDTP.getEndDateStr_()!=null &&
					  !"".equals(paymentstatuschangeDTP.getEndDateStr_())) {
					  projectPayment.setEndDate(DateUtil.formatDDMMYYYY(paymentstatuschangeDTP.
							  getEndDateStr_())); 
					  }
			 
			 if(paymentstatuschangeDTP.getPaymentAmount_()!=null) {
			 projectPayment.setPaymentAmount(paymentstatuschangeDTP.getPaymentAmount_());
			 }
			 if(paymentstatuschangeDTP.getCurrency_()!=null && !"".equals(paymentstatuschangeDTP.getCurrency_())) {
				 projectPayment.setCurrency(paymentstatuschangeDTP.getCurrency_());
				 }
			 return projectPaymentRepository.save(projectPayment); 
		}
		
		return null;
	}

	@Override
	public List<ProjectPayment> findAllProjectPaymentsByRoleAndUserId(Long uid, String rolename) {
		List<ProjectPayment> payments = null;
		if(rolename.equals("CLIENT"))
		{
			
			payments= projectPaymentRepository.findAllProjectPaymentsByRoleAndClientUserId(uid);
		}
		else if(rolename.equals("FREELANCER"))
		{
			
			payments= projectPaymentRepository.findAllProjectPaymentsByRoleAndFreelancerUserId(uid);
		}
		else if(rolename.equals("AGENT"))
		{
			//need to re-write
			payments= projectPaymentRepository.findAllProjectPaymentsByRoleAndFreelancerUserId(uid);
		}
		return payments;
	}
	@Override
	public List<ProjectPayment> findAllProjectPaymentsByProjectCodeAndStatus(String projectcode, String paystatus) {
		return projectPaymentRepository.findAllProjectPaymentsByProjectCodeAndStatus(projectcode,paystatus);
	}
	@Override
	public void deletePayment(Long id_del) {
		projectPaymentRepository.deleteById(id_del);;
		
	}

} 
