package com.devfriendsgroup.service;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.devfriendsgroup.model.Contract;
import com.devfriendsgroup.model.User;
import com.devfriendsgroup.model.UserRole;
import com.devfriendsgroup.repository.ContractRepository;
import com.devfriendsgroup.repository.ProjectPaymentRepository;
import com.devfriendsgroup.repository.ProjectRepository;
import com.devfriendsgroup.repository.RoleRepository;
import com.devfriendsgroup.repository.TemplateRepository;
import com.devfriendsgroup.repository.UserInfoRepository;
import com.devfriendsgroup.repository.UserRepository;
import com.devfriendsgroup.repository.UserRoleRepository;
import com.devfriendsgroup.util.FileUtils;
import com.devfriendsgroup.util.SecurityHelper;

@Service
public class ContractServiceImpl implements ContractService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserRoleRepository userRoleRepository;

	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder; 
	@Autowired
	UserInfoRepository userInfoRepository; 
	@Autowired
	ProjectRepository projectRepository;


	@Autowired
	ProjectPaymentRepository projectPaymentRepository;
	@Autowired
	ContractRepository contractRepository;
	@Autowired
	TemplateRepository templateRepository;
	@Autowired
	private FileUtils fileUtils;


	@Override
	public Contract addOrUpdateContract(Contract contract,MultipartFile file) {
		System.out.println("contract======file========"+file.getOriginalFilename());
		User currentUser = SecurityHelper.getCurrentUser(); 
		User loggedinUser =userRepository.findByUserId(currentUser.getUserId());
		if(contract.getOtherFfnotProject()!=null && "".equals(contract.getOtherFfnotProject())) {
			contract.setProject(null);
		}
		if(file!=null && !file.isEmpty())
		{
			String filePath=  fileUtils.uploadFile(file);  // Upload document to a file	
		if(filePath!=null && !filePath.isEmpty())
		{	
			contract.setContractUpload(filePath);
		}
		}
		if(contract.getId()==null)
		{
			//SAVE 			   
			contract.setCreatedOn(new Date()); 
			contract.setCreatedBy(loggedinUser);
			contract.setStatus("ACTIVE");
			contract.setContractStatus("CREATED");	
			
			


		}
		else
		{

			Optional<Contract> contractOp=contractRepository.findById(contract.getId());
			if(contractOp.isPresent())
			{
				//UPDATE
				Contract contractin=contractOp.get();
				contract.setUpdatedBy(loggedinUser);
				contract.setUpdatedOn(new Date());
				contract.setCreatedBy(contractin.getCreatedBy());
				contract.setCreatedOn(contractin.getCreatedOn());
				contract.setContarctCode(contractin.getContarctCode());
				contract.setContractStatus(contractin.getContractStatus());				
				
				contract.setStatus(contractin.getStatus());
				contract.setSign1Upload(contractin.getSign1Upload());
				contract.setSign2Upload(contractin.getSign2Upload());
				contract.setOtherFfnotProject(contractin.getOtherFfnotProject());
				
			}
		}
		return contractRepository.save(contract);
	}

	@Override
	public List<Contract> findAllContractBySenderOrReceiver(Long userId) {
		return contractRepository.findAllContractBySenderOrReceiver(userId);
	}
	@Override
	public void deleteContract(Long contractId) {
		Contract contract=contractRepository.findContractById(contractId);
		if(contract!=null) {
			contract.setStatus("INACTIVE");
			contractRepository.save(contract); 
		}	

	}

	@Override
	public Contract findContract(Long id_edit) {
		return contractRepository.findById(id_edit).get();
	}

	@Override
	public Contract findContractByContractCode(String contractcode) {
		return contractRepository.findByContarctCode(contractcode);
	}
	
	
	@Override
	public Contract UploadSignedContract(Long cid, MultipartFile file) {		
		String filePath=  fileUtils.uploadFile(file);  // Upload document to a file	
		if(filePath!=null && !filePath.isEmpty())
		{
			Contract contract=contractRepository.findById(cid).get();
			if(contract!=null)
			{
				if(contract.getSign1Upload()==null)
				{
					contract.setSign1Upload(filePath);
					contract.setContractStatus("INPROGRESS");
				}
				else if(contract.getSign2Upload()==null)
				{
					contract.setSign2Upload(filePath);
					contract.setContractStatus("COMPLETED");
				}
				contract.setUpdatedOn(new Date());
				return contractRepository.save(contract);
			}
		}
		return null;
	}

	@Override
	public Contract UploadSignedContractByLink(String contractCode, MultipartFile file,String type) {
		
		String filePath=  fileUtils.uploadFile(file);  // Upload document to a file	
		if(filePath!=null && !filePath.isEmpty())
		{
			Contract contract=contractRepository.findByContarctCode(contractCode);
			if(contract!=null)
			{
				
				if(type.equals("lnk1248836"))//link1
				{
					contract.setSign1Upload(filePath);
					contract.setContractStatus("INPROGRESS");
				}
				else if(type.equals("lnk455836"))//link2
				{
					contract.setSign2Upload(filePath);
					contract.setContractStatus("COMPLETED");
				}				
				contract.setUpdatedOn(new Date());
				return contractRepository.save(contract);
			}
		}
		return null;
	}

} 
