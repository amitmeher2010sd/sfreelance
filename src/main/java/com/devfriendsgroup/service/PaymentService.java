package com.devfriendsgroup.service;

import java.util.List;

import com.devfriendsgroup.model.PaymentstatuschangeDTP;
import com.devfriendsgroup.model.ProjectPayment;


public interface PaymentService {	
	
	public List<ProjectPayment> findAllProjectPaymentsByProjectCode(String projectCode);
	ProjectPayment addOrUpdateProjectPayment(ProjectPayment projectPayment);
	public void deleteProjectPayment(Long id_del);
	public ProjectPayment findProjectPayment(Long id_edit);
	public List<ProjectPayment> findAllPendingPaymentsByUserId(Long uid);
	public List<ProjectPayment> findAllProjectPaymentsBySearch(Long userId, String projectStatus);
	public ProjectPayment changePaymentStatus(PaymentstatuschangeDTP paymentstatuschangeDTP);
	public List<ProjectPayment> findAllProjectPaymentsByRoleAndUserId(Long uid,String rolename);
	public ProjectPayment updatePaymentStatus(PaymentstatuschangeDTP paymentstatuschangeDTP) ;
	public List<ProjectPayment> findAllProjectPaymentsByProjectCodeAndStatus(String projectcode, String paystatus);
	public void deletePayment(Long id_del);

} 
	