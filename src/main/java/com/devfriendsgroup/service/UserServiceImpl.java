package com.devfriendsgroup.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.devfriendsgroup.model.User;
import com.devfriendsgroup.model.UserDTO;
import com.devfriendsgroup.model.UserInfo;
import com.devfriendsgroup.model.UserRole;
import com.devfriendsgroup.repository.ProjectPaymentRepository;
import com.devfriendsgroup.repository.ProjectRepository;
import com.devfriendsgroup.repository.RoleRepository;
import com.devfriendsgroup.repository.UserInfoRepository;
import com.devfriendsgroup.repository.UserRepository;
import com.devfriendsgroup.repository.UserRoleRepository;
import com.devfriendsgroup.util.SecurityHelper;

@Service 
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private UserRoleRepository userRoleRepository;
	
	@Autowired
	private RoleRepository roleRepository; 
	
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder; 
	
	@Autowired
	private UserInfoRepository userInfoRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	PaymentService paymentService;
	
	@Override
	public User getUserForLogin(String userName, String password) {		
		return userRepository.getUserForLogin(userName, password);   
	}

	@Override
	public UserRole getUserRoleByUserId(Long userId) { 
		return userRoleRepository.getUserRoleByUserId(userId); 
	}

	@Override
	public User changePassword(Long userId, String oldPassword, String newPassword) {
		
		User user = userRepository.getOne(userId); 
		User finalUser = null;
		if(bCryptPasswordEncoder.matches(oldPassword, user.getPassword())) {
			
			finalUser= changePwd(userId,newPassword) ;
		}
		
		return finalUser;
	}

	@Override
	public User changePwd(Long userId, String pwd) {
		
		User user = userRepository.findByUserId(userId);
		
		if(user!=null) {
			user.setPassword(bCryptPasswordEncoder.encode(pwd)); 
			user.setUpdatedBy(user); 
			user.setUpdatedOn(new Date());  
			user = userRepository.save(user); 
		}
		
		return user;
	}

	@Override
	public User getUserByUserNameOrMobileNumber(String unameOrmob) {
		return userRepository.getUserByUserNameOrMobileNumber(unameOrmob);   
	}

	@Override
	public User saveOtpForForgotPassword(User existingUser, String otp) {
		existingUser.setOtp(otp); 
		return userRepository.save(existingUser); 
	}

	@Override
	public User getUserById(Long userId) {
		return userRepository.getOne(userId); 
	}

	@Override
	public User matchOtp(String otp) {
		return userRepository.matchOtp(otp); 
	}

	@Override
	public User changePasswordForForgotCasePost(User existingUser, String password) {
		
		existingUser.setPassword(bCryptPasswordEncoder.encode(password)); 
		existingUser.setOtp(null); 
		User finalUser = userRepository.save(existingUser);  
		
		return finalUser; 
	}

	@Override
	public User registerUser(String name, String userName, String password, String mobileNumber, String emailId, String remark, String address, String userType, String gender) {

		User user = new User();
		user.setName(name);
		user.setUserName(userName);
		user.setPassword(bCryptPasswordEncoder.encode(password));  
		user.setMobileNo(mobileNumber);
		user.setEmail(emailId);
		user.setGender(gender); 
		user.setAddress(address); 
		user.setRemark(remark);   
		user.setCreatedOn(new Date()); 
		user.setCreatedBy(userRepository.findByUserId(1l)); //Created by super admin user. 
		user.setStatus("ACTIVE");
		user.setIsblocked(0);
		User finalUser = userRepository.save(user); 
		
		UserRole userRole = new UserRole(); 
		userRole.setUserId(finalUser); 
		userRole.setAdminId(userRepository.findByUserId(2l)); //Default Admin
		if(userType.equals("CLIENT")) {
			userRole.setRoleId(roleRepository.findRoleByRoleId(4l));   
			userRole.setAgentId(userRepository.findByUserId(3l)); //Default Client Agent
		}
		if(userType.equals("FREELANCER")) {
			userRole.setRoleId(roleRepository.findRoleByRoleId(5l));   
			userRole.setAgentId(userRepository.findByUserId(4l)); //Default Freelancer Agent
		}
		userRole.setCreatedOn(new Date()); 
		userRole.setCreatedBy(userRepository.findByUserId(1l)); //Created by super admin user. 
		userRole.setStatus("ACTIVE"); 
		userRoleRepository.save(userRole); 
		
		return finalUser; 
	}

	@Override
	public UserRole getUserRoleByUserName(String uname) {
		return userRoleRepository.getUserRoleByUserName(uname);
	}

	@Override
	public UserRole getUserRoleById(Long id) {
		return userRoleRepository.findById(id).get();
	}

	@Override
	public UserDTO getUserDTOById(Long userId) {
		UserDTO dto=new UserDTO();
		dto.setUserInfo(userInfoRepository.findUserInfoByUserId(userId));
		//dto.setProject(projectRepository.findAllProjectByClientUserId(userId));
		dto.setUserRole(userRoleRepository.getUserRoleByUserId(userId));
		//dto.setPayments(paymentService.findAllProjectPaymentsByRoleAndUserId(userId, dto.getUserRole().getRoleId().getRoleName()));	
		//System.out.println("getUserDTOById=my-profile==="+dto);
		return dto;
	}

	

	@Override
	public UserInfo geMyProfile(Long userId) {
		return userInfoRepository.findUserInfoByUserId(userId);
	}
	@Override
	public UserInfo editProfile(UserInfo userInfo) {
		
		User finalUser = null;
		User user = userRepository.findByUserId(userInfo.getUserId()); 
		if(user==null)
		{
			return null;
		}
		
		user.setAddress(userInfo.getAddress()); 
		user.setUpdatedBy(user); 
		user.setUpdatedOn(new Date());  		
		finalUser = userRepository.save(user); 
		System.out.println("User  saved");
		if(finalUser!=null)
		{
			
			UserInfo userInfodb =userInfoRepository.findUserInfoByUserId(user.getUserId());
			if(userInfodb!=null)
			{
				
				userInfodb.setAddress(userInfo.getAddress());
				userInfodb.setFbLink(userInfo.getFbLink());
				userInfodb.setIdProofDocument(userInfo.getIdProofDocument());
				userInfodb.setIdProofName(userInfo.getIdProofName());
				userInfodb.setIdProofNumber(userInfo.getIdProofNumber());
				userInfodb.setLinkdeanLink(userInfo.getLinkdeanLink());
				userInfodb.setInstaLink(userInfo.getInstaLink());
				userInfodb.setOrganizationName(userInfo.getOrganizationName());
				userInfodb.setOrganizationDetails(userInfo.getOrganizationDetails());
				userInfodb.setStandardPayment(userInfo.getStandardPayment());
				userInfodb.setCurrency(userInfo.getCurrency());
				userInfodb.setProfession(userInfo.getProfession());
				userInfodb.setUpdatedOn(new Date());
				userInfodb.setUpdatedBy(userRepository.findByUserId(SecurityHelper.getCurrentUser().getUserId()));
				userInfodb.setWebsiteLink(userInfo.getWebsiteLink());
				userInfo=userInfoRepository.save(userInfodb);
				System.out.println("UserInfo  saved");
			}
			
		}
		
		return userInfo;
	}
	@Override
	public List<UserRole> findAllUserRoleByRole(String roleName) {
		return userRoleRepository.findAllUserRoleByRole(roleName);
	}
	
}
	