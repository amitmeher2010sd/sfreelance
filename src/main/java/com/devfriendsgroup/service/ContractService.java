package com.devfriendsgroup.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.devfriendsgroup.model.ProjectPayment;
import com.devfriendsgroup.model.Contract;
import com.devfriendsgroup.model.Project;
import com.devfriendsgroup.model.ProjectAllocation;
import com.devfriendsgroup.model.Role;
import com.devfriendsgroup.model.Template;
import com.devfriendsgroup.model.User;
import com.devfriendsgroup.model.UserInfo;
import com.devfriendsgroup.model.UserRole;

public interface ContractService {

	
	Contract addOrUpdateContract(Contract contract, MultipartFile file);
	List<Contract> findAllContractBySenderOrReceiver(Long userId);
	Contract findContract(Long id_edit);
	void deleteContract(Long id_del);
	Contract findContractByContractCode(String contractcode);
	Contract UploadSignedContract(Long cid, MultipartFile file);
	Contract UploadSignedContractByLink(String ucode, MultipartFile file, String type);	
	
   
} 
	