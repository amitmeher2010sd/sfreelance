package com.devfriendsgroup.util;

import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.RandomStringUtils;


public class MasterData {
	
	public final static Double USD_TO_INR=81d;
	public final static String YEAR="2024";
	public final static Double CAD_TO_INR=61.3d;
	public final static String STATUS_INACTIVE="INACTIVE";
	public final static String STATUS_ACTIVE="ACTIVE";
	
	public static String otpGenertor()
	{
		 SecureRandom random= new SecureRandom();
		 int num=random.nextInt(100000);
		 String formatted=String.format("%06d", num);
		 return formatted; 
	 } 
	public static String randomCode()
	{
		return  RandomStringUtils.randomAlphanumeric(30);
	 } 
	public static List<String> currencyList()
	{
		return Arrays.asList("INR","USD","CAD");
	 } 
	public static List<String> paymentTypeList()
	{
		return Arrays.asList("CREDIT","DEBIT");
	} 
	public static List<String> paymentStatusList()
	{
		return Arrays.asList("PENDING","PAID","CANCELLED");
	} 
	public static List<String> demoStatusList()
	{
		return Arrays.asList("PENDING","SCHEDULE_MEETING_IN_FUTURE","WAITING_SCHEDULE","SCHEDULED","MEETING_DONE","HOLD","SELECTED","REJECTED");
	} 
	
	public static List<String> projectstatusList()
	{
		return Arrays.asList("PENDING","HOLD","INPROGRESS","CANCELLED","COMPLETED");
	}
	public static List<String> projectAllocationstatusList()
	{
		return Arrays.asList("PENDING","HOLD","INPROGRESS","COMPLETED","CLOSED");
	}
	public static List<String> userStatusList()
	{
		return Arrays.asList("ALL_FREELANCER","ALLOCATED_FREELANCER","NONE_ALLOCATED_FREELANCER","ALL_CLIENT","ALLOCATED_CLIENT","NONE_ALLOCATED_CLIENT");
	} 
	public static List<String> professionList()
	{
		return Arrays.asList("JAVA DEVELOPER","PYTHON DEVELOPER","SALEFORCE DEVELOPER","ANGULAR DEVELOPER","REACTJS DEVELOPER","UI DEVELOPER","AWS DEVELOPER","DEVOPS DEVELOPER","AZURE DEVELOPER","GCP DEVELOPER","DBA DEVELOPER","PLAQL/SSIS/POWERBI DEVELOPER");
	} 
	
	
	public static Map<String,String> countryCodeList()
	{
		Map<String,String> map=new HashMap<String,String>();
		map.put("", "-select-");
		map.put("+91", "IND");
		map.put("+1", "USA");
		map.put("1", "CAN");
		map.put("+20", "EGY");
		map.put("+61", "AUS");
		map.put("+43", "AUS(");
		return map;
		
	} 
	public static String CalendarData(String type)
	{
		Calendar calendar = Calendar.getInstance();  
		   
		   if(type.equals("YEAR"))
		   {
			   return (calendar.get(Calendar.YEAR)+"").trim();//Year: 2017
		   }
		   else if(type.equals("MONTH"))
		   {
			   return (calendar.get(Calendar.MONTH)+1+"").trim();
		   }
		   else if(type.equals("DATE"))
		   {
			   return (calendar.get(Calendar.DATE)+"").trim();//Day: 20
		   }
		   return null;
		
	} 
	public static String getLastDayOfMonth(int year, int month) throws Exception{
	    DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	    Date date = sdf.parse(year+"-"+(month<10?("0"+month):month)+"-01");

	    Calendar calendar = Calendar.getInstance();
	    calendar.setTime(date);

	    calendar.add(Calendar.MONTH, 1);
	    calendar.set(Calendar.DAY_OF_MONTH, 1);
	    calendar.add(Calendar.DATE, -1);

	    Date lastDayOfMonth = calendar.getTime();
	    return (lastDayOfMonth.getDate()+"").trim();
//Last Day of Month: 2017-01-31
	    //return sdf.format(lastDayOfMonth);
	}
	
	public static List<String> paymentFrequencyList()
	{
		return Arrays.asList("Monthly","Bi-Weekly","Weekly","Hourly");
	}
}
