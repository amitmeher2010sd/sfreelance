package com.devfriendsgroup.util;

import java.security.SecureRandom;

import org.apache.commons.lang3.RandomStringUtils;

public class CodeGenerator {

	public static String otpGenertor()
	{
		 SecureRandom random= new SecureRandom();
		 int num=random.nextInt(100000);
		 String formatted=String.format("%06d", num);
		 return formatted; 
	 } 
	public static String randomCode()
	{
		return  RandomStringUtils.randomAlphanumeric(30);
	 } 
	
	
}
