package com.devfriendsgroup.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;


@Component
public class FileUtils {
	
	private static final String EXTERNAL_FILE_PATH = "C:/fileDownloadExample/";
	/*
	private static final String UPLOADFILE_BASEPATH_WINDOWS = "C:";
	private static final String UPLOADFILE_BASEPATH_LINUX = "";
	private static final String UPLOADFILE_UPLOAD_DIRECTORY = "TUTORIAL_FMS";

*/	
	
	@Value("${base_path_window}")
	private  String BASEPATH_WINDOWS;
	@Value("${base_path_LINUX}")
	private  String BASEPATH_LINUX;
	@Value("${base_upload_directory}")
	private  String UPLOAD_DIRECTORY;
	
	@Value("${base_file_folder}")
	private  String DBRMCFT_DOC;
	
	
	/*
	 * @Value("${base_video_folder}") private String TUTORIAL_VIDEO;
	 */
	
	 
	
	public  String getPathToViewFile(String module){
		/*
		 * if(module.equalsIgnoreCase("PDF")) { module=TUTORIAL_PDF; } else
		 * if(module.equalsIgnoreCase("VIDEO")) { module=TUTORIAL_VIDEO; }
		 */
		return getRootPath()+File.separator+module;
	}
	
	
	
	//String DIRECTORY = "C:\\TUTORIAL_FMS\\TUTORIAL_PDF";
	public enum OS {
        WINDOWS, LINUX, MAC, SOLARIS
    };// Operating systems.

	private static OS os = null;
	
	public static OS getOS() {
	    if (os == null) {
	        String operSys = System.getProperty("os.name").toLowerCase();
	        if (operSys.contains("win")) {
	            os = OS.WINDOWS;
	        } else if (operSys.contains("nix") || operSys.contains("nux")
	                || operSys.contains("aix")) {
	            os = OS.LINUX;
	        } else if (operSys.contains("mac")) {
	            os = OS.MAC;
	        } else if (operSys.contains("sunos")) {
	            os = OS.SOLARIS;
	        }
	    }
	    return os;
	}
	public  String getRootPath(){
		String rootpath= null;
		if(getOS().equals(OS.WINDOWS))
		{
			rootpath= BASEPATH_WINDOWS+File.separator+UPLOAD_DIRECTORY;
		}
		else if(getOS().equals(OS.LINUX))
		{
			rootpath= BASEPATH_LINUX+File.separator
					+ UPLOAD_DIRECTORY;
		}
		System.out.println("rootpath::------------------::"+rootpath);
		return rootpath;
	}
	public  String getPathToStoreDocument(String module){	
	
		String rootpath= getRootPath();
	
		//CREATE ROOT PATH FOLDER
		File rootDir = new File(rootpath);
			if (!rootDir.exists()) {
				rootDir.mkdir();
			}
		//CREATE CHIELD FOLDER
		String uploadPath = rootpath+File.separator+module;
		File uploadDir = new File(uploadPath);
			if (!uploadDir.exists()) {
				uploadDir.mkdir();
			}
		 return uploadPath;
	}
	/**
	 * STORE DOCUMENT ON FOLDER STRUCTURE
	 */
	
	public  String upload(MultipartFile multipartFile,String module) throws IOException {
 
		BufferedOutputStream stream = null;
		
		boolean bool = false;

		String uploadPath =getPathToStoreDocument(module);
		String filePath = uploadPath + File.separator + multipartFile.getOriginalFilename();
		File storeFile = new File(filePath);
		bool = storeFile.exists();
		if (bool == true) 
		{
			// check doc exits or not .if true than exist else false
			return null;
		}

		byte[] bytes = multipartFile.getBytes();
		// saves the file on disk
		stream = new BufferedOutputStream(new FileOutputStream(storeFile));
		stream.write(bytes);
		stream.close();
		// item.write(storeFile);
		String docname = multipartFile.getOriginalFilename();

		if(docname!=null)
		{
			filePath=addCurrenDateTimeToDocAndRenameIt(docname,uploadPath,filePath);

		}
		System.out.println("filePath---------------------"+filePath);
		return  filePath;
	}
	
	/**
	 * ADD CURRENT DATE TIME TO AVOID DUCUMENT NAME DUBLICACY
	 */
	public static  String addCurrenDateTimeToDocAndRenameIt(String docname,String uploadPath,String filePath) throws IOException {
		
		if(docname.contains("."))
		{				 
			String data[]=docname.split("\\.");				
			Date dt=new Date();
			Calendar cal = Calendar.getInstance();				
			
			String meridiem = cal.getDisplayName(Calendar.AM_PM, Calendar.SHORT, Locale.getDefault());				 
			String currdt="_"+dt.getYear()+"_"+dt.getMonth()+"_"+dt.getDay()+"_"+meridiem;
			String filePathNew = uploadPath + File.separator +"RFTS_"+data[0]+currdt+"."+data[1];
			
			filePath=renameFile(filePath,filePathNew);
		}
		return filePath;
	}
   public static  String newChangeFileName(String docname) throws IOException {
		
	   String newName="";
		if(docname.contains("."))
		{				 
			String data[]=docname.split("\\.");				
			Date dt=new Date();
			Calendar cal = Calendar.getInstance();				
			
			String meridiem = cal.getDisplayName(Calendar.AM_PM, Calendar.SHORT, Locale.getDefault());				 
			String currdt="_"+dt.getYear()+"_"+dt.getMonth()+"_"+dt.getDay()+"_"+meridiem;
			newName ="RFTS_"+data[0]+currdt+"."+data[1];
			
			
		}
		return newName;
	}
	
	/**
	 * RENAME DOCUMENT NAME
	 */
	public static String renameFile(String filePath, String filePathNew) throws IOException 
	{
		File srcFile = new File(filePath);
		boolean bSucceeded = false;
		try 
		{
			File destFile = new File(filePathNew);
			if (destFile.exists()) 
			{
				if (!destFile.delete()) 
				{
					throw new IOException(filePath + " could not be renamed to " + filePathNew);
				}
			}
			if (!srcFile.renameTo(destFile)) 
			{
				throw new IOException(filePath + " could not be renamed to " + filePathNew);
			} 
			else 
			{
				bSucceeded = true;
				if (bSucceeded == true) 
				{
					System.out.println(filePath + " is successfully renamed to " + filePathNew);
					return filePathNew;
				}
			}
		} 
		finally 
		{
			if (bSucceeded) 
			{
				// srcFile.delete();
			}
		}
		return null;
	}
	
	 public static void copyFile(InputStream in, OutputStream out) throws IOException
     {
         byte[] buffer = new byte[1024];
         int read;
         while ((read = in.read(buffer)) != -1)
         {
             out.write(buffer, 0, read);
         }
     }
	 
		public  ResponseEntity<ByteArrayResource> commonfile(String rootpath,String fileName,ServletContext servletContext)
				throws IOException {
			
			MediaType mediaType = MediaTypeUtils.getMediaTypeForFileName(servletContext, fileName);
			System.out.println("fileName: " + fileName);
			System.out.println("mediaType: " + mediaType);

			Path path = Paths.get(rootpath + File.separator + fileName);
			byte[] data = Files.readAllBytes(path);
			ByteArrayResource resource = new ByteArrayResource(data);

			return ResponseEntity.ok()
					// Content-Disposition
					.header(HttpHeaders.CONTENT_DISPOSITION, "inline;filename=" + path.getFileName().toString())
					// Content-Type
					.contentType(mediaType) //
					// Content-Lengh
					.contentLength(data.length) //
					.body(resource);
		}
		public String uploadFile(MultipartFile multipartFile)
		{
			 
			
			if(!multipartFile.isEmpty())
			{

				try 
				{
					 String path = this.upload(multipartFile, "CONTRACT_DOC");  
					 return path;
					 
				} 
				catch (IOException e) 
				{
					System.out.println("ERROR: WHILE UPLOAD PDF FILE "+e.getMessage());			
					return null;
				}
			}
			else
			{
				return null;
			}

			
		}

	
	public void downloadPDFResource(HttpServletRequest request, HttpServletResponse response,String filepath) throws IOException {

		/* File file = new File(EXTERNAL_FILE_PATH + fileName); */
		File file = new File(filepath);
		if (file.exists()) {

			//get the mimetype
			String mimeType = URLConnection.guessContentTypeFromName(file.getName());
			if (mimeType == null) {
				//unknown mimetype so set the mimetype to application/octet-stream
				mimeType = "application/octet-stream";
			}

			response.setContentType(mimeType);

			/**
			 * In a regular HTTP response, the Content-Disposition response header is a
			 * header indicating if the content is expected to be displayed inline in the
			 * browser, that is, as a Web page or as part of a Web page, or as an
			 * attachment, that is downloaded and saved locally.
			 * 
			 */

			/**
			 * Here we have mentioned it to show inline
			 */
			response.setHeader("Content-Disposition", String.format("inline; filename=\"" + file.getName() + "\""));

			 //Here we have mentioned it to show as attachment
			 //response.setHeader("Content-Disposition", String.format("attachment; filename=\"" + file.getName() + "\""));

			response.setContentLength((int) file.length());

			InputStream inputStream = new BufferedInputStream(new FileInputStream(file));

			FileCopyUtils.copy(inputStream, response.getOutputStream());

		}
	}

}
