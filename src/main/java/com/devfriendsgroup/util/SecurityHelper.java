package com.devfriendsgroup.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.devfriendsgroup.model.LoggedInUser;
import com.devfriendsgroup.model.User;

public class SecurityHelper {

	public static User getCurrentUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		LoggedInUser currentUser = (LoggedInUser) auth.getPrincipal();
		
		return currentUser.getDbUser();
	}
	public static User setCurrentUser(User user) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		LoggedInUser currentUser = (LoggedInUser) auth.getPrincipal();
		currentUser.setDbUser(user);
		return currentUser.getDbUser();
	}
	public static String getCurrentRole() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String currentRole=SecurityHelper.getCurrentRole();
		 System.out.println("currentRole======"+currentRole);
		if(!"[ADMIN]".equals(currentRole))
			{
		    	  System.out.println("validateAdminRole==error=addRole==PERMISSIONERROR========");
		    	return "redirect:/error?code=PERMISSIONERROR"; 
			}	
	    return "";
		
	}
}
