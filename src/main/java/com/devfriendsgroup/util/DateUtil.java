package com.devfriendsgroup.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;

public class DateUtil {

	public static Date formatDDMMYYYY(String requestDate) {

		Date responseDate=null;
		try {
			responseDate = new SimpleDateFormat("dd-MM-yyyy").parse(requestDate);
			return responseDate;			
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		} 
		
	}
	
	public static int checkGreater(Date date1, Date date2) {

		int result = date1.compareTo(date2);

		if(result == 0) {
		    System.out.println("Both dates are equal");
		}
		else if (result < 0) {
			System.out.println("Date1 is before Date2");
		}
		else {
			System.out.println("Date1 is after Date2");
		}
		
		return result;
	}
	
	public static int getMonth(Date requestDate) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(requestDate);
		return calendar.get(Calendar.MONTH)+1;
		
	}
	


}
