package com.devfriendsgroup.controller;

import java.security.Principal;
import java.util.Arrays;
import java.util.List;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.devfriendsgroup.service.AdminService;
import com.devfriendsgroup.service.UserDetailsServiceImpl;
import com.devfriendsgroup.util.SecurityHelper;

@Controller 
public class WelcomeController {
	
	@Autowired
	private UserDetailsServiceImpl userDetailsServiceImpl;
	@Autowired
	private AdminService adminService;

	
	@GetMapping("/successpage")
	public String successpage(HttpServletRequest request,Model model,Principal principal,HttpSession httpSession) {	
		if(principal != null) {
			String switchuser_=(String) httpSession.getAttribute("switchuser");
			
			return userDetailsServiceImpl.portalRedirectUrl(httpSession,switchuser_);
		}
		else {
			return "redirect:/user-login"; 
		}
		
	} 
	@GetMapping("/")
	public String index(HttpServletRequest request, Principal principal,HttpSession httpSession) {	
		
		if(principal != null) {
			
			String switchuser_=(String) httpSession.getAttribute("switchuser");
			return userDetailsServiceImpl.portalRedirectUrl(httpSession,switchuser_);
		}
		else {
			return "redirect:/user-login"; 
		}
		
	} 
 
	@GetMapping("/login-fail")
	public String loginFailure(HttpServletRequest request, RedirectAttributes message) {	
			
		message.addFlashAttribute("outcome", "Invalid Credentials"); 
		
		return "redirect:/user-login"; 
	} 
	
	@GetMapping("/access-denied")
	public String accessdenied(HttpServletRequest request, RedirectAttributes message) {	
			
		message.addFlashAttribute("outcome", "You are not permitted"); 
		
		return "redirect:/user-login"; 
	} 
	@GetMapping("/internal-access-denied")
	public String internalaccessdenied(HttpServletRequest request, RedirectAttributes message) {	
			
		message.addFlashAttribute("outcome", "You are not permitted"); 
		
		return "redirect:/user-login"; 
	} 
	@GetMapping("/user-login")
	public String userLogin(HttpServletRequest request) {	
		
		return "site.userLogin";  
	} 
	@GetMapping("/home") 
	public String dashBoard(HttpServletRequest request, Principal principal,HttpSession httpSession,Model model) {	
		 model.addAttribute("dashBoardResponse", adminService.loadDashBoardData(SecurityHelper.getCurrentUser().getUserId())); 
		return "site.dashBoard";  
	} 	
	@GetMapping("/payment-dashboard") 
	public String ecommerceDashboard(HttpServletRequest request, Principal principal,HttpSession httpSession,Model model) {	
		
		System.out.println("-------------------paymentDashboard----------------");
		 model.addAttribute("dashBoardResponse", adminService.loadDashBoardData(SecurityHelper.getCurrentUser().getUserId())); 
			
		return "site.paymentDashboard";  
	} 
	
	@GetMapping("/post-outer/{skill}")
	public String post(HttpServletRequest request, Model model,@PathVariable String skill) {
		model.addAttribute("skill",skill.equals("latest")?"":"Search for "+skill);
			model.addAttribute("postList", adminService.findAllPublishedPost(skill));
		
		List<String> skills=Arrays.asList("Java","DevOps",".Net","Database","Angular","React JS","Web Design");
		model.addAttribute("skills", skills);
		return "site.post_outer"; 
	} 
	
	
	
	
	
}
	