package com.devfriendsgroup.controller;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.devfriendsgroup.model.Contract;
import com.devfriendsgroup.model.Project;
import com.devfriendsgroup.model.User;
import com.devfriendsgroup.model.UserRole;
import com.devfriendsgroup.service.AdminService;
import com.devfriendsgroup.service.ContractService;
import com.devfriendsgroup.service.ProjectService;
import com.devfriendsgroup.service.TemplateService;
import com.devfriendsgroup.service.UserService;
import com.devfriendsgroup.util.FileUtils;
import com.devfriendsgroup.util.SecurityHelper;

@Controller
public class ContractController {
	
	
	@Autowired
	private UserService userService;
	@Autowired
	private ProjectService projectService;
	@Autowired
	private ContractService contractService;
	@Autowired
	private TemplateService templateService;

	
	@Autowired
	private FileUtils fileUtils;
	
	
	@Value("${base_path_window}")
	private  String BASEPATH_WINDOWS;
	@Value("${base_path_LINUX}")
	private  String BASEPATH_LINUX;
	@Value("${base_upload_directory}")
	private  String UPLOAD_DIRECTORY;
	
	@Value("${base_file_folder}")
	private  String CONTRACT_DOC;
	
		@GetMapping("/contracts") 
		public String contractsGet(HttpServletRequest request, Model model) {
			User user = SecurityHelper.getCurrentUser(); 
			UserRole userRole = userService.getUserRoleByUserId(user.getUserId()); 
			List<Contract> responseList=contractService.findAllContractBySenderOrReceiver(user.getUserId());
			model.addAttribute("responseList",responseList ); 
			model.addAttribute("userRole", userRole);
			return "site.contracts";   	
		} 
		@GetMapping("/contract") 
		public String contractGet(HttpServletRequest request, Model model) {
			User user = SecurityHelper.getCurrentUser();
			UserRole userRole = userService.getUserRoleByUserId(user.getUserId());
			List<Project> projectList=projectService.findAllProjectByUserRoleId(userRole.getUserRoleId());
			model.addAttribute("projectList",projectList.stream().filter(p->p.getStatus().equals("ACTIVE")).filter(p->!p.getProjectStatus().equals("CANCELLED")).collect(Collectors.toList()) ); 
			model.addAttribute("templateList",templateService.findAllCustomTemplateByUserId(user.getUserId())); 
			return "site.contract";   
		}
		@PostMapping("/contract") 
		public String contractPost(HttpServletRequest request, RedirectAttributes message,@ModelAttribute("contract") Contract contract,@RequestParam("file") MultipartFile file) {
			Contract response = contractService.addOrUpdateContract(contract,file);  
			if(response!=null)
			{
				message.addFlashAttribute("successMessage", "Contract ("+response.getContarctName()+") is "+(contract.getId()!=null?"Updated":"Saved")+" Successfully."); 
			
			}
			else {
				message.addFlashAttribute("failureMessage", "Unable To Save/Update"); 
			} 
			
			return "redirect:/contracts";    
			
		} 	
		@PostMapping("/upload-signed-contract") 
		public String uploadsignedcontract(HttpServletRequest request, RedirectAttributes message,@RequestParam("cid") Long cid,@RequestParam("file") MultipartFile file) {
			Contract response = contractService.UploadSignedContract(cid,file);  
			if(response!=null)
			{
				message.addFlashAttribute("successMessage", "Signd Contract is uploaded Successfully."); 
			
			}
			else {
				message.addFlashAttribute("failureMessage", "Unable To Upload"); 
			} 
			
			return "redirect:/contracts";    
			
		} 	
		
		@PostMapping("/del-contract") 
		public String contractproject(HttpServletRequest request,RedirectAttributes message, Model model, @RequestParam("id_del")Long id_del) {	
			
			contractService.deleteContract(id_del); 
			message.addFlashAttribute("successMessage", "Contract is Deleted Successfully."); 
			return "redirect:/contracts";         
		}
		@PostMapping("/edit-contract") 
		public String editcontract(HttpServletRequest request, Model model, @RequestParam("id_edit")Long id_edit) {	
			model.addAttribute("contract", contractService.findContract(id_edit));  
			User user = SecurityHelper.getCurrentUser();
			UserRole userRole = userService.getUserRoleByUserId(user.getUserId());
			List<Project> projectList=projectService.findAllProjectByUserRoleId(userRole.getUserRoleId());
			model.addAttribute("projectList",projectList.stream().filter(p->p.getStatus().equals("ACTIVE")).filter(p->!p.getProjectStatus().equals("CANCELLED")).collect(Collectors.toList()) ); 
			model.addAttribute("templateList",templateService.findAllCustomTemplateByUserId(user.getUserId()));
			return "site.contract" ;  
			
		}  
		@GetMapping("/view-contract/{contractcode}") 
		public String viewcontract(@PathVariable("contractcode")String contractcode,Model model) {		
			Contract contract=contractService.findContractByContractCode(contractcode);
			model.addAttribute("contract", contract);  
			return "site.viewcontract" ; 			
		}  
		
		
		//=================LINK GENERATED UPLOAD====================================
		
		@GetMapping("/link") 
		public String uploadsignedcontractLink(Model message,@RequestParam(value="cd",required = false)String contractCode,@RequestParam(value="tp",required = false) String type) {
			
			if(type==null || "".equals(type)) { 
				 System.out.println("upload-signed-contract==invalid=");
				 message.addAttribute("failureMessage", "Invalid Link");
				 message.addAttribute("invalid", "YES");
			 } 
			
			else if(!type.equals("lnk1248836") && !type.equals("lnk455836")) { 
				 System.out.println("upload-signed-contract==invalid=");
				 message.addAttribute("failureMessage", "Invalid Link");
				 message.addAttribute("invalid", "YES");
			 }
			else {
				 System.out.println("upload-signed-contract===");
				 Contract contract= contractService.findContractByContractCode(contractCode);
			 if(contract!=null) { 
				 message.addAttribute("cd", contractCode); 	
				 message.addAttribute("type", type);
				 message.addAttribute("invalid", "NO");
			 } 
			 else 
			 { 
				 message.addAttribute("failureMessage", "Invalid Link");
				 message.addAttribute("invalid", "YES");
			 }
			 }
				
				return "site.contractlink";    
			
		}
		@PostMapping("/link") 
		public String uploadsignedcontractLink(RedirectAttributes message,@RequestParam("type") String type,@RequestParam("cd") String contractCode,@RequestParam("file") MultipartFile file) {
			if(type==null || "".equals(type)) { 
				 System.out.println("upload-signed-contract==invalid=");
				 message.addFlashAttribute("failureMessage", "Invalid Link");
				 message.addFlashAttribute("invalid", "YES");
			 } 
			
			else if(!type.equals("lnk1248836") && !type.equals("lnk455836")) { 
				 System.out.println("upload-signed-contract==invalid=");
				 message.addFlashAttribute("failureMessage", "Invalid Link");
				 message.addFlashAttribute("invalid", "YES");
			 }
				
			 Contract response = contractService.UploadSignedContractByLink(contractCode,file,type);
			 if(response!=null) { 
				 message.addFlashAttribute("successMessage","Signd Contract is uploaded Successfully.");
				 message.addFlashAttribute("invalid", "NO");
			 } 
			 else 
			 { 
				 message.addFlashAttribute("failureMessage", "Unable To Upload");
				 message.addFlashAttribute("invalid", "NO");
			 }
			
			 return "redirect:/link?cd="+contractCode+"&tp="+type;   
			
		}
		@GetMapping("/contract/file/{contractcode}")
		public void downloadPDFResource(HttpServletRequest request, HttpServletResponse response,
				@PathVariable("contractcode") String contractcode) throws IOException {
			Contract contract=contractService.findContractByContractCode(contractcode);
			
			//CONTRACT_DOC
			String filepath=null;
			if(contract.getContractUpload()!=null && contract.getSign1Upload()==null && contract.getSign2Upload()==null)
			{
				filepath=contract.getContractUpload();
			}
			else if(contract.getContractUpload()==null && contract.getSign1Upload()!=null && contract.getSign2Upload()==null)
			{
				filepath=contract.getSign1Upload();
			}
			else if(contract.getContractUpload()==null && contract.getSign1Upload()!=null && contract.getSign2Upload()!=null)
			{
				filepath=contract.getSign2Upload();
			}
			else if(contract.getContractUpload()!=null && contract.getSign1Upload()!=null && contract.getSign2Upload()==null)
			{
				filepath=contract.getSign1Upload();
			}
			else if(contract.getContractUpload()!=null && contract.getSign1Upload()!=null && contract.getSign2Upload()!=null)
			{
				filepath=contract.getSign2Upload();
			}
			else {			
				System.out.println("Comming==downloadPDF==NO PATH==");
			}
			fileUtils.downloadPDFResource(request, response,filepath);
		}
		
// 
		@PostMapping("/mail-contract-send") 
		public String mailcontractsend(HttpServletRequest request,RedirectAttributes message,@RequestParam("contractId")Long contractId,@RequestParam("toUser")String[] toUsers) {
			
			System.out.println("contractId---------"+contractId);
			List<String> lst=Arrays.asList(toUsers);
			System.out.println("toUsers--lst-------"+lst);
			Contract contract=contractService.findContract(contractId);
			String[] filepath=new String[1];
			filepath[0]=contract.getSign2Upload();
			String subject="Test Mail ";
			String body="<h1>Check attachment for file!</h1>";
			//boolean flag=mailUtils.mailSendWithAttachment(toUsers,filepath,subject,body);
			if(true==true)
			{
				message.addFlashAttribute("successMessage", "Mail is sent Successfully."); 
			
			}
			else {
				message.addFlashAttribute("failureMessage", "Unable To sent mail"); 
			} 
			return "redirect:/contracts";    
			
		}

}
	