package com.devfriendsgroup.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.devfriendsgroup.model.Career;
import com.devfriendsgroup.model.Post;
import com.devfriendsgroup.model.Role;
import com.devfriendsgroup.model.Skill;
import com.devfriendsgroup.repository.CareerRepository;
import com.devfriendsgroup.service.AdminService;

@Controller
@RequestMapping("/master")
public class AdminController {

	@Autowired
	private AdminService adminService;
	@Autowired
	private CareerRepository careerRepository;
	
	
	
	@GetMapping("/role") 
	public String createRole(HttpServletRequest request, Model model) {			
		return "site.role";   

	} 
	
	@PostMapping("/role") 
	public String addRole(HttpServletRequest request, RedirectAttributes message,@ModelAttribute("role") Role role) {

		Role roleResponse = adminService.addOrUpdateRole(role);  
		if(roleResponse!=null)
		{
			message.addFlashAttribute("successMessage", "Role ("+roleResponse.getRoleDisplayName()+") is "+(role.getRoleId()!=null?"Updated":"Updated")+" Successfully."); 

		}
		else {
			message.addFlashAttribute("failureMessage", "Unable To "+(role.getRoleId()!=null?"Updated":"Updated")+" Role."); 
		} 

		return "redirect:/roles";    

	} 	

	@GetMapping("/roles") 
	public String manageRole(HttpServletRequest request, Model model) {	

		List<Role> roleList = adminService.findAllRole(); 
		model.addAttribute("roleList", roleList); 			
		return "site.list-roles";   

	} 

	@PostMapping("/del-role") 
	public String deleteRole(HttpServletRequest request, Model model,RedirectAttributes message, @RequestParam("id_del")Long id_del) {	

		adminService.deleteRole(id_del,"INACTIVE");   
		message.addFlashAttribute("successMessage", "Role is deleted Successfully."); 
		return "redirect:/roles";     
	}
	@PostMapping("/active-role") 
	public String Activerole(HttpServletRequest request, Model model,RedirectAttributes message, @RequestParam("id_active")Long id_active) {	
		adminService.deleteRole(id_active,"ACTIVE");  
		message.addFlashAttribute("successMessage", "Role is Activated Successfully."); 
		return "redirect:/roles"; 
	}
	@PostMapping("/edit-role") 
	public String editRole(HttpServletRequest request, Model model, @RequestParam("id_edit")Long id_edit) {	
		model.addAttribute("role", adminService.getRoleById(id_edit));  

		return "site.role";   

	}  
	
	
	@GetMapping("/skill") 
	public String createskill(HttpServletRequest request, Model model) {			
		return "site.skill";   

	} 

	@PostMapping("/skill") 
	public String addskill(HttpServletRequest request, RedirectAttributes message,@ModelAttribute("skill") Skill skill) {

		Skill skill_ = adminService.addOrUpdateSkill(skill);  
		if(skill_!=null)
		{
			message.addFlashAttribute("successMessage", "skill ("+skill_.getSkillName()+") is "+(skill_.getSkillId()!=null?"Updated":"Updated")+" Successfully."); 

		}
		else {
			message.addFlashAttribute("failureMessage", "Unable To "+(skill.getSkillName()!=null?"Updated":"Updated")+" skill."); 
		} 

		return "redirect:/skills";    

	} 	

	@GetMapping("/skills") 
	public String manageskills(HttpServletRequest request, Model model) {					
		model.addAttribute("skillList", adminService.findAllSkills()); 			
		return "site.skills";   

	} 


	@PostMapping("/del-skill") 
	public String deleteskill(HttpServletRequest request, Model model,RedirectAttributes message, @RequestParam("id_del")Long id_del) {	

		adminService.deleteSkill(id_del,"INACTIVE");  
		message.addFlashAttribute("successMessage", "Skill is deleted Successfully."); 
		return "redirect:/skills";     
	}
	@PostMapping("/active-skill") 
	public String Activekill(HttpServletRequest request, Model model,RedirectAttributes message, @RequestParam("id_del")Long id_del) {	

		adminService.deleteSkill(id_del,"ACTIVE");  
		message.addFlashAttribute("successMessage", "Skill is Activated Successfully."); 
		return "redirect:/skills";     
	}






	@PostMapping("/edit-skill") 
	public String editskill(HttpServletRequest request, Model model, @RequestParam("id_edit")Long id_edit) {	
		model.addAttribute("skill", adminService.getSkillById(id_edit));  

		return "site.skill";   

	}  
	
	
	
	
	@GetMapping("/careers") 
	public String careerGet(Model model) {
		model.addAttribute("dataList", careerRepository.findAllPublishedCareer());			
		return "site.careers";   

	}
	@GetMapping("/career") 
	public String career() {
		return "site.career";   

	}
	@PostMapping("/career") 
	public String career(@ModelAttribute("career")Career career,Model model,RedirectAttributes message) {
		career.setPublished("Publish".equals(career.getPublishedStr()));
		Career response = careerRepository.save(career);  
		if(response!=null)
		{
			message.addFlashAttribute("successMessage", "Career saved Successfully."); 

		}
		else {
			message.addFlashAttribute("failureMessage", "Unable to save Career"); 
		} 

		return "redirect:/master/careers";    

	} 
	@PostMapping("/edit-career") 
	public String editcareers(@RequestParam("id_edit")String id,Model model) {
		model.addAttribute("data", careerRepository.findCareerById(Long.parseLong(id)));
		return "site.career";   

	} 
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//===============POST=============
	@GetMapping("/post") 
	public String post(HttpServletRequest request, Model model) {			
		return "site.post";   

	} 

	@PostMapping("/post") 
	public String postadd(HttpServletRequest request, RedirectAttributes message,@ModelAttribute("post") Post post) {		
		Post res = adminService.addOrUpdatePost(post);
		if(res!=null)
		{
			message.addFlashAttribute("successMessage", "Post ("+res.getTitle()+") is "+(post.getId()!=null?"Updated":"Updated")+" Successfully."); 

		}
		else {
			message.addFlashAttribute("failureMessage", "Unable To "+(res.getTitle()!=null?"Updated":"Updated")+" Role."); 
		} 

		return "redirect:/posts";    

	} 	

	@GetMapping("/posts") 
	public String posts(HttpServletRequest request, Model model) {			
		model.addAttribute("postList", adminService.findAllPost());	
		return "site.posts";   

	} 


	@PostMapping("/del-post") 
	public String deletepost(HttpServletRequest request, Model model,RedirectAttributes message, @RequestParam("id_del")Long id_del) {	

		adminService.deletePost(id_del,"INACTIVE");  		
		
			message.addFlashAttribute("successMessage", "Contractor is deleted Successfully."); 
	
		return "redirect:/posts";     
	}
	@PostMapping("/active-post") 
	public String Activepost(HttpServletRequest request, Model model,RedirectAttributes message, @RequestParam("id_del")Long id_del) {	

		adminService.deletePost(id_del,"ACTIVE");  
		message.addFlashAttribute("successMessage", "Post is Activated Successfully."); 
		return "redirect:/posts";     
	}



	@PostMapping("/edit-post") 
	public String editspost(HttpServletRequest request, Model model, @RequestParam("id_edit")Long id_edit) {	
		
		model.addAttribute("post", adminService.findPostById(id_edit));

		return "site.post";   

	} 
	
	@PostMapping("/publish-post") 
	public String publishpost(HttpServletRequest request, Model model,RedirectAttributes message, @RequestParam("publish_id")Long publish_id, @RequestParam("publish_flag")boolean publish_flag) {	
		adminService.publishPost(publish_id,publish_flag); 
		String str=publish_flag==true?"Published":"Un-Published";
		
		message.addFlashAttribute("successMessage", " Post is "+str+" Successfully."); 
		return "redirect:/posts";     
	}
	
	

	
}
