package com.devfriendsgroup.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.devfriendsgroup.model.Project;
import com.devfriendsgroup.model.ProjectAllocation;
import com.devfriendsgroup.model.User;
import com.devfriendsgroup.model.UserInfo;
import com.devfriendsgroup.model.UserRole;
import com.devfriendsgroup.service.AdminService;
import com.devfriendsgroup.service.ProjectService;
import com.devfriendsgroup.service.SearchAndReportService;
import com.devfriendsgroup.service.UserService;
import com.devfriendsgroup.util.MasterData;
import com.devfriendsgroup.util.SecurityHelper;

@Controller
public class ProjectController {
	
	@Autowired
	private UserService userService;
	@Autowired
	private AdminService adminService;
	
	@Autowired
	private ProjectService projectService;
	@Autowired
	private SearchAndReportService searchAndReportService;

	
	//==============PROHECT================================
	
	@GetMapping("/projects") 
	public String projectsGet(HttpServletRequest request, Model model,@RequestParam(value="projectStatus",required=false)String projectStatus,@RequestParam(value="projectcodes",required=false)String projectcodes) {
		User user = SecurityHelper.getCurrentUser(); 
		UserRole userRole = userService.getUserRoleByUserId(user.getUserId()); 
		if(projectStatus==null || "".equals(projectStatus)) {
			projectStatus="INPROGRESS";
		}
		if(projectcodes!=null && !"".equals(projectcodes)) {
			model.addAttribute("responseList", projectService.findAllProjectByProjectCodes(projectcodes)); 
		}else {
			model.addAttribute("responseList", projectService.findAllProjectByUserRoleId(userRole.getUserRoleId(),projectStatus)); 
		}
		
		model.addAttribute("userRole", userRole);
		model.addAttribute("projectStatus", projectStatus);
		model.addAttribute("statusList",MasterData.projectstatusList());
		return "site.projects";   
	} 
	@GetMapping("/project") 
	public String projectGet(HttpServletRequest request, Model model) {
		User user = SecurityHelper.getCurrentUser();
		Project project=new Project();
		project.setProjectCode(projectService.getNewProjectCode());		
		model.addAttribute("project",project); 
		model.addAttribute("currencyList",MasterData.currencyList()); 
		
		model.addAttribute("clientList",searchAndReportService.getNonAllocatedUser(user.getUserId(),"ACTIVE","c","")); 
		return "site.project";   
	}
	@PostMapping("/project") 
	public String projectPost(HttpServletRequest request, RedirectAttributes message,@ModelAttribute("project") Project project) {
		Project response = projectService.addOrUpdateProject(project);  
		if(response!=null)
		{
			message.addFlashAttribute("successMessage", "Project ("+response.getProjectName()+") is "+(project.getId()!=null?"Updated":"Saved")+" Successfully."); 
		
		}
		else {
			message.addFlashAttribute("failureMessage", "Unable To Save/Update"); 
		} 
		
		return "redirect:/projects";    
		
	} 	
	@PostMapping("/del-project") 
	public String deleteproject(HttpServletRequest request,RedirectAttributes message, Model model, @RequestParam("id_del")Long id_del) {	
		
		boolean flag =projectService.deleteProject(id_del); 
		if(flag==true)
		{
			message.addFlashAttribute("successMessage", "Poject is deleted Successfully."); 
		}
		else {
			message.addFlashAttribute("failureMessage", "Project already Allocated. Unable to delete Poject !"); 
		} 		
		return "redirect:/projects";         
	}
	@PostMapping("/edit-project") 
	public String editproject(HttpServletRequest request, Model model, @RequestParam("id_edit")Long id_edit) {	
		model.addAttribute("project", projectService.findProject(id_edit));  
		User user = SecurityHelper.getCurrentUser();
		model.addAttribute("currencyList",MasterData.currencyList()); 
		model.addAttribute("clientList",adminService.getAllUsers(user.getUserId(),"c","","","")); 
		return "site.project" ;  
		
	}  
	@PostMapping("/project-status-change") 
	public String projecttaggingstatust(HttpServletRequest request, RedirectAttributes message, @RequestParam("pstatus")String pstatus, @RequestParam("projectId")Long projectId, @RequestParam("usercode")String usercode) {
		Project response = projectService.changeProjectStatus(projectId,pstatus);
		if(response!=null)
		{
			message.addFlashAttribute("successMessage", "Project status is changed Successfully."); 
		
		}
		else {
			message.addFlashAttribute("failureMessage", "Unable To change status"); 
		} 
		
		return "redirect:/projects/"+usercode.trim()+"";    
		
	}
		
	@GetMapping("/project-allocations") 
	public String projectallocationList(HttpServletRequest request, Model model,@RequestParam(value="projectAllocationStatus",required=false)String projectAllocationStatus) {
		if(projectAllocationStatus==null || "".equals(projectAllocationStatus)) {
			projectAllocationStatus="INPROGRESS";
		}
		
		User user = SecurityHelper.getCurrentUser(); 
		UserRole userRole = userService.getUserRoleByUserId(user.getUserId()); 
		model.addAttribute("responseList", projectService.findAllProjectAllocationByCreatedBy(user.getUserId(),projectAllocationStatus)); 
		model.addAttribute("userRole", userRole);
		model.addAttribute("projectAllocationStatus", projectAllocationStatus);
		model.addAttribute("statusList",MasterData.projectAllocationstatusList());
		model.addAttribute("USD_TO_INR", MasterData.USD_TO_INR);
		model.addAttribute("CAD_TO_INR", MasterData.CAD_TO_INR);
		
		return "site.projectallocations";   
	} 
	
	
	@GetMapping("/project-allocation") 
	public String projectallocation(HttpServletRequest request, Model model) {
		User user = SecurityHelper.getCurrentUser(); 
		UserRole userRole = userService.getUserRoleByUserId(user.getUserId()); 
		List<Project> projectList=projectService.findAllProjectByUserRoleId(userRole.getUserRoleId());
		model.addAttribute("projectList",projectList.stream().filter(p->p.getStatus().equals("ACTIVE")).filter(p->!p.getProjectStatus().equals("CANCELLED")).collect(Collectors.toList()) ); 
		//List<UserRole> userRoleList =adminService.findAllUserRoleByUserAndRole(userRole.getUserId().getUserId(),"FREELANCER"); 
		
		List<UserInfo> userInfoList=adminService.getAllUsers(userRole.getUserId().getUserId(),"f","ACTIVE","","");
		model.addAttribute("currencyList",MasterData.currencyList()); 
		model.addAttribute("userInfoList", userInfoList); 	
		model.addAttribute("USD_TO_INR", MasterData.USD_TO_INR);
		model.addAttribute("CAD_TO_INR", MasterData.CAD_TO_INR);
		model.addAttribute("paymentFrequencyList",MasterData.paymentFrequencyList()); 
		return "site.projectallocation";   
	}
	
	@PostMapping("/project-allocation") 
	public String projectallocation(HttpServletRequest request, RedirectAttributes message,@ModelAttribute("projectAllocation") ProjectAllocation projectAllocation) {
		ProjectAllocation response =projectService.addOrUpdateProjectAllocation(projectAllocation);  
		if(response!=null)
		{
			message.addFlashAttribute("successMessage", "Project Allocated Successfully."); 
		
		}
		else {
			message.addFlashAttribute("failureMessage", "Unable To Save/Update"); 
		} 
		
		return "redirect:/projects";    
		
	} 	
	 
	@GetMapping("/project-allocations/{projectcodeInternal}") 
	public String projectallocations(HttpServletRequest request, Model model,@PathVariable("projectcodeInternal")String projectcodeInternal) {	
		model.addAttribute("responseList", projectService.findAllProjectAllocationsByProjectCode(projectcodeInternal)); 
		model.addAttribute("projectcode",projectcodeInternal);	
		User user = SecurityHelper.getCurrentUser(); 
		UserRole userRole = userService.getUserRoleByUserId(user.getUserId()); 
		model.addAttribute("userRole", userRole);
		model.addAttribute("statusList",MasterData.projectAllocationstatusList());
		model.addAttribute("USD_TO_INR", MasterData.USD_TO_INR);
		model.addAttribute("CAD_TO_INR", MasterData.CAD_TO_INR);
		return "site.projectallocations";   
	} 
	
}
	