package com.devfriendsgroup.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.devfriendsgroup.model.PaymentResponseSearchDTO;
import com.devfriendsgroup.model.ProjectPayment;
import com.devfriendsgroup.model.SearchRequest;
import com.devfriendsgroup.model.SearchResponse;
import com.devfriendsgroup.service.AdminService;
import com.devfriendsgroup.service.SearchAndReportService;
import com.devfriendsgroup.util.MasterData;
@Controller
public class SearchAndReportController {

	@Autowired
	private SearchAndReportService searchAndReportService;

	@Autowired
	private AdminService adminService;

	@PostMapping("/search")
	public String searchpost(Model model, @ModelAttribute("searchRequest") SearchRequest searchRequest) {
		model.addAttribute("searchResponse",
				loadSearchPreeData(searchAndReportService.getSearchDetails(searchRequest)));
		model.addAttribute("searchRequest", searchRequest);
		return "site.search";

	}

	@GetMapping("/search")
	public String searchget(Model model) {
		model.addAttribute("searchResponse", loadSearchPreeData(new SearchResponse()));
		return "site.search";

	}

	public SearchResponse loadSearchPreeData(SearchResponse searchResponse) {
		searchResponse.setSkillList(adminService.findAllSkills());
		searchResponse.setPaymentStatusList(MasterData.paymentStatusList());
		searchResponse.setUSD_TO_INR(MasterData.USD_TO_INR);
		searchResponse.setUserstatusList(MasterData.userStatusList());
		searchResponse.setPaymentTypeList(MasterData.paymentTypeList());
		searchResponse.setProjectStatusList(MasterData.projectstatusList());

		
		if (searchResponse.getPaymentsList() != null && searchResponse.getPaymentsList().size() > 0) {
			
			List<PaymentResponseSearchDTO> paymentResponseSearchDTOs = new ArrayList<>();

			List<String> names = searchResponse.getPaymentsList().stream().map(p -> p.getFreelancerName()).distinct()
					.collect(Collectors.toList());
			/*
			 * public String name; public String clientTotal; public String clientPaid;
			 * public String clientPending; public String freelancerTotal; public String
			 * freelancerPaid; public String freelancerPending; public String totalProfit;
			 */
			Double clientTotalSum = 0D;
			Double clientPaidSum = 0D;
			Double clientPendingSum = 0D;

			Double freelancerTotalSum = 0D;
			Double freelancerPaidSum = 0D;
			Double freelancerPendingSum = 0D;
			Double totalProfitSum = 0D;
			
			
			for (String name : names) {
				Double clientTotal = 0D;
				Double clientPaid = 0D;
				Double clientPending = 0D;

				Double freelancerTotal = 0D;
				Double freelancerPaid = 0D;
				Double freelancerPending = 0D;
				PaymentResponseSearchDTO dto = new PaymentResponseSearchDTO();
				for (ProjectPayment p : searchResponse.getPaymentsList()) {
					if (name.equalsIgnoreCase(p.getFreelancerName()) && p.getPaymentfor().equals("CREDIT")) {
						if (p.getPaymentStatus().equals("PAID")) {
							clientPaid = clientPaid + p.getPaymentAmount();
						}
						if (p.getPaymentStatus().equals("PENDING")) {
							clientPending = clientPending + p.getPaymentAmount();

						}
						clientTotal = clientTotal + p.getPaymentAmount();
					} else if (name.equalsIgnoreCase(p.getFreelancerName()) && p.getPaymentfor().equals("DEBIT")) {
						if (p.getPaymentStatus().equals("PAID")) {
							freelancerPaid = freelancerPaid + p.getPaymentAmount();
						} else if (p.getPaymentStatus().equals("PENDING")) {
							freelancerPending = freelancerPending + p.getPaymentAmount();

						}
						freelancerTotal = freelancerTotal + p.getPaymentAmount();
					}

				}

				dto.setName(name);
				dto.setClientTotal(clientTotal);
				dto.setClientPaid(clientPaid);
				dto.setClientPending(clientPending);
				dto.setFreelancerTotal(freelancerTotal);
				dto.setFreelancerPaid(freelancerPaid);
				dto.setFreelancerPending(freelancerPending);
				dto.setTotalProfit(clientTotal - freelancerTotal);
				paymentResponseSearchDTOs.add(dto);
				
				
				 clientTotalSum = clientTotalSum+dto.getClientTotal();
				 clientPaidSum =clientPaidSum+dto.getClientPaid();
				 clientPendingSum = clientPendingSum+dto.getClientPending();

				 freelancerTotalSum =freelancerTotalSum+dto.getFreelancerTotal();
				 freelancerPaidSum = freelancerPaidSum+dto.getFreelancerPaid();
				 freelancerPendingSum = freelancerPendingSum+dto.getFreelancerPending();
				 totalProfitSum =totalProfitSum+dto.getTotalProfit();
				
				
			}
			PaymentResponseSearchDTO dto = new PaymentResponseSearchDTO();
			dto.setName("TOTAL");
			dto.setClientTotal(clientTotalSum);
			dto.setClientPaid(clientPaidSum);
			dto.setClientPending(clientPendingSum);
			dto.setFreelancerTotal(freelancerTotalSum);
			dto.setFreelancerPaid(freelancerPaidSum);
			dto.setFreelancerPending(freelancerPendingSum);
			dto.setTotalProfit(totalProfitSum);
			paymentResponseSearchDTOs.add(dto);
			searchResponse.setPaymentResponseSearchDTOs(paymentResponseSearchDTOs);

			/*
			 * 
			 * for(ProjectPayment p:paymentsCrList) {
			 * if(p.getPaymentStatus().equals("PAID")) {
			 * if(paymentsListCrPaid.containsKey(p.getFreelancerName())) { Double
			 * value=paymentsListCrPaid.get(p.getFreelancerName())+p.getPaymentAmount();
			 * paymentsListCrPaid.put(p.getFreelancerName(), value);
			 * clientCr=clientCr+p.getPaymentAmount();
			 * paymentsListCrPaid.put(p.getFreelancerName(), value); }else {
			 * paymentsListCrPaid.put(p.getFreelancerName(), p.getPaymentAmount());
			 * clientCr=p.getPaymentAmount(); }
			 * 
			 * } if(p.getPaymentStatus().equals("PENDING")) {
			 * if(paymentsListCrPending.containsKey(p.getFreelancerName())) { Double
			 * value=paymentsListCrPending.get(p.getFreelancerName())+p.getPaymentAmount();
			 * paymentsListCrPending.put(p.getFreelancerName(), value);
			 * 
			 * clientDr=clientDr+p.getPaymentAmount(); }else {
			 * paymentsListCrPending.put(p.getFreelancerName(), p.getPaymentAmount());
			 * clientDr=p.getPaymentAmount(); }
			 * 
			 * }
			 * 
			 * };
			 * 
			 * for(ProjectPayment p:paymentsDrList) {
			 * if(p.getPaymentStatus().equals("PAID")) {
			 * if(paymentsListDrPaid.containsKey(p.getFreelancerName())) { Double
			 * value=paymentsListDrPaid.get(p.getFreelancerName())+p.getPaymentAmount();
			 * paymentsListDrPaid.put(p.getFreelancerName(), value); }else {
			 * paymentsListDrPaid.put(p.getFreelancerName(), p.getPaymentAmount()); }
			 * freelancerCr=freelancerCr+p.getPaymentAmount(); }
			 * if(p.getPaymentStatus().equals("PENDING")) {
			 * if(paymentsListDrPending.containsKey(p.getFreelancerName())) { Double
			 * value=paymentsListDrPending.get(p.getFreelancerName())+p.getPaymentAmount();
			 * paymentsListDrPending.put(p.getFreelancerName(), value); }else {
			 * paymentsListDrPending.put(p.getFreelancerName(), p.getPaymentAmount()); }
			 * freelancerDr=freelancerDr+p.getPaymentAmount(); }
			 * 
			 * };
			 */
			
		}
		;

		return searchResponse;

	}

	@GetMapping(path = "/revenue-graph")
	public ResponseEntity<List<String>> revenuegraph(@RequestParam("data") String data) throws Exception {
		return ResponseEntity.ok(searchAndReportService.revenuegraph(data));
	}

}
