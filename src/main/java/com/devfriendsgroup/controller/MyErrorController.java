package com.devfriendsgroup.controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MyErrorController implements ErrorController  {

	
    @RequestMapping("/error")
    public String handleError(HttpServletRequest request,Model model,@RequestParam(value="code",required=false)String code) {
    	
    	if("PERMISSIONERROR".equals(code))
    	{
    		model.addAttribute("error", "You Are Not Authorize");
    		 return "site.internalerror";
    	}
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        if (status != null) {
            Integer statusCode = Integer.valueOf(status.toString());
            if(statusCode == HttpStatus.NOT_FOUND.value()) {
            	model.addAttribute("error", "Page Not Found");
            	// return "error-400";
            }
            else if(statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
               // return "error-500";
            	model.addAttribute("error", "Internal Server Error");
            }
            return "site.internalerror";
        }
        return "site.internalerror";
    }
}