package com.devfriendsgroup.controller;



import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.devfriendsgroup.model.JKFreelanceUserInfo;
import com.devfriendsgroup.website.repository.WebsiteUserInfoRepository;

@Controller
public class JKFreelanceController {
	@Value("${JK_FREELANCE_URL}")
	 String JKFREELANCEURL;
	@Autowired
	private WebsiteUserInfoRepository userInfoRepository; 
	
	
		@GetMapping("/jkfreelancers") 
		public String jkfreelancersGet(HttpServletRequest request, Model model,JKFreelanceUserInfo userInfo) {
			model.addAttribute("userList", userInfoRepository.findAll());
			model.addAttribute("JKFREELANCEURL", JKFREELANCEURL);
			return "site.jkfreelancers";   

		}

		@PostMapping("/edit-jkfreelancer") 
		public String editjkfreelancer(@RequestParam("userId")String userId,Model model) {
			model.addAttribute("userInfo", userInfoRepository.findUserInfoByUserId(Long.parseLong(userId)));
			return "site.jkfreelancer";   

		} 
		@GetMapping("/user-view")  
		public String websiteuserview(Model model,@RequestParam("username")String username) {	
			model.addAttribute("userInfo", userInfoRepository.findUserInfosByUsername(username)); 	
			return "site.userview";   

		}  
		
		
		
	
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
				
		/*
		private List<JKFreelanceUserInfo> getUserInfoList(JKFreelanceUserInfo userInfo) {
			List<JKFreelanceUserInfo> userList=null;
			 // Create a new RestTemplate instance
	        RestTemplate restTemplate = new RestTemplate();

	        // Create HTTP headers
	        HttpHeaders headers = new HttpHeaders();
	        headers.set("Custom-Header", "HeaderValue"); // Example of adding a custom header
	        headers.setContentType(MediaType.APPLICATION_JSON);
	        // Create an HttpEntity with just headers (no body)
	        HttpEntity<JKFreelanceUserInfo> requestEntity = new HttpEntity<>(userInfo,headers);

	        // Make the POST request and get the response entity
	        ResponseEntity<JKFreelanceUserInfo[]> responseEntity = restTemplate.postForEntity(JKFREELANCEURL+"/public/user/list", requestEntity, JKFreelanceUserInfo[].class);

	        // Check the HTTP status code and handle the response accordingly
	        if (responseEntity.getStatusCode() == HttpStatus.OK) {
	        	// Convert the response body from an array to a List
	            JKFreelanceUserInfo[] userArray = responseEntity.getBody();
	             userList = Arrays.asList(userArray);
	            // Print the response body
	            System.out.println("Response: " + responseEntity.getBody());
	        } else {
	            // Print the status code if request failed
	            System.out.println("Request failed with status code: " + responseEntity.getStatusCode());
	        }
			return userList;
		} 
		
		
		
		@PostMapping("/jkfreelancer") 
		public String jkfreelancerPost(HttpServletRequest request,RedirectAttributes message, Model model,@ModelAttribute("userInfo") JKFreelanceUserInfo userInfo) {
			System.out.println("jkfreelancerPost===="+userInfo.getId());
			System.out.println("jkfreelancerPost===="+userInfo.getStatus());
			
			userInfo.setStatusNew(userInfo.getStatus())	;
			// Create a new RestTemplate instance
	        RestTemplate restTemplate = new RestTemplate();


	        // Create HTTP headers
	        HttpHeaders headers = new HttpHeaders();
	        headers.set("Custom-Header", "HeaderValue"); // Example of adding a custom header
	        headers.setContentType(MediaType.APPLICATION_JSON);
	        // Create an HttpEntity with just headers (no body)
	        HttpEntity<JKFreelanceUserInfo> requestEntity = new HttpEntity<>(userInfo,headers);

	        // Make the POST request and get the response entity
	        ResponseEntity<String> responseEntity = restTemplate.postForEntity(JKFREELANCEURL+"/public/user/update", requestEntity, String.class);
	       
	        // Check the HTTP status code and handle the response accordingly
	        if (responseEntity.getStatusCode() == HttpStatus.OK) {
	     		message.addFlashAttribute("successMessage", responseEntity.getBody()); 
	        } else {
	            // Print the status code if request failed
	        	message.addFlashAttribute("failureMessage", responseEntity.getBody()); 
	        }
			
			
			return "redirect:/jkfreelancers";   

		} 
		
		/*
		 * @PostMapping("/user-reg") public String userrequestspost(HttpServletRequest
		 * request,RedirectAttributes model,@ModelAttribute("userRequest") UserRequest
		 * userRequest) { System.out.println("userRequest============="+userRequest);
		 * UserRequest userRequest_ = adminService.saveUserRequest(userRequest);
		 * if(userRequest_!=null) { model.addFlashAttribute("sms", "success");
		 * 
		 * } else { model.addFlashAttribute("sms", "fail"); }
		 * 
		 * return "redirect:/user-reg";
		 * 
		 * }
		 */

		
     
   
} 
	