package com.devfriendsgroup.controller;


import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.devfriendsgroup.model.PaymentstatuschangeDTP;
import com.devfriendsgroup.model.ProjectPayment;
import com.devfriendsgroup.service.AdminService;
import com.devfriendsgroup.service.PaymentService;
import com.devfriendsgroup.service.ProjectService;
import com.devfriendsgroup.util.DateUtil;
import com.devfriendsgroup.util.MasterData;
import com.devfriendsgroup.util.SecurityHelper;
import com.fasterxml.jackson.core.JsonProcessingException;


import java.util.*;
@Controller
public class PaymentController {
	
	
	@Autowired
	private PaymentService paymentService;
	@Autowired
	private ProjectService projectService;
	
	@GetMapping("/payments/{projectcode}") 
	public String listprojectspayment(HttpServletRequest request, Model model,@PathVariable("projectcode")String projectcode,@RequestParam(value="paystatus",required=false)String paystatus) {	
		
		if(paystatus!=null && !"".equals(paystatus)) {
			model.addAttribute("responseList", paymentService.findAllProjectPaymentsByProjectCodeAndStatus(projectcode,paystatus)); 
			
		}
		else
		{
			model.addAttribute("responseList", paymentService.findAllProjectPaymentsByProjectCode(projectcode)); 
		}
		
				
		model.addAttribute("project",projectService.findByProjectCodeInternal(projectcode));	
		model.addAttribute("currencyList",MasterData.currencyList()); 
		model.addAttribute("USD_TO_INR", MasterData.USD_TO_INR);
		model.addAttribute("paymentStatusList",MasterData.paymentStatusList()); 
		model.addAttribute("projectcode", projectcode);
		model.addAttribute("paystatus", paystatus);
		model.addAttribute("paymentTypeList", MasterData.paymentTypeList());
		model.addAttribute("currentMonth", DateUtil.getMonth(new Date()));
		return "site.payments";   
	} 

	@GetMapping("/payments") 
	public String projectsGet(HttpServletRequest request, Model model,@RequestParam(value="paystatus",required=false)String paystatus,@RequestParam(value="pcode",required=false)String pcode) {
		if(paystatus==null || "".equals(paystatus)) {
			paystatus="INPROGRESS";
		}
		else {
			model.addAttribute("paystatus", paystatus);
			model.addAttribute("search", "ACTIVE");
		}
		if(pcode!=null && !"".equals(pcode)) {
			return "redirect:/payments/"+pcode+"?paystatus="+paystatus;  
			
		}
		else {
			model.addAttribute("responseList", paymentService.findAllProjectPaymentsBySearch(SecurityHelper.getCurrentUser().getUserId(),paystatus)); 
				
		}
		
		model.addAttribute("projectcode", pcode);
		model.addAttribute("paystatus", paystatus);
		model.addAttribute("paymentStatusList",MasterData.paymentStatusList()); 
		model.addAttribute("USD_TO_INR", MasterData.USD_TO_INR);
		model.addAttribute("currentMonth", DateUtil.getMonth(new Date()));
		return "site.payments";   
	} 
	
	@GetMapping("/search-payments") 
	public String searchpayments(HttpServletRequest request, Model model) {
		model.addAttribute("responseList", paymentService.findAllProjectPaymentsBySearch(SecurityHelper.getCurrentUser().getUserId(),"PENDING"));
		model.addAttribute("paymentStatusList",MasterData.paymentStatusList()); 
		model.addAttribute("USD_TO_INR", MasterData.USD_TO_INR);
		model.addAttribute("currentMonth", DateUtil.getMonth(new Date()));
		return "site.payments";   
	} 
	@PostMapping("/project-payment") 
	public String projectpayment(HttpServletRequest request, RedirectAttributes message,@ModelAttribute("projectPayment") ProjectPayment projectPayment) {
		
		
		ProjectPayment response = paymentService.addOrUpdateProjectPayment(projectPayment);  
		if(response!=null)
		{
			message.addFlashAttribute("successMessage", "Project Payment is "+(projectPayment.getId()!=null?"Updated":"Saved")+" Successfully."); 		
		}
		else {
			message.addFlashAttribute("failureMessage", "Unable To Save/Update"); 
		} 		
		return "redirect:/payments/"+response.getProjectAllocation().getProject().getProjectCodeInternal()+"";   

	} 	
	@PostMapping("/del-project-payment") 
	public String delprojectpayment(HttpServletRequest request,RedirectAttributes message, Model model, @RequestParam("id_del")Long id_del) {
		
		paymentService.deleteProjectPayment(id_del); 
		message.addFlashAttribute("successMessage", "Projec tPayment Deleted Successfully."); 
		return "redirect:/list-projects-payment";         
	}
	@PostMapping("/edit-project-payment") 
	public String editprojectpayment(HttpServletRequest request, Model model, @RequestParam("id_edit")Long id_edit) {	
		return "site.projectPayment" ;  
		
	} 
	@PostMapping("/payment-status-change") 
	public String paymentstatuschange(@ModelAttribute("paymentstatuschangeDTP")PaymentstatuschangeDTP paymentstatuschangeDTP,RedirectAttributes message){
		ProjectPayment response = paymentService.updatePaymentStatus(paymentstatuschangeDTP);
		if(response!=null)
		{
			message.addFlashAttribute("successMessage", "Payment status is changed Successfully."); 		
		}
		else {
			message.addFlashAttribute("failureMessage", "Unable To change status"); 
		} 		
		return "redirect:/payments/"+paymentstatuschangeDTP.getProjectcodeStatusChange()+"";   
		
	}
	public String paymentstatuschange1(@ModelAttribute("paymentstatuschangeDTP")PaymentstatuschangeDTP paymentstatuschangeDTP,RedirectAttributes message){
		ProjectPayment response = paymentService.updatePaymentStatus(paymentstatuschangeDTP);
		if(response!=null)
		{
			message.addFlashAttribute("successMessage", "Payment status is changed Successfully."); 		
		}
		else {
			message.addFlashAttribute("failureMessage", "Unable To change status"); 
		} 		
		return "redirect:/payments/"+paymentstatuschangeDTP.getProjectcodeStatusChange()+"";   
		
	}

	/*
	 * @GetMapping("/payment-calender") public String
	 * paymentcalender(HttpServletRequest request, Model model) throws
	 * JsonProcessingException {
	 * model.addAttribute("payResList",projectService.getPaymentsCalenders(
	 * SecurityHelper.getCurrentUser().getUserId())); return "site.paymentcalender";
	 * }
	 */
	@GetMapping("/load-payment-calender")
    public ResponseEntity<?> getSearchResultViaAjax(
             @RequestBody Map<String, String> searchInput) {
        return ResponseEntity.ok("[{title: 'Birthday Party',start: new Date(y, m, d, 19, 0),end: new Date(y, m, d, 22, 30),allDay: false,url: 'http://google.com/'}]");

    }
	@GetMapping("/pay-calender") 
	public String paycalender(HttpServletRequest request, Model model) throws JsonProcessingException {		
		model.addAttribute("payResList",projectService.getPayCalenders(SecurityHelper.getCurrentUser().getUserId())); 
		return "site.paycalender";   
	} 
	@PostMapping("/del-payment") 
	public String paymentdel(HttpServletRequest request, Model model,RedirectAttributes message, @RequestParam("id_del")Long id_del, @RequestParam("pcodedel")String projectCodeInternaldel) {	

		paymentService.deletePayment(id_del);   
		message.addFlashAttribute("successMessage", "Payment is deleted Successfully."); 
		return "redirect:/payments/"+projectCodeInternaldel+"";   
	}
	
}
	