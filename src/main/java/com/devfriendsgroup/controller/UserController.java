package com.devfriendsgroup.controller;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.devfriendsgroup.model.Applyclient;
import com.devfriendsgroup.model.Contractors;
import com.devfriendsgroup.model.Role;
import com.devfriendsgroup.model.User;
import com.devfriendsgroup.model.UserInfo;
import com.devfriendsgroup.model.UserRequest;
import com.devfriendsgroup.model.UserRole;
import com.devfriendsgroup.service.AdminService;
import com.devfriendsgroup.service.UserDetailsServiceImpl;
import com.devfriendsgroup.service.UserService;
import com.devfriendsgroup.util.CodeGenerator;
import com.devfriendsgroup.util.MasterData;
import com.devfriendsgroup.util.SecurityHelper;
import com.devfriendsgroup.util.SendSmsUtil;

@Controller
public class UserController {
	
	@Autowired
	private UserService userService; 
	
	@Autowired
	private AdminService adminService; 
	
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder; 
	@Autowired
	private UserDetailsServiceImpl userDetailsServiceImpl;
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String dashBoardPage(ModelMap model, HttpServletRequest request, HttpServletResponse response, String error,String invalid,
			String logout, RedirectAttributes message,HttpSession httpSession) {

		if (error != null) {
			model.addAttribute("error_msg", "Your username and password is invalid.");
			message.addFlashAttribute("loginFail", "Invalid User Credential");
			return "redirect:/";
		} else if (logout != null) {
			model.addAttribute("success_msg", "You have been logged out successfully.");
			message.addFlashAttribute("logout", "User Logged Out Successfully");
			return "redirect:/";
		} 
		else if (invalid != null) {
			model.addAttribute("error_msg", "Un Authorised User");
			message.addFlashAttribute("loginFail", "Invalid User");
			return "redirect:/";
		}
		else {  
			return userDetailsServiceImpl.portalRedirectUrl(httpSession,null);
		} 

	}
	
	@RequestMapping(value = "/pwd-change", method = RequestMethod.GET) 
    public String changePassword(ModelMap model, HttpServletRequest request, HttpServletResponse response)  
    { 	
		
		User loggedinUser = SecurityHelper.getCurrentUser();
		Long userId = loggedinUser.getUserId(); 
		model.addAttribute("userId", userId); 
		User user = adminService.getUserById(userId);   
		model.addAttribute("userPassword", user.getPassword());
	
		
        return "site.changePassword";       
    }  
	 
	@RequestMapping(value = "/pwd-change", method = RequestMethod.POST) 
    public String updatePassword(ModelMap model, HttpServletRequest request, HttpServletResponse response,
    		RedirectAttributes message, @RequestParam(value="userId", required=false)Long userId,
    		@RequestParam(value="oldPassword", required=false)String oldPassword, @RequestParam(value="newPassword", required=false)String newPassword)  
    { 
		
		User loggedinUser = SecurityHelper.getCurrentUser(); 
		String currentLoggedinUserPassword = loggedinUser.getPassword();
		if(bCryptPasswordEncoder.matches(oldPassword, currentLoggedinUserPassword)) {
			User user = userService.changePassword(userId, oldPassword, newPassword);  
			if(user != null) {
				message.addFlashAttribute("successMessage", "Password Changed Successfully."); 
			}
			else {
				message.addFlashAttribute("failureMessage", "Unable To Change Password."); 
			}
		}
		else {
			message.addFlashAttribute("failureMessage", "Unable To Match With Old Password"); 
			
		}
		
		return "redirect:/pwd-change"; 
    } 
    
    @RequestMapping(value = "/edit-profile", method = RequestMethod.GET) 
    public String editProfile(ModelMap model, HttpServletRequest request, HttpServletResponse response)  
    { 	
    	User loggedinUser = SecurityHelper.getCurrentUser();
		Long userId = loggedinUser.getUserId();
    	UserInfo userInfo = userService.geMyProfile(userId);   
    		userInfo.setUserRoleTest(userService.getUserRoleByUserId(userId));
		
		model.addAttribute("userInfo", userInfo);  

		
        return "site.editProfile";       
    }  
    
    @RequestMapping(value = "/edit-profile", method = RequestMethod.POST) 
    public String editProfile(RedirectAttributes message,@ModelAttribute("userInfo") UserInfo userInfo)  
    { 
    	UserInfo userInfoRespo= userService.editProfile(userInfo);   
		if(userInfoRespo != null) {
			message.addFlashAttribute("successMessage", "Profile Updated Successfully."); 
		}
		else {
			message.addFlashAttribute("failureMessage", "Unable To Update Profile."); 
		}
		
		return "redirect:/edit-profile"; 
    }
    
    @GetMapping("/forgot-pass") 
	public String forgotPassword() {	
		
    	return "site.forgotpass";   
	} 
    
    @PostMapping("/forgot-pass") 
	public String forgotPasswordPost(@RequestParam("unameOrmob") String unameOrmob, 			
			RedirectAttributes message) {	
		
    	User existingUser = userService.getUserByUserNameOrMobileNumber(unameOrmob); 
    	if(existingUser == null) {
    		message.addFlashAttribute("failureMessage", "Invalid User Name or Email !"); 
    		
    	}
    	else {
    	
    		User user = userService.changePwd(existingUser.getUserId(),CodeGenerator.otpGenertor());  
    		if(user != null) {
    			message.addFlashAttribute("successMessage", "Your reset password form has been sent to your email !"); 
    		} 
    		else {
    			message.addFlashAttribute("failureMessage", "Unable to send Recover password !");  
    		}  
    		
    		
    	}
    	return "redirect:/forgot-pass"; 
	} 
    @GetMapping("/my-profile") 
   	public String myProfile(HttpServletRequest request, Model model) {	
    	User loggedinUser = SecurityHelper.getCurrentUser();
		Long userId = loggedinUser.getUserId(); 
    	model.addAttribute("userDTO", userService.getUserDTOById(userId)); 
   		return "site.myprofile";   
   	}
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    //====================FUTURE USE==================================
    
    @PostMapping("/forgot-pass11") 
   	public String forgotPasswordPost1(@RequestParam("unameOrmob") String unameOrmob, 			
   			RedirectAttributes message) {	
   		
       	User existingUser = userService.getUserByUserNameOrMobileNumber(unameOrmob); 
       	if(existingUser == null) {
       		message.addFlashAttribute("noDataFound", "This User Name or Mobile Number is not associated with any User."); 
       		return "redirect:/forgot-pass"; 
       	}
       	else {
       		String mobNumber = existingUser.getMobileNo(); 
       		String otp = CodeGenerator.otpGenertor(); 
       		String msg = "Hi "+existingUser.getName()+", Use "+otp+" as One Time Password for changing your sFreelance account's password.";     		
       		SendSmsUtil.sendTransactionalMessage(mobNumber, msg); 
       		User user = userService.saveOtpForForgotPassword(existingUser, otp);  
       		if(user != null) {
       			message.addFlashAttribute("successOtpMsg", "OTP has been sent successfully to the registered mobile number."); 
       		} 
       		else {
       			message.addFlashAttribute("failureOtpMsg", "Unable to send OTP.");  
       		}  
       		
           	return "redirect:/submitOtp.htm?userId="+existingUser.getUserId();   
       	}
       	 
   	} 
    
    @GetMapping("/resen535353dOtp.htm") 
	public String resendOtp(HttpServletRequest request, Model model, 
			@RequestParam(value="userId", required=false)Long userId, RedirectAttributes message) {	
		
    	User existingUser = userService.getUserById(userId); 
    	
    	String mobNumber = existingUser.getMobileNo(); 
		String otp = CodeGenerator.otpGenertor(); 
		String msg = "Hi "+existingUser.getName()+", Use "+otp+" as One Time Password for changing your sFreelance account's password.";     		
		SendSmsUtil.sendTransactionalMessage(mobNumber, msg); 
		User user = userService.saveOtpForForgotPassword(existingUser, otp);  
		if(user != null) {
			message.addFlashAttribute("successOtpMsg", "OTP has been sent successfully to the registered mobile number."); 
		} 
		else {
			message.addFlashAttribute("failureOtpMsg", "Unable to send OTP.");  
		} 
    	
		return "redirect:/submitOtp.htm?userId="+existingUser.getUserId();      
	} 
    
    @GetMapping("/submitOtp.htm") 
	public String submitOtp(HttpServletRequest request, Model model,
			@RequestParam(value="userId", required=false)Long userId) {	
		
    	User existingUser = userService.getUserById(userId); 
    	model.addAttribute("user", existingUser); 
    	
    	return "site.submitOtp";   
	}  
    
    @PostMapping("/submitOtpPost353535.htm") 
	public String submitOtpPost(HttpServletRequest request, Model model,
			@RequestParam(value="otp", required=false)String otp, 
			@RequestParam(value="userId", required=false)Long userId,
			RedirectAttributes message) {	
		
    	User existingUser = userService.getUserById(userId); 
    	
    	User user = userService.matchOtp(otp);   
    	if(user != null) {
    		return "redirect:/changePasswordForForgotCase.htm?userId="+user.getUserId();   
    	} 
		else {
			message.addFlashAttribute("failureOtpSubmissionMsg", "Please enter valid OTP."); 
			return "redirect:/submitOtp.htm?userId="+existingUser.getUserId();  
		} 
	}  
	
    @GetMapping("/changePasswordForForgotCase.htm") 
	public String changePasswordForForgotCase(HttpServletRequest request, Model model,
			@RequestParam(value="userId", required=false)Long userId) {	
		
    	User existingUser = userService.getUserById(userId); 
    	
    	model.addAttribute("user", existingUser);  
    	
    	return "site.changePasswordForForgotCase";   
	}  
    
    @PostMapping("/changePasswordForForgotCase.htm") 
	public String changePasswordForForgotCasePost(HttpServletRequest request, Model model,
			@RequestParam(value="password", required=false)String password,
			@RequestParam(value="userId", required=false)Long userId,
			RedirectAttributes message) {	
		
    	User existingUser = userService.getUserById(userId); 
    	
    	User finalUser = userService.changePasswordForForgotCasePost(existingUser, password);  
    	if(finalUser != null) { 
    		message.addFlashAttribute("successPwdChangeMsg", "Password has been changed successfully.");  
    	} 
		else {
			message.addFlashAttribute("failurePwdChangeMsg", "Unable to change password."); 
		} 
    	
    	return "redirect:/userLogin.htm";   
	}   
  //====================FUTURE USE==================================
    @PostMapping("/switch-user") 
  	public String changePasswordForForgotCasePost(HttpSession httpSession, Model model,
  			@RequestParam(value="switchuser", required=false)String switchuser,  		
  			RedirectAttributes message) {	
  		
    	return userDetailsServiceImpl.portalRedirectUrl(httpSession,switchuser);
      	
  	} 
    @GetMapping("/user/{rtype}") 
	public String createUser(HttpServletRequest request, Model model,@PathVariable("rtype")String rtype) {	

		loadDataUser(model,rtype);
		return "site.user";   

	} 	

	public void loadDataUser( Model model,String rtype) {	

		User user = SecurityHelper.getCurrentUser(); 
		UserRole userRole = userService.getUserRoleByUserId(user.getUserId());  

		List<Role> roleList = adminService.findAllActiveRole(); 
		List<Role> finalRoleList = new ArrayList<Role>(); 
		if(roleList != null && roleList.size() > 0) {
			if(userRole.getRoleId().getRoleName().equals("ADMIN")) {
				if(rtype!=null & !rtype.equals("") & "cst".equals(rtype)) {
					finalRoleList.add(adminService.findActiveRoleByRoleCode("CONSULTANCY")); 
				}
					
			}			
			else if(userRole.getRoleId().getRoleName().equals("CONSULTANCY")) {

				if(rtype!=null & !rtype.equals("") & "c".equals(rtype)) {
					finalRoleList.add(adminService.findActiveRoleByRoleCode("CLIENT")); 
				}else if(rtype!=null & !rtype.equals("") & "f".equals(rtype)) {
					finalRoleList.add(adminService.findActiveRoleByRoleCode("FREELANCER")); 
				}


			}
		}
		model.addAttribute("userRole", userRole);	
		model.addAttribute("roleList", finalRoleList);
		model.addAttribute("rtype", rtype);	
		model.addAttribute("currencyList",MasterData.currencyList()); 
		model.addAttribute("skillList", adminService.findAllSkills()); 	
		model.addAttribute("contractorList",adminService.findAllByUserIdAndStatus(user.getUserId(),"ACTIVE"));
		
		model.addAttribute("bankList",adminService.findAllBankByStatus("ACTIVE"));
		model.addAttribute("countryCodeList",MasterData.countryCodeList()); 
		model.addAttribute("professionList", MasterData.professionList());
		

	} 




	@PostMapping("/user") 
	public String addUserInfo(HttpServletRequest request, Model model,@ModelAttribute("userInfo") UserInfo userInfo, RedirectAttributes message) {	
		UserInfo userInfoRes = adminService.addUserInfo(userInfo);   
		if(userInfoRes != null) {
			message.addFlashAttribute("successMessage", "User Information ("+userInfoRes.getUser().getName()+") is "+(userInfo.getId()!=null?"Updated":"Saved")+" Successfully."); 
		}
		else {
			message.addFlashAttribute("failureMessage", "Unable To Save User."); 
		}
		
		return "redirect:/users/"+userInfo.getRtype()+"";
	}

	@PostMapping("/edit-user") 
	public String editUser(HttpServletRequest request, Model model, @RequestParam("id_edit")Long id_edit,@RequestParam("rtype")String rtype) {	
		UserInfo userInfo = adminService.findUserInfoById(id_edit);
		loadDataUser(model,rtype);		
		model.addAttribute("currencyList",MasterData.currencyList()); 
		
		//skills
		String skills="";
		String sk=userInfo.getSkills();
		if(sk!=null && !"".equals(sk))
		{
			String[] array=sk.split(",");
			for (int i = 0; i < array.length; i++) {
				skills="'"+array[i]+"',"+skills;
				
			}
		}
		
		userInfo.setSkills("["+skills+"]");

		model.addAttribute("userInfo", userInfo);  
		UserRole userRole = userService.getUserRoleByUserId(userInfo.getUser().getUserId()); 
		model.addAttribute("userRole", userRole); 
		model.addAttribute("skillList", adminService.findAllActiveSkills()); 	
		model.addAttribute("bankList",adminService.findAllBankByStatus("ACTIVE"));
		model.addAttribute("countryCodeList",MasterData.countryCodeList()); 
		return "site.user";   

	}  


	@RequestMapping(value = "/validate-user-name.htm", method = RequestMethod.GET)
	@ResponseBody 
	public String  validateUserName(@RequestParam("userName") String userName, ModelMap model) throws JSONException
	{	

		JSONArray jarr = new JSONArray();
		JSONObject jobj = new JSONObject();
		User user = adminService.getUserByUserName(userName);  
		if(user != null) {
			jobj.put("duplicateUserName", "User Having This Name Is Already Exist"); 
		}
		jarr.put(jobj);
		return jarr.toString();

	}   



	@GetMapping("/users/{rtype}")  
	public String manageUser(@RequestParam(value="status",required = false) String status,HttpServletRequest request, Model model,HttpSession httpSession,@PathVariable("rtype")String rtype) {	
		if(status==null || "".equals(status)){
			status="ACTIVE";
	    }
		User user = SecurityHelper.getCurrentUser(); 
		UserRole userRole = userService.getUserRoleByUserId(user.getUserId());  	
		List<UserInfo> userInfoList = new ArrayList<UserInfo>();
		
	    if(userRole.getRoleId().getRoleName().equals("ADMIN")) {
			userInfoList = adminService.getAllUsersByUserIdAndRole(user.getUserId(),"CONSULTANCY"); 
		}
		else if(userRole.getRoleId().getRoleName().equals("CONSULTANCY")) {		
			userInfoList = adminService.getAllUsers(user.getUserId(),rtype,"","",status); 
		}
	    
	    
	    
	    
	    
		model.addAttribute("userInfoList", userInfoList); 	
		model.addAttribute("userRole", userRole);
		
		model.addAttribute("status", status);
		
		return "site.users";   

	}  

	@GetMapping("/enableUser.htm") 
	public String enableUser(HttpServletRequest request, Model model,@RequestParam("rtype")String rtype, @RequestParam("userIdForEnableUser")Long userIdForEnableUser, RedirectAttributes message) {	

		User user = adminService.enableUser(userIdForEnableUser);  
		message.addFlashAttribute("rtype", rtype);
		if(user!=null)
		{
			message.addFlashAttribute("successMessage", "User ("+user.getName()+") is Activated Successfully."); 
		}
		else {
			message.addFlashAttribute("failureMessage", "Unable proceed"); 
		} 
		
		
		return "redirect:/users/"+rtype+"";      
	} 

	@GetMapping("/disableUser.htm") 
	public String disableUser(HttpServletRequest request,@RequestParam("rtype")String rtype, Model model, @RequestParam("userIdForDisableUser")Long userIdForDisableUser, RedirectAttributes message) {	

		User user = adminService.disableUser(userIdForDisableUser);  
		if(user!=null)
		{
			message.addFlashAttribute("successMessage", "User ("+user.getName()+") is De-Activated Successfully."); 
		}
		else {
			message.addFlashAttribute("failureMessage", "Unable proceed"); 
		} 
		

		message.addAttribute("rtype", rtype);
		return "redirect:/users/"+rtype+"";     
	} 
	
	//============================
		@GetMapping("/contractor") 
		public String createscontractor(HttpServletRequest request, Model model) {			
			return "site.contractor";   

		} 

		@PostMapping("/contractor") 
		public String addcontractor(HttpServletRequest request, RedirectAttributes message,@ModelAttribute("contractor") Contractors contractor) {

			Contractors contractor_ = adminService.addOrUpdateContractors(contractor);
			if(contractor_!=null)
			{
				message.addFlashAttribute("successMessage", "Name: ("+contractor.getName()+") is "+(contractor.getContractorId()!=null?"Updated":"Updated")+" Successfully."); 

			}
			else {
				message.addFlashAttribute("failureMessage", "Unable To "+(contractor.getName()!=null?"Updated":"Updated")+" Role."); 
			} 

			return "redirect:/contractors";    

		} 	

		@GetMapping("/contractors") 
		public String managescontractor(@RequestParam(value="status",required = false) String status,HttpServletRequest request, Model model) {			
			if(status==null || "".equals(status)){
				status="ACTIVE";
		    }
			model.addAttribute("contractorList", adminService.findAllByUserId(SecurityHelper.getCurrentUser().getUserId(),status)); 			
			model.addAttribute("status", status);
			return "site.contractors";   

		} 

		//============applyclient================
			@GetMapping("/applyclient") 
			public String createsapplyclient(HttpServletRequest request, Model model) {	
				User user = SecurityHelper.getCurrentUser();
				model.addAttribute("skillList", adminService.findAllActiveSkills()); 
				model.addAttribute("freelancerList", adminService.getAllUsers(user.getUserId(),"f","","","")); 
				model.addAttribute("demoStatusList", MasterData.demoStatusList()); 
				return "site.applyclient";   

			} 

			@PostMapping("/applyclient") 
			public String addapplyclient(HttpServletRequest request, RedirectAttributes message,@ModelAttribute("applyclient") Applyclient applyclient) {

				Applyclient applyclient_ = adminService.addOrUpdateApplyclient(applyclient);
				if(applyclient_!=null)
				{
					message.addFlashAttribute("successMessage", "Name: ("+applyclient.getName()+") is "+(applyclient.getId()!=null?"Updated":"Updated")+" Successfully."); 

				}
				else {
					message.addFlashAttribute("failureMessage", "Unable To "+(applyclient.getName()!=null?"Updated":"Updated")+" Role."); 
				} 

				return "redirect:/applyclients";    

			} 	

			@GetMapping("/applyclients") 
			public String managesapplyclient(HttpServletRequest request, Model model,@RequestParam(value="demostatus",required=false)String demostatus) {			
				if(demostatus==null || "".equals(demostatus)) {
					demostatus="SCHEDULED";
				}
				model.addAttribute("applyclientList", adminService.findAllApplyclientByUserIdAndDemoStatus(SecurityHelper.getCurrentUser().getUserId(),demostatus)); 
				model.addAttribute("demoStatusList", MasterData.demoStatusList()); 
				model.addAttribute("demostatus", demostatus);
				return "site.applyclients";   

			} 
			
			
			@PostMapping("/edit-applyclient") 
			public String editsapplyclient(HttpServletRequest request, Model model, @RequestParam("id_edit")Long id_edit) {	
				
				model.addAttribute("applyclient", adminService.findApplyclientById(id_edit));
				User user = SecurityHelper.getCurrentUser();
				model.addAttribute("skillList", adminService.findAllActiveSkills()); 
				model.addAttribute("freelancerList", adminService.getAllUsers(user.getUserId(),"f","","","")); 
				model.addAttribute("demoStatusList", MasterData.demoStatusList()); 
				
				
				
				return "site.applyclient";   

			} 
			
		

		@PostMapping("/del-contractor") 
		public String deletecontractor(HttpServletRequest request, Model model,RedirectAttributes message, @RequestParam("id_del")Long id_del) {	

			boolean flag=adminService.deleteContractor(id_del,"INACTIVE");  		
			if(flag==true)
			{
				message.addFlashAttribute("successMessage", "Contractor is deleted Successfully."); 
			}
			else {
				message.addFlashAttribute("failureMessage", "Unable to delete contractor !"); 
			} 
			return "redirect:/contractors";     
		}
		@PostMapping("/active-contractor") 
		public String Activecontractor(HttpServletRequest request, Model model,RedirectAttributes message, @RequestParam("id_del")Long id_del) {	

			adminService.deleteContractor(id_del,"ACTIVE");  
			message.addFlashAttribute("successMessage", "Contractor is Activated Successfully."); 
			return "redirect:/contractors";     
		}






		@PostMapping("/edit-contractor") 
		public String editscontractor(HttpServletRequest request, Model model, @RequestParam("id_edit")Long id_edit) {	
			
			model.addAttribute("contractor", adminService.findContractorsById(id_edit));

			return "site.contractor";   

		} 
		
		@GetMapping("/user-requests") 
		public String userRequests(HttpServletRequest request, Model model) {			
			model.addAttribute("userRequestList", adminService.findAllUserRequest());
			return "site.userRequests";   

		} 
		
		@GetMapping("/user-reg") 
		public String registerUserout(HttpServletRequest request, Model model) {			
			
			return "site.registerUserout";   

		} 
		@PostMapping("/user-reg") 
		public String userrequestspost(HttpServletRequest request,RedirectAttributes model,@ModelAttribute("userRequest") UserRequest userRequest) {
			UserRequest userRequest_ = adminService.saveUserRequest(userRequest);
			if(userRequest_!=null)
			{
				model.addFlashAttribute("sms", "success"); 

			} 
			else {
				model.addFlashAttribute("sms", "fail"); 
			} 

			return "redirect:/user-reg";    

		} 
		
     
   
} 
	