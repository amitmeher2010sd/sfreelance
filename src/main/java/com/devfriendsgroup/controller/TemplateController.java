package com.devfriendsgroup.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.devfriendsgroup.model.Template;
import com.devfriendsgroup.model.User;
import com.devfriendsgroup.model.UserRole;
import com.devfriendsgroup.service.TemplateService;
import com.devfriendsgroup.service.UserService;
import com.devfriendsgroup.util.SecurityHelper;

@Controller
public class TemplateController {

	@Autowired
	private UserService userService;
	@Autowired
	private TemplateService templateService;
	@GetMapping("/templates") 
	public String templatesGet(HttpServletRequest request, Model model) {
		User user = SecurityHelper.getCurrentUser(); 
		UserRole userRole = userService.getUserRoleByUserId(user.getUserId()); 
		List<Template> responseList=templateService.findAllCustomTemplateByUserId(user.getUserId());//default template
		model.addAttribute("responseList",responseList ); 
		model.addAttribute("userRole", userRole);

		return "site.templates";   		
	} 
	@GetMapping("/template") 
	public String templateGet(HttpServletRequest request, Model model) {

		return "site.template";   
	}
	@PostMapping("/template") 
	public String templatePost(HttpServletRequest request, RedirectAttributes message,@ModelAttribute("template") Template template) {
		Template response = templateService.addOrUpdateTemplate(template);  
		if(response!=null)
		{
			message.addFlashAttribute("successMessage", "Contract ("+response.getTemplateName()+") is "+(template.getId()!=null?"Updated":"Saved")+" Successfully."); 

		}
		else {
			message.addFlashAttribute("failureMessage", "Unable To Save/Update"); 
		} 

		return "redirect:/templates";    

	} 	
	@PostMapping("/del-template") 
	public String delcontract(HttpServletRequest request,RedirectAttributes message, Model model, @RequestParam("id_del")Long id_del) {	

		templateService.deleteTemplate(id_del); 
		message.addFlashAttribute("successMessage", "Contract is Deleted Successfully."); 
		return "redirect:/templates";         
	}
	@PostMapping("/edit-template") 
	public String edittemplate(HttpServletRequest request, Model model, @RequestParam("id_edit")Long id_edit) {	
		model.addAttribute("template", templateService.findTemplate(id_edit));  
		return "site.template" ;  

	}  

}
