package com.devfriendsgroup.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devfriendsgroup.model.Notification;
import com.devfriendsgroup.model.Template;
import com.devfriendsgroup.model.UserRequest;
import com.devfriendsgroup.service.AdminService;
import com.devfriendsgroup.service.TemplateService;

@RestController
@RequestMapping("/api")
public class CommonRestController {
	
	@Autowired
	private TemplateService templateService;
	@Autowired
	private AdminService adminService;
	
	@PostMapping(path= "/load-template")
    public ResponseEntity<String> loadTemplate(@RequestBody Template template) 
    {
		Template response = templateService.findTemplate(template.getId());
         
        return ResponseEntity.ok(response.getTemplateContent());
    }
	@PostMapping(path= "/user-requests")
    public ResponseEntity<String> userRequests(@RequestBody UserRequest userRequest) 
    {
		UserRequest response = adminService.saveUserRequest(userRequest);
		String sms=response!=null?"success":"fail";
         
        return ResponseEntity.ok(sms);
    }
	
	@GetMapping(path= "/validate-user")
    public ResponseEntity<String> validateUser(@RequestParam("data") String data) 
    {
		String code=adminService.validateUser(data);
		//String sms=response!=null?"ContactExist":"UserNameExist";
         
        return ResponseEntity.ok(code);
    }
	@GetMapping(path= "/get-notifications")
    public ResponseEntity<List<Notification>> notifications() 
    {         
        return ResponseEntity.ok(adminService.findAllNotification());
    }
		 	 	
	
	
	
}
	