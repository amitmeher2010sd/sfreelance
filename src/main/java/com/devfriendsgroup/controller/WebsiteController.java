package com.devfriendsgroup.controller;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.devfriendsgroup.website.model.WebsiteERole;
import com.devfriendsgroup.website.model.WebsiteRole;
import com.devfriendsgroup.website.model.WebsiteUser;
import com.devfriendsgroup.website.model.WebsiteUserInfo;
import com.devfriendsgroup.website.repository.WebsiteRoleRepository;
import com.devfriendsgroup.website.repository.WebsiteSkillRepository;
import com.devfriendsgroup.website.repository.WebsiteUserInfoRepository;
import com.devfriendsgroup.website.repository.WebsiteUserRepository;

@Controller
public class WebsiteController {
	
	@Autowired
	private WebsiteUserInfoRepository userInfoRepository; 
	@Autowired
	private WebsiteSkillRepository websiteSkillRepository; 
	@Autowired
	private WebsiteUserRepository userRepository; 
	@Autowired
	private WebsiteRoleRepository roleRepository; 
	@Autowired
	private WebsiteUserRepository websiteUserRepository; 

	
	
	@Autowired
	PasswordEncoder encoder;

	@GetMapping("/website-users")  
	public String websiteUsers(Model model) {	
		model.addAttribute("uinfoList", userInfoRepository.findAll()); 	
		return "site.websiteUsers";   

	}  
	@GetMapping("/website-user-view")  
	public String websiteuserview(Model model,@RequestParam("username")String username) {	
		model.addAttribute("userInfo", userInfoRepository.findUserInfosByUsername(username)); 	
		return "site.websiteuserview";   

	}  
	@PostMapping("/website-user-edit")  
	public String websiteuseredit(Model model,@RequestParam("id_edit")Long id_edit) {	
		model.addAttribute("userInfo", userInfoRepository.findById(id_edit).get()); 
		model.addAttribute("skillList",websiteSkillRepository.findAll()); 
		return "site.websiteuser";   

	}  
	@GetMapping("/website-user")  
	public String websiteuser(Model model) {	
		model.addAttribute("skillList",websiteSkillRepository.findAll()); 
		return "site.websiteuser";   

	}   
	@PostMapping("/website-user") 
	public String adduser(HttpServletRequest request, RedirectAttributes message,@ModelAttribute("websiteUserInfo") WebsiteUserInfo websiteUserInfo) {

		
		String response= adduserByConsultancy(websiteUserInfo);
		if("MSG_SUCCESS".equalsIgnoreCase(response))
		{
			message.addFlashAttribute("successMessage", "User  is "+(websiteUserInfo.getId()!=null?"Updated":"Saved")+" Successfully."); 

		}
		else {
			message.addFlashAttribute("failureMessage", "Unable To "+(websiteUserInfo.getId()!=null?"Updated":"Saved")+" User."); 
		} 

		return "redirect:/website-users";

	} 	
public String adduserByConsultancy(WebsiteUserInfo userInfo) {
		String sms="MSG_FAIL";
		String primarySkill = Arrays.toString(userInfo.getPrimarySkillArray());
		String secondaryskill = Arrays.toString(userInfo.getSecondaryskillsArray());
		if(userInfo.getId()==null)
		{
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			// 2021-03-24T16:44:39.083+08:00
			SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
			System.out.println(sdf2.format(timestamp));
			if (userRepository.existsByUsername(userInfo.getPhone())) {
				sms="Phone Exist";
				return sms;
			}
			if (userRepository.existsByEmail(userInfo.getEmail())) {
				sms="Phone Exist";
				return sms;
			}
			userInfo.setPassword("123456");
			// Create new user's account
			WebsiteUser user = new WebsiteUser(null, userInfo.getName(),
					userInfo.getEmail(), encoder.encode(userInfo.getPassword()),
					userInfo.getEmail(),userInfo.getPhone(), null,null,userInfo.getStatus(),sdf2.format(timestamp),userInfo.getPassword(),false);
			
			

			Set<WebsiteRole> roles = new HashSet<>();
			WebsiteRole userRole =null;
			String usercode="";
			userRole = roleRepository.findByName(WebsiteERole.ROLE_DEVELOPER)
					.orElseThrow(() -> new RuntimeException("MSG_CANDIDATE_NOTFOUND"));
			roles.add(userRole);
			usercode="MSG_USERCODE_DEV";
			user.setRolename(userRole.getName().name());
			user.setRoles(roles);
			WebsiteUser userdb = userRepository.save(user);
			if(userdb!=null) {
				userInfo.setPrimaryskills(primarySkill);
				userInfo.setSecondaryskills(secondaryskill);
				userInfo.setCreatedBy(userdb);
				userInfo.setCreatedOn(new Date());
				userInfo.setStatus("ACTIVE");
				userInfo.setUser(userdb);
				userInfo.setCompanyName(userInfo.getCompanyName());
				userInfo.setUserCode("646464464");//;(CmmonUtil.generateCode(usercode));

				userInfo.setMessage("MSG_FAIL");
				userInfo.setUpdatedOn(new Date());
				userInfo=userInfoRepository.save(userInfo);
				if(userInfo!=null)
				{
					userInfo.setMessage("MSG_SUCCESS");
					sms="MSG_SUCCESS";
										
				}
				else {
					userRepository.delete(userdb);
				}
			}
		}else {
			Optional<WebsiteUserInfo> userInfodbex=userInfoRepository.findById(userInfo.getId());
			if(userInfodbex.isPresent())
			{
				WebsiteUser userdb = userRepository.findById(userInfodbex.get().getUser().getId()).get();
				userdb.setStatus(userInfo.getStatus());
				userdb.setName(userInfo.getName());
				userdb.setEmail(userInfo.getEmail());
				userdb.setPhone(userInfo.getPhone());
				userRepository.save(userdb);
				
				
				WebsiteUserInfo userInfoEx=userInfodbex.get();
			userInfoEx.setAddress(userInfo.getAddress());
			userInfoEx.setCompanyName(userInfo.getCompanyName());
			userInfoEx.setDateofbirth(userInfo.getDateofbirth());
			userInfoEx.setDateofFounded(userInfo.getDateofFounded());
			userInfoEx.setDescription(userInfo.getDescription());
			userInfoEx.setExperience(userInfo.getExperience());
			userInfoEx.setFacebook(userInfo.getFacebook());
			userInfoEx.setDesignation(userInfo.getDesignation());
			userInfoEx.setSecondaryphone(userInfo.getSecondaryphone());
			userInfoEx.setSecondaryskills(userInfo.getSecondaryskills());
			userInfoEx.setPrimaryskills(userInfo.getPrimaryskills());
			userInfoEx.setLinkedin(userInfo.getLinkedin());
			userInfoEx.setWebsite(userInfo.getWebsite());	
			userInfoEx.setTwitter(userInfo.getTwitter());
			userInfoEx.setGender(userInfo.getGender());
			userInfoEx.setPrimaryskills(primarySkill);
			userInfoEx.setSecondaryskills(secondaryskill);
			userInfoEx.setSupportmode(userInfo.getSupportmode());
			userInfoEx.setSupporttype(userInfo.getSupporttype());
			userInfoEx.setSupporthour(userInfo.getSupporthour());
			userInfoEx.setBankmodetype(userInfo.getBankmodetype());
			userInfoEx.setBankmodenumber(userInfo.getBankmodenumber());
			userInfoEx.setAccountsholdername(userInfo.getAccountsholdername());
			userInfoEx.setAccountsifsc(userInfo.getAccountsifsc());
			userInfoEx.setAccountsbankname(userInfo.getAccountsbankname());
			userInfoEx.setAccountsaccno(userInfo.getAccountsaccno());
			userInfoEx.setJobsupporttimeslot(userInfo.getJobsupporttimeslot());
			userInfoEx.setOthertimeslot(userInfo.getOthertimeslot());
			userInfoEx.setOtherskills(userInfo.getOtherskills());
			
			
			
			
			 userInfo=userInfoRepository.save(userInfoEx);
			
			
			userInfo.setMessage("MSG_SUCCESS");
			sms="MSG_SUCCESS";
			}
		}	
		return sms;
	}
} 
	