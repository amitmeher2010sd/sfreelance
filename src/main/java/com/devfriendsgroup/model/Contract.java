package com.devfriendsgroup.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;
@Data
@Entity
@Table(name="contracts_info", schema = "public")
public class Contract  implements Serializable {
	
	private static final long serialVersionUID = 285701719160134651L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long Id;
	
	@ManyToOne
	@JoinColumn(name="project_id")
	private Project project;
	
	@Column(name="contarct_name") 
	private String contarctName; 
	
	@Column(name="contarct_code") 
	private String contarctCode; 

	@Column(name="sign1_upload")
	private String sign1Upload; 

	@Column(name="sign2_upload")
	private String sign2Upload; 
	
	@Column(name="contract_upload")
	private String contractUpload; 

	@Column(name="remarks")
	private String remarks;
	
	@Column(name="other_ifnot_project")
	private String otherFfnotProject;
	
	@ManyToOne
	@JoinColumn(name="client_id")
	private User client;
	
	@ManyToOne
	@JoinColumn(name="freelancer_id")
	private User freelancer;
	
	@ManyToOne
	@JoinColumn(name="created_by")
	private User createdBy;

	@Column(name="created_on")
	private Date createdOn;

	@ManyToOne
	@JoinColumn(name="updated_by")
	private User updatedBy;

	@Column(name="updated_on")
	private Date updatedOn;

	@Column(name="status")
	private String status;
	
	@Column(name="contract_status")
	private String contractStatus;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public String getContarctName() {
		return contarctName;
	}

	public void setContarctName(String contarctName) {
		this.contarctName = contarctName;
	}

	public String getContarctCode() {
		return contarctCode;
	}

	public void setContarctCode(String contarctCode) {
		this.contarctCode = contarctCode;
	}

	public String getSign1Upload() {
		return sign1Upload;
	}

	public void setSign1Upload(String sign1Upload) {
		this.sign1Upload = sign1Upload;
	}

	public String getSign2Upload() {
		return sign2Upload;
	}

	public void setSign2Upload(String sign2Upload) {
		this.sign2Upload = sign2Upload;
	}

	public String getContractUpload() {
		return contractUpload;
	}

	public void setContractUpload(String contractUpload) {
		this.contractUpload = contractUpload;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getOtherFfnotProject() {
		return otherFfnotProject;
	}

	public void setOtherFfnotProject(String otherFfnotProject) {
		this.otherFfnotProject = otherFfnotProject;
	}

	public User getClient() {
		return client;
	}

	public void setClient(User client) {
		this.client = client;
	}

	public User getFreelancer() {
		return freelancer;
	}

	public void setFreelancer(User freelancer) {
		this.freelancer = freelancer;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getContractStatus() {
		return contractStatus;
	}

	public void setContractStatus(String contractStatus) {
		this.contractStatus = contractStatus;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
} 
