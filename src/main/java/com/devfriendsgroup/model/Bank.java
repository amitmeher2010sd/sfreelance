package com.devfriendsgroup.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;
//@Data
@Entity
@Table(name="bank_details", schema = "public")
public class Bank  implements Serializable {
	
	private static final long serialVersionUID = 285701719160134651L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long Id;
	
	
	
	@Column(name="bank_name") 
	private String bankName; 
	
	@Column(name="account_no") 
	private String accountNo; 

	@Column(name="ifsc_code")
	private String ifscCode; 

	@Column(name="account_holder_name")
	private String accountHolderName; 
	
	@Column(name="account_holder_address")
	private String accountHolderAddress; 

	@Column(name="account_holder_contact_no")
	private String accountHolderContactNo;	
	@Column(name="google_pay")
	private String googlePay;	
	@Column(name="any_upi")
	private String anyUpi;	
	
	
	
	
	@ManyToOne
	@JoinColumn(name="created_by")
	private User createdBy;

	@Column(name="created_on")
	private Date createdOn;

	@ManyToOne
	@JoinColumn(name="updated_by")
	private User updatedBy;

	@Column(name="updated_on")
	private Date updatedOn;

	@Column(name="status")
	private String status;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getAccountHolderName() {
		return accountHolderName;
	}

	public void setAccountHolderName(String accountHolderName) {
		this.accountHolderName = accountHolderName;
	}

	public String getAccountHolderAddress() {
		return accountHolderAddress;
	}

	public void setAccountHolderAddress(String accountHolderAddress) {
		this.accountHolderAddress = accountHolderAddress;
	}

	public String getAccountHolderContactNo() {
		return accountHolderContactNo;
	}

	public void setAccountHolderContactNo(String accountHolderContactNo) {
		this.accountHolderContactNo = accountHolderContactNo;
	}

	public String getGooglePay() {
		return googlePay;
	}

	public void setGooglePay(String googlePay) {
		this.googlePay = googlePay;
	}

	public String getAnyUpi() {
		return anyUpi;
	}

	public void setAnyUpi(String anyUpi) {
		this.anyUpi = anyUpi;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
} 
