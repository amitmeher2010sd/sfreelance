package com.devfriendsgroup.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;
import lombok.ToString;
@ToString
//@Data
@Entity
@Table(name="user_info", schema = "public")
public class UserInfo  implements Serializable {
	
	private static final long serialVersionUID = 285701719160134651L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long Id;
	
	@ManyToOne
	@JoinColumn(name="user_id")
	private User user;
	
	@Column(name="profile_pic")
	private String profilePic; 
	
	@Column(name="profession")
	private String profession; 
	
	@Column(name="organization_name")
	private String organizationName; 
	
	@Column(name="organization_details")
	private String organizationDetails; 
	
	@Column(name="logo")
	private String logo;
	
	@Column(name="id_proof_name")
	private String idProofName; 
	
	@Column(name="id_proof_number")
	private String idProofNumber;
	
	@Column(name="id_proof_document")
	private String idProofDocument; 
	
	@Column(name="fb_link")
	private String fbLink; 
	
	@Column(name="website_link")
	private String websiteLink; 
	
	@Column(name="insta_link")
	private String instaLink; 
	
	@Column(name="linkdean_link")
	private String linkdeanLink; 
	
	@Column(name="standard_payment")
	private String standardPayment; 
	
	
	@Column(name="currency")
	private String currency;
	
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	@Column(name="support_type")
	private String supportType; 
	
	@ManyToOne
	@JoinColumn(name="created_by")
	private User createdBy;
	
		
	
	@Column(name="created_on")
	private Date createdOn;
		
	@ManyToOne
	@JoinColumn(name="updated_by")
	private User updatedBy;
	
	@Column(name="updated_on")
	private Date updatedOn;	
	
	@Column(name="status")
	private String status;
	
	@Column(name="skills")
	private String skills;
	@Column(name="skills_other")
	private String skillsOther;
	
	
	@Transient
	private String countryCode;

	
	@Transient
	private Long roleId;
	@Transient
    private Long userId;
	@Transient
	private String userName;
	@Transient
	private String mobileNo; 
	
	@Transient
	private String remark; 
	
	@Transient
	private int isblocked;
	
	@Transient
	private String name; 
	
	@Transient
	private String email;
	
	@Transient
	private String address; 
	
	@Transient
	private String otp; 
	
	@Transient
	private String gender; 

	@Transient
	private String rtype; 
	
	@Column(name="user_type")
	private String userType;

	@ManyToOne
	@JoinColumn(name="contractor_id")
	private Contractors contractors;
	@Transient
	private Long contractorId;
	@Transient
	private UserRole userRoleTest;
	@Column(name="experience")
	private String experience;	
	@ManyToOne
	@JoinColumn(name="user_bank")
	private Bank bankInfo;
	@Transient
	private Long bankId;

	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getProfilePic() {
		return profilePic;
	}
	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}
	public String getProfession() {
		return profession;
	}
	public void setProfession(String profession) {
		this.profession = profession;
	}
	public String getOrganizationName() {
		return organizationName;
	}
	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}
	public String getOrganizationDetails() {
		return organizationDetails;
	}
	public void setOrganizationDetails(String organizationDetails) {
		this.organizationDetails = organizationDetails;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public String getIdProofName() {
		return idProofName;
	}
	public void setIdProofName(String idProofName) {
		this.idProofName = idProofName;
	}
	public String getIdProofNumber() {
		return idProofNumber;
	}
	public void setIdProofNumber(String idProofNumber) {
		this.idProofNumber = idProofNumber;
	}
	public String getIdProofDocument() {
		return idProofDocument;
	}
	public void setIdProofDocument(String idProofDocument) {
		this.idProofDocument = idProofDocument;
	}
	public String getFbLink() {
		return fbLink;
	}
	public void setFbLink(String fbLink) {
		this.fbLink = fbLink;
	}
	public String getWebsiteLink() {
		return websiteLink;
	}
	public void setWebsiteLink(String websiteLink) {
		this.websiteLink = websiteLink;
	}
	public String getInstaLink() {
		return instaLink;
	}
	public void setInstaLink(String instaLink) {
		this.instaLink = instaLink;
	}
	public String getLinkdeanLink() {
		return linkdeanLink;
	}
	public void setLinkdeanLink(String linkdeanLink) {
		this.linkdeanLink = linkdeanLink;
	}
	public String getStandardPayment() {
		return standardPayment;
	}
	public void setStandardPayment(String standardPayment) {
		this.standardPayment = standardPayment;
	}
	public String getSupportType() {
		return supportType;
	}
	public void setSupportType(String supportType) {
		this.supportType = supportType;
	}
	public User getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public User getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSkills() {
		return skills;
	}
	public void setSkills(String skills) {
		this.skills = skills;
	}
	public String getSkillsOther() {
		return skillsOther;
	}
	public void setSkillsOther(String skillsOther) {
		this.skillsOther = skillsOther;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public int getIsblocked() {
		return isblocked;
	}
	public void setIsblocked(int isblocked) {
		this.isblocked = isblocked;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getOtp() {
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getRtype() {
		return rtype;
	}
	public void setRtype(String rtype) {
		this.rtype = rtype;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public Contractors getContractors() {
		return contractors;
	}
	public void setContractors(Contractors contractors) {
		this.contractors = contractors;
	}
	public Long getContractorId() {
		return contractorId;
	}
	public void setContractorId(Long contractorId) {
		this.contractorId = contractorId;
	}
	public UserRole getUserRoleTest() {
		return userRoleTest;
	}
	public void setUserRoleTest(UserRole userRoleTest) {
		this.userRoleTest = userRoleTest;
	}
	public String getExperience() {
		return experience;
	}
	public void setExperience(String experience) {
		this.experience = experience;
	}
	public Bank getBankInfo() {
		return bankInfo;
	}
	public void setBankInfo(Bank bankInfo) {
		this.bankInfo = bankInfo;
	}
	public Long getBankId() {
		return bankId;
	}
	public void setBankId(Long bankId) {
		this.bankId = bankId;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
	
} 
	