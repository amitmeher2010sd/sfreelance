package com.devfriendsgroup.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.*;
@ToString
//@Data 
@Entity
@Table(name="payment_history", schema = "public")
public class ProjectPayment  implements Serializable {

	private static final long serialVersionUID = 285701719160134651L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long Id;

	@ManyToOne
	@JoinColumn(name="project_allocation_id")
	private ProjectAllocation projectAllocation;	

	@Column(name="payment_date")
	private Date paymentDate; 
	
	@Column(name="start_date")
	private Date startDate;
	
	@Column(name="end_date")
	private Date endDate; 
	
	@Column(name="payment_amount")
	private Double paymentAmount; 
	@Column(name="currency")
	private String currency; 

	
	
	@Column(name="remarks")
	private String remarks; 

	@ManyToOne
	@JoinColumn(name="created_by")
	private User createdBy;

	@Column(name="created_on")
	private Date createdOn;

	@ManyToOne
	@JoinColumn(name="updated_by")
	private User updatedBy;

	@Column(name="updated_on")
	private Date updatedOn;

	@Column(name="status")
	private String status;
	
	@Column(name="payment_status")
	private String paymentStatus;
	@Column(name="paymentfor")
	private String paymentfor;
	
	@Column(name="hourly_or_project_wise")
	private boolean hourlyOrProjectWise;
	@Column(name="advance")
	private boolean advance;
	
	@Transient
	private String startDateStr; 

	@Transient
	private String endDateStr; 
	@Transient
	private String paymentDateStr; 
	@Transient
	private String projectCode;
	
	@Transient
	private String paymentAmount_;

	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public ProjectAllocation getProjectAllocation() {
		return projectAllocation;
	}
	public void setProjectAllocation(ProjectAllocation projectAllocation) {
		this.projectAllocation = projectAllocation;
	}
	public Date getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Double getPaymentAmount() {
		return paymentAmount;
	}
	public void setPaymentAmount(Double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public User getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public User getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public String getPaymentfor() {
		return paymentfor;
	}
	public void setPaymentfor(String paymentfor) {
		this.paymentfor = paymentfor;
	}
	public boolean isHourlyOrProjectWise() {
		return hourlyOrProjectWise;
	}
	public void setHourlyOrProjectWise(boolean hourlyOrProjectWise) {
		this.hourlyOrProjectWise = hourlyOrProjectWise;
	}
	public boolean isAdvance() {
		return advance;
	}
	public void setAdvance(boolean advance) {
		this.advance = advance;
	}
	public String getStartDateStr() {
		return startDateStr;
	}
	public void setStartDateStr(String startDateStr) {
		this.startDateStr = startDateStr;
	}
	public String getEndDateStr() {
		return endDateStr;
	}
	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}
	public String getPaymentDateStr() {
		return paymentDateStr;
	}
	public void setPaymentDateStr(String paymentDateStr) {
		this.paymentDateStr = paymentDateStr;
	}
	public String getProjectCode() {
		return projectCode;
	}
	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}
	public String getPaymentAmount_() {
		return paymentAmount_;
	}
	public void setPaymentAmount_(String paymentAmount_) {
		this.paymentAmount_ = paymentAmount_;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getFreelancerName() {
		return projectAllocation.getUserFreelancer().getName();
	}
	public String getClientName() {
		return projectAllocation.getProject().getUserClient().getName();
	}
	
	
} 
