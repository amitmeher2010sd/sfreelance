package com.devfriendsgroup.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
@Data
@AllArgsConstructor
public class JKFreelanceUser {
  private Long id;
  private String name;
  private String username;
  private String password;
  private String email;
  private String phone;
private String rolename;
private String status;
private String activationcode;
private String secretkey;
private boolean verificationmailsent;
public JKFreelanceUser() {
}
}
