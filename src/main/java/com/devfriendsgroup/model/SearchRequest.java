package com.devfriendsgroup.model;

import java.util.List;

import lombok.Data;
import lombok.ToString;

public class SearchRequest {
 public String startDateStr;
 public String endDateStr;
 public String user;
 public String skills;
 public String rdSearchBy;
 public String paystatus;
 public String payType; 
 public String anysearch; 
 public String projectstatus;
public String getStartDateStr() {
	return startDateStr;
}
public void setStartDateStr(String startDateStr) {
	this.startDateStr = startDateStr;
}
public String getEndDateStr() {
	return endDateStr;
}
public void setEndDateStr(String endDateStr) {
	this.endDateStr = endDateStr;
}
public String getUser() {
	return user;
}
public void setUser(String user) {
	this.user = user;
}
public String getSkills() {
	return skills;
}
public void setSkills(String skills) {
	this.skills = skills;
}
public String getRdSearchBy() {
	return rdSearchBy;
}
public void setRdSearchBy(String rdSearchBy) {
	this.rdSearchBy = rdSearchBy;
}
public String getPaystatus() {
	return paystatus;
}
public void setPaystatus(String paystatus) {
	this.paystatus = paystatus;
}
public String getPayType() {
	return payType;
}
public void setPayType(String payType) {
	this.payType = payType;
}
public String getAnysearch() {
	return anysearch;
}
public void setAnysearch(String anysearch) {
	this.anysearch = anysearch;
}
public String getProjectstatus() {
	return projectstatus;
}
public void setProjectstatus(String projectstatus) {
	this.projectstatus = projectstatus;
}
 
 
 
}
