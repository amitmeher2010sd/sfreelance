package com.devfriendsgroup.model;


import lombok.Data;
import lombok.ToString;
@ToString
@Data
public class PaymentResponseSearchDTO {
 public String name;
 public Double clientTotal;
 public Double clientPaid;
 public Double clientPending;
 public Double freelancerTotal;
 public Double freelancerPaid;
 public Double freelancerPending; 
 public Double totalProfit;
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public Double getClientTotal() {
	return clientTotal;
}
public void setClientTotal(Double clientTotal) {
	this.clientTotal = clientTotal;
}
public Double getClientPaid() {
	return clientPaid;
}
public void setClientPaid(Double clientPaid) {
	this.clientPaid = clientPaid;
}
public Double getClientPending() {
	return clientPending;
}
public void setClientPending(Double clientPending) {
	this.clientPending = clientPending;
}
public Double getFreelancerTotal() {
	return freelancerTotal;
}
public void setFreelancerTotal(Double freelancerTotal) {
	this.freelancerTotal = freelancerTotal;
}
public Double getFreelancerPaid() {
	return freelancerPaid;
}
public void setFreelancerPaid(Double freelancerPaid) {
	this.freelancerPaid = freelancerPaid;
}
public Double getFreelancerPending() {
	return freelancerPending;
}
public void setFreelancerPending(Double freelancerPending) {
	this.freelancerPending = freelancerPending;
}
public Double getTotalProfit() {
	return totalProfit;
}
public void setTotalProfit(Double totalProfit) {
	this.totalProfit = totalProfit;
} 
 
 
}
