package com.devfriendsgroup.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;


//@Data
@Entity
@Table(name="applyclient", schema = "public")
public class Applyclient    implements Serializable{
	
	private static final long serialVersionUID = 285701719160134651L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long Id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="gender")
	private String gender; 
	
	@Column(name="mobile")
	private String mobile; 
	
	
	@Column(name="email") 
	private String email; 
	@Column(name="requirments_details") 
	private String requirmentsDetails; 
	
	@Column(name="freelancer_details") 
	private String freelancerDetails; 
	

	@ManyToOne
	@JoinColumn(name="created_by")
	private User createdBy;
	
	@Column(name="created_on")
	private Date createdOn;
		
	@ManyToOne
	@JoinColumn(name="updated_by")
	private User updatedBy;
	
	@Column(name="updated_on")
	private Date updatedOn; 
	
	@Column(name="status")
	private String status;
	@Column(name="address")
	private String address;
	@Transient
	private String countryCode;	
	
	@Column(name="applycode")
	private String applycode;
	@Column(name="meettime")
	private String meettime;
	@Column(name="meetDate")
	private String meetDate;
	@Column(name="remarks")
	private String remarks;
	@Column(name="skills")
	private String skills;
	@Column(name="demostatus")
	private String demostatus;
	
	@ManyToOne
	@JoinColumn(name="freelancer_user_id")
	private User freelancer;
	@Transient
	private Long freelancerId;
	
	@Column(name="whatsappgroup")
	private String whatsappgroup;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRequirmentsDetails() {
		return requirmentsDetails;
	}

	public void setRequirmentsDetails(String requirmentsDetails) {
		this.requirmentsDetails = requirmentsDetails;
	}

	public String getFreelancerDetails() {
		return freelancerDetails;
	}

	public void setFreelancerDetails(String freelancerDetails) {
		this.freelancerDetails = freelancerDetails;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getApplycode() {
		return applycode;
	}

	public void setApplycode(String applycode) {
		this.applycode = applycode;
	}

	public String getMeettime() {
		return meettime;
	}

	public void setMeettime(String meettime) {
		this.meettime = meettime;
	}

	public String getMeetDate() {
		return meetDate;
	}

	public void setMeetDate(String meetDate) {
		this.meetDate = meetDate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getSkills() {
		return skills;
	}

	public void setSkills(String skills) {
		this.skills = skills;
	}

	public String getDemostatus() {
		return demostatus;
	}

	public void setDemostatus(String demostatus) {
		this.demostatus = demostatus;
	}

	public User getFreelancer() {
		return freelancer;
	}

	public void setFreelancer(User freelancer) {
		this.freelancer = freelancer;
	}

	public Long getFreelancerId() {
		return freelancerId;
	}

	public void setFreelancerId(Long freelancerId) {
		this.freelancerId = freelancerId;
	}

	public String getWhatsappgroup() {
		return whatsappgroup;
	}

	public void setWhatsappgroup(String whatsappgroup) {
		this.whatsappgroup = whatsappgroup;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
} 
	