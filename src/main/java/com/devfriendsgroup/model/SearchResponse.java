package com.devfriendsgroup.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Data;

@Data
public class SearchResponse {
	
	List<UserInfo> userInfoList;
	List<ProjectAllocation> projectAllocationList;
	List<ProjectPayment> paymentsList;
	List<Skill> skillList;
	List<String> paymentStatusList;
	Double USD_TO_INR;
	List<String> userstatusList;
	List<String> paymentTypeList;
	List<String> projectStatusList;
	Map<String, Double> paymentsListCrPaid =new HashMap<>();
	Map<String, Double> paymentsListCrPending =new HashMap<>();
	Map<String, Double> paymentsListDrPaid =new HashMap<>();
	Map<String, Double> paymentsListDrPending =new HashMap<>();
	List<PaymentResponseSearchDTO> paymentResponseSearchDTOs;
	public List<UserInfo> getUserInfoList() {
		return userInfoList;
	}
	public void setUserInfoList(List<UserInfo> userInfoList) {
		this.userInfoList = userInfoList;
	}
	public List<ProjectAllocation> getProjectAllocationList() {
		return projectAllocationList;
	}
	public void setProjectAllocationList(List<ProjectAllocation> projectAllocationList) {
		this.projectAllocationList = projectAllocationList;
	}
	public List<ProjectPayment> getPaymentsList() {
		return paymentsList;
	}
	public void setPaymentsList(List<ProjectPayment> paymentsList) {
		this.paymentsList = paymentsList;
	}
	public List<Skill> getSkillList() {
		return skillList;
	}
	public void setSkillList(List<Skill> skillList) {
		this.skillList = skillList;
	}
	public List<String> getPaymentStatusList() {
		return paymentStatusList;
	}
	public void setPaymentStatusList(List<String> paymentStatusList) {
		this.paymentStatusList = paymentStatusList;
	}
	public Double getUSD_TO_INR() {
		return USD_TO_INR;
	}
	public void setUSD_TO_INR(Double uSD_TO_INR) {
		USD_TO_INR = uSD_TO_INR;
	}
	public List<String> getUserstatusList() {
		return userstatusList;
	}
	public void setUserstatusList(List<String> userstatusList) {
		this.userstatusList = userstatusList;
	}
	public List<String> getPaymentTypeList() {
		return paymentTypeList;
	}
	public void setPaymentTypeList(List<String> paymentTypeList) {
		this.paymentTypeList = paymentTypeList;
	}
	public List<String> getProjectStatusList() {
		return projectStatusList;
	}
	public void setProjectStatusList(List<String> projectStatusList) {
		this.projectStatusList = projectStatusList;
	}
	public Map<String, Double> getPaymentsListCrPaid() {
		return paymentsListCrPaid;
	}
	public void setPaymentsListCrPaid(Map<String, Double> paymentsListCrPaid) {
		this.paymentsListCrPaid = paymentsListCrPaid;
	}
	public Map<String, Double> getPaymentsListCrPending() {
		return paymentsListCrPending;
	}
	public void setPaymentsListCrPending(Map<String, Double> paymentsListCrPending) {
		this.paymentsListCrPending = paymentsListCrPending;
	}
	public Map<String, Double> getPaymentsListDrPaid() {
		return paymentsListDrPaid;
	}
	public void setPaymentsListDrPaid(Map<String, Double> paymentsListDrPaid) {
		this.paymentsListDrPaid = paymentsListDrPaid;
	}
	public Map<String, Double> getPaymentsListDrPending() {
		return paymentsListDrPending;
	}
	public void setPaymentsListDrPending(Map<String, Double> paymentsListDrPending) {
		this.paymentsListDrPending = paymentsListDrPending;
	}
	public List<PaymentResponseSearchDTO> getPaymentResponseSearchDTOs() {
		return paymentResponseSearchDTOs;
	}
	public void setPaymentResponseSearchDTOs(List<PaymentResponseSearchDTO> paymentResponseSearchDTOs) {
		this.paymentResponseSearchDTOs = paymentResponseSearchDTOs;
	}
	
}
