package com.devfriendsgroup.model;

import lombok.Data;

@Data
public class SearchInput {
public String day;
public String month;
public String year;
}
