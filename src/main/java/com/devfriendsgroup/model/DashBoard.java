package com.devfriendsgroup.model;

import java.util.List;

import lombok.Data;
import lombok.ToString;
@ToString
//@Data
public class DashBoard {
	private Long totalUser=0l;	
	private Long totalFreelancerUser=0l;	
	private Long totalClientUser=0l;	
	private Long totalProject=0l;	
	private Long totalActiveProjectAllocation=0l;
	private Long totalContract=0l;
	private Long totalContractor=0l;
	private Long totalClosedProject=0l;
	private List<Notification> notifications;
	public Long getTotalUser() {
		return totalUser;
	}
	public void setTotalUser(Long totalUser) {
		this.totalUser = totalUser;
	}
	public Long getTotalFreelancerUser() {
		return totalFreelancerUser;
	}
	public void setTotalFreelancerUser(Long totalFreelancerUser) {
		this.totalFreelancerUser = totalFreelancerUser;
	}
	public Long getTotalClientUser() {
		return totalClientUser;
	}
	public void setTotalClientUser(Long totalClientUser) {
		this.totalClientUser = totalClientUser;
	}
	public Long getTotalProject() {
		return totalProject;
	}
	public void setTotalProject(Long totalProject) {
		this.totalProject = totalProject;
	}
	public Long getTotalActiveProjectAllocation() {
		return totalActiveProjectAllocation;
	}
	public void setTotalActiveProjectAllocation(Long totalActiveProjectAllocation) {
		this.totalActiveProjectAllocation = totalActiveProjectAllocation;
	}
	public Long getTotalContract() {
		return totalContract;
	}
	public void setTotalContract(Long totalContract) {
		this.totalContract = totalContract;
	}
	public Long getTotalContractor() {
		return totalContractor;
	}
	public void setTotalContractor(Long totalContractor) {
		this.totalContractor = totalContractor;
	}
	public Long getTotalClosedProject() {
		return totalClosedProject;
	}
	public void setTotalClosedProject(Long totalClosedProject) {
		this.totalClosedProject = totalClosedProject;
	}
	public List<Notification> getNotifications() {
		return notifications;
	}
	public void setNotifications(List<Notification> notifications) {
		this.notifications = notifications;
	}
	
}
