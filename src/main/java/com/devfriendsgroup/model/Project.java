package com.devfriendsgroup.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;
//@Data
@Entity
@Table(name="project", schema = "public")
public class Project  implements Serializable {

	

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	@Override
	public String toString() {
		return "Project [Id=" + Id + ", userRoleAgent=" + userRoleAgent + ", projectCodeInternal=" + projectCodeInternal
				+ ", projectCode=" + projectCode + ", projectName=" + projectName + ", projectDesc=" + projectDesc
				+ ", requiredSkils=" + requiredSkils + ", projectCost=" + projectCost + ", startDate=" + startDate
				+ ", endDate=" + endDate + ", projectStatus=" + projectStatus + ", createdBy=" + createdBy
				+ ", createdOn=" + createdOn + ", updatedBy=" + updatedBy + ", updatedOn=" + updatedOn + ", status="
				+ status + ", usercode=" + usercode + ", projectAllocation=" + projectAllocation + ", userClient="
				+ userClient + "]";
	}
	private static final long serialVersionUID = 285701719160134651L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long Id;
	
	@ManyToOne
	@JoinColumn(name="agent_user_role_id")
	private UserRole userRoleAgent;
	
	public UserRole getUserRoleAgent() {
		return userRoleAgent;
	}

	public void setUserRoleAgent(UserRole userRoleAgent) {
		this.userRoleAgent = userRoleAgent;
	}
	@Column(name="project_code_internal") 
	private String projectCodeInternal; 
	public String getProjectCodeInternal() {
		return projectCodeInternal;
	}

	public void setProjectCodeInternal(String projectCodeInternal) {
		this.projectCodeInternal = projectCodeInternal;
	}
	@Column(name="project_code") 
	private String projectCode; 
	
	@Column(name="project_name")
	private String projectName; 
	
	@Column(name="project_desc")
	private String projectDesc;

	@Column(name="required_skils")
	private String requiredSkils;
	
	@Column(name="project_cost")
	private Double projectCost; 
	@Column(name="currency")
	private String currency;
	
	
	@Column(name="start_date") 
	private Date startDate; 

	@Column(name="end_date")
	private Date endDate; 
	
	@Column(name="project_status")
	private String projectStatus; 

	@ManyToOne
	@JoinColumn(name="created_by")
	private User createdBy;

	@Column(name="created_on")
	private Date createdOn;

	@ManyToOne
	@JoinColumn(name="updated_by")
	private User updatedBy;

	@Column(name="updated_on")
	private Date updatedOn;

	@Column(name="status")
	private String status;

	@Column(name="freequency")
	private String paymentFrequency;

	
	@Transient
	private String usercode;
	
	@ManyToOne
	@JoinColumn(name="project_allocation_id")
	private ProjectAllocation projectAllocation;
	
	@ManyToOne
	@JoinColumn(name="client_user_id")
	private User userClient;
	
	
	@Transient
	private String startDateStr; 

	@Transient
	private String endDateStr;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getProjectCode() {
		return projectCode;
	}

	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectDesc() {
		return projectDesc;
	}

	public void setProjectDesc(String projectDesc) {
		this.projectDesc = projectDesc;
	}

	public String getRequiredSkils() {
		return requiredSkils;
	}

	public void setRequiredSkils(String requiredSkils) {
		this.requiredSkils = requiredSkils;
	}

	public Double getProjectCost() {
		return projectCost;
	}

	public void setProjectCost(Double projectCost) {
		this.projectCost = projectCost;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getProjectStatus() {
		return projectStatus;
	}

	public void setProjectStatus(String projectStatus) {
		this.projectStatus = projectStatus;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPaymentFrequency() {
		return paymentFrequency;
	}

	public void setPaymentFrequency(String paymentFrequency) {
		this.paymentFrequency = paymentFrequency;
	}

	public String getUsercode() {
		return usercode;
	}

	public void setUsercode(String usercode) {
		this.usercode = usercode;
	}

	public ProjectAllocation getProjectAllocation() {
		return projectAllocation;
	}

	public void setProjectAllocation(ProjectAllocation projectAllocation) {
		this.projectAllocation = projectAllocation;
	}

	public User getUserClient() {
		return userClient;
	}

	public void setUserClient(User userClient) {
		this.userClient = userClient;
	}

	public String getStartDateStr() {
		return startDateStr;
	}

	public void setStartDateStr(String startDateStr) {
		this.startDateStr = startDateStr;
	}

	public String getEndDateStr() {
		return endDateStr;
	}

	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	} 
	
	

} 
