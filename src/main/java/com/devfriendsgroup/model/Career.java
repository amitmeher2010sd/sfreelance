package com.devfriendsgroup.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="career", schema = "public")
public class Career extends BaseCommon   implements Serializable{
	private static final long serialVersionUID = 285701719160134651L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long Id;
	
	@Column(name="logo")
	private String logo;
	
	@Column(name="title")
	private String title; 
	
	@Column(name="location")
	private String location; 
	
	@Column(name="totalvacancy")
	private String totalvacancy; 
	@Column(name="experience")
	private String experience; 
	@Column(name="published") 
	private boolean published;
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getTotalvacancy() {
		return totalvacancy;
	}
	public void setTotalvacancy(String totalvacancy) {
		this.totalvacancy = totalvacancy;
	}
	public boolean isPublished() {
		return published;
	}
	public void setPublished(boolean published) {
		this.published = published;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public String toString() {
		return "Career [Id=" + Id + ", logo=" + logo + ", title=" + title + ", location=" + location + ", totalvacancy="
				+ totalvacancy + ", published=" + published + "]";
	} 
	@Transient
	private String publishedStr;
	public String getExperience() {
		return experience;
	}
	public void setExperience(String experience) {
		this.experience = experience;
	}
	public String getPublishedStr() {
		return publishedStr;
	}
	public void setPublishedStr(String publishedStr) {
		this.publishedStr = publishedStr;
	} 
	
	
	
} 
	