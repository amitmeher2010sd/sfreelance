package com.devfriendsgroup.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;


//@Data
@Entity
@Table(name="post", schema = "public")
public class Post extends BaseCommon   implements Serializable{
	private static final long serialVersionUID = 285701719160134651L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long Id;
	
	@Column(name="title")
	private String title;
	
	@Column(name="posteddate")
	private Date posteddate; 
	
	@Column(name="skills")
	private String skills; 
	
	@Column(name="description")
	private String description; 
	@Column(name="status") 
	private String status; 
	
	@ManyToOne
	@JoinColumn(name="created_by")
	@JsonIgnore
	private User createdBy;
	
	@Column(name="created_on")
	private Date createdOn;
		
	@ManyToOne
	@JoinColumn(name="updated_by")
	@JsonIgnore
	private User updatedBy;
	
	@Column(name="updated_on")
	private Date updatedOn; 
	@Column(name="posted")
	private boolean posted; 
	
	
public Long getId() {
		return Id;
	}


	public void setId(Long id) {
		Id = id;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public Date getPosteddate() {
		return posteddate;
	}


	public void setPosteddate(Date posteddate) {
		this.posteddate = posteddate;
	}


	public String getSkills() {
		return skills;
	}


	public void setSkills(String skills) {
		this.skills = skills;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public User getCreatedBy() {
		return createdBy;
	}


	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}


	public Date getCreatedOn() {
		return createdOn;
	}


	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}


	public User getUpdatedBy() {
		return updatedBy;
	}


	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}


	public Date getUpdatedOn() {
		return updatedOn;
	}


	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}


	public boolean isPosted() {
		return posted;
	}


	public void setPosted(boolean posted) {
		this.posted = posted;
	}


	public String getPosteddateStr() {
		return posteddateStr;
	}


	public void setPosteddateStr(String posteddateStr) {
		this.posteddateStr = posteddateStr;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


@Transient
private String posteddateStr;
} 
	