package com.devfriendsgroup.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="contactinfo_config", schema = "public")
public class ContactinfoConfig    implements Serializable{
	private static final long serialVersionUID = 285701719160134651L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long Id;
	
	@Column(name="address")
	private String address;
	
	@Column(name="phoneno")
	private String phoneno; 
	
	@Column(name="mail")
	private String mail; 
	
	@Column(name="fax")
	private String fax; 
	
	@Column(name="whatsappno")
	private String whatsappno; 
	
	@Column(name="whatsapplink")
	private String whatsapplink; 
	
	@Column(name="other_param_1")
	private String param1;
	
	@Column(name="other_param_2")
	private String param2; 
	
	@Column(name="other_param_3")
	private String param3; 
	
	@Column(name="other_param_4")
	private String param4; 
} 
	