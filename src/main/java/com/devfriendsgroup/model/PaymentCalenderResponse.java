package com.devfriendsgroup.model;



public class PaymentCalenderResponse {
	//title: 'Birthday Party',start: new Date(y, m, d, 19, 0),end: new Date(y, m, d, 22, 30),allDay: false,url:
public String title;
public String start;
//public String end;
public String allDay;
public String url;
public String desc;

public PaymentCalenderResponse() {
	
}
public PaymentCalenderResponse(String title, String start, String allDay, String url, String desc) {
	this.title=title;
	this.start=start;
	this.allDay=allDay;
	this.url=url;
	this.desc=desc;
}
public String getTitle() {
	return title;
}
public void setTitle(String title) {
	this.title = title;
}
public String getStart() {
	return start;
}
public void setStart(String start) {
	this.start = start;
}
public String getAllDay() {
	return allDay;
}
public void setAllDay(String allDay) {
	this.allDay = allDay;
}
public String getUrl() {
	return url;
}
public void setUrl(String url) {
	this.url = url;
}
public String getDesc() {
	return desc;
}
public void setDesc(String desc) {
	this.desc = desc;
}



}
