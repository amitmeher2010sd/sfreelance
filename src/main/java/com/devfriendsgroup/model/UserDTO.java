package com.devfriendsgroup.model;

import java.util.List;

import lombok.Data;
import lombok.ToString;
@ToString
@Data
public class UserDTO {
	private UserInfo userInfo;
	private UserRole userRole;
	private List<Project>  project;
	private List<ProjectPayment>  payments;
	public UserInfo getUserInfo() {
		return userInfo;
	}
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	public UserRole getUserRole() {
		return userRole;
	}
	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}
	public List<Project> getProject() {
		return project;
	}
	public void setProject(List<Project> project) {
		this.project = project;
	}
	public List<ProjectPayment> getPayments() {
		return payments;
	}
	public void setPayments(List<ProjectPayment> payments) {
		this.payments = payments;
	}
	
}
