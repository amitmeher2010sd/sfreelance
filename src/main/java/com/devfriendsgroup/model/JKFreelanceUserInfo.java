package com.devfriendsgroup.model;
import java.util.Date;



import lombok.AllArgsConstructor;
import lombok.Data;
@Data
@AllArgsConstructor
public class JKFreelanceUserInfo {
	public JKFreelanceUserInfo() {
		// TODO Auto-generated constructor stub
	}
	private Long id;
	private JKFreelanceUser user;
	public String userCode;
	public String dateofbirth;
	public String companyName;
	public String dateofFounded;

	public String address;
	public String description;
	public String secondaryphone;
	public String website;
	public String facebook;
	public String twitter;
	public String linkedin;
	public String designation;
	public String primaryskills;
	public String secondaryskills;
	public String experience;
	private Date createdOn;
	private String status;
	private String message;

	private String gender;

	private String profilephoto;

	private String cvfiles;
	private String idsfiles;

	private String syllabusfiles;
	
	private String ids;
	private String cvs;

	private String[] primarySkillArray;
	private String[] secondaryskillsArray;

	private String parentUsercode;

	private String supportmode;
	private String supporttype;
	private String supporthour;
	private String jobsupporttimeslot;
	private String othertimeslot;
	private String bankmodetype;
	private String bankmodenumber;
	private String accountsholdername;
	private String accountsifsc;
	private String accountsbankname;
	private String accountsaccno;
	private String packagename;
	private String otherskills;
	private String profileverification;
	private boolean hideMyPersonalData;
	private String perHours;
	private String name;
	private String username;
	private String password;
	private String email;
	private String phone;
	private String statusNew;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public JKFreelanceUser getUser() {
		return user;
	}
	public void setUser(JKFreelanceUser user) {
		this.user = user;
	}
	public String getUserCode() {
		return userCode;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	public String getDateofbirth() {
		return dateofbirth;
	}
	public void setDateofbirth(String dateofbirth) {
		this.dateofbirth = dateofbirth;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getDateofFounded() {
		return dateofFounded;
	}
	public void setDateofFounded(String dateofFounded) {
		this.dateofFounded = dateofFounded;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getSecondaryphone() {
		return secondaryphone;
	}
	public void setSecondaryphone(String secondaryphone) {
		this.secondaryphone = secondaryphone;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public String getFacebook() {
		return facebook;
	}
	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}
	public String getTwitter() {
		return twitter;
	}
	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}
	public String getLinkedin() {
		return linkedin;
	}
	public void setLinkedin(String linkedin) {
		this.linkedin = linkedin;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getPrimaryskills() {
		return primaryskills;
	}
	public void setPrimaryskills(String primaryskills) {
		this.primaryskills = primaryskills;
	}
	public String getSecondaryskills() {
		return secondaryskills;
	}
	public void setSecondaryskills(String secondaryskills) {
		this.secondaryskills = secondaryskills;
	}
	public String getExperience() {
		return experience;
	}
	public void setExperience(String experience) {
		this.experience = experience;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getProfilephoto() {
		return profilephoto;
	}
	public void setProfilephoto(String profilephoto) {
		this.profilephoto = profilephoto;
	}
	public String getCvfiles() {
		return cvfiles;
	}
	public void setCvfiles(String cvfiles) {
		this.cvfiles = cvfiles;
	}
	public String getIdsfiles() {
		return idsfiles;
	}
	public void setIdsfiles(String idsfiles) {
		this.idsfiles = idsfiles;
	}
	public String getSyllabusfiles() {
		return syllabusfiles;
	}
	public void setSyllabusfiles(String syllabusfiles) {
		this.syllabusfiles = syllabusfiles;
	}
	public String getIds() {
		return ids;
	}
	public void setIds(String ids) {
		this.ids = ids;
	}
	public String getCvs() {
		return cvs;
	}
	public void setCvs(String cvs) {
		this.cvs = cvs;
	}
	public String[] getPrimarySkillArray() {
		return primarySkillArray;
	}
	public void setPrimarySkillArray(String[] primarySkillArray) {
		this.primarySkillArray = primarySkillArray;
	}
	public String[] getSecondaryskillsArray() {
		return secondaryskillsArray;
	}
	public void setSecondaryskillsArray(String[] secondaryskillsArray) {
		this.secondaryskillsArray = secondaryskillsArray;
	}
	public String getParentUsercode() {
		return parentUsercode;
	}
	public void setParentUsercode(String parentUsercode) {
		this.parentUsercode = parentUsercode;
	}
	public String getSupportmode() {
		return supportmode;
	}
	public void setSupportmode(String supportmode) {
		this.supportmode = supportmode;
	}
	public String getSupporttype() {
		return supporttype;
	}
	public void setSupporttype(String supporttype) {
		this.supporttype = supporttype;
	}
	public String getSupporthour() {
		return supporthour;
	}
	public void setSupporthour(String supporthour) {
		this.supporthour = supporthour;
	}
	public String getJobsupporttimeslot() {
		return jobsupporttimeslot;
	}
	public void setJobsupporttimeslot(String jobsupporttimeslot) {
		this.jobsupporttimeslot = jobsupporttimeslot;
	}
	public String getOthertimeslot() {
		return othertimeslot;
	}
	public void setOthertimeslot(String othertimeslot) {
		this.othertimeslot = othertimeslot;
	}
	public String getBankmodetype() {
		return bankmodetype;
	}
	public void setBankmodetype(String bankmodetype) {
		this.bankmodetype = bankmodetype;
	}
	public String getBankmodenumber() {
		return bankmodenumber;
	}
	public void setBankmodenumber(String bankmodenumber) {
		this.bankmodenumber = bankmodenumber;
	}
	public String getAccountsholdername() {
		return accountsholdername;
	}
	public void setAccountsholdername(String accountsholdername) {
		this.accountsholdername = accountsholdername;
	}
	public String getAccountsifsc() {
		return accountsifsc;
	}
	public void setAccountsifsc(String accountsifsc) {
		this.accountsifsc = accountsifsc;
	}
	public String getAccountsbankname() {
		return accountsbankname;
	}
	public void setAccountsbankname(String accountsbankname) {
		this.accountsbankname = accountsbankname;
	}
	public String getAccountsaccno() {
		return accountsaccno;
	}
	public void setAccountsaccno(String accountsaccno) {
		this.accountsaccno = accountsaccno;
	}
	public String getPackagename() {
		return packagename;
	}
	public void setPackagename(String packagename) {
		this.packagename = packagename;
	}
	public String getOtherskills() {
		return otherskills;
	}
	public void setOtherskills(String otherskills) {
		this.otherskills = otherskills;
	}
	public String getProfileverification() {
		return profileverification;
	}
	public void setProfileverification(String profileverification) {
		this.profileverification = profileverification;
	}
	public boolean isHideMyPersonalData() {
		return hideMyPersonalData;
	}
	public void setHideMyPersonalData(boolean hideMyPersonalData) {
		this.hideMyPersonalData = hideMyPersonalData;
	}
	public String getPerHours() {
		return perHours;
	}
	public void setPerHours(String perHours) {
		this.perHours = perHours;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getStatusNew() {
		return statusNew;
	}
	public void setStatusNew(String statusNew) {
		this.statusNew = statusNew;
	}
	
	
	
}
