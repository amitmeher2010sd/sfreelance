package com.devfriendsgroup.model;



import lombok.Data;
import lombok.ToString;
@Data
public class PaymentstatuschangeDTP {
	
	public String projectcodeStatusChange;
	public String paymentDateStrM;
	public String remarks; 
	public String pstatus;
	public Long paymentId;
	public Double paymentAmount_;
	public String currency_;
	public String endDateStr_;
	public String startDateStr_;
	public String getProjectcodeStatusChange() {
		return projectcodeStatusChange;
	}
	public void setProjectcodeStatusChange(String projectcodeStatusChange) {
		this.projectcodeStatusChange = projectcodeStatusChange;
	}
	public String getPaymentDateStrM() {
		return paymentDateStrM;
	}
	public void setPaymentDateStrM(String paymentDateStrM) {
		this.paymentDateStrM = paymentDateStrM;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getPstatus() {
		return pstatus;
	}
	public void setPstatus(String pstatus) {
		this.pstatus = pstatus;
	}
	public Long getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(Long paymentId) {
		this.paymentId = paymentId;
	}
	public Double getPaymentAmount_() {
		return paymentAmount_;
	}
	public void setPaymentAmount_(Double paymentAmount_) {
		this.paymentAmount_ = paymentAmount_;
	}
	public String getCurrency_() {
		return currency_;
	}
	public void setCurrency_(String currency_) {
		this.currency_ = currency_;
	}
	public String getEndDateStr_() {
		return endDateStr_;
	}
	public void setEndDateStr_(String endDateStr_) {
		this.endDateStr_ = endDateStr_;
	}
	public String getStartDateStr_() {
		return startDateStr_;
	}
	public void setStartDateStr_(String startDateStr_) {
		this.startDateStr_ = startDateStr_;
	}
	
	
	
}
