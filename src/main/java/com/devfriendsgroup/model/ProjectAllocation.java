package com.devfriendsgroup.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;
//@Data
@Entity
@Table(name="project_allocation", schema = "public")
public class ProjectAllocation  implements Serializable {
	
	
	private static final long serialVersionUID = 285701719160134651L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long Id;
	
	@ManyToOne
	@JoinColumn(name="project_id")
	private Project project;
	
	@Column(name="start_date")
	private Date startDate; 

	@Column(name="end_date")
	private Date endDate;
	
	@Column(name="revise_end_date")
	private Date reviseEndDate; 
	
	
	@ManyToOne
	@JoinColumn(name="freelancer_user_id")
	private User userFreelancer;
	
	@Column(name="freelancer_cost")
	private Double freelancerCost;	
	
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	@Column(name="currency")
	private String currency; 
	@Column(name="project_status")
	private String projectStatus; 
	
	@ManyToOne
	@JoinColumn(name="created_by")
	private User createdBy;

	@Column(name="created_on")
	private Date createdOn;

	@ManyToOne
	@JoinColumn(name="updated_by")
	private User updatedBy;

	@Column(name="updated_on")
	private Date updatedOn;

	@Column(name="status")
	private String status;
	@Column(name="payment_frequency_debit")
	private String paymentFrequencyDebit;
	@Column(name="payment_frequency_credit")
	private String paymentFrequencyCredit;
	
	@Column(name="payment_frequency_payment")
	private String paymentFrequencyPayment;
	
	@Column(name="whatsapp_group")
	private String whatsappGroup;
	
	@Column(name="client_pay_advance")
	private boolean clientPayAdvance;
	
	@Column(name="freelancer_pay_advance")
	private boolean freelancerPaidAdvance;
	
	
	
	
	@Transient
	private String usercode;
	@Transient
	private Long projectId;
	
	@Transient
	private String startDateStr; 

	@Transient
	private String endDateStr;

	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public Project getProject() {
		return project;
	}
	public void setProject(Project project) {
		this.project = project;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Date getReviseEndDate() {
		return reviseEndDate;
	}
	public void setReviseEndDate(Date reviseEndDate) {
		this.reviseEndDate = reviseEndDate;
	}
	public User getUserFreelancer() {
		return userFreelancer;
	}
	public void setUserFreelancer(User userFreelancer) {
		this.userFreelancer = userFreelancer;
	}
	public Double getFreelancerCost() {
		return freelancerCost;
	}
	public void setFreelancerCost(Double freelancerCost) {
		this.freelancerCost = freelancerCost;
	}
	public String getProjectStatus() {
		return projectStatus;
	}
	public void setProjectStatus(String projectStatus) {
		this.projectStatus = projectStatus;
	}
	public User getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public User getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPaymentFrequencyDebit() {
		return paymentFrequencyDebit;
	}
	public void setPaymentFrequencyDebit(String paymentFrequencyDebit) {
		this.paymentFrequencyDebit = paymentFrequencyDebit;
	}
	public String getPaymentFrequencyCredit() {
		return paymentFrequencyCredit;
	}
	public void setPaymentFrequencyCredit(String paymentFrequencyCredit) {
		this.paymentFrequencyCredit = paymentFrequencyCredit;
	}
	public String getPaymentFrequencyPayment() {
		return paymentFrequencyPayment;
	}
	public void setPaymentFrequencyPayment(String paymentFrequencyPayment) {
		this.paymentFrequencyPayment = paymentFrequencyPayment;
	}
	public String getWhatsappGroup() {
		return whatsappGroup;
	}
	public void setWhatsappGroup(String whatsappGroup) {
		this.whatsappGroup = whatsappGroup;
	}
	public boolean isClientPayAdvance() {
		return clientPayAdvance;
	}
	public void setClientPayAdvance(boolean clientPayAdvance) {
		this.clientPayAdvance = clientPayAdvance;
	}
	public boolean isFreelancerPaidAdvance() {
		return freelancerPaidAdvance;
	}
	public void setFreelancerPaidAdvance(boolean freelancerPaidAdvance) {
		this.freelancerPaidAdvance = freelancerPaidAdvance;
	}
	public String getUsercode() {
		return usercode;
	}
	public void setUsercode(String usercode) {
		this.usercode = usercode;
	}
	public Long getProjectId() {
		return projectId;
	}
	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}
	public String getStartDateStr() {
		return startDateStr;
	}
	public void setStartDateStr(String startDateStr) {
		this.startDateStr = startDateStr;
	}
	public String getEndDateStr() {
		return endDateStr;
	}
	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	} 

	
} 
