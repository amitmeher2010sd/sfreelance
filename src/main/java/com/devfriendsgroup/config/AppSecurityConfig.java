package com.devfriendsgroup.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.session.HttpSessionEventPublisher;

import com.devfriendsgroup.service.UserDetailsServiceImpl;

@EnableWebSecurity
@Configuration
public class AppSecurityConfig extends WebSecurityConfigurerAdapter {
	 private static final String[] WHITELIST_ADMIN = {"/admin/**"};
	 private static final String[] WHITELIST_CONSULTANCY = {"/S/**"};
	
	@Autowired
	private UserDetailsServiceImpl userDetailsServiceImpl;

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
		return bCryptPasswordEncoder;
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsServiceImpl).passwordEncoder(passwordEncoder());
	}

	@Bean
	public HttpSessionEventPublisher httpSessionEventPublisher() {
		return new HttpSessionEventPublisher();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http.authorizeRequests()
			.antMatchers("/static/**").permitAll()
			.antMatchers("/user-login").permitAll()
			.antMatchers("/user-reg").permitAll()
			.antMatchers("/link").permitAll()	
			//.antMatchers("/admin/**","/user/cst","/user/cst").hasAnyAuthority("ADMIN") 
			
			
			
			//This URL can be accessed by specified role based loggedin user.  
			/*.antMatchers("/dashBoard.htm").authenticated()*/  //This URL can be accessed by any loggedin user.   
				.and().formLogin()
				.loginPage("/user-login").failureUrl("/login-fail").defaultSuccessUrl("/successpage")
					.usernameParameter("username").passwordParameter("password").permitAll().and().logout()
				.logoutUrl("/logout").invalidateHttpSession(true).deleteCookies("JSESSIONID")
					.logoutSuccessUrl("/user-login").and().httpBasic();
		http.sessionManagement().sessionFixation().newSession();
		http.exceptionHandling().accessDeniedPage("/access-denied"); 
		
	}

}
	