<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<script src="${contextPath}/static/eMartBharat/js/bootbox.min.js"></script>
  <!-- dashboard Start -->
<div class="vd_content-wrapper">
   <div class="vd_container">
      <div class="vd_content clearfix">
         <div class="vd_head-section clearfix">
            <div class="vd_panel-header">
               <ul class="breadcrumb">
                  <li><a href="index.html">Home</a> </li>
                  <li class="active">Dashboard</li>
               </ul>
               <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step=5  data-position="left">
                  <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
                  <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
                  <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>
               </div>
            </div>
         </div>
         <!-- vd_head-section -->
         <div class="vd_title-section clearfix">
            <div class="vd_panel-header">
               <h1>Welcome ${name},</h1>              
               <div class="vd_panel-menu  hidden-xs">
                  <div class="menu no-bg vd_red" data-original-title="Start Layout Tour Guide" data-toggle="tooltip" data-placement="bottom" onClick="javascript:introJs().setOption('showBullets', false).start();"> <span class="menu-icon font-md"><i class="fa fa-question-circle"></i></span> </div>
                  <!-- menu -->
                  <div class="menu">
                     <div data-action="click-trigger"> <span class="menu-icon mgr-10"><i class="fa fa-filter"></i></span>Filter <i class="fa fa-angle-down"></i> </div>
                     <div class="vd_mega-menu-content width-xs-2 left-xs" data-action="click-target">
                        <div class="child-menu">
                           <div class="content-list content-menu">
                              <ul class="list-wrapper pd-lr-10">
                                 <li>
                                    <a href="${contextPath}/home">
                                       <div class="menu-icon"><i class=" fa fa-user"></i></div>
                                       <div class="menu-text">Default Dashboard</div>
                                    </a>
                                 </li>
                                 <li>
                                     <a href="${contextPath}/payment-dashboard">
                                       <div class="menu-icon"><i class=" fa fa-user"></i></div>
                                       <div class="menu-text">Payment Dashboard</div>
                                    </a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- menu --> 
               </div>
               <!-- vd_panel-menu --> 
            </div>
            <!-- vd_panel-header --> 
         </div>
         <!-- vd_title-section -->
         <div class="vd_content-section clearfix">
            <div class="row">
               <div class="col-md-7 mgbt-md-20 mgbt-lg-0">
                  <div class="panel vd_interactive-widget light-widget widget">
                     <div class="panel-body-list">
                        <div class="vd_panel-menu">
                           <div data-action="refresh" onClick="loadRevinueGraph();" class="menu entypo-icon smaller-font" data-placement="bottom" data-toggle="tooltip" data-original-title="Refresh"> <i class="icon-cycle"></i> </div>
                           <div class="menu entypo-icon smaller-font" data-placement="bottom" data-toggle="tooltip" data-original-title="Config">
                              <div data-action="click-trigger" class="menu-trigger"> <i class="icon-cog"></i> </div>
                              <div class="vd_mega-menu-content  width-xs-2  left-xs" data-action="click-target">
                                 <div class="child-menu">
                                    <div class="content-list content-menu">
                                       <ul class="list-wrapper pd-lr-10">
                                          <li>
                                             <a href="#">
                                                <div class="menu-icon"><i class=" fa fa-user"></i></div>
                                                <div class="menu-text">Panel Menu</div>
                                             </a>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="menu entypo-icon" data-placement="bottom" data-toggle="tooltip" data-original-title="Close" data-action="close"> <i class="icon-cross"></i> </div>
                        </div>
                        <!-- vd_panel-menu --> 
                        <div class="pd-20">
                           <h5 class="mgbt-xs-20 mgtp-20"><span class="menu-icon append-icon"><i class="icon-graph"></i></span> <strong>Revenue Vs Weekly Vs Month Graph</strong> (Current Month) </h5>
                           <div id="revenue-line-chart" style="height:255px; "></div>
                        </div>
                        
                        <div class="row mgbt-xs-0">
                           <div class="col-sm-4 mgbt-xs-15 mgbt-sm-0">
                              <div class="item-left pd-15">
                                 <div id="revenuegraphchart" style="height:100px;"></div>
                                 <div id="revenuebarchartweeky" style="height: 350px;width:200px;" > </div>
                              </div>
                           </div>
                           <div class="col-sm-8">                             
                               <div class="item-right  pd-15">
                               <br><br>
                                 <div id="revenuebarchart" style="height: 400px;" > </div>
                              </div>
                           </div>
                            
                        </div>
                        
                     </div>
                  </div>
                  <!-- Panel Widget -->               
               </div>
               <!--col-md-7 -->
               <div class="col-md-5">
                  <div class="row">                     
	                      <div class="col-md-6">
	                        <div class="vd_status-widget vd_bg-green widget">
	                           <div class="vd_panel-menu">
	                              <div data-action="refresh" data-original-title="Refresh" data-rel="tooltip" class=" menu entypo-icon smaller-font"> <i class="icon-cycle"></i> </div>
	                           </div>
	                           <!-- vd_panel-menu --> 
	                           <a class="panel-body"  href="${contextPath}/users/f">
	                              <div class="clearfix">
	                                 <span class="menu-icon">
	                                 <i class="icon-users"></i>
	                                 </span> 
	                                 <span class="menu-value">
	                                 ${dashBoardResponse.totalFreelancerUser} </br> 
	                                 </span>  
	                              </div>
	                              <div class="menu-text clearfix">
	                                 Total Freelancers
	                              </div>
	                           </a>
	                        </div>
	                     </div>
	                     <!--col-md-6 --> 
	                     <!--col-md-6 --> 
	                      <div class="col-md-6">
	                        <div class="vd_status-widget vd_bg-green widget">
	                           <div class="vd_panel-menu">
	                              <div data-action="refresh" data-original-title="Refresh" data-rel="tooltip" class=" menu entypo-icon smaller-font"> <i class="icon-cycle"></i> </div>
	                           </div>
	                           <!-- vd_panel-menu --> 
	                            <a class="panel-body"  href="${contextPath}/users/c">
	                              <div class="clearfix">
	                                 <span class="menu-icon">
	                                 <i class="icon-users"></i>
	                                 </span> 
	                                 <span class="menu-value">
	                                 ${dashBoardResponse.totalClientUser} </br> 
	                                 </span>  
	                              </div>
	                              <div class="menu-text clearfix">
	                                 Total Clients
	                              </div>
	                           </a>
	                        </div>
	                     </div>
	                     <!--col-md-6 --> 
                  </div>
                  <!-- .row -->
                  <div class="row">
	                     <div class="col-xs-6">
	                        <div class="vd_status-widget vd_bg-red  widget">
	                           <div class="vd_panel-menu">
	                              <div data-action="refresh" data-original-title="Refresh" data-rel="tooltip" class=" menu entypo-icon smaller-font"> <i class="icon-cycle"></i> </div>
	                           </div>
	                           <!-- vd_panel-menu --> 
	                                                     
	                            <a class="panel-body"  href="${contextPath}/projects?projectStatus=INPROGRESS">
	                              <div class="clearfix">
	                                 <span class="menu-icon">
	                                 <i class="icon-bars"></i>
	                                 </span> 
	                                 <span class="menu-value">
	                                 ${(dashBoardResponse.totalProject)-(dashBoardResponse.totalClosedProject)} </br> 
	                                 </span>  
	                              </div>
	                              <div class="menu-text clearfix">
	                                 Total Active Projects
	                              </div>
	                           </a>
	                        </div>
	                     </div>
                  
                     <div class="col-xs-6">
                        <div class="vd_status-widget vd_bg-red  widget">
                           <div class="vd_panel-menu">
                              <div data-action="refresh" data-original-title="Refresh" data-rel="tooltip" class=" menu entypo-icon smaller-font"> <i class="icon-cycle"></i> </div>
                           </div>
                           <!-- vd_panel-menu --> 
                                                     
                            <a class="panel-body"  href="${contextPath}/projects">
                              <div class="clearfix">
                                 <span class="menu-icon">
                                 <i class="icon-bars"></i>
                                 </span> 
                                 <span class="menu-value">
                                 ${dashBoardResponse.totalClosedProject} </br> 
                                 </span>  
                              </div>
                              <div class="menu-text clearfix">
                                 Total Closed Projects
                              </div>
                           </a>
                        </div>
                     </div>
                     </div>
                     
                      <!-- .row -->
                  <div class="row">
                     <div class="col-xs-6">
                        <div class="vd_status-widget vd_bg-blue widget">
                           <div class="vd_panel-menu">
                              <div data-action="refresh" data-original-title="Refresh" data-rel="tooltip" class=" menu entypo-icon smaller-font"> <i class="icon-cycle"></i> </div>
                           </div>
                           <!-- vd_panel-menu --> 
                           <a class="panel-body"  href="${contextPath}/">
                              <div class="clearfix">
                                 <span class="menu-icon">
                                 <i class="fa fa-comments"></i>
                                 </span>
                                 <span class="menu-value">
                                 ${dashBoardResponse.totalContractor}
                                 </span>  
                              </div>
                              <div class="menu-text clearfix">
                                 Total Contractors
                              </div>
                           </a>
                        </div>
                     </div>
                     <!--col-xs-6 --> 
                
                      <div class="col-xs-6">
                        <div class="vd_status-widget vd_bg-grey widget">
                           <div class="vd_panel-menu">
                              <div data-action="refresh" data-original-title="Refresh" data-rel="tooltip" class=" menu entypo-icon smaller-font"> <i class="icon-cycle"></i> </div>
                           </div>
                           <!-- vd_panel-menu --> 
                           <a class="panel-body"  href="#">
                              <div class="clearfix">
                                 <span class="menu-icon">
                                 <i class="fa fa-tasks"></i>
                                 </span>
                                 <span class="menu-value">
                                 ${dashBoardResponse.totalContract}
                                 </span>  
                              </div>
                              <div class="menu-text clearfix">
                                 Total Contracts
                              </div>
                           </a>
                        </div>
                     </div>
                     <!--col-md-xs-6 --> 
                  </div>
                  <!-- .row --> 
             
         </div>
         <!-- .vd_content-section --> 
         
         
   
               <!--col-md-7 -->
              <!-- col-md-6 -->
              <div class="col-md-5">
                <div class="panel widget">
                  <div class="panel-body-list  table-responsive">
                    <table class="table no-head-border table-striped">
                      <thead class="vd_bg-blue vd_white">
                        <tr>
                          <th><i class="fa fa-search append-icon"></i>Last 5 Search Terms</th>
                          <th style="width:20px">Results</th>
                          <th style="width:140px">Number of Uses</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Pan</td>
                          <td class="text-center">4</td>
                          <td class="text-center">20</td>
                        </tr>
                        <tr>
                          <td>Fork</td>
                          <td class="text-center">1</td>
                          <td class="text-center">5</td>
                        </tr>
                        <tr>
                          <td>Big Bowl</td>
                          <td class="text-center">7</td>
                          <td class="text-center">23</td>
                        </tr>
                        <tr>
                          <td>Cooking Utensils</td>
                          <td class="text-center">2</td>
                          <td class="text-center">1</td>
                        </tr>
                        <tr>
                          <td>Ketchup Bottle</td>
                          <td class="text-center">3</td>
                          <td class="text-center">12</td>
                        </tr>
                         <tr>
                          <td>Ketchup Bottle</td>
                          <td class="text-center">3</td>
                          <td class="text-center">12</td>
                        </tr>
                         <tr>
                          <td>Ketchup Bottle</td>
                          <td class="text-center">3</td>
                          <td class="text-center">12</td>
                        </tr>
                         <tr>
                          <td>Ketchup Bottle</td>
                          <td class="text-center">3</td>
                          <td class="text-center">12</td>
                        </tr>
                         <tr>
                          <td>Ketchup Bottle</td>
                          <td class="text-center">3</td>
                          <td class="text-center">12</td>
                        </tr>
                      
                      </tbody>
                    </table>
                  </div>
                </div>
              
              </div>
              <!-- col-md-6 --> 
              
              
              
            </div>
          <!-- .row --> 
             
         </div>
         <!-- .vd_content-section --> 
         
         
         
         
         
         
         
         
      </div>
      <!-- .vd_content --> 
   </div>
   <!-- .vd_container --> 
</div>
<!-- .vd_content-wrapper --> 
<!-- dashboard end -->



<div>
<div id="Maildivbar">

</div>

<div id="Maildivbarweekly">

</div>


<!-- Placed at the end of the document so the pages load faster --> 
<script type="text/javascript" src="${contextPath}/static/sFreelance/js/jquery.js"></script> 

<script>
loadRevinueGraph();
	function loadRevinueGraph(){ 	
	$("#Maildivbar").hide();
	$("#Maildivbarweekly").hide();
		
	var year="2024";
		$.ajax({
			type : "GET",
			url : "${contextPath}/revenue-graph?data="+year,
			success : function(response) {	
			
			var lst="";
			var lstweekly="";
				
				var count=1;
				
				$.each(response, function(key, value) {
				
				var data=value.split("-");
				if(13==count){
				var data1=value.split("--");//300S200S100--500S200S300--400S200S200--100S200S-100
				var count1=1;
				$.each(data1, function(k, v) {
				var datav=v.split("S");
					lstweekly=lstweekly+"<div id='divbarweekly"+count1+"'><p class='vd_bg-green'><strong class='mgr-10 mgl-10'>"+count1+"-"+year+"</strong></p><div style='padding: 0 10px 10px;'><div> CREDIT: <strong>"+datav[0]+" INR</strong></div><div> DEBIT: <strong>"+datav[1]+" INR</strong></div><div> PROFIT: <strong>"+datav[2]+" INR</strong></div></div></div>";
		         count1++;
		         })
				
				}
				else{
							lst=lst+"<div id='divbar"+count+"'><p class='vd_bg-green'><strong class='mgr-10 mgl-10'>"+count+"-"+year+"</strong></p><div style='padding: 0 10px 10px;'><div> CREDIT: <strong>"+data[0]+" INR</strong></div><div> DEBIT: <strong>"+data[1]+" INR</strong></div><div> PROFIT: <strong>"+data[2]+" INR</strong></div></div></div>";
					}
    			count++;
				})	
				console.log(lst);
				
				 $("#Maildivbar").html(lst);	
				 $("#Maildivbarweekly").html(lstweekly);
				 	
			}, 
			error : function(error) {
			alert("error===="+error);
			} 
		});
	} 
	  
</script>
<script type="text/javascript">
         $(window).load(function () 
         	{
         		 
         		function showTooltip(x, y, index) {
         		var contents=$("#divbar"+index).html();
         		//alert("index==="+index+"="+contents);
         			$('<div id="tooltip">' + contents + '</div>').css({
         				position: 'absolute',
         				display: 'none',
         				top: y + 5,
         				left: x + 20,    
         				size: '10',  
         //				'border-top' : '3px solid #1FAE66',
         				'background-color': '#111111',
         				color: "#FFFFFF",
         				opacity: 0.85
         			}).appendTo("body").fadeIn(200);
         		}
         function showTooltipWeekly(x, y, index) {
         		var contents=$("#divbarweekly"+index).html();
         		
         			$('<div id="tooltip">' + contents + '</div>').css({
         				position: 'absolute',
         				display: 'none',
         				top: y + 5,
         				left: x + 20,    
         				size: '10',  
    
         				'background-color': '#111111',
         				color: "#FFFFFF",
         				opacity: 0.85
         			}).appendTo("body").fadeIn(200);
         		}
         
         /* REVENUE LINE CHART */
         
         	var d2 = [ [1, 250],
                     [2, 150],
                     [3, 50],
                     [4, 200],
                     [5,50],
                     [6, 150],
                     [7, 150],
                     [8, 200],
                     [9, 100],
                     [10, 250],
                     [11,250],
                     [12, 200],
                     [13, 300]			
         
         ];
         	var d1 = [
         			[1, 650],
                     [2, 550],
                     [3, 450],
                     [4, 550],
                     [5, 350],
                     [6, 500],
                     [7, 600],
                     [8, 450],
                     [9, 300],
                     [10, 600],
                     [11, 400],
                     [12, 500],
                     [13, 700]					
         			
         ];
         	var plot = $.plotAnimator($("#revenue-line-chart"), [
         			{  	label: "Revenue",
         				data: d2, 	
         				lines: {				
         					fill: 0.4,
         					lineWidth: 0,				
         				},
         				color:['#f2be3e']
         			}
         		],{	xaxis: {
         		tickLength: 0,
         		tickDecimals: 0,
         		min:2,
         
         				font :{
         					lineHeight: 13,
         					style: "normal",
         					weight: "bold",
         					family: "sans-serif",
         					variant: "small-caps",
         					color: "#6F7B8A"
         				}
         			},
         			yaxis: {
         				ticks: 3,
                         tickDecimals: 0,
         				tickColor: "#f0f0f0",
         				font :{
         					lineHeight: 13,
         					style: "normal",
         					weight: "bold",
         					family: "sans-serif",
         					variant: "small-caps",
         					color: "#6F7B8A"
         				}
         			},
         			grid: {
         				backgroundColor: { colors: [ "#fff", "#fff" ] },
         				borderWidth:1,borderColor:"#f0f0f0",
         				margin:0,
         				minBorderMargin:0,							
         				labelMargin:20,
         				hoverable: true,
         				clickable: true,
         				mouseActiveRadius:6
         			},
         			legend: { show: false}
         		});
         
         		$(window).on("resize", function(){
         			plot.resize();
         			plot.setupGrid();
         			plot.draw();
         		});
         				
         
         /* REVENUE DONUT CHART */
         	
         		var data2 = [],
         			series = 3;
         		var data2 = [
         			{ label: "CREDIT",  data: 35},
         			{ label: "DEBIT",  data: 35},
         			{ label: "PROFIT",  data: 35 }
         		];
         		var revenuegraphchart_ = $("#revenuegraphchart");
         		
         		$("#revenuegraphchart").bind("plotclick", function (event, pos, item) {
         			if (item) {
         				$("#clickdata").text(" - click point " + item.dataIndex + " in " + item.series.label);
         				plot.highlight(item.series, item.datapoint);
         			}
         		});
         		$.plot(revenuegraphchart_, data2, {
         			series: {
         				pie: { 
         					innerRadius: 0.4,
         					show: true
         				}
         			},
         			grid: {
         				hoverable: true,
         				clickable: true,
         			},
         			colors: ["#F85D2C","#23709e", "#1FAE66 "]				
         		});
         		
         		
         /* REVENUE BAR CHART */	
         	
         		var bar_chart_data = [ ["Jan", 0.4], ["Feb", 0.4], ["Mar", 0.4], ["Apr", 0.4], ["May", 0.4], ["Jun", 0.4], ["Jul", 0.4], ["Aug", 1], ["Sep", 2], ["Oct", 3], ["Nov", 2], ["Dec", 1] ];
         		
                 var bar_chart = $.plot(
                 $("#revenuebarchart"), [{
                     data: bar_chart_data,
          //           color: "rgba(31,174,102, 0.8)",
          			color: "#F85D2C" ,
                     shadowSize: 0,
                     bars: {
                         show: true,
                         lineWidth: 0,
                         fill: true,
                         fillColor: {
                             colors: [{
                                 opacity: 1
                             }, {
                                 opacity: 1
                             }]
                         }
                     }
                 }], {
                     series: {
                         bars: {
                             show: true,
                             barWidth: 0.9,
         					align: "center"
                         }
                     },
                     grid: {
                         show: true,
                         hoverable: true,
                         borderWidth: 0
                     },
                     yaxis: {
                         min: 0,
                         max: 20,
         				show: false
                     },
         			xaxis: {
         				mode: "categories",
         				tickLength: 0,
         				color: "#FFFFFF",				
         			}			
                 });
         		
         	   var previousPoint2 = null;
                $("#revenuebarchart").bind("plothover", function (event, pos, item) {
                     $("#x").text(pos.x.toFixed(2));
                     $("#y").text(pos.y.toFixed(2));
                     if (item) {
                         if (previousPoint2 != item.dataIndex) {
                             previousPoint2 = item.dataIndex;
                             $("#tooltip").remove();
                             var x = item.datapoint[0] + 1,
                                 y = item.datapoint[1].toFixed(2);
                           
         								showTooltip(item.pageX, item.pageY,x);
                         }
                     }
                 });
         
                 $('#revenuebarchart').bind("mouseleave", function () {
                     $("#tooltip").remove();
                 });
                 
                 
                 
                  /* weekly REVENUE BAR CHART */	
         	
         		var bar_chart_dataweeky = [ ["1st", 1], ["2nd", 2], ["3rd", 2], ["4th", 1]];
         		
                 var bar_chartweeky = $.plot(
                 $("#revenuebarchartweeky"), [{
                     data: bar_chart_dataweeky,
          			color: "#23709e" ,
                     shadowSize: 0,
                     bars: {
                         show: true,
                         lineWidth: 0,
                         fill: true,
                         fillColor: {
                             colors: [{
                                 opacity: 1
                             }, {
                                 opacity: 1
                             }]
                         }
                     }
                 }], {
                     series: {
                         bars: {
                             show: true,
                             barWidth: 0.9,
         					align: "center"
                         }
                     },
                     grid: {
                         show: true,
                         hoverable: true,
                         borderWidth: 0
                     },
                     yaxis: {
                         min: 0,
                         max: 20,
         				show: false
                     },
         			xaxis: {
         				mode: "categories",
         				tickLength: 0,
         				color: "#FFFFFF",				
         			}			
                 });
         		
         	   var previousPoint2weeky = null;
                $("#revenuebarchartweeky").bind("plothover", function (event, pos, item) {
                     $("#x").text(pos.x.toFixed(2));
                     $("#y").text(pos.y.toFixed(2));
                     if (item) {
                         if (previousPoint2weeky != item.dataIndex) {
                             previousPoint2weeky = item.dataIndex;
                             $("#tooltip").remove();
                             var x = item.datapoint[0] + 1,
                                 y = item.datapoint[1].toFixed(2);
                           
         								showTooltipWeekly(item.pageX, item.pageY,x);
                         }
                     }
                 });
         
                 $('#revenuebarchartweeky').bind("mouseleave", function () {
                     $("#tooltip").remove();
                 });
         
                 
                 
                 
                 
                 
                 
                 
         
         
         
       
         });
      </script>