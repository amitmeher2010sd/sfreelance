<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<c:if test="${not empty successMessage}">
   <script>
      bootbox.alert("${successMessage}");  
   </script>
</c:if>
<c:if test="${not empty failureMessage}">
   <script>
      bootbox.alert("${failureMessage}"); 
   </script>
</c:if>
<!-- Middle Content Start -->    
<div class="vd_content-wrapper">
   <div class="vd_container">
      <div class="vd_content clearfix">
         <div class="vd_head-section clearfix">
            <div class="vd_panel-header">
               <ul class="breadcrumb">
                  <li><a href="${contextPath}/">Home</a> </li>
                  <li><a href="${contextPath}/">Project Management</a> </li>
                  <li class="active">Project Allocation</li>
               </ul>
               <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step=5  data-position="left">
             <!--      <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
                  <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
                  <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>
         -->       </div>
            </div>
         </div>
         <div class="vd_title-section clearfix">
            <div class="vd_panel-header">
               <h1>Project Allocation</h1>
               <small class="subtitle">Form Basic, Form Advanced</small>
            </div>
         </div>
         <!-- vd_title-section -->
         <div class="vd_content-section clearfix">
            <div class="row" id="advanced-input">
               <div class="col-md-12">
                  <div class="panel widget">
                     <div class="panel-heading vd_bg-grey">
                        <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-bar-chart-o"></i> </span> Project Allocation Details </h3>
                     </div>
                     <div class="panel-body">
                        <form class="form-horizontal"  action="${contextPath}/project-allocation" id="register-form" method="post" modelAttribute="projectAllocation">
                           <input type="hidden" name="${_csrf.parameterName}"
                              value="${_csrf.token}" />
                           <div class="alert alert-danger vd_hidden">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-cross"></i></button>
                              <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Oh snap!</strong> Change a few things up and try submitting again. 
                           </div>
                           <div class="alert alert-success vd_hidden">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-cross"></i></button>
                              <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Well done!</strong>. 
                           </div>
                           <div class="panel-body table-responsive">
                              <div ><strong>Please Select Any Project List (Below): <span class="vd_red">*</span></strong>. </div>
                              <table class="table table-striped" id="data-tables-project">
                                 <thead>
                                    <tr>
                                       <th>select</th>
                                       <th>#</th>
                                       <th>Project Code</th>
                                       <th>Project Name</th>
                                       <th>Required Skils</th>
                                       <th>Start Date</th>
                                       <th>End Date</th>
                                       <th>Project Cost</th>
                                       <th>Project Allocated To</th>
                                       <th>Project Status</th>
                                    </tr>
                                 </thead>
                                 <tbody id="tbd">
                                    <c:forEach items="${projectList}" var="data" varStatus="serialNumber">
                                       <tr style="color:${data.projectStatus ne 'PENDING'?'#15dc15':'black'}">
                                          <td>
                                             <input type="radio" onchange="getProject(this.value);" name="chkproject" value="${data.projectCode}-${data.projectName}[]${data.getId()}">
                                          </td>
                                          <td>${serialNumber.count}</td>
                                          <td>${data.projectCode}</td>
                                          <td>${data.projectName}</td>
                                          <td>${data.requiredSkils}</td>
                                          <td>
                                             <fmt:formatDate type = "both"   value = "${data.startDate}" />
                                          </td>
                                          <td>
                                             <fmt:formatDate type = "both"   value = "${data.endDate}" />
                                          </td>
                                          <td>${data.projectCost}</td>
                                          <td>
                                             <c:choose>
                                                <c:when test="${not empty data.projectAllocation}">
                                                   FREELANCER: ${data.projectAllocation.userFreelancer.name}-${data.projectAllocation.userFreelancer.userName}-${data.projectAllocation.userFreelancer.mobileNo}
                                                   <br>FROM: 
                                                   <fmt:formatDate type = "both"   value = "${data.projectAllocation.startDate}" />
                                                   to 
                                                   <fmt:formatDate type = "both"   value = "${data.projectAllocation.endDate}" />
                                                   <br>Allocation Status: ${data.projectAllocation.projectStatus}
                                                </c:when>
                                                <c:otherwise>														
                                                   Not Allocated
                                                </c:otherwise>
                                             </c:choose>
                                          </td>
                                          <td><span  class="txt_field_green">${data.projectStatus}</span> </td>
                                       </tr>
                                    </c:forEach>
                                 </tbody>
                              </table>
                           </div>
                           <hr>
                           <div class="panel-body table-responsive">
                              <div ><strong>Please Select Any Freelancer List (Below): <span class="vd_red">*</span></strong>. </div>
                              <table class="table table-striped" id="data-tables-user">
                                 <thead>
                                    <tr>
                                       <th>Select</th>
                                        <th>#</th>
                                       <th>Name</th>
                                       <th>User Name</th>
                                       <th>Skills</th>
                                       <th>Mobile Number</th>
                                       <th>Email Id</th>
                                       <th>Status</th>
                                    </tr>
                                 </thead>
                                 <tbody id="tbd">
                                    <c:forEach items="${userInfoList}" var="data" varStatus="serialNumber">
                                       <tr>
                                          <td>
                                             <input type="radio"  onchange="getUser(this.value);" name="chkuser" value="${data.user.name}-${data.user.userName}-${data.user.mobileNo}[]${data.user.userId}">
                                          </td>
                                          <td>${serialNumber.count}</td>
                                          <td>${data.user.name}</td>
                                          <td>${data.user.userName}</td>
                                          <td><span  class="txt_field_green">${data.skills}</span> </td>
                                          <td>${data.user.mobileNo}</td>
                                          <td>${data.user.email}</td>
                                          <td><span  class="txt_field_green">${data.user.status}</span></td>
                                       </tr>
                                    </c:forEach>
                                 </tbody>
                              </table>
                           </div>
                           <hr>
                           <div class="form-group">
                              <label class="col-sm-2 control-label">Selected Project <span class="vd_red">*</span></label>
                              <div class="col-sm-7 controls">
                                 <input type="hidden" class="form-control required" id="projectId"	name="projectId"  maxlength="20">
                                 <input type="text" class="form-control required" id="projectId_"	name="projectId_"  readonly>
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-sm-2 control-label">Selected Freelancer <span class="vd_red">*</span></label>
                              <div class="col-sm-7 controls">
                                 <input type="hidden" class="form-control required" id="freelancerId"	name="userFreelancer.userId"  >
                                 <input type="text" class="form-control required" id="freelancerId_"	name="freelancerId_"  readonly>
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-sm-2 control-label"> Start Date <span class="vd_red">*</span></label>
                              <div class="col-sm-3 controls">
                                 <input type="test" class="form-control ignoreValidation date" id="startDateStr"
                                    name="startDateStr" value="${project.startDate}" >
                              </div>
                              <label class="col-sm-2 control-label"> End Date <span class="vd_red">*</span></label>
                              <div class="col-sm-3 controls">
                                 <input type="test" class="form-control ignoreValidation date" id="endDateStr"
                                    name="endDateStr" value="${project.endDate}" >
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-sm-2 control-label">Payment Frequency (Client)<span class="vd_red"></span> </label>
                              <div class="col-sm-3 controls">
                                 <select class="form-control form-control-sm required" id="paymentFrequencyDebit" name="paymentFrequencyDebit" onchange="getpaymentFrequencyPayment(this.value)">
                                    <option value="">--Select--</option>
                                       <c:forEach items="${paymentFrequencyList}" var="data">
														<option  value="${data}" >${data}</option> 
													</c:forEach>
									
                                 </select>
                              </div>
                              <label class="col-sm-2 control-label">Freelancer Payment <span class="vd_red"></span> </label>
                              <div class="col-sm-3 controls">
                                 <input type="number" step="0.01" class="form-control required" id="freelancerCost" name="freelancerCost" onchange="getpaymentFrequencyPayment(this.value)" 
                                    maxlength="100">(Monthly Payment)
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-sm-2 control-label">Currency <span class="vd_red">*</span> </label>
                              <div class="col-sm-3 controls">
                                 <select name="currency" id="currency" class="form-control populate required">
                                    <option value="">-Select-</option>
                                    <c:forEach items="${currencyList}" var="data">
                                       <option ${data eq userInfo.currency?'selected':''} value="${data}">${data}</option> 
                                    </c:forEach>
                                 </select>
                              </div>
                              <div class="col-sm-1 controls">
                                 <input type="number" readonly step="0.01" value="0.0" class="form-control required" id="paymentFrequencyPayment" name="paymentFrequencyPayment" 
                                    maxlength="100">
                              </div>
                               <label class="col-sm-2 control-label">Payment Frequency (Freelancer)<span class="vd_red"></span> </label>
                              <div class="col-sm-3 controls">
                                 <select class="form-control form-control-sm required" id="paymentFrequencyCredit" name="paymentFrequencyCredit">
                                    <option value="">--Select--</option>
                                <c:forEach items="${paymentFrequencyList}" var="data">
														<option  value="${data}" >${data}</option> 
													</c:forEach>
                                 </select>
                              </div>
                           </div>
                            <div class="form-group">
                              <label class="col-sm-2 control-label"> WhatsApp Group <span class="vd_red">*</span></label>
                              <div class="col-sm-2 controls">
                                 <input type="test" class="form-control " id="whatsappGroup"
                                    name="whatsappGroup" value="${project.whatsappGroup}" >
                              </div>   
                               <label class="col-sm-2 control-label">Is Freelancer Take Advance Pay ?<span class="vd_red">*</span></label>
                              <div class="col-sm-1 controls">
                                 <input type="checkbox" class="form-control " id="freelancerPaidAdvance" style="width: 18px;"
                                    name="freelancerPaidAdvance" >
                              </div>  
                               <label class="col-sm-2 control-label">Is Client Give Advance Pay?<span class="vd_red">*</span></label>
                              <div class="col-sm-1 controls">
                                 <input type="checkbox"  class="form-control " id="clientPayAdvance" style="width: 18px;"
                                    name="clientPayAdvance" >
                              </div>                            
                           </div>
                           <div class="form-group form-actions">
                              <div class="col-sm-4"> </div>
                              <div class="col-sm-7">
                                 <button class="btn vd_btn vd_bg-green vd_white" type="submit"><i class="icon-ok"></i> Save</button>
                                 <a href="${contextPath}/" class="btn vd_btn vd_bg-grey" type="button" >Back</a>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
                  <!-- Panel Widget --> 
               </div>
               <!-- col-md-12 --> 
            </div>
            <!-- row -->
         </div>
         <!-- .vd_content-section --> 
      </div>
      <!-- .vd_content --> 
   </div>
   <!-- .vd_container --> 
</div>
<!-- .vd_content-wrapper --> 
<!-- Middle Content End --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script type="text/javascript" src="${contextPath}/static/sFreelance/js/jquery.js"></script> 
<%-- <%@include file="/WEB-INF/views/layout/common.jsp" %> --%>
<script type="text/javascript">
   $(document).ready(function() {
   		"use strict";	
   
           var form_register = $('#register-form');
           var error_register = $('.alert-danger', form_register);
           var success_register = $('.alert-success', form_register);
   
           form_register.validate({
               errorElement: 'div', //default input error message container
               errorClass: 'vd_red', // default input error message class
               focusInvalid: false, // do not focus the last invalid input
               ignore: ".ignoreValidation",
               rules: {
                  
                    projectId: {
                       required: true,                 
                   },
                    projectId_: {
                       required: true,                 
                   },
                    freelancerId: {
                       required: true,                 
                   },
                   freelancerId_: {
                       required: true,                 
                   },               
                  freelancerCost: {
                       required: true,                 
                   },
                    paymentFrequency: {
                       required: true,                 
                   },
                   
               },
   			
   
             	errorPlacement: function(error, element) {
   				if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
   					element.parent().append(error);
   				} else if (element.parent().hasClass("vd_input-wrapper")){
   					error.insertAfter(element.parent());
   				}else {
   					error.insertAfter(element);
   				}
   			}, 
   			
               invalidHandler: function (event, validator) { //display error alert on form submit              
   					success_register.fadeOut();
   					error_register.fadeIn();
   					scrollTo(form_register,-100);
   
               },
   
               highlight: function (element) { // hightlight error inputs
   		
   				$(element).addClass('vd_bd-red');
   				$(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
   
               },
   
               unhighlight: function (element) { // revert the change dony by hightlight
                   $(element)
                       .closest('.control-group').removeClass('error'); // set error class to the control group
               },
   
               success: function (label, element) {
                   label
                       .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                   	.closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
   				$(element).removeClass('vd_bd-red');
   				
               },
   
               submitHandler: function (form) {
   					success_register.fadeIn();
   					error_register.fadeOut();
   					scrollTo(form_register,-100);	
   					$('#register-form').submit();				
               }
           });
        $('#data-tables-user').dataTable({
           scrollY: '400px',
           scrollCollapse: true,
           paging: false,
      
   });
      $('#data-tables-project').dataTable({
           scrollY: '400px',
           scrollCollapse: true,
           paging: false,
      
   });
   });
   function getUser(fid){
   const fidArr = fid.split("[]");
   $('#freelancerId').val(fidArr[1]);
    $('#freelancerId_').val(fidArr[0]);
   }
   function getProject(pid){
   const pidArr = pid.split("[]");
   $('#projectId').val(pidArr[1]);
    $('#projectId_').val(pidArr[0]);
   
   }
   function getpaymentFrequencyPayment(){
    
   var v=document.getElementById("paymentFrequencyDebit").value;
   	var amount=0.0;	
   	var pay=parseFloat(document.getElementById("freelancerCost").value);						
   	if(v=="Bi-Weekly")
   	{
   		amount=pay/2;
   	}
   	else if(v=="Tri-Weekly")
   	{
   		amount=pay/3;
   	}
   	else if(v=="Monthly")
   	{
   		amount=pay/1;
   	}
   	else if(v=="Weekly")
   	{
   		amount=pay/4;
   	}
   	else if(v=="Hourly")
   	{
   		amount=pay;
   	}
   	
   	
   	document.getElementById("paymentFrequencyPayment").value=amount;
   }
   
</script>