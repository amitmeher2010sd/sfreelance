<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<!-- Middle Content Start -->
<div class="vd_content-wrapper">
   <div class="vd_container">
      <div class="vd_content clearfix">
         <div class="vd_head-section clearfix">
            <div class="vd_panel-header">
               <ul class="breadcrumb">
                  <li><a href="${contextPath}/">Home</a> </li>
                  <li><a href="${contextPath}/">Project Management</a> </li>
                  <li class="active">Manage Roles</li>
               </ul>
            </div>
         </div>
         <div class="vd_title-section clearfix">
            <div class="vd_panel-header">
               <h1>Manage Project</h1>
               <small class="subtitle">Look <a href="http://www.datatables.net">below</a> for more information</small> 
            </div>
         </div>
         <div class="vd_content-section clearfix">
            <div class="row">
               <div class="col-md-12">
                  <div class="panel widget">
                     <c:if test="${not empty successMessage}">
                        <div class="alert alert-success alert-dismissable">
                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cross"></i></button>
                           <i class="fa fa-check-circle append-icon"></i><strong>Well done!</strong> ${successMessage} 
                        </div>
                     </c:if>
                     <c:if test="${not empty failureMessage}">
                        <div class="alert alert-danger alert-dismissable alert-condensed">
                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cross"></i></button>
                           <i class="fa fa-exclamation-circle append-icon"></i>
                           <strong>Oh snap!</strong> ${failureMessage} 
                        </div>
                     </c:if>
                     <div class="panel-heading vd_bg-grey">
                        <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span>List Of Projects</h3>
                     </div>
                     <div class="panel-body table-responsive">
                        <div class="form-group">
                           <label class="col-sm-2 control-label">By Project Status<span class="vd_red"></span> </label>
                           <div class="col-sm-3 controls">
                              <select class="form-control form-control-sm"  name="projectStatus"  onChange="loadDataByStatus(this.value);">   
                                <c:forEach items="${statusList}" var="data">
                                       <option  value="${data}" ${projectStatus eq data?'selected':''}>${data}</option> 
                                    </c:forEach>
                                  </select> 
                           </div>
                        </div>
                        <a  href="${contextPath}/project"  style="margin-left: 92%;" class="btn vd_btn vd_bg-green vd_white">Add New</a>
                        </br>  </br>
                        <table class="table table-striped " id="data-tables">
                           <thead>
                              <tr>
                                 <th>#</th>
                                 <th>Project Code</th>
                                 <th>Project Name</th>
                                 <th>Required Skils</th>
                                 <th>Start Date</th>
                                 <th>End Date</th>
                                 <th>Project Cost</th>
                                 <th>Project Allocation</th>
                                 <th>Project Status</th>
                                 <th>created On</th>
                                 <th>updated On</th>
                                 <th>Status</th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody id="tbd">
                              <c:forEach items="${responseList}" var="data" varStatus="serialNumber">
                                <tr style="color:${data.projectStatus eq 'COMPLETED'?'green':'black'};">
                                    <td>${serialNumber.count}</td>
                                    <td><span class="lbl_green">${data.projectCode}</span></td>
                                    <td>${data.projectName}</td>
                                    <td>${data.requiredSkils}</td>
                                    <td>
                                       <fmt:formatDate type = "DATE"   value = "${data.startDate}" />
                                    </td>
                                    <td>
                                       <fmt:formatDate type = "DATE"   value = "${data.endDate}" />
                                    </td>
                                    <td><b><span class="lbl_green">${data.projectCost} ${data.currency}/${data.paymentFrequency}</span></b></td>
                                    <td>
                                       <strong><u>CLIENTS:</u></strong></br>
                                       <c:choose>
                                          <c:when test="${not empty data.userClient}">
                                             ${data.userClient.name}-${data.userClient.userName}-${data.userClient.mobileNo}											
                                          </c:when>
                                          <c:otherwise>														
                                             Project Not Tagged
                                          </c:otherwise>
                                       </c:choose>
                                       <c:if test="${userRole.roleId.roleName eq 'CONSULTANCY'}">
                                          <strong><u>FREELANCER:</u></strong></br>
                                          <c:choose>
                                             <c:when test="${not empty data.projectAllocation.userFreelancer}">
                                                ${data.projectAllocation.userFreelancer.name}-${data.projectAllocation.userFreelancer.mobileNo}  </br>  
                                                <strong><u>Allocation Status:</u></strong>${data.projectAllocation.projectStatus}                                            
                                              </c:when>
                                             <c:otherwise>														
                                                Not Allocated
                                             </c:otherwise>
                                          </c:choose>
                                       </c:if>
                                    </td>
                                    <td>
                                       <c:choose>
                                          <c:when test="${data.projectStatus eq 'CANCELLED'}">
                                             <span class="lbl_red">${data.projectStatus}</span>
                                          </c:when>
                                          <c:when test="${data.projectStatus eq 'COMPLETED'}">
                                             <span class="lbl_green">${data.projectStatus}</span>
                                          </c:when>
                                          <c:when test="${data.projectStatus eq 'INPROGRESS'}">
                                             <span class="lbl_green">${data.projectStatus}</span>
                                          </c:when>
                                          <c:otherwise>														
                                             <span class="lbl_red">${data.projectStatus}</span>													
                                          </c:otherwise>
                                       </c:choose>
                                    </td>
                                     <td>
                                       <fmt:formatDate type = "both"   value = "${data.createdOn}" />&nbsp;${not empty data.createdBy?'By:':''} ${data.createdBy.userName}
                                    </td>
                                    <td>
                                       <fmt:formatDate type = "both"   value = "${data.updatedOn}" />&nbsp;${not empty data.updatedBy?'By:':''} ${data.updatedBy.userName}
                                    </td>
                                    <td>
                                       <c:choose>
                                          <c:when test="${data.status eq 'ACTIVE'}">
                                             <span class="label label-success">${data.status}</span>
                                          </c:when>
                                          <c:otherwise>														
                                             <span class="label label-danger" style="font-size: 14px;">${data.status}</span>													
                                          </c:otherwise>
                                       </c:choose>
                                    </td>
                                    
                                    <td>
                                       <c:if test="${userRole.roleId.roleName eq 'CONSULTANCY'}">
                                          <c:if test="${data.status eq 'ACTIVE'  and data.projectStatus eq 'PENDING'}"></br>
                                             <button class="btn btn-success btn-sm" 
                                                onclick="editData('${data.getId()}')">
                                             <i class="fa fa-pencil" aria-hidden="true"></i>
                                             </button>
                                          </c:if>
                                          <c:if test="${data.projectStatus eq 'PENDING'}">
                                             <button class="btn btn-danger btn-sm"  onclick="deleteData('${data.getId()}')">
                                             <i class="fa fa-times" aria-hidden="true"></i>
                                             </button>
                                          </c:if>
                                       </c:if>
                                       <c:if test="${data.status eq 'ACTIVE' and (data.projectStatus eq 'PENDING' or data.projectStatus eq 'INPROGRESS'  or data.projectStatus eq 'HOLD')}">
                                          </br>
                                          <button class="btn btn-primary btn-sm"  onclick="psmodal('${data.getId()}','${data.projectCode}',${not empty data.projectAllocation?data.projectAllocation.userFreelancer.userId:0})">
                                          <i class="append-icon fa fa-fw fa-forward"></i>  Status Update
                                          </button>
                                          
                                       </c:if>
                                       <c:if test="${data.projectStatus eq 'INPROGRESS' or data.projectStatus eq 'HOLD'}">	
                                             <a href="${contextPath}/payments/${data.projectCodeInternal}" class="btn btn-success  btn-sm" >
                                             <i class="append-icon fa fa-fw fa-money"></i> Payment
                                             </a>
                                             <a href="${contextPath}/project-allocations/${data.projectCodeInternal}" class="btn btn-success  btn-sm" >
                                             <i class="append-icon fa fa-fw fa-users"></i> Allocation
                                             </a>
                                          </c:if>
                                    </td>
                                 </tr>
                              </c:forEach>
                           </tbody>
                        </table>
                     </div>
                  </div>
                  <!-- Panel Widget --> 
               </div>
               <!-- col-md-12 --> 
            </div>
            <!-- row --> 
         </div>
         <!-- .vd_content-section --> 
      </div>
      <!-- .vd_content --> 
   </div>
   <!-- .vd_container --> 
</div>
<!-- .vd_content-wrapper --> 
<!-- Modal -->
<div class="modal fade" id="myModal_statusChange"  role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog">
      <form class="form-horizontal"  action="${contextPath}/project-status-change" id="projectstatuschange" method="post" modelAttribute="projectTagging">
         <div class="modal-content">
            <div class="modal-header vd_bg-blue vd_white">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
               <h4 class="modal-title" id="myModalLabel">Project Status Change For : <i id="pcode"></i></h4>
            </div>
            <div class="modal-body">
               <input type="hidden" name="${_csrf.parameterName}"
                  value="${_csrf.token}" />
               <input type="hidden" name="projectId" id="projectId" />
               <input type="hidden" name="usercode" value=" ${usercode}" />
               <div class="form-group">
                  <label class="col-sm-4 control-label">Status</label>
                  <div class="col-sm-7 controls">                     
                      <select name="pstatus" class="width-40" required>
                              <c:forEach items="${statusList}" var="data">
                                       <option  value="${data}">${data}</option> 
                                    </c:forEach>
                                  </select> 
                     
                  </div>
               </div>
            </div>
            <div class="modal-footer background-login">
               <button type="reset" class="btn vd_btn vd_bg-grey" data-dismiss="modal">Close</button>
               <button type="submit" class="btn vd_btn vd_bg-green">Save changes</button>
            </div>
         </div>
         <!-- /.modal-content --> 
      </form>
   </div>
   <!-- /.modal-dialog --> 
</div>
<!-- /.modal --> 
<!-- Modal -->
<div class="modal fade" id="myModaladdPaymodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
   <div class="modal-dialog">
      <form class="form-horizontal"  action="${contextPath}/project-payment" id="register-form" method="post" modelAttribute="projectPayment">
         <div class="modal-content">
            <div class="modal-header vd_bg-blue vd_white">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
               <h4 class="modal-title" id="myModalLabel1">Add Payment Details</h4>
            </div>
            <div class="modal-body">
               <input type="hidden" name="${_csrf.parameterName}"
                  value="${_csrf.token}" />
               <input type="hidden" name="Id" 
                  value="${projectPayment.getId()}" />
               <input type="hidden" name="usercode" 
                  value="${usercode}" />
               <input type="hidden" name="user.userId" 
                  id="useruserId" />
               <div class="alert alert-danger vd_hidden">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-cross"></i></button>
                  <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Oh snap!</strong> Change a few things up and try submitting again. 
               </div>
               <div class="alert alert-success vd_hidden">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-cross"></i></button>
                  <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Well done!</strong>. 
               </div>
               <div class="form-group">
                  <label class="col-sm-2 control-label">Project Code <span class="vd_red">*</span></label>
                  <div class="col-sm-3 controls">
                     <input type="hidden" class="form-control" required id="projectId_pay" name="projectId" value=""   class="width-70"
                        maxlength="500">
                     <input type="test" class="form-control required" readonly  id="pcode_pay" name="pcode_pay" value=""   >
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-2 control-label"> Payment Date <span class="vd_red">*</span></label>
                  <div class="col-sm-3 controls">
                     <input type="text" class="form-control ignoreValidation date" id="paymentDate"
                        name="paymentDate"  >
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-2 control-label"> Start Date <span class="vd_red">*</span></label>
                  <div class="col-sm-3 controls">
                     <input type="text" class="form-control  ignoreValidation date" id="startDate"
                        name="startDate"  >
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-2 control-label"> End Date <span class="vd_red">*</span></label>
                  <div class="col-sm-3 controls">
                     <input type="text" class="form-control  ignoreValidation date" id="endDate"
                        name="endDate"  >
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-2 control-label">Payment Amount <span class="vd_red"></span> </label>
                  <div class="col-sm-3 controls">
                     <input type="number" step="0.01" class="form-control required" id="paymentAmount" name="paymentAmount" value="${userInfo.paymentAmount}"
                        maxlength="100">
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-2 control-label">Remarks <span class="vd_red">*</span> </label>
                  <div class="col-sm-7 controls">
                     <textarea  rows="2" cols="20" id="remarks required" name="remarks" 
                        tabindex="2" class="form-control" maxlength="500">${projectPayment.remarks}</textarea>
                  </div>
               </div>
            </div>
            <div class="modal-footer background-login">
               <a type="button" href="${contextPath}/list-projects-tagging/${usercode}" class="btn vd_btn vd_bg-grey" data-dismiss="modal">Close</a>
               <button type="submit" class="btn vd_btn vd_bg-green">Save changes</button>
            </div>
         </div>
         <!-- /.modal-content --> 
      </form>
   </div>
   <!-- /.modal-dialog --> 
</div>
<!-- /.modal --> 
<form action="${contextPath}/edit-project" method="POST" id="editForm">
   <input type="hidden" name="${_csrf.parameterName}"
      value="${_csrf.token}" />
   <input type="hidden" name="id_edit" id="id_edit" />
</form>
<form action="${contextPath}/del-project" method="POST"
   id="deleteForm">
   <input type="hidden" name="${_csrf.parameterName}"
      value="${_csrf.token}" />
   <input type="hidden" name="id_del" id="id_del" />
</form>
<form action="${contextPath}/project" method="POST"	id="addproject">
   <input type="hidden" name="${_csrf.parameterName}"
      value="${_csrf.token}" />
   <input type="hidden" name="id_ptagging" id="id_ptagging" value="${id_ptagging}" />
</form>
<form action="${contextPath}/projects" method="GET" id="statusForm">
   <input type="hidden" name="${_csrf.parameterName}"
      value="${_csrf.token}" />
   <input type="hidden" name="projectStatus" id="projectStatus" />
</form>
<!-- Placed at the end of the document so the pages load faster --> 
<script type="text/javascript" src="${contextPath}/static/sFreelance/js/jquery.js"></script> 
<%-- <%@include file="/WEB-INF/views/layout/common.jsp" %> --%>
<script> 
   function loadDataByStatus(status){
   	$("#projectStatus").val(status);
   $("#statusForm").submit();
   }
   
   
   function editData(id){
   	$("#id_edit").val(id);
   	bootbox.confirm("Do you want to edit?",
   	function(result) {
   		if (result == true) {
   		   $("#editForm").submit();
   		} 
   	});
   }	
   function deleteData(id){
   	$("#id_del").val(id);
   	bootbox.confirm("Do you want to delete?",
       function(result) {
           if (result == true) {
               $("#deleteForm").submit();
           } 
       });
       }
    
   function psmodal(id,code,urId){	
   if(urId!='0'){
   	$("#projectId").val(id);
   	$("#pcode").text(code);
   	$('#myModal_statusChange').modal('show');
   	}
   	else
   	{
   	bootbox.alert("Not Allowed to Change Project Status , Please Allocate to Any One !");
       }
   	
       }
   function addPaymodal(id,code,urId){
   	
   if(urId!='0'){
   	$("#projectId_pay").val(id);
   	$("#pcode_pay").val(code);
   	$("#useruserId").val(urId);
   	
   	$('#myModaladdPaymodal').modal('show');
   	}
   	else
   	{
   	bootbox.alert("Project Is Not Allocated To Any Freelancer !");
       }    
       }
       
</script>   
<script>
   $(document).ready(function() {		
   
           var form_register = $('#register-form');
           var error_register = $('.alert-danger', form_register);
           var success_register = $('.alert-success', form_register);
   
           form_register.validate({
               errorElement: 'div', //default input error message container
               errorClass: 'vd_red', // default input error message class
               focusInvalid: false, // do not focus the last invalid input
               ignore: "",
               rules: {
                  
                    projectName: {
                       required: true,                 
                   },
                    projectCode: {
                       required: true,                 
                   },
                    projectDesc: {
                       required: true,                 
                   },
                    requiredSkils: {
                       required: true,                 
                   },
                    startDate: {
                       required: true,                 
                   },
                    endDate: {
                       required: true,                 
                   },
                    projectCost: {
                       required: true,                 
                   },
                    freelancerPayment: {
                       required: true,                 
                   }
                  ,
                    revisedStartDate: {
                       required: true,                 
                   }
                   ,
                    revisedEndDate: {
                       required: true,                 
                   }
                   ,
                    revisedProjectCost: {
                       required: true,                 
                   }
                  
               },
   			
   
             	errorPlacement: function(error, element) {
   				if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
   					element.parent().append(error);
   				} else if (element.parent().hasClass("vd_input-wrapper")){
   					error.insertAfter(element.parent());
   				}else {
   					error.insertAfter(element);
   				}
   			}, 
   			
               invalidHandler: function (event, validator) { //display error alert on form submit              
   					success_register.fadeOut();
   					error_register.fadeIn();
   					scrollTo(form_register,-100);
   
               },
   
               highlight: function (element) { // hightlight error inputs
   		
   				$(element).addClass('vd_bd-red');
   				$(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
   
               },
   
               unhighlight: function (element) { // revert the change dony by hightlight
                   $(element)
                       .closest('.control-group').removeClass('error'); // set error class to the control group
               },
   
               success: function (label, element) {
                   label
                       .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                   	.closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
   				$(element).removeClass('vd_bd-red');
   				
               },
   
               submitHandler: function (form) {
   					success_register.fadeIn();
   					error_register.fadeOut();
   					scrollTo(form_register,-100);	
   					$('#register-form').submit();				
               }
           });
           
           
           
      
           
           
   });
   
   	
   
</script>