<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<!-- Middle Content Start -->    
<div class="vd_content-wrapper">
<div class="vd_container">
<div class="vd_content clearfix">
   <div class="vd_head-section clearfix">
      <div class="vd_panel-header">
         <ul class="breadcrumb">
            <li><a href="${contextPath}/">Home</a> </li>
            <li><a href="${contextPath}/">Project Management</a> </li>
            <li class="active"> ${not empty project.getId() ?'Modify Project':'Create Project'}</li>
         </ul>
         <div class="vd_title-section clearfix">
            <div class="vd_panel-header">
               <h1> ${not empty project.getId() ?'Modify Project':'Create Project'}</h1>
               <small class="subtitle">Fill the below form ('*' for Mandetory Field)</small>
            </div>
         </div>
         <!-- vd_title-section -->
         <div class="vd_content-section clearfix">
            <div class="row" id="advanced-input">
               <div class="col-md-12">
                  <div class="panel widget">
                     <div class="panel-heading vd_bg-grey">
                        <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-bar-chart-o"></i> </span> Project Details </h3>
                     </div>
                     <div class="panel-body">
                        <form class="form-horizontal"  action="${contextPath}/project" id="register-form" method="post" modelAttribute="project">
                           <input type="hidden" name="${_csrf.parameterName}"
                              value="${_csrf.token}" />
                           <input type="hidden" name="Id" value="${project.getId()}" />
                           <div class="alert alert-danger vd_hidden">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-cross"></i></button>
                              <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Oh snap!</strong> Change a few things up and try submitting again. 
                           </div>
                           <div class="alert alert-success vd_hidden">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-cross"></i></button>
                              <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Well done!</strong>. 
                           </div>
                           <c:choose>
                              <c:when test="${not empty project.getId()}">
                                 <div class="form-group">
                                    <label class="col-sm-2 control-label">Clients <span class="vd_red">*</span> </label>
                                    <div class="col-sm-3 controls">
                                       <input type="hidden" sclass="form-control required" name="userClient.userId" id="userClientuserId" value="${project.userClient.userId}"
                                          maxlength="100">
                                       <label class="col-sm-2 control-label">${project.userClient.name}-${project.userClient.userName}-${project.userClient.email} </label>
                                    </div>
                                 </div>
                              </c:when>
                              <c:otherwise>
                                 <div class="form-group">
                                    <label class="col-sm-2 control-label">Clients <span class="vd_red">*</span> </label>
                                    <div class="col-sm-3 controls">
                                       <select name="userClient.userId" id="userClientuserId" class="form-control populate required" ${not empty project.getId() ?'readonly':''}>
                                       <option value="">-Select-</option>
                                       <c:forEach items="${clientList}" var="data">
                                          <option ${data.user.userId eq project.userClient.userId ?'selected':''} value="${data.user.userId}">${data.user.name}-${data.user.userName}-${data.user.email}</option> 
                                       </c:forEach>
                                       </select>
                                    </div>
                                 </div>
                              </c:otherwise>
                           </c:choose>
                           <div class="form-group">
                              <label class="col-sm-2 control-label">Project Code <span class="vd_red">*</span></label>
                              <div class="col-sm-3 controls">
                                 <label class="col-sm-2 control-label">${project.projectCode}</label>
                                 <input type="${not empty project.projectCode ?'hidden':'text'}" class="form-control required" id="projectCode" name="projectCode" value="${project.projectCode}" ${not empty project.projectCode ?'readonly':''}   class="width-70" maxlength="500">
                              </div>
                              <label class="col-sm-2 control-label">Project Name <span class="vd_red">*</span></label>
                              <div class="col-sm-3 controls">
                                 <label  class="col-sm-2 control-label">${project.projectName}</label>
                                 <input type="${not empty project.projectName ?'hidden':'text'}" class="form-control required" id="projectName" name="projectName" value="${project.projectName}" maxlength="100" ${not empty project.projectName ?'readonly':''}>
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-sm-2 control-label"> Start Date <span class="vd_red">*</span></label>
                              <div class="col-sm-3 controls">
                                 <input type="text" class="form-control ignoreValidation date" id="startDateStr"
                                    name="startDateStr" value="<fmt:formatDate type = "DATE" pattern="dd-MM-yyyy"   value = "${project.startDate}" />" >
                              </div>
                              <label class="col-sm-2 control-label"> End Date <span class="vd_red">*</span></label>
                              <div class="col-sm-3 controls">
                                 <input type="text" class="form-control ignoreValidation date" id="endDateStr"
                                    name="endDateStr" value="<fmt:formatDate type = "DATE" pattern="dd-MM-yyyy"    value = "${project.endDate}" />" >
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-sm-2 control-label">Project Cost <span class="vd_red"></span> </label>
                              <div class="col-sm-3 controls">
                                 <input type="number" step="0.01" class="form-control required" id="projectCost" name="projectCost" value="${project.projectCost}"
                                    maxlength="100">
                              </div>
                              <label class="col-sm-2 control-label">Currency <span class="vd_red">*</span> </label>
                              <div class="col-sm-2 controls">
                                 <select name="currency" id="currency" class="form-control populate required">
                                    <option value="">-Select-</option>
                                    <c:forEach items="${currencyList}" var="data">
                                       <option ${data eq project.currency?'selected':''} value="${data}">${data}</option> 
                                    </c:forEach>
                                 </select>
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-sm-2 control-label">Payment Frequency <span class="vd_red"></span> </label>
                              <div class="col-sm-3 controls">
                                 <select class="form-control form-control-sm required" id="paymentFrequency" name="paymentFrequency">
                                    <option value="">--Select--</option>
                                    <option ${project.paymentFrequency eq 'WEEKLY' ?'selected':''} value="WEEKLY">WEEKLY</option> 
                                    <option ${project.paymentFrequency eq 'BI-WEEKLY' ?'selected':''} value="Bi-Weekly">BI-WEEKLY</option> 
                                    <option ${project.paymentFrequency eq 'TRI-WEEKLY' ?'selected':''} value="TRI-WEEKLY">TRI-WEEKLY</option> 
                                    <option ${project.paymentFrequency eq 'MONTHLY' ?'selected':''} value="MONTHLY">MONTHLY</option> 
                                    <option ${project.paymentFrequency eq 'HOURLY' ?'selected':''} value="HOURLY">HOURLY</option> 
                                    <option ${project.paymentFrequency eq 'TASK WISE' ?'selected':''} value="TASK WISE">TASK WISE</option> 
                                     <option ${project.paymentFrequency eq 'PROJECT WISE' ?'selected':''} value="PROJECT WISE">PROJECT WISE</option> 
                                 </select>
                              </div>
                             
                           </div>
                           <div class="form-group">
                              <label class="col-sm-2 control-label">Project Desc <span class="vd_red">*</span> </label>
                              <div class="col-sm-7 controls">
                                 <textarea  rows="2" cols="20" id="projectDesc" name="projectDesc" 
                                    tabindex="2" class="form-control required" maxlength="500">${project.projectDesc}</textarea>
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-sm-2 control-label">Required Skils <span class="vd_red">*</span> </label>
                              <div class="col-sm-7 controls">
                                 <textarea  rows="2" cols="20" id="requiredSkils" name="requiredSkils" 
                                    tabindex="2" class="form-control required" maxlength="500">${project.requiredSkils}</textarea>
                              </div>
                           </div>
                           <div class="form-group form-actions">
                              <div class="col-sm-4"> </div>
                              <div class="col-sm-7">
                                 <button class="btn vd_btn vd_bg-green vd_white" type="submit" id="submit-register" name="submit-register"><i class="icon-ok"></i> ${not empty project.getId() ?'Update':'Save'}</button>
                                 <a href="${contextPath}/projects" class="btn vd_btn vd_bg-grey" type="button" >Back</a>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
                  <!-- Panel Widget --> 
               </div>
               <!-- col-md-12 --> 
            </div>
            <!-- row -->
         </div>
         <!-- .vd_content-section --> 
      </div>
      <!-- .vd_content --> 
   </div>
   <!-- .vd_container --> 
</div>
</div>
</div>

<!-- .vd_content-wrapper --> 
<!-- Middle Content End --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script type="text/javascript" src="${contextPath}/static/sFreelance/js/jquery.js"></script> 
<%-- <%@include file="/WEB-INF/views/layout/common.jsp" %> --%>
<script type="text/javascript">
   $(document).ready(function() {
   		"use strict";	
   //https://jqueryvalidation.org/validate/
           var form_register = $('#register-form');
           var error_register = $('.alert-danger', form_register);
           var success_register = $('.alert-success', form_register);
   
           form_register.validate({
               errorElement: 'div', //default input error message container
               errorClass: 'vd_red', // default input error message class
               focusInvalid: false, // do not focus the last invalid input
               ignore: ".ignoreValidation",
               rules: {
                   
                    	
                  
               },
   			
   
             	errorPlacement: function(error, element) {
   				if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
   					element.parent().append(error);
   				} else if (element.parent().hasClass("vd_input-wrapper")){
   					error.insertAfter(element.parent());
   				}else {
   					error.insertAfter(element);
   				}
   			}, 
   			
               invalidHandler: function (event, validator) { //display error alert on form submit              
   					success_register.fadeOut();
   					error_register.fadeIn();
   					scrollTo(form_register,-100);
   
               },
   
               highlight: function (element) { // hightlight error inputs
   		
   				$(element).addClass('vd_bd-red');
   				$(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
   
               },
   
               unhighlight: function (element) { // revert the change dony by hightlight
                   $(element)
                       .closest('.control-group').removeClass('error'); // set error class to the control group
               },
   
               success: function (label, element) {
                   label
                       .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                   	.closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
   				$(element).removeClass('vd_bd-red');
   				
               },
   
               submitHandler: function (form) {
   					success_register.fadeIn();
   					error_register.fadeOut();
   					scrollTo(form_register,-100);	
   					$('#register-form').submit();				
               }
           });
   });
</script>