	<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<!-- Middle Content Start -->
<div class="vd_content-wrapper">
   <div class="vd_container">
      <div class="vd_content clearfix">
         <div class="vd_head-section clearfix">
            <div class="vd_panel-header">
               <ul class="breadcrumb">
                  <li><a href="${contextPath}/">Home</a> </li>
                  <li><a href="${contextPath}/">Project Management</a> </li>
                  <li class="active">Project Allocation History</li>
               </ul>
            </div>
         </div>
         <div class="vd_title-section clearfix">
            <div class="vd_panel-header">
               <h1>Project Allocation  History</h1>
               <small class="subtitle">Look <a href="http://www.datatables.net">below</a> for more information</small> 
            </div>
         </div>
         <div class="vd_content-section clearfix">
            <div class="row">
               <div class="col-md-12">
                  <div class="panel widget">
                     <div class="panel-heading vd_bg-grey">
                        <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span>List Of Project Allocation History</h3>
                     </div>
                     <div class="panel-body table-responsive">
                        <div class="form-group">
                           <label class="col-sm-2 control-label">By Status<span class="vd_red"></span> </label>
                           <div class="col-sm-3 controls">
                              <select class="form-control form-control-sm"  name="projectAllocationStatus"  onChange="loadByPAllocationStatus(this.value);">   
                              <c:forEach items="${statusList}" var="data">
                                       <option  value="${data}" ${projectAllocationStatus eq data?'selected':''}>${data}</option> 
                                    </c:forEach>
                                  </select> 
                          
                           </div>
                        </div>
                        <c:set var = "credit" value = "${0}"/>
                        <c:set var = "debit" value = "${0}"/>
                        <c:set var = "currency" value = "INR"/>
                        <c:set var = "usdInInr" value = "${USD_TO_INR}"/>
                        <c:set var = "usdIncad" value = "${CAD_TO_INR}"/>
                        <table class="table table-striped" id="data-tables">
                           <thead>
                              <tr>
                                 <th>#</th>
                                 <th>Project Details</th>
                                 <th>Start Date</th>
                                 <th>End Date</th>
                                 <th>Freelancer/Client Details</th>
                                
                                   <th style="color:green;">Client Payment (CREDIT)</th>
                                 <th style="color:red;">Freelancer Payment (DEBIT)</th>
                                 <th>Profit/Loss</th>
                                 <th>End Of Support Date</th>
                                 <th>WhatsApp Group</th>
                                 
                                  <th>Pay Type</th>
                                 
                                 <th>Allocation Status</th>
                                 <th>created On</th>
                                 <th>Status</th>                                 
                              </tr>
                           </thead>
                           <tfoot>
		<tr style="display:none;">
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td>
		
			<p style="font-weight: bold;color: green;"><strong> CREDIT:</strong> </p>
			</td>
		<td><p style="font-weight: bold;color: red;"><strong> DEBIT:</strong> </p></td>
		<td></td>
			<td>
			<p style="font-weight: bold;color: blue;"><strong> PROFIT:</strong> </p>
			</td>
			<td>
			<p style="font-weight: bold;color: blue;"><strong> LOSS:</strong> </p>
			</td>
			
			<td></td>
			<td></td>
		</tr>
	
	
	</tfoot>
                           <tbody id="tbd">
                              <c:forEach items="${responseList}" var="data" varStatus="serialNumber">
                              <tr style="color:black">
                                    <td>${serialNumber.count}
                                    <c:set var = "credit_temp" value = "${0}"/>
                        <c:set var = "debit_temp" value = "${0}"/> </td>
                                 
                                    </td>
                                    <td>${data.project.projectName} (<a href="${contextPath}/projects?&projectcodes=${data.project.projectCodeInternal}"><span class="lbl_green">${data.project.projectCode}</span></a>&#128279;)</td>
                                    <td>
                                       <fmt:formatDate type = "DATE" pattern="dd-MMM-yyyy"    value = "${data.startDate}" />
                                    </td>
                                    <td>
                                       <fmt:formatDate type = "DATE" pattern="dd-MMM-yyyy"    value = "${data.endDate}" />
                                    </td>
                                    <td>
                                      <strong><u>CLIENTS:</u></strong></br>
                                       ${data.project.userClient.name}-${data.project.userClient.userName}-${data.project.userClient.mobileNo}	</br>										
                                        <strong><u>FREELANCER:</u></strong></br>
                                          ${data.userFreelancer.name}-${data.userFreelancer.mobileNo}
                                          
                                    
                                    </td>
                                  
                                      <td><span class="${data.projectStatus eq 'INPROGRESS'?'label label-primary':''}"><span class="label label-success">
                                      
                                      <c:set var = "credit_temp" value = "${data.project.projectCost}"/>  
                                       ${credit_temp}   ${data.project.currency}  
                                       <c:if test="${data.project.currency eq 'USD'}">	
                                       <c:set var = "credit_temp" value = "${((data.project.projectCost)*(usdInInr))}"/>  	                                            
		                                    or ${credit_temp} ${currency}       		                                             
		                                </c:if>
		                                 <c:if test="${data.project.currency eq 'CAD'}">	
                                       <c:set var = "credit_temp" value = "${((data.project.projectCost)*(usdIncad))}"/>  	                                            
		                                    or ${credit_temp} ${currency}       		                                             
		                                </c:if>
                                      </span> / ${data.project.paymentFrequency}</span></br>
                                         (Payment Release: </br>${data.paymentFrequencyCredit})
                                      </td>
                                   
                                    <td>
                                    
                                    <span class="${data.projectStatus eq 'INPROGRESS'?'label label-primary':''}"><span class="label label-danger">
                                    <c:set var = "debit_temp" value = "${data.freelancerCost}"/>
                                     ${debit_temp} ${data.currency}  
                                     <c:if test="${data.currency eq 'USD'}">
                                     <c:set var = "debit_temp" value = "${((data.freelancerCost)*(usdInInr))}"/>		                                            
		                                     or ${debit_temp}  ${currency}       		                                             
		                                </c:if>
		                                <c:if test="${data.currency eq 'CAD'}">
                                     <c:set var = "debit_temp" value = "${((data.freelancerCost)*(usdIncad))}"/>		                                            
		                                     or ${debit_temp}  ${currency}       		                                             
		                                </c:if>
                                    </span> ${data.paymentFrequencyDebit eq 'Hourly'?'/Hourly':'/MONTHLY'}</span></br>
                                    (Payment Release: </br>${data.paymentFrequencyDebit})
                                    </td>
                                   <td>                                      
                                     <fmt:formatNumber type="number" maxFractionDigits="2" value="${credit_temp-debit_temp}"/> INR
                                    </td>
                                    <td>
                                       <span class="${data.projectStatus eq 'INPROGRESS'?'label label-danger':''}">
                                          <fmt:formatDate type = "DATE" pattern="dd-MMM-yyyy"    value = "${data.reviseEndDate}" />
                                       </span>
                                    </td>                                 
                                    <td>
                                       ${data.whatsappGroup}
                                    </td>
                                    <td>
                                       1. Client Pay Advance: ?? ${data.clientPayAdvance eq true ?'Yes':'No'}<br>
                                       2. Freelancer Pay Advance: ?? ${data.freelancerPaidAdvance eq true ?'Yes':'No'}
                                    </td>
                                    <td>
                                       <c:choose>
                                          <c:when test="${data.projectStatus eq 'CLOSED' || data.projectStatus eq 'CANCELLED'}">
                                             <span class="${data.projectStatus eq 'INPROGRESS'?'lbl_red':''}">${data.projectStatus}</span>
                                          </c:when>
                                          <c:when test="${data.projectStatus eq 'COMPLETED'}">
                                             <span class="${data.projectStatus eq 'INPROGRESS'?'lbl_green':''}">${data.projectStatus}</span>
                                          </c:when>
                                          <c:when test="${data.projectStatus eq 'INPROGRESS'}">
                                             <span class="${data.projectStatus eq 'INPROGRESS'?'lbl_green':''}">${data.projectStatus}</span>
                                          </c:when>
                                          <c:otherwise>														
                                             <span class="${data.projectStatus eq 'INPROGRESS'?'lbl_red':''}">${data.projectStatus}</span>													
                                          </c:otherwise>
                                       </c:choose>
                                    </td>
                                    <td>
                                       <fmt:formatDate type = "DATE" pattern="dd-MMM-yyyy"    value = "${data.createdOn}" />                                       
                                       
		                                
		                                       <c:choose>
		                                          <c:when test="${data.project.currency eq 'USD'}">		                                            
		                                             <c:set var = "credit" value = "${credit+((data.project.projectCost)*(usdInInr))}"/>		                                             
		                                          </c:when>
		                                          <c:when test="${data.project.currency eq 'INR'}">
		                                             <c:set var = "credit" value = "${credit+(data.project.projectCost)}"/>		                                    		
		                                          </c:when>
		                                           <c:otherwise>														
		                                         								
		                                          </c:otherwise>
		                                       </c:choose>		                                      
		                                      
		                                       
		                                        <c:choose>
		                                          <c:when test="${data.currency eq 'USD'}">
		                                        		<c:set var = "debit" value = "${debit+((data.freelancerCost)*(usdInInr))}"/>
		                                          </c:when>
		                                          <c:when test="${data.currency eq 'INR'}">
		                                             <c:set var = "debit" value = "${debit+data.freelancerCost}"/>
		                                          </c:when>
		                                           <c:otherwise>													
		                                         												
		                                          </c:otherwise>
		                                       </c:choose>		                                    
		                                 
                                     </td>
                                     <td>
                                       <c:choose>
                                          <c:when test="${data.status eq 'ACTIVE'}">
                                             <span class="${data.projectStatus eq 'INPROGRESS'?'label label-success':''}">${data.status}</span>
                                          </c:when>
                                          <c:otherwise>														
                                             <span class="${data.projectStatus eq 'INPROGRESS'?'label label-danger':''}" style="font-size: 14px;">${data.status}</span>													
                                          </c:otherwise>
                                       </c:choose>
                                    </td>
                                    
                                 </tr>
                              </c:forEach>
                           </tbody>
                        </table>
                                   <p style="font-weight: bold;color: green;"><strong>CREDIT: <fmt:formatNumber value="${credit}" maxFractionDigits="2"/></strong>&nbsp;${currency} </p>
                        <p style="font-weight: bold;color: red;"><strong>DEBIT: <fmt:formatNumber value="${debit}" maxFractionDigits="2"/></strong>&nbsp;${currency} </p>
                        </hr>------------------------------</hr>
                        <p style="font-weight: bold;color: blue;"><strong> ${credit-debit>0?'PROFIT':'LOSS'}: <fmt:formatNumber type="number" maxFractionDigits="2" value="${credit-debit}"/>&nbsp;${currency}</strong> </p>
                        
                     </div>
                  </div>
                  <!-- Panel Widget --> 
               </div>
               <!-- col-md-12 --> 
            </div>
            <!-- row --> 
         </div>
         <!-- .vd_content-section --> 
      </div>
      <!-- .vd_content --> 
   </div>
   <!-- .vd_container --> 
</div>
<!-- .vd_content-wrapper --> 
<form action="${contextPath}/project-allocations" method="GET" id="statusForm">
   <input type="hidden" name="${_csrf.parameterName}"
      value="${_csrf.token}" />
   <input type="hidden" name="projectAllocationStatus" id="projectAllocationStatus" />
</form>
<%-- <%@include file="/WEB-INF/views/layout/common.jsp" %> --%>
<script> 
   function loadByPAllocationStatus(status){
   	$("#projectAllocationStatus").val(status);
   	$("#statusForm").submit();
   }
   function psmodal(paymentId){		
   $("#paymentId").val(paymentId);		
   $('#myModal_statusChange').modal('show');
   }
</script>