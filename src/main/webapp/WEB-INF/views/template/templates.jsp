<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<!-- Middle Content Start -->
<div class="vd_content-wrapper">
   <div class="vd_container">
      <div class="vd_content clearfix">
         <div class="vd_head-section clearfix">
            <div class="vd_panel-header">
               <ul class="breadcrumb">
                  <li><a href="${contextPath}/">Home</a> </li>
                  <li><a href="${contextPath}/">Manage Template</a> </li>
                  <li class="active">Templates</li>
               </ul>
            </div>
         </div>
         <div class="vd_title-section clearfix">
            <div class="vd_panel-header">
               <h1>Manage Templates</h1>
               <small class="subtitle">Look <a href="${contextPath}/templates">below</a> for more information</small> 
            </div>
         </div>
         <div class="vd_content-section clearfix">
            <div class="row">
               <div class="col-md-12">
                  <div class="panel widget">
                     <c:if test="${not empty successMessage}">
                        <div class="alert alert-success alert-dismissable">
                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cross"></i></button>
                           <i class="fa fa-check-circle append-icon"></i><strong>Well done!</strong> ${successMessage} 
                        </div>
                     </c:if>
                     <c:if test="${not empty failureMessage}">
                        <div class="alert alert-danger alert-dismissable alert-condensed">
                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cross"></i></button>
                           <i class="fa fa-exclamation-circle append-icon"></i>
                           <strong>Oh snap!</strong> ${failureMessage} 
                        </div>
                     </c:if>
                     <div class="panel-heading vd_bg-grey">
                        <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span>List Of Templates</h3>
                     </div>
                     <div class="panel-body table-responsive">
                        <table class="table table-striped" id="data-tables">
                           <thead>
                              <tr>
                                 <th>Sl No</th>
                                 <th>Template Name</th>
                                 <th>Remarks</th>
                                 <th>Type</th>
                                 <th>created On</th>
                                 <th>Updated On</th>
                                 <th>Status</th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody id="tbd">
                              <c:forEach items="${responseList}" var="data" varStatus="serialNumber">
                                 <tr>
                                    <td>${serialNumber.count}</td>
                                    <td>${data.templateName}</td>
                                    <td>${data.remarks}</td>
                                    <td>${data.type}</td>
                                    <td>
                                       <fmt:formatDate type = "both"   value = "${data.createdOn}" />
                                    </td>
                                    <td>
                                       <fmt:formatDate type = "both"   value = "${data.updatedOn}" />
                                    </td>
                                    <td>
                                       <c:choose>
                                          <c:when test="${data.status eq 'ACTIVE'}">
                                             <span class="label label-success">${data.status}</span>
                                          </c:when>
                                          <c:otherwise>														
                                             <span class="label label-danger" style="font-size: 14px;">${data.status}</span>													
                                          </c:otherwise>
                                       </c:choose>
                                    </td>
                                    <td>
                                       <c:if test="${data.status eq 'ACTIVE' and data.type eq 'DEFAULT' and userRole.roleId.roleName eq 'ADMIN'}"> 
                                          <button class="btn btn-success btn-sm" 
                                             onclick="editData('${data.getId()}')">
                                          <i class="fa fa-pencil" aria-hidden="true"></i>
                                          </button>
                                          <button class="btn btn-danger btn-sm"  onclick="deleteData('${data.getId()}')">
                                          <i class="fa fa-times" aria-hidden="true"></i>
                                          </button>
                                       </c:if>
                                       <c:if test="${data.status eq 'ACTIVE' and data.type eq 'CUSTOM' and  (userRole.roleId.roleName eq 'CLIENT_AGENT'  or userRole.roleId.roleName eq 'FREELANCER_AGENT')}"> 
                                          <button class="btn btn-success btn-sm" 
                                             onclick="editData('${data.getId()}')">
                                          <i class="fa fa-pencil" aria-hidden="true"></i>
                                          </button>
                                          <button class="btn btn-danger btn-sm"  onclick="deleteData('${data.getId()}')">
                                          <i class="fa fa-times" aria-hidden="true"></i>
                                          </button>
                                       </c:if>
                                    </td>
                                 </tr>
                              </c:forEach>
                           </tbody>
                        </table>
                     </div>
                  </div>
                  <!-- Panel Widget --> 
               </div>
               <!-- col-md-12 --> 
            </div>
            <!-- row --> 
         </div>
         <!-- .vd_content-section --> 
      </div>
      <!-- .vd_content --> 
   </div>
   <!-- .vd_container --> 
</div>
<!-- .vd_content-wrapper --> 
<form action="${contextPath}/edit-template" method="POST" id="editForm">
   <input type="hidden" name="${_csrf.parameterName}"
      value="${_csrf.token}" />
   <input type="hidden" name="id_edit" id="id_edit" />
</form>
<form action="${contextPath}/del-template" method="POST"
   id="deleteForm">
   <input type="hidden" name="${_csrf.parameterName}"
      value="${_csrf.token}" />
   <input type="hidden" name="id_del" id="id_del" />
</form>
<!-- Placed at the end of the document so the pages load faster --> 
<script type="text/javascript" src="${contextPath}/static/sFreelance/js/jquery.js"></script> 
<%-- <%@include file="/WEB-INF/views/layout/common.jsp" %> --%>
<script>  
   function editData(id){
   	$("#id_edit").val(id);
   	bootbox.confirm("Do you want to edit?",
   	function(result) {
   		if (result == true) {
   		   $("#editForm").submit();
   		} 
   	});
   }	
   function deleteData(id){
   	$("#id_del").val(id);
   	bootbox.confirm("Do you want to delete?",
       function(result) {
           if (result == true) {
               $("#deleteForm").submit();
           } 
       });
       }
    
   
</script>