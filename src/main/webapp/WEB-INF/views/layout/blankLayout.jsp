<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/> 

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8" />
    <title>sFreelance Web Portal</title>
    <meta name="keywords" content="HTML5 Template, CSS3, All Purpose Admin Template, Vendroid" />
    <meta name="description" content="Login Pages - Responsive Admin HTML Template">
    <meta name="author" content="JKfreelance"> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0">    
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="${contextPath}/static/sFreelance/img/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="${contextPath}/static/sFreelance/img/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="${contextPath}/static/sFreelance/img/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="${contextPath}/static/sFreelance/img/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="${contextPath}/static/sFreelance/img/ico/favicon.png">
    <link href="${contextPath}/static/sFreelance/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="${contextPath}/static/sFreelance/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="${contextPath}/static/sFreelance/css/font-entypo.css" rel="stylesheet" type="text/css">    
    <link href="${contextPath}/static/sFreelance/css/fonts.css"  rel="stylesheet" type="text/css">         
    <link href="${contextPath}/static/sFreelance/plugins/jquery-ui/jquery-ui.custom.min.css" rel="stylesheet" type="text/css">    
    <link href="${contextPath}/static/sFreelance/plugins/prettyPhoto-plugin/css/prettyPhoto.css" rel="stylesheet" type="text/css">
    <link href="${contextPath}/static/sFreelance/plugins/isotope/css/isotope.css" rel="stylesheet" type="text/css">
    <link href="${contextPath}/static/sFreelance/plugins/pnotify/css/jquery.pnotify.css" media="screen" rel="stylesheet" type="text/css">    
	<link href="${contextPath}/static/sFreelance/plugins/google-code-prettify/prettify.css" rel="stylesheet" type="text/css">      
    <link href="${contextPath}/static/sFreelance/plugins/mCustomScrollbar/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css">
    <link href="${contextPath}/static/sFreelance/plugins/tagsInput/jquery.tagsinput.css" rel="stylesheet" type="text/css">
    <link href="${contextPath}/static/sFreelance/plugins/bootstrap-switch/bootstrap-switch.css" rel="stylesheet" type="text/css">    
    <link href="${contextPath}/static/sFreelance/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css">    
    <link href="${contextPath}/static/sFreelance/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css">
    <link href="${contextPath}/static/sFreelance/plugins/colorpicker/css/colorpicker.css" rel="stylesheet" type="text/css">            
    <link href="${contextPath}/static/sFreelance/css/theme.min.css" rel="stylesheet" type="text/css">
    <link href="${contextPath}/static/sFreelance/css/chrome.css" rel="stylesheet" type="text/chrome"> 
    <link href="${contextPath}/static/sFreelance/css/theme-responsive.min.css" rel="stylesheet" type="text/css"> 
    <link href="${contextPath}/static/sFreelance/custom/custom.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="${contextPath}/static/sFreelance/js/modernizr.js"></script> 
    <script type="text/javascript" src="${contextPath}/static/sFreelance/js/mobile-detect.min.js"></script> 
    <script type="text/javascript" src="${contextPath}/static/sFreelance/js/mobile-detect-modernizr.js"></script> 
    <script type="text/javascript" src="${contextPath}/static/sFreelance/js/jquery.js"></script> 
	<script type="text/javascript" src="${contextPath}/static/sFreelance/js/excanvas.js"></script>      
	<script type="text/javascript" src="${contextPath}/static/sFreelance/js/bootstrap.min.js"></script> 
	<script type="text/javascript" src='${contextPath}/static/sFreelance/plugins/jquery-ui/jquery-ui.custom.min.js'></script>
	<script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>
	<script type="text/javascript" src="${contextPath}/static/sFreelance/js/caroufredsel.js"></script> 
	<script type="text/javascript" src="${contextPath}/static/sFreelance/js/plugins.js"></script>
	<script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/breakpoints/breakpoints.js"></script>
	<script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/dataTables/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/prettyPhoto-plugin/js/jquery.prettyPhoto.js"></script> 
	<script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/tagsInput/jquery.tagsinput.min.js"></script>
	<script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/bootstrap-switch/bootstrap-switch.min.js"></script>
	<script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/blockUI/jquery.blockUI.js"></script>
	<script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/pnotify/js/jquery.pnotify.min.js"></script>
	<script type="text/javascript" src="${contextPath}/static/sFreelance/js/theme.js"></script>
	<script type="text/javascript" src="${contextPath}/static/sFreelance/custom/custom.js"></script>
	<script src="${contextPath}/static/sFreelance/js/bootbox.min.js"></script> 
</head>
<body>
	<div class="vd_body">
		<div class="content">
		  <div class="container"> 
		    <div class="vd_content-wrapper">
		      <div class="vd_container">
		        <div class="vd_content clearfix">
		          <div class="vd_content-section clearfix">
		            <div class="vd_register-page">
		            	<div class="heading clearfix">
		              		<tiles:insertAttribute name="header" />
		              	</div> 
		              	<tiles:insertAttribute name="body" />
					</div>
		          </div>
		        </div>
		      </div>
		    </div>
		  </div>
		</div>
		<tiles:insertAttribute name="footer" />
	</div> 
</body>
</html>
	