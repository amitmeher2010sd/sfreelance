<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE HTML>
<html>
   <head id="id_header">
      <meta charset="utf-8" />
      <title>sFreelance Web Portal</title>
      <meta name="keywords" content="HTML5 Template, CSS3, All Purpose Admin Template, " />
      <meta name="description" content="Responsive Admin Template for multipurpose use">
      <meta name="author" content="JKfreelance">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <!-- Fav and touch icons -->
      <link rel="apple-touch-icon-precomposed" sizes="144x144" href="${contextPath}/static/sFreelance/img/ico/apple-touch-icon-144-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="114x114" href="${contextPath}/static/sFreelance/img/ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="${contextPath}/static/sFreelance/img/ico/apple-touch-icon-72-precomposed.png">
      <link rel="apple-touch-icon-precomposed" href="img/ico/apple-touch-icon-57-precomposed.png">
      <link rel="shortcut icon" href="img/ico/favicon.png">
      <!-- CSS -->
      <!-- Bootstrap & FontAwesome & Entypo CSS -->
      <link href="${contextPath}/static/sFreelance/css/bootstrap.min.css" rel="stylesheet" type="text/css">
      <link href="${contextPath}/static/sFreelance/css/font-awesome.min.css" rel="stylesheet" type="text/css">
      <!--[if IE 7]>
      <link type="text/css" rel="stylesheet" href="${contextPath}/static/sFreelance/css/font-awesome-ie7.min.css">
      <![endif]-->
      <link href="${contextPath}/static/sFreelance/css/font-entypo.css" rel="stylesheet" type="text/css">
      <!-- Fonts CSS -->
      <link href="${contextPath}/static/sFreelance/css/fonts.css"  rel="stylesheet" type="text/css">
      <!-- Plugin CSS -->
      <link href="${contextPath}/static/sFreelance/plugins/jquery-ui/jquery-ui.custom.min.css" rel="stylesheet" type="text/css">
      <link href="${contextPath}/static/sFreelance/plugins/prettyPhoto-plugin/css/prettyPhoto.css" rel="stylesheet" type="text/css">
      <link href="${contextPath}/static/sFreelance/plugins/isotope/css/isotope.css" rel="stylesheet" type="text/css">
      <link href="${contextPath}/static/sFreelance/plugins/pnotify/css/jquery.pnotify.css" media="screen" rel="stylesheet" type="text/css">
      <link href="${contextPath}/static/sFreelance/plugins/google-code-prettify/prettify.css" rel="stylesheet" type="text/css">
      <link href="${contextPath}/static/sFreelance/plugins/mCustomScrollbar/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css">
      <link href="${contextPath}/static/sFreelance/plugins/tagsInput/jquery.tagsinput.css" rel="stylesheet" type="text/css">
      <link href="${contextPath}/static/sFreelance/plugins/bootstrap-switch/bootstrap-switch.css" rel="stylesheet" type="text/css">
      <link href="${contextPath}/static/sFreelance/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css">
      <link href="${contextPath}/static/sFreelance/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css">
      <link href="${contextPath}/static/sFreelance/plugins/colorpicker/css/colorpicker.css" rel="stylesheet" type="text/css">
      <!-- Specific CSS -->
      <link href="${contextPath}/static/sFreelance/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css">
      <link href="${contextPath}/static/sFreelance/plugins/fullcalendar/fullcalendar.print.css" rel="stylesheet" type="text/css">
      <link href="plugins/introjs/css/introjs.min.css" rel="stylesheet" type="text/css">
      <link href="${contextPath}/static/sFreelance/plugins/dataTables/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
      <link href="plugins/dataTables/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
      <!-- Theme CSS -->
      <link href="${contextPath}/static/sFreelance/css/theme.min.css" rel="stylesheet" type="text/css">
      <!--[if IE]> 
      <link href="${contextPath}/static/sFreelance/css/ie.css" rel="stylesheet" >
      <![endif]-->
      <link href="${contextPath}/static/sFreelance/css/chrome.css" rel="stylesheet" type="text/chrome">
      <!-- chrome only css -->    
      <!-- Responsive CSS -->
      <link href="${contextPath}/static/sFreelance/css/theme-responsive.min.css" rel="stylesheet" type="text/css">
      	<!-- Specific CSS -->
	<link href="${contextPath}/static/sFreelance/plugins/morris/morris.css" rel="stylesheet" type="text/css">    
      
      <!-- for specific page in style css -->
      <!-- for specific page responsive in style css -->
      <!-- Custom CSS -->
      <link href="${contextPath}/static/sFreelance/custom/custom.css" rel="stylesheet" type="text/css">
  <script src="https://code.jquery.com/jquery-1.12.4.js" integrity="sha256-Qw82+bXyGq6MydymqBxNPYTaUXXq7c8v3CwiYwLLNXU=" crossorigin="anonymous"></script>
      <!-- Head SCRIPTS -->
      <script type="text/javascript" src="${contextPath}/static/sFreelance/js/modernizr.js"></script> 
      <script type="text/javascript" src="${contextPath}/static/sFreelance/js/mobile-detect.min.js"></script> 
      <script type="text/javascript" src="${contextPath}/static/sFreelance/js/mobile-detect-modernizr.js"></script> 
      <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!--[if lt IE 9]>
      <script type="text/javascript" src="${contextPath}/static/sFreelance/js/html5shiv.js"></script>
      <script type="text/javascript" src="${contextPath}/static/sFreelance/js/respond.min.js"></script>     
      <![endif]-->
      <link href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css"  rel="stylesheet" type="text/css">
      <link href="https://cdn.datatables.net/datetime/1.1.2/css/dataTables.dateTime.min.css"  rel="stylesheet" type="text/css">
     
             <style>
/* Center the loader */
#loading {
  /* (A1) COVER FULL PAGE */
  position: fixed;
  top: 0; left: 0; z-index: 999;
  width: 100vw; height: 100vh;
 
  /* (A2) SPINNER IMAGE */
  background-color: transparent;
  background-image: url("https://i.gifer.com/VAyR.gif");
  background-position: center;
  background-repeat: no-repeat;
}
</style>
   </head>
   <body id="dashboard" class="full-layout  nav-right-hide nav-right-start-hide  nav-top-fixed      responsive    clearfix" data-active="dashboard "  data-smooth-scrolling="1">
    <div id="loading"></div>
      <div class="vd_body">
         <tiles:insertAttribute name="header" />
         <div class="content">
            <div class="container">
               <tiles:insertAttribute name="leftMenu" />
               <tiles:insertAttribute name="body" />
            </div>
         </div>
         <tiles:insertAttribute name="footer" />
      </div>
      <!--
         <a class="back-top" href="#" id="back-top"> <i class="icon-chevron-up icon-white"> </i> </a> -->
      <!-- Javascript =============================================== --> 
      <!-- Placed at the end of the document so the pages load faster --> 
      <!--[if lt IE 9]>
      <script type="text/javascript" src="${contextPath}/static/sFreelance/js/excanvas.js"></script>      
      <![endif]-->
      <script type="text/javascript" src="${contextPath}/static/sFreelance/js/bootstrap.min.js"></script> 
      <script type="text/javascript" src='${contextPath}/static/sFreelance/plugins/jquery-ui/jquery-ui.custom.min.js'></script>
      <script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>
      <script type="text/javascript" src="${contextPath}/static/sFreelance/js/caroufredsel.js"></script> 
      <script type="text/javascript" src="${contextPath}/static/sFreelance/js/plugins.js"></script>
      <script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/breakpoints/breakpoints.js"></script>
      <script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/dataTables/jquery.dataTables.min.js"></script>
      <script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/prettyPhoto-plugin/js/jquery.prettyPhoto.js"></script> 
      <script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
      <script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/tagsInput/jquery.tagsinput.min.js"></script>
      <script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/bootstrap-switch/bootstrap-switch.min.js"></script>
      <script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/blockUI/jquery.blockUI.js"></script>
      <script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/pnotify/js/jquery.pnotify.min.js"></script>
      <script type="text/javascript" src="${contextPath}/static/sFreelance/js/theme.js"></script>
      <script type="text/javascript" src="${contextPath}/static/sFreelance/custom/custom.js"></script>
            
      <!-- Specific Page Scripts Put Here -->
<script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/raphael/raphael.min.js"></script>
<script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/morris/morris.min.js"></script>
      <!-- Specific Page Scripts Put Here -->
      <!-- Flot Chart  -->
	<!-- Specific Page Scripts Put Here -->
<%-- 	<script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/flot/jquery.flot.min.js"></script>
	<script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/flot/jquery.flot.resize.min.js"></script>
	<script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/flot/jquery.flot.pie.min.js"></script>
	<script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/flot/jquery.flot.categories.min.js"></script>
	<script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/flot/jquery.flot.time.min.js"></script>
	<script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/flot/jquery.flot.animator.min.js"></script>
      
  --%>     <!-- Vector Map -->
      <script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
      <script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
      <!-- Calendar -->
      <script type="text/javascript" src='${contextPath}/static/sFreelance/plugins/moment/moment.min.js'></script>
      <script type="text/javascript" src='${contextPath}/static/sFreelance/plugins/jquery-ui/jquery-ui.custom.min.js'></script>
      <script type="text/javascript" src='${contextPath}/static/sFreelance/plugins/fullcalendar/fullcalendar.min.js'></script>
      <!-- Intro JS (Tour) -->
      <script type="text/javascript" src='${contextPath}/static/sFreelance/plugins/introjs/js/intro.min.js'></script>
      <!-- Sky Icons -->
      <script type="text/javascript" src='${contextPath}/static/sFreelance/plugins/skycons/skycons.js'></script>
      <script src="${contextPath}/static/sFreelance/js/bootbox.min.js"></script> 
      <script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/dataTables/dataTables.bootstrap.js"></script>

      <script type="text/javascript">   
      
         var _gaq = _gaq || [];
         _gaq.push(['_setAccount', 'UA-XXXXX-X']);
         _gaq.push(['_trackPageview']);
         
         (function() {
             var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
             ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
             var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
         })();
    
         $(document).ready(function() {
         		"use strict";
         		
         		var table = $('#data-tables').dataTable({
        			order: [[0, 'desc']],
    			});
         		$('#data-tables1').dataTable({
        				order: [[0, 'desc']],
    			});
    			$('#data-tables_payment').dataTable({
        				order: [[4, 'desc']],
    			});  
    			
    			
    			  $("#loading").hide();
    			 
         } );
         
       
          
         $( function() {
          $( ".date" ).datepicker({
         dateFormat: "dd-mm-yy"  
         });
         } );
        
     
     $(document).ready(function() {
    function makeAjaxCall() {
        $.ajax({
            url: './api/get-notifications',  // Replace with your API endpoint or URL
            type: 'GET',           // Use 'POST' if required
            success: function(response) {
            	 $("#notificationsUl").html("");	
            	 var li='';
            	$.each(response, function(index, item) {
            		li=li+'<li class="notif" ><a href="#"><div class="menu-icon vd_yellow"><i class="fa fa-suitcase"></i></div>&nbsp;&nbsp;&nbsp;&nbsp;'+item.title+'<div class="menu-text"><div class="menu-info"><span class="menu-date">'+item.createdBy+'</span></div> </div> </a></li>';	
            	})
            var ul='<span id="notifications"></span><ul  class="list-wrapper pd-lr-10" id="notificationsUl">'+li+'</ul>';
            
            $("#notifications").html(ul);
            	
            },
            error: function(error) {
                console.log("Error:", error);
                // Handle the error response here
            }
        });
    }

    // Call the function every 1 minute (60000 milliseconds)
    setInterval(makeAjaxCall, 10000);

    // Optionally, call it once immediately
    makeAjaxCall();
});
     </script>
   </body>
</html>