<!-- Specific Page Scripts Put Here -->
<script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/flot/jquery.flot.min.js"></script>
<script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/flot/jquery.flot.resize.min.js"></script>
<script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/flot/jquery.flot.pie.min.js"></script>
<script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/flot/jquery.flot.categories.min.js"></script>
<script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/flot/jquery.flot.time.min.js"></script>
<script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/flot/jquery.flot.animator.min.js"></script>
      