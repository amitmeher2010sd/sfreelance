<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>  
<c:set var="contextPath" value="${pageContext.request.contextPath}"/> 

<!-- Footer Start -->
  <footer class="footer-1"  id="footer">      
    <div class="vd_bottom ">
        <div class="container">
            <div class="row">
              <div class=" col-xs-12">
                <div class="copyright">
                  	<!--&nbsp;&nbsp;&nbsp;&nbsp;                     &nbsp;&nbsp;&nbsp;&nbsp;Copyright &copy;2014 JKfreelance Inc. All Rights Reserved -->
                </div>
              </div>	
            </div><!-- row -->
        </div><!-- container -->
    </div>
  </footer>
<!-- Footer END -->

<%-- <!-- chat start -->  
  <div class="vd_chat-menu hidden-xs">
      <div class="vd_mega-menu-wrapper">
          <div class="vd_mega-menu">
             <ul class="mega-ul">

			    <li class="one-big-icon mega-li mgl-10"> 
			      <a href="index.html" class="mega-link" data-action="click-trigger">
			    	<span class="mega-icon"><img alt="example image" src="${contextPath}/static/sFreelance/img/avatar/avatar.jpg"></span> 
					<span class="badge vd_bg-red">10</span>        
			      </a>
			      <div class="vd_mega-menu-content  open-top width-xs-4 width-md-5 width-lg-4 center-xs-4" data-action="click-target">
			        <div class="child-menu">  
			           <div class="title"> 
			           	   Jessyline <i>(online)</i>
			               <div class="vd_panel-menu">
			                     <div  data-rel="tooltip"  data-original-title="Make a Call" class="menu entypo-icon smaller-font">
			                        <i class="icon-phone"></i>
			                    </div>               
			                     <div  data-rel="tooltip"  data-original-title="Video Call" class="menu">
			                        <i class="fa fa-video-camera"></i>
			                    </div>               
			                     <div  data-rel="tooltip"  data-original-title="Message Setting" class="menu smaller-font entypo-icon">
			                        <i class="icon-cog"></i>
			                    </div>   
			                     <div data-rel="tooltip"  data-original-title="Close" class="menu entypo-icon">
			                        <i class="icon-cross"></i>
			                    </div>                                                                                                 
			                </div>
			           </div>                 
					   <div class="content-list content-image content-menu">
			           	<div data-rel="scroll">	
			               <ul class="list-wrapper pd-lr-10">
			                    <li> <a href="#"> 
			                    		<div class="menu-icon"><img alt="example image" src="${contextPath}/static/sFreelance/img/avatar/avatar.jpg"></div> 
			                            <div class="menu-text"> Do you play or follow any sports?
			                            	<div class="menu-info">
			                                    <span class="menu-date">12 Minutes Ago </span>                                                                                                        
			                            	</div>
			                            </div> 
			                    </a> </li>
			                    <li class="align-right"> <a href="#"> 
			                    		<div class="menu-icon"><img alt="example image" src="${contextPath}/static/sFreelance/img/avatar/avatar-2.jpg"></div>  
			                            <div class="menu-text">  Good job mate !
			                            	<div class="menu-info">
			                                    <span class="menu-date">1 Hour 20 Minutes Ago </span>                                                                         
			                              
			                            	</div>                            
			                            </div> 
			                    </a> 
			                    </li>    
			                  </ul>
			               </div>			               
			               <div class="closing chat-area">
			               	   <div class="chat-box">
			                   	<input type="text" placeholder="chat here.www." />
			                   </div>
			                   <div class="vd_panel-menu">
			                         <div  data-rel="tooltip"  data-original-title="Insert Picture" class="menu">
			                            <i class="icon-camera"></i>
			                        </div>               
			                         <div  data-rel="tooltip"  data-original-title="Emoticons" class="menu">
			                            <i class="fa fa-smile-o"></i>
			                        </div>                                                                                  
			                    </div>
			               </div>                                                                       
			           </div>                              
			        </div> <!-- child-menu -->                      
			      </div>   <!-- vd_mega-menu-content --> 
			    </li>  
                         
		</ul>
          </div>   
      </div>      
  </div> --%>


	