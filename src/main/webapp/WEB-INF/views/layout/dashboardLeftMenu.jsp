<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
    <% String connectedDB = (String)request.getSession().getAttribute("dbname"); %> 
        <% String appVersion_ = (String)request.getSession().getAttribute("appVersion"); %> 
      
<div class="vd_navbar vd_nav-width vd_navbar-tabs-menu vd_navbar-left  ">
   
   <!-- Menu Start -->
   <div class="navbar-menu clearfix">
   <%--    <div class="vd_panel-menu hidden-xs">
         <span data-original-title="Expand All" data-toggle="tooltip" data-placement="bottom" data-action="expand-all" class="menu" data-intro="<strong>Expand Button</strong><br/>To expand all menu on left navigation menu." data-step=4 >
         <i class="fa fa-sort-amount-asc"></i>
         </span>                   
      </div> --%>
         
                            											
      <h4 class="menu-title hide-nav-medium hide-nav-small">Hi <security:authentication property="principal.username" />,</h4>
   
      <div class="vd_menu">
         <ul>
          <li>
               <a href="${contextPath}/">        
               <span class="menu-text">DB :: <%=connectedDB %></span>  &nbsp;&nbsp;
               <span class="menu-text">App:: <%=appVersion_%></span>  
               <span class="menu-badge"><span class="badge vd_bg-black-30"></span></span>
               </a>
            </li>
            <li>
               <a href="${contextPath}/">
               <span class="menu-icon"><i class="fa fa-dashboard"></i></span> 
               <span class="menu-text">Dashboard</span>  
               <span class="menu-badge"><span class="badge vd_bg-black-30"></span></span>
               </a>
            </li>
             <li>
                  <a href="javascript:void(0);" data-action="click-trigger">
                  <span class="menu-icon entypo-icon"><i class="append-icon icon-cc-by"> </i></span> 
                  <span class="menu-text">Manage JK Freelance</span>  
                  <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
                  </a>
                  <div class="child-menu"  data-action="click-target">
                     <ul>
                     	<li>
                           <a href="${contextPath}/jkfreelancers">
                           <span class="menu-text">Manage Freelancer</span>                                      
                           </a>
                        </li>
                        
                        <li>
                           <a href="${contextPath}/website-users">
                           <span class="menu-text">Website Users</span>                                      
                           </a>
                        </li>
                         <li>
                           <a href="${contextPath}/master/careers">
                           <span class="menu-text">All Careers</span>  
                           </a>
                        </li>
                        
                       
                     </ul>
                  </div>
               </li>
              <li>
                  <a href="javascript:void(0);" data-action="click-trigger">
                  <span class="menu-icon entypo-icon"><i class="append-icon icon-cc-by"> </i></span> 
                  <span class="menu-text">Manage SFreelance</span>  
                  <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
                  </a>
                  <div class="child-menu"  data-action="click-target">
                     <ul>
                     	<li>
		                  <a href="javascript:void(0);" data-action="click-trigger">
		                  <span class="menu-icon entypo-icon"><i class="fa fa-sitemap"> </i></span> 
		                  <span class="menu-text">Master</span>  
		                  <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
		                  </a>
		                  <div class="child-menu"  data-action="click-target">
		                     <ul>                       
		                        <li>
		                           <a href="${contextPath}/master/roles">
		                           <span class="menu-text">All Roles</span>  
		                           </a>
		                        </li>                      
		                        <li>
		                           <a href="${contextPath}/master/skills">
		                           <span class="menu-text">All Skills</span>  
		                           </a>
		                        </li>                         
		                     </ul>
		                  </div>
               			</li>
		               	<li>
		                  <a href="${contextPath}/applyclients">
		                  <span class="menu-icon entypo-icon"><i class="append-icon icon-cc-by"> </i></span> 
		                  <span class="menu-text">New Requests</span>  
		                  <span class="menu-badge"><span class="badge vd_bg-black-30"></span></span>
		                  </a>		                  
		               </li>
               			<li>
		                  <a href="${contextPath}/users/cst">
		                  <span class="menu-icon entypo-icon"><i class="fa fa-sitemap"> </i></span> 
		                  <span class="menu-text">Manage Consultancy</span>  
		                  <span class="menu-badge"><span class="badge vd_bg-black-30"></span></span>
		                  </a>		                 
              		 	</li>
              		  	<li>
		                  <a href="${contextPath}/contractors">
		                  <span class="menu-icon entypo-icon"><i class="append-icon fa fa-fw fa-users"> </i></span> 
		                  <span class="menu-text">Manage Contractors</span>  
		                  <span class="menu-badge"><span class="badge vd_bg-black-30"></span></span>
		                  </a>                 
               			</li>
		               <li>
		                  <a href="${contextPath}/users/c">
		                  <span class="menu-icon entypo-icon"><i class="append-icon fa fa-fw fa-users"> </i></span> 
		                  <span class="menu-text">Manage Clients</span>  
		                  <span class="menu-badge"><span class="badge vd_bg-black-30"></span></span>
		                  </a>		                 
		               </li>
		               <li>
		                  <a href="${contextPath}/users/f">
		                  <span class="menu-icon entypo-icon"><i class="append-icon fa fa-fw fa-users"> </i></span> 
		                  <span class="menu-text">Manage Freelancers</span>  
		                  <span class="menu-badge"><span class="badge vd_bg-black-30"></span></span>
		                  </a>		                 
		               </li>
				        <li>
		                  <a href="${contextPath}/projects" >
		                  <span class="menu-icon entypo-icon"><i class="append-icon icon-network"> </i></span> 
		                  <span class="menu-text">Manage Project</span>  
		                  <span class="menu-badge"><span class="badge vd_bg-black-30"></span></span>
		                  </a>		                  
		               </li>
		               <li>
		                  <a href="${contextPath}/project-allocations" >
		                  <span class="menu-icon entypo-icon"><i class="icon-bookmark"> </i></span> 
		                  <span class="menu-text">Manage Allocation</span>  
		                  <span class="menu-badge"><span class="badge vd_bg-black-30"></span></span>
		                  </a>		                  
		               </li>
		               <li>
		                  <a href="${contextPath}/search-payments/">
		                  <span class="menu-icon entypo-icon"><i class="append-icon fa fa-fw fa-money"> </i></span> 
		                  <span class="menu-text">Manage Payments</span>  
		                  <span class="menu-badge"><span class="badge vd_bg-black-30"></span></span>
		                  </a>		                  
		               </li>    
				       <li>
		                  <a href="${contextPath}/search">
		                  <span class="menu-icon entypo-icon"><i class="append-icon fa fa-fw fa-search"> </i></span> 
		                  <span class="menu-text">Search</span>  
		                  <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
		                  </a>		                  
		               </li>
		               <li>
		                  <a href="${contextPath}/pay-calender">
		                  <span class="menu-icon entypo-icon"><i class="append-icon fa fa-fw fa-search"> </i></span> 
		                  <span class="menu-text">Payment Calender</span>  
		                  <span class="menu-badge"><span class="badge vd_bg-black-30"></span></span>
		                  </a>		                  
		               </li>
		               
                     </ul>
                  </div>
               </li>
        
           <%--     <li>
                  <a href="javascript:void(0);" data-action="click-trigger">
                  <span class="menu-icon entypo-icon"><i class="fa fa-sitemap"> </i></span> 
                  <span class="menu-text">Master</span>  
                  <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
                  </a>
                  <div class="child-menu"  data-action="click-target">
                     <ul>                       
                        <li>
                           <a href="${contextPath}/master/roles">
                           <span class="menu-text">All Roles</span>  
                           </a>
                        </li>                      
                        <li>
                           <a href="${contextPath}/master/skills">
                           <span class="menu-text">All Skills</span>  
                           </a>
                        </li>                         
                     </ul>
                  </div>
               </li> --%>
               <%-- <li>
                  <a href="javascript:void(0);" data-action="click-trigger">
                  <span class="menu-icon entypo-icon"><i class="fa fa-sitemap"> </i></span> 
                  <span class="menu-text">Manage Consultancy</span>  
                  <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
                  </a>
                  <div class="child-menu"  data-action="click-target">
                     <ul>
                        <li>
                           <a href="${contextPath}/user/cst">
                           <span class="menu-text">Add Consultancy</span>                                      
                           </a>
                        </li>
                        <li>
                           <a href="${contextPath}/users/cst">
                           <span class="menu-text">All Consultancy</span>                                      
                           </a>
                        </li>
                     </ul>
                  </div>
               </li>
          --%>
           
            
           <%--  <li>
                  <a href="javascript:void(0);" data-action="click-trigger">
                  <span class="menu-icon entypo-icon"><i class="append-icon icon-cc-by"> </i></span> 
                  <span class="menu-text">New Requests</span>  
                  <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
                  </a>
                  <div class="child-menu"  data-action="click-target">
                     <ul>
                     	<li>
                           <a href="${contextPath}/user-requests">
                           <span class="menu-text">User Requests</span>                                      
                           </a>
                        </li>
                        <li>
                           <a href="${contextPath}/applyclients">
                           <span class="menu-text">New Client & Meeting</span>                                      
                           </a>
                        </li>
                        
                     </ul>
                  </div>
               </li> --%>
              <%--  <li>
                  <a href="javascript:void(0);" data-action="click-trigger">
                  <span class="menu-icon entypo-icon"><i class="append-icon fa fa-fw fa-users"> </i></span> 
                  <span class="menu-text">Manage Contractors</span>  
                  <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
                  </a>
                  <div class="child-menu"  data-action="click-target">
                     <ul>
                        <li>
                           <a href="${contextPath}/contractor">
                           <span class="menu-text">Add Contractor</span>                                      
                           </a>
                        </li>
                        <li>
                           <a href="${contextPath}/contractors">
                           <span class="menu-text">All Contractors</span>                                      
                           </a>
                        </li>
                     </ul>
                  </div>
               </li>
               <li>
                  <a href="javascript:void(0);" data-action="click-trigger">
                  <span class="menu-icon entypo-icon"><i class="append-icon fa fa-fw fa-users"> </i></span> 
                  <span class="menu-text">Manage Clients</span>  
                  <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
                  </a>
                  <div class="child-menu"  data-action="click-target">
                     <ul>
                      
                        <li>
                           <a href="${contextPath}/user/c">
                           <span class="menu-text">Add Client</span>                                      
                           </a>
                        </li>
                        <li>
                           <a href="${contextPath}/users/c">
                           <span class="menu-text">All Client</span>                                      
                           </a>
                        </li>
                     </ul>
                  </div>
               </li>
               <li>
                  <a href="javascript:void(0);" data-action="click-trigger">
                  <span class="menu-icon entypo-icon"><i class="append-icon fa fa-fw fa-users"> </i></span> 
                  <span class="menu-text">Manage Freelancers</span>  
                  <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
                  </a>
                  <div class="child-menu"  data-action="click-target">
                     <ul>
                        <li>
                           <a href="${contextPath}/user/f">
                           <span class="menu-text">Add Freelancer</span>                                      
                           </a>
                        </li>
                        <li>
                           <a href="${contextPath}/users/f">
                           <span class="menu-text">All Freelancer</span>                                      
                           </a>
                        </li>
                     </ul>
                  </div>
               </li>
               <li>
                  <a href="javascript:void(0);" data-action="click-trigger">
                  <span class="menu-icon entypo-icon"><i class="append-icon icon-network"> </i></span> 
                  <span class="menu-text">Manage Project</span>  
                  <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
                  </a>
                  <div class="child-menu"  data-action="click-target">
                     <ul>
                        <li>
                           <a href="${contextPath}/project">
                           <span class="menu-text">Create Project</span>                                      
                           </a>
                        </li>
                        <li>
                           <a href="${contextPath}/projects">
                           <span class="menu-text">All Projects</span>                                      
                           </a>
                        </li>
                     </ul>
                  </div>
               </li>
               <li>
                  <a href="javascript:void(0);" data-action="click-trigger">
                  <span class="menu-icon entypo-icon"><i class="icon-bookmark"> </i></span> 
                  <span class="menu-text">Manage Allocation</span>  
                  <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
                  </a>
                  <div class="child-menu"  data-action="click-target">
                     <ul>
                        <li>
                           <a href="${contextPath}/project-allocation">
                           <span class="menu-text">New Allocation</span>                                      
                           </a>
                        </li>
                        <li>
                           <a href="${contextPath}/project-allocations">
                           <span class="menu-text">All Allocations</span>                                      
                           </a>
                        </li>
                     </ul>
                  </div>
               </li>
               <li>
                  <a href="javascript:void(0);" data-action="click-trigger">
                  <span class="menu-icon entypo-icon"><i class="append-icon fa fa-fw fa-money"> </i></span> 
                  <span class="menu-text">Manage Payments</span>  
                  <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
                  </a>
                  <div class="child-menu"  data-action="click-target">
                     <ul>
                        <li>
                           <a href="${contextPath}/search-payments/">
                           <span class="menu-text">Pending Payments</span>                                      
                           </a>
                        </li>
                        
                     </ul>
                  </div>
               </li> --%>
            
            <%-- 
               <li>
                  <a href="javascript:void(0);" data-action="click-trigger">
                  <span class="menu-icon entypo-icon"><i class="append-icon icon-cc-nc-eu"> </i></span> 
                  <span class="menu-text">Manage Template</span>  
                  <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
                  </a>
                  <div class="child-menu"  data-action="click-target">
                     <ul>
                        <li>
                           <a href="${contextPath}/template">
                           <span class="menu-text">Create New Template</span>                                      
                           </a>
                        </li>
                        <li>
                           <a href="${contextPath}/templates">
                           <span class="menu-text">All Template</span>                                      
                           </a>
                        </li>
                     </ul>
                  </div>
               </li> --%>
           
           
              <%--  <li>
                  <a href="javascript:void(0);" data-action="click-trigger">
                  <span class="menu-icon entypo-icon"><i class="icon-palette"> </i></span> 
                  <span class="menu-text">Contract Board</span>  
                  <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
                  </a>
                  <div class="child-menu"  data-action="click-target">
                     <ul>
                        <li>
                           <a href="${contextPath}/contract">
                           <span class="menu-text">Create Contract</span>                                      
                           </a>
                        </li>
                        <li>
                           <a href="${contextPath}/contracts">
                           <span class="menu-text">All Contracts</span>                                      
                           </a>
                        </li>
                     </ul>
                  </div>
               </li> --%>
              <%--  <li>
                  <a href="javascript:void(0);" data-action="click-trigger">
                  <span class="menu-icon entypo-icon"><i class="append-icon fa fa-fw fa-search"> </i></span> 
                  <span class="menu-text">Search & Report</span>  
                  <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
                  </a>
                  <div class="child-menu"  data-action="click-target">
                     <ul>
                    	
                        <li>
                           <a href="${contextPath}/search">
                           <span class="menu-text">Search</span>                                      
                           </a>
                        </li>
                        <li>
                           <a href="${contextPath}/pay-calender">
                          <span class="menu-text">Payment Calender</span>                                    
                           </a>
                        </li>
                       
                     </ul>
                  </div>
               </li> --%>
             
           <%-- 
               <li>
                  <a href="javascript:void(0);" data-action="click-trigger">
                  <span class="menu-icon entypo-icon"><i class="append-icon fa fa-fw fa-external-link"> </i></span> 
                  <span class="menu-text">Manage CMS</span>  
                  <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
                  </a>
                  <div class="child-menu"  data-action="click-target">
                     <ul>
                        <li>
                           <a href="${contextPath}/post">
                           <span class="menu-text">Add Post</span>                                      
                           </a>
                        </li>
                        <li>
                           <a href="${contextPath}/posts">
                           <span class="menu-text">All Posts</span>                                      
                           </a>
                        </li>
                     </ul>
                  </div>
               </li> --%>
            
               <li>
            
         </ul>
         <!-- Head menu search form ends -->         
      </div>
   </div>
   <div class="navbar-spacing clearfix">
   </div>
   <div class="vd_menu vd_navbar-bottom-widget">
      <ul>
         <li>
            <a href="javascript:logout()">
            <span class="menu-icon"><i class="fa fa-sign-out"></i></span>          
            <span class="menu-text">Logout</span>             
            </a>
         </li>
      </ul>
   </div>
</div>
<!-- Menu end -->
<!-- chart Middle Content Start -->
<div class="vd_navbar vd_nav-width vd_navbar-chat vd_bg-black-80 vd_navbar-right   ">
   <div class="navbar-tabs-menu clearfix">
      <span class="expand-menu" data-action="expand-navbar-tabs-menu">
      <span class="menu-icon menu-icon-left">
      <i class="fa fa-ellipsis-h"></i>
      <span class="badge vd_bg-red">
      20
      </span>                    
      </span>
      <span class="menu-icon menu-icon-right">
      <i class="fa fa-ellipsis-h"></i>
      <span class="badge vd_bg-red">
      20
      </span>                    
      </span>                
      </span>  
      <div class="menu-container">
         <div class="navbar-search-wrapper">
            <div class="navbar-search vd_bg-black-30">
               <span class="append-icon"><i class="fa fa-search"></i></span>
               <input type="text" placeholder="Search" class="vd_menu-search-text no-bg no-bd vd_white width-70" name="search">
               <div class="pull-right search-config">
                  <a  data-toggle="dropdown" href="javascript:void(0);" class="dropdown-toggle" ><span class="prepend-icon vd_grey"><i class="fa fa-cog"></i></span></a>
                  <ul role="menu" class="dropdown-menu">
                     <li><a href="#">Action</a></li>
                     <li><a href="#">Another action</a></li>
                     <li><a href="#">Something else here</a></li>
                     <li class="divider"></li>
                     <li><a href="#">Separated link</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="navbar-menu clearfix">
      <div class="content-list content-image content-chat">
         <ul class="list-wrapper no-bd-btm pd-lr-10">
            <li class="group-heading vd_bg-black-20">FAVORITE</li>
            <li>
               <a href="#" class="mega-link" data-action="click-trigger">
                  <div class="menu-icon"><img src="${contextPath}/static/sFreelance/img/avatar/avatar.jpg" alt="example image"></div>
                  <div class="menu-text">
                     Jessylin
                     <div class="menu-info">
                        <span class="menu-date">Administrator </span>                                                         
                     </div>
                  </div>
                  <div class="menu-badge"><span class="badge status vd_bg-green">&nbsp;</span></div>
               </a>
            </li>
            <li>
               <a href="#" class="mega-link" data-action="click-trigger">
                  <div class="menu-icon"><img src="${contextPath}/static/sFreelance/img/avatar/avatar-2.jpg" alt="example image"></div>
                  <div class="menu-text">
                     Rodney Mc.Cardo
                     <div class="menu-info">
                        <span class="menu-date">Designer </span>                                                         
                     </div>
                  </div>
                  <div class="menu-badge"><span class="badge status vd_bg-grey">&nbsp;</span></div>
               </a>
            </li>
            <li>
               <a href="#">
                  <div class="menu-icon"><img src="${contextPath}/static/sFreelance/img/avatar/avatar-3.jpg" alt="example image"></div>
                  <div class="menu-text">
                     Theresia Minoque
                     <div class="menu-info">
                        <span class="menu-date">Engineering </span>                                                         
                     </div>
                  </div>
                  <div class="menu-badge"><span class="badge status vd_bg-green">&nbsp;</span></div>
               </a>
            </li>
            <li class="group-heading vd_bg-black-20">FRIENDS</li>
            <li>
               <a href="#">
                  <div class="menu-icon"><img src="${contextPath}/static/sFreelance/img/avatar/avatar-4.jpg" alt="example image"></div>
                  <div class="menu-text">
                     Greg Grog
                     <div class="menu-info">
                        <span class="menu-date">Developer </span>                                                         
                     </div>
                  </div>
                  <div class="menu-badge"><span class="badge status vd_bg-grey">&nbsp;</span></div>
               </a>
            </li>
            <li>
               <a href="#">
                  <div class="menu-icon"><img src="${contextPath}/static/sFreelance/img/avatar/avatar-5.jpg" alt="example image"></div>
                  <div class="menu-text">
                     Stefanie Imburgh
                     <div class="menu-info">
                        <span class="menu-date">Dancer</span>                                                         
                     </div>
                  </div>
                  <div class="menu-badge"><span class="vd_grey font-sm"><i class="fa fa-mobile"></i></span></div>
               </a>
            </li>
            <li>
               <a href="#">
                  <div class="menu-icon"><img src="${contextPath}/static/sFreelance/img/avatar/avatar-6.jpg" alt="example image"></div>
                  <div class="menu-text">
                     Matt Demon
                     <div class="menu-info">
                        <span class="menu-date">Musician </span>                                                         
                     </div>
                  </div>
                  <div class="menu-badge"><span class="vd_grey font-sm"><i class="fa fa-mobile"></i></span></div>
               </a>
            </li>
           
         </ul>
      </div>
   </div>
   <div class="navbar-spacing clearfix">
   </div>
</div>
<!-- chart Middle Content end -->



