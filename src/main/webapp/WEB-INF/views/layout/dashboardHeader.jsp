<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.*"%>
<c:set var="contextPath1" value="${pageContext.request.contextPath}" />
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<% String session_roleCode = (String)request.getSession().getAttribute("roleCode"); %> 
<c:set var = "roleCodeSession" value = "<%=session_roleCode%>" />	
<% String session_switchuser = (String)request.getSession().getAttribute("switchuser"); %> 
<c:set var = "switchuserSession" value = "<%=session_switchuser%>" />	

<% List<String> session_usernameList = (List<String>)request.getSession().getAttribute("usernameList"); %> 


 <% String appVersion_ = (String)request.getSession().getAttribute("appVersion"); %> 
        
        <% if(appVersion_=="" || appVersion_==null){%>
        <script>
       window.location.href = "${contextPath1}/user-login";
       
        </script>
       
        <% }%>   
  
<style>
   img {
   margin-top: -17px;
   max-width: 100%;
   }
   .vd_mega-menu-wrapper .vd_mega-menu>.mega-ul .mega-image img {
    -webkit-border-radius: 30px;
    -moz-border-radius: 30px;
    border-radius: 30px;
    /* vertical-align: top; */
    margin-top: 0px;
}
</style>
<!-- Header Start -->
<header class="header-1" id="header">
   <div class="vd_top-menu-wrapper">
      <div class="container ">
         <div class="vd_top-nav vd_nav-width  ">
            <div class="vd_panel-header">
               <div class="logo">
                <%--   <a href="${contextPath1}/"><img alt="logo" src="${contextPath1}/static/sFreelance/img/logo.png"></a> --%>
                  <a href="${contextPath1}/"><img alt="logo" src="https://jkfreelance.com/static/home/assets/images/logo.PNG"></a>
               </div>
               <!-- logo -->
              <%--  <div class="vd_panel-menu  hidden-sm hidden-xs" data-intro="<strong>Minimize Left Navigation</strong><br/>Toggle navigation size to medium or small size. You can set both button or one button only. See full option at documentation." data-step=1>
                  <span class="nav-medium-button menu" data-toggle="tooltip" data-placement="bottom" data-original-title="Medium Nav Toggle" data-action="nav-left-medium">
                  <i class="fa fa-bars"></i>
                  </span>
                  <span class="nav-small-button menu" data-toggle="tooltip" data-placement="bottom" data-original-title="Small Nav Toggle" data-action="nav-left-small">
                  <i class="fa fa-ellipsis-v"></i>
                  </span> 
               </div> --%>
               <div class="vd_panel-menu left-pos visible-sm visible-xs">
                  <span class="menu" data-action="toggle-navbar-left">
                  <i class="fa fa-ellipsis-v"></i> <i class="fa fa-ellipsis-v"></i><i class="fa fa-ellipsis-v"></i><i class="fa fa-ellipsis-v"></i><i class="fa fa-ellipsis-v"></i>
                 
                  
                  </span>  
               </div>
              <!--  <div class="vd_panel-menu visible-sm visible-xs">
                  <span class="menu visible-xs" data-action="submenu">
                  <i class="fa fa-bars"></i>
                  </span>        
                  <span class="menu visible-sm visible-xs" data-action="toggle-navbar-right">
                  <i class="fa fa-comments"></i>
                  </span>                   
               </div> -->
               <!-- vd_panel-menu -->
            </div>
            <!-- vd_panel-header -->
         </div>
         <div class="vd_container">
            <div class="row">
             <%--   <div class="col-sm-5 col-xs-12">
                  <div class="vd_menu-search">                 
                      <c:choose>
                           <c:when test="${(roleCodeSession eq 'ADMIN') or (not empty switchuserSession)}">
                          
						   							<select class="vd_menu-search-text width-90" id="userSelected" name="userSelected" > 
						   							<c:forEach items="<%=session_usernameList%>" var="data">
														<option  value="${data}" ${switchuserSession eq data?'selected':''}>${data}</option> 
													</c:forEach>
						   						</select>  
						 
						   	 </c:when>
                            <c:otherwise>	
                            			<input type="text" class="vd_menu-search-text width-50"  /> 									
                            </c:otherwise>
                        </c:choose>
						   						     
                     
                  </div>
               </div> --%>
               <div class="col-sm-12 col-xs-12">
                  <div class="vd_mega-menu-wrapper">                  
                     <div class="vd_mega-menu pull-right">
                        <ul class="mega-ul">
                           <li id="top-menu-2" class="one-icon mega-li">
                              <a href="index.html" class="mega-link" data-action="click-trigger">
                              <span class="mega-icon"><i class="fa fa-envelope"></i></span> 
                              <span class="badge vd_bg-red">1</span>        
                              </a>
                              <div class="vd_mega-menu-content width-xs-3 width-sm-4 width-md-5 width-lg-4 right-xs left-sm" data-action="click-target">
                                 <div class="child-menu">
                                    <div class="title">
                                       Messages  
                                       <div class="vd_panel-menu">
                                          <span data-original-title="Message Setting" data-toggle="tooltip" data-placement="bottom" class="menu">
                                          <i class="fa fa-cog"></i>
                                          </span>                                                                              
                                       </div>
                                    </div>
                                    <div class="content-list content-image">
                                       <div  data-rel="scroll">
                                          <ul class="list-wrapper pd-lr-10">
                                          
                                             <li>
                                                <div class="menu-icon"><img alt="" src="${contextPath1}/static/sFreelance/img/avatar/avatar.jpg"></div>
                                                <div class="menu-text">
                                                   Do you play or follow any sports?
                                                   <div class="menu-info">
                                                      <span class="menu-date">12 Minutes Ago </span>                                                                         
                                                      <span class="menu-action">
                                                      <span class="menu-action-icon" data-original-title="Mark as Unread" data-toggle="tooltip" data-placement="bottom">
                                                      <i class="fa fa-eye"></i>
                                                      </span>                                                                            
                                                      </span>                                
                                                   </div>
                                                </div>
                                             </li>
                                            
                                            
                                          </ul>
                                       </div>
                                       <div class="closing text-center" style="">
                                          <a href="#">See All Notifications <i class="fa fa-angle-double-right"></i></a>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- child-menu -->                      
                              </div>
                              <!-- vd_mega-menu-content --> 
                           </li>
                           <li id="top-menu-3"  class="one-icon mega-li">
                              <a href="index.html" class="mega-link" data-action="click-trigger">
                              <span class="mega-icon"><i class="fa fa-globe"></i></span> 
                              <span class="badge vd_bg-red">1</span>        
                              </a>
                              <div class="vd_mega-menu-content  width-xs-3 width-sm-4  center-xs-3 left-sm" data-action="click-target">
                                 <div class="child-menu">
                                    <div class="title">
                                       Notifications 
                                       <div class="vd_panel-menu">
                                          <span data-original-title="Notification Setting" data-toggle="tooltip" data-placement="bottom" class="menu">
                                          <i class="fa fa-cog"></i>
                                          </span>                   
                                          <!--                     <span class="text-menu" data-original-title="Settings" data-toggle="tooltip" data-placement="bottom">
                                             Settings
                                             </span> -->                                                              
                                       </div>
                                    </div>
                                    <div class="content-list">
                                       <div  data-rel="scroll">
                                       <span id="notifications"></span>
                                         <!-- <ul  class="list-wrapper pd-lr-10" id="notifications">
                                             <!-- <li>
                                                <a href="#">
                                                   <div class="menu-icon vd_yellow"><i class="fa fa-suitcase"></i></div>
                                                   <div class="menu-text">
                                                      Someone has give you a surprise 
                                                      <div class="menu-info"><span class="menu-date">12 Minutes Ago</span></div>
                                                   </div>
                                                </a>
                                             </li>
                                            
                                          </ul> -->
                                       </div>
                                       <div class="closing text-center" style="">
                                          <a href="#">See All Notifications <i class="fa fa-angle-double-right"></i></a>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- child-menu -->                      
                              </div>
                              <!-- vd_mega-menu-content -->         
                           </li>
                           <li id="top-menu-profile" class="profile mega-li">
                              <a href="#" class="mega-link"  data-action="click-trigger">
                             
                              <%String gender=(String)session.getAttribute("gender");%>
                             
                                 <span  class="mega-image">
                                 <img src="${contextPath1}/static/sFreelance/img/avtar/${gender eq 'MALE'?'male':'fmale_'}.PNG" alt="" />               
                                 </span>
                                 <span class="mega-name">
                                    <b>
                                       
                            <%--            <c:choose>
                           <c:when test="${not empty switchuserSession}">
                          ${switchuserSession}
						   	 </c:when>
                            <c:otherwise>	
                            		<security:authentication property="principal.username" />									
                            </c:otherwise>
                        </c:choose> --%>
                                    </b>
                                    <i class="fa fa-caret-down fa-fw"></i> 
                                 </span>
                              </a>
                              <div class="vd_mega-menu-content  width-xs-2  left-xs left-sm" data-action="click-target">
                                 <div class="child-menu">
                                    <div class="content-list content-menu">
                                       <ul class="list-wrapper pd-lr-10">
                                          <li>
                                             <a href="${contextPath1}/edit-profile">
                                                <div class="menu-icon"><i class=" fa fa-user"></i></div>
                                                <div class="menu-text">Edit Profile</div>
                                             </a>
                                          </li>
                                          <li>
                                             <a href="${contextPath1}/pwd-change">
                                                <div class="menu-icon"><i class=" fa fa-trophy"></i></div>
                                                <div class="menu-text">Change Password</div>
                                             </a>
                                          </li>
                                        <!--   <li>
                                             <a href="#">
                                                <div class="menu-icon"><i class=" fa fa-lock
                                                   "></i></div>
                                                <div class="menu-text">Privacy</div>
                                             </a>
                                          </li>
                                          <li>
                                             <a href="#">
                                                <div class="menu-icon"><i class=" fa fa-cogs"></i></div>
                                                <div class="menu-text">Settings</div>
                                             </a>
                                          </li> -->
                                           <li class="line"></li>
                                          <li>
                                             <a href="javascript:logout()">
                                                <div class="menu-icon"><i class=" fa fa-sign-out"></i></div>
                                                <div class="menu-text">Sign Out</div>
                                             </a>
                                          </li>
                                         <!-- 
                                          <li>
                                             <a href="#">
                                                <div class="menu-icon"><i class=" fa fa-question-circle"></i></div>
                                                <div class="menu-text">Help</div>
                                             </a>
                                          </li>
                                          <li>
                                             <a href="#">
                                                <div class="menu-icon"><i class=" glyphicon glyphicon-bullhorn"></i></div>
                                                <div class="menu-text">Report a Problem</div>
                                             </a>
                                          </li> -->
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </li>
                           <li id="top-menu-settings" class="one-big-icon hidden-xs hidden-sm mega-li" data-intro="<strong>Toggle Right Navigation </strong><br/>On smaller device such as tablet or phone you can click on the middle content to close the right or left navigation." data-step=2 data-position="left">
                              <a href="#" class="mega-link"  data-action="toggle-navbar-right">
                                 <span class="mega-icon">
                                 <i class="fa fa-comments"></i> 
                                 </span> 
                                 <!--            <span  class="mega-image">
                                    <img src="${contextPath1}/static/sFreelance/img/avatar/avatar.jpg" alt="" />               
                                    </span> -->           
                                 <span class="badge vd_bg-red">8</span>               
                              </a>
                           </li>
                        </ul>
                        <!-- Head menu search form ends -->                         
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- container --> 
   </div>
   <!-- vd_primary-menu-wrapper --> 
</header>
<!-- Placed at the end of the document so the pages load faster --> 
  
<!-- Header Ends --> 
<form action="${contextPath1}/logout" id="logout" method="post">
   <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />	
</form>
<form action="${contextPath1}/switch-user" id="switchuserForm" method="post">
   <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />	
   <input type="hidden" name="switchuser" id="switchuser"  />	
</form>

<script>
   function logout(){
   	$("#logout").submit();   
   }  

   $("#userSelected").on('change', function() {
  $("#switchuser").val(this.value); 
  $("#switchuserForm").submit();   
});
</script>