<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>  
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<style>
   footer .vd_bottom {
   background-color: #252525;
   color: #f5f5f5;
   padding-top: 10px;
   padding-bottom: 10px;
   margin-top: 60px;
   }
</style>
<footer class="footer-2"  id="footer">
   <div class="vd_bottom ">
      <div class="container">
         <div class="row">
            <div class=" col-xs-12">
               <div class="copyright text-center">
                  Copyright &copy;2014 JKfreelance Inc. All Rights Reserved 
               </div>
            </div>
         </div>
      </div>
   </div>
</footer>