<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<head>
   <meta charset="utf-8" />
   <title>sFreelance Web Portal</title>
   <meta name="keywords" content="HTML5 Template, CSS3, Metro Slider, Elegant HTML5 Theme" />
   <meta name="description" content="Multipurpose Responsive HTML5 Themes with Animated Metro Slider - Vencorp">
   <meta name="author" content="JKfreelance">
   <!-- Set the viewport width to device width for mobile -->
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <!-- Fav and touch icons -->
   <link rel="apple-touch-icon-precomposed" sizes="144x144" href="${contextPath}/static/sFreelance/img/ico/apple-touch-icon-144-precomposed.png">
   <link rel="apple-touch-icon-precomposed" sizes="114x114" href="${contextPath}/static/sFreelance/img/ico/apple-touch-icon-114-precomposed.png">
   <link rel="apple-touch-icon-precomposed" sizes="72x72" href="${contextPath}/static/sFreelance/img/ico/apple-touch-icon-72-precomposed.png">
   <link rel="apple-touch-icon-precomposed" href="${contextPath}/static/sFreelance/img/ico/apple-touch-icon-57-precomposed.png">
   <link rel="shortcut icon" href="${contextPath}/static/sFreelance/img/ico/favicon.png">
   <!-- CSS -->
   <!-- Bootstrap & FontAwesome & Entypo CSS -->
   <link href="${contextPath}/static/sFreelance/css/bootstrap.min.css" rel="stylesheet" type="text/css">
   <link href="${contextPath}/static/sFreelance/css/font-awesome.min.css" rel="stylesheet" type="text/css">
   <!--[if IE 7]>
   <link type="text/css" rel="stylesheet" href="${contextPath}/static/sFreelance/css/font-awesome-ie7.min.css">
   <![endif]-->
   <link href="${contextPath}/static/sFreelance/css/font-entypo.css" rel="stylesheet" type="text/css">
   <!-- Fonts CSS -->
   <link href="${contextPath}/static/sFreelance/css/fonts.css"  rel="stylesheet" type="text/css">
   <!-- Plugin CSS -->
   <link href="${contextPath}/static/sFreelance/plugins/jquery-ui/jquery-ui.custom.min.css" rel="stylesheet" type="text/css">
   <link href="${contextPath}/static/sFreelance/plugins/prettyPhoto-plugin/css/prettyPhoto.css" rel="stylesheet" type="text/css">
   <link href="${contextPath}/static/sFreelance/plugins/isotope/css/isotope.css" rel="stylesheet" type="text/css">
   <link href="${contextPath}/static/sFreelance/plugins/pnotify/css/jquery.pnotify.css" media="screen" rel="stylesheet" type="text/css">
   <link href="${contextPath}/static/sFreelance/plugins/google-code-prettify/prettify.css" rel="stylesheet" type="text/css">
   <link href="${contextPath}/static/sFreelance/plugins/mCustomScrollbar/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css">
   <link href="${contextPath}/static/sFreelance/plugins/tagsInput/jquery.tagsinput.css" rel="stylesheet" type="text/css">
   <link href="${contextPath}/static/sFreelance/plugins/bootstrap-switch/bootstrap-switch.css" rel="stylesheet" type="text/css">
   <link href="${contextPath}/static/sFreelance/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css">
   <link href="${contextPath}/static/sFreelance/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css">
   <link href="${contextPath}/static/sFreelance/plugins/colorpicker/css/colorpicker.css" rel="stylesheet" type="text/css">
   <!-- Specific CSS -->
   <link href="${contextPath}/static/sFreelance/plugins/layerslider/css/layerslider.css" rel="stylesheet" type="text/css">
   <link href="${contextPath}/static/sFreelance/css/animate.css" rel="stylesheet" type="text/css">
   <!-- Theme CSS -->
   <link href="${contextPath}/static/sFreelance/css/theme.min.css" rel="stylesheet" type="text/css">
   <!--[if IE]> 
   <link href="${contextPath}/static/sFreelance/css/ie.css" rel="stylesheet" >
   <![endif]-->
   <link href="${contextPath}/static/sFreelance/css/chrome.css" rel="stylesheet" type="text/chrome">
   <!-- chrome only css -->    
   <!-- Responsive CSS -->
   <link href="${contextPath}/static/sFreelance/css/theme-responsive.min.css" rel="stylesheet" type="text/css">
   <!-- for specific page in style css -->
   <!-- for specific page responsive in style css -->
   <!-- Custom CSS -->
   <link href="${contextPath}/static/sFreelance/custom/custom.css" rel="stylesheet" type="text/css">
   <script src="${contextPath}/static/sFreelance/js/bootbox.min.js"></script>
   <link rel="stylesheet" type="text/css" href="${contextPath}/static/sFreelance/css/datatables.min.css"/>
   <script type="text/javascript" src="${contextPath}/static/sFreelance/js/datatables.min.js"></script> 
   <!-- Head SCRIPTS -->
   <script type="text/javascript" src="${contextPath}/static/sFreelance/js/modernizr.js"></script> 
   <script type="text/javascript" src="${contextPath}/static/sFreelance/js/mobile-detect.min.js"></script> 
   <script type="text/javascript" src="${contextPath}/static/sFreelance/js/mobile-detect-modernizr.js"></script> 
   <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!--[if lt IE 9]>
   <script type="text/javascript" src="${contextPath}/static/sFreelance/js/html5shiv.js"></script>
   <script type="text/javascript" src="${contextPath}/static/sFreelance/js/respond.min.js"></script>     
   <![endif]-->
</head>
<body id="login-page" class="middle-layout no-nav-left no-nav-right  nav-top-fixed background-login     responsive remove-navbar front-layout   clearfix" data-active="login-page "  data-smooth-scrolling="1">
   <div class="vd_body">
   <!-- Header Start -->
   <header class="header-2" id="header">
      <div class="vd_top-menu-wrapper vd_bg-white light-top-menu">
         <div class="container">
            <div class="vd_top-nav vd_nav-width  ">
               <div class="vd_panel-header">
                  <div class="logo">
                     <a href="#"><img alt="logo" style="margin-top: -19px;" src="${contextPath}/static/sFreelance/img/logo.png"></a>
                  </div>
                  <!-- logo -->
                  <div class="vd_panel-menu visible-sm visible-xs">
                     <span class="menu visible-xs" data-action="submenu">
                     <i class="fa fa-bars"></i>
                     </span>                                 
                  </div>
                  <!-- vd_panel-menu -->
               </div>
               <!-- vd_panel-header -->
            </div>
            <div class="vd_container">
               <div class="row">
                  <div class="col-sm-8 col-xs-12">
                     <div class="vd_mega-menu-wrapper horizontal-menu">
                        <div class="vd_mega-menu">
                           <ul class="mega-ul nav">
                              <li class="mega-li"> 
                                 <a href="${contextPath}/" class="mega-link" data-waypoint="home"> 
                                 <span class="menu-text"><i class="fa fa-home font-md hidden-xs"></i><span class="visible-xs">Home</span></span>
                                 </a> 
                              </li>
                              <li class="mega-li"> 
                                 <a href="${contextPath}/" class="mega-link"> 
                                 <span class="menu-text">About Us</span>
                                 </a> 
                              </li>
                              <li class="mega-li"> 
                                 <a href="${contextPath}/" class="mega-link"> 
                                 <span class="menu-text">Services</span>
                                 </a> 
                              </li>
                              <li class="mega-li"> 
                                 <a href="${contextPath}/" class="mega-link"> 
                                 <span class="menu-text">Clients</span>
                                 </a> 
                              </li>
                              <li class="mega-li"> 
                                 <a href="${contextPath}/" class="mega-link"> 
                                 <span class="menu-text">Contact</span>
                                 </a> 
                              </li>
                              <li class="mega-li"> 
                                 <a href="${contextPath}/post-outer/latest" class="mega-link"> 
                                 <span class="menu-text">Job Post</span>
                                 </a> 
                              </li>
                           </ul>
                           <!-- Head menu search form ends -->                         
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-4 col-xs-12">
                     <div class="vd_mega-menu-wrapper">
                        <div class="vd_mega-menu pull-right">
                           <ul class="mega-ul">
                              <li id="top-menu-1" class="one-icon mega-li"> 
                                 <a href="${contextPath}/user-login" class="btn vd_btn vd_bg-yellow font-semibold">SignIn</a>	
                              </li>
                              <li id="top-menu-1" class="one-icon mega-li"> 
                                 <a href="${contextPath}/user-reg" class="btn vd_btn vd_bg-yellow font-semibold">Register</a>	
                              </li>
                           </ul>
                           <!-- Head menu search form ends -->                         
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- container --> 
      </div>
      <!-- vd_primary-menu-wrapper --> 
   </header>
   <!-- Header Ends -->    
   <body id="login-page" class="middle-layout no-nav-left no-nav-right  nav-top-fixed background-login     responsive remove-navbar front-layout   clearfix" data-active="login-page "  data-smooth-scrolling="1">
      <div class="vd_body">
         <!-- Header Ends --> 
         <div class="content">
            <div class="clearfix pd-20" >
               <div class="container">
                  <div class="vd_content clearfix">
                     <div class="row mgtp-20">
                        <div class="col-md-8">
                           <div class="panel widget light-widget panel-bd-top">
                              <div class="panel-heading no-title"> </div>
                              <div class="panel-body">
                                 <h2 class="mgtp--5">Posted Requirments ${skill}</h2>
                                 <div class="content-list content-blog-large">
                                    <h2 class="mgtp--5">${skill}</h2>
                                    <ul class="list-wrapper">
                                       <c:forEach items="${postList}" var="data" varStatus="serialNumber">
                                          <li>
                                             <div class="menu-icon"> <img alt="example image" src="${contextPath}/static/sFreelance/img/blog/01.jpg"> </div>
                                             <div class="menu-text">
                                                <h4 class="blog-title font-bold letter-xs"><a href="#">${data.title} </a></h4>
                                                <div class="menu-info">
                                                   <div class="menu-date font-xs">
                                                      <fmt:formatDate type = "both"   value = "${data.posteddate}" />
                                                   </div>
                                                </div>
                                                <div class="menu-tags  mgbt-xs-15"> Skills: 
                                                   <a href="#"><span class="label vd_bg-yellow"> ${data.skills}</span></a>
                                                </div>
                                                <p>${data.description} ...</p>
                                                <a class="btn vd_btn vd_bg-green btn-sm" href="#">Read More</a> 
                                             </div>
                                          </li>
                                       </c:forEach>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                           <!-- Panel Widget --> 
                        </div>
                        <!-- col-md-4 -->
                        <div class="col-md-4">
                           <div class="panel widget light-widget panel-bd-top vd_bdt-yellow">
                              <div class="panel-heading no-title"> </div>
                              <div class="panel-body">
                                 <h2 class="mgtp--5">Popular Skills</h2>
                                 <div class="content-list content-image">
                                    <ul class="list-wrapper no-bd-btm">
                                       <c:forEach items="${skills}" var="data">
                                          <li>
                                             <a href="${contextPath}/post-outer/${data}">
                                                <div class="menu-icon" style="max-width: 73%;" ><img src="${contextPath}/static/sFreelance/img/blog/01-square.jpg" alt="example image"></div>
                                                <div class="menu-text pd-5">
                                                   <h5 class="mgbt-xs-0">${data}</h5>
                                                   <div class="menu-info">
                                                      <jsp:useBean id="now" class="java.util.Date" />
                                                      <div class="menu-date font-xs">
                                                         Posted: 
                                                         <fmt:formatDate  type = "date"    value = "${now}" />
                                                      </div>
                                                   </div>
                                                </div>
                                             </a>
                                          </li>
                                       </c:forEach>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                           <!-- Panel Widget --> 
                        </div>
                        <!-- col-md-4 --> 
                     </div>
                     <!--row --> 
                  </div>
               </div>
            </div>
         </div>
         <!-- Middle Content End --> 
         <!--
            </div></div>-->
         <!-- Footer Start -->
         <footer class="footer-2"  id="footer">
            <div class="vd_bottom vd_bg-dark-green pd-20">
               <div class="container">
                  <div class="row">
                     <div class=" col-xs-12">
                        <div class="text-center mgbt-xs-10">
                           <a class="btn vd_btn vd_bg-facebook vd_round-btn btn-sm  mgr-10"><i class="fa fa-facebook fa-fw "></i></a>
                           <a class="btn vd_btn vd_bg-googleplus vd_round-btn btn-sm  mgr-10"><i class="fa fa-google-plus fa-fw"></i></a>
                           <a class="btn vd_btn vd_bg-twitter vd_round-btn btn-sm mgr-10"><i class="fa fa-twitter fa-fw "></i></a>                    
                        </div>
                        <div class="copyright text-center">
                           <p><span class="mgr-10">795 Folsom Ave, Suite 600</span><span class="mgr-10">-</span><span class="mgr-10">San Francisco, CA 94107</span><br/> 
                              P: (123) 456-7890 
                           </p>
                           Copyright &copy;2014 JKfreelance Inc. All Rights Reserved 
                        </div>
                     </div>
                  </div>
                  <!-- row -->
               </div>
               <!-- container -->
            </div>
         </footer>
         <!-- Footer END -->
      </div>
      <!-- .vc_body END  -->
      <a id="back-top" href="#" data-action="backtop" class="vd_back-top visible"> <i class="fa  fa-angle-up"> </i> </a>
      <!--
         <a class="back-top" href="#" id="back-top"> <i class="icon-chevron-up icon-white"> </i> </a> -->
      <!-- Javascript =============================================== --> 
      <!-- Placed at the end of the document so the pages load faster --> 
      <script type="text/javascript" src="${contextPath}/static/sFreelance/js/jquery.js"></script> 
      <!--[if lt IE 9]>
      <script type="text/javascript" src="${contextPath}/static/sFreelance/js/excanvas.js"></script>      
      <![endif]-->
      <script type="text/javascript" src="${contextPath}/static/sFreelance/js/bootstrap.min.js"></script> 
      <script type="text/javascript" src='${contextPath}/static/sFreelance/plugins/jquery-ui/jquery-ui.custom.min.js'></script>
      <script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>
      <script type="text/javascript" src="${contextPath}/static/sFreelance/js/caroufredsel.js"></script> 
      <script type="text/javascript" src="${contextPath}/static/sFreelance/js/plugins.js"></script>
      <script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/breakpoints/breakpoints.js"></script>
      <script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/dataTables/jquery.dataTables.min.js"></script>
      <script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/prettyPhoto-plugin/js/jquery.prettyPhoto.js"></script> 
      <script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
      <script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/tagsInput/jquery.tagsinput.min.js"></script>
      <script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/bootstrap-switch/bootstrap-switch.min.js"></script>
      <script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/blockUI/jquery.blockUI.js"></script>
      <script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/pnotify/js/jquery.pnotify.min.js"></script>
      <script type="text/javascript" src="${contextPath}/static/sFreelance/js/theme.js"></script>
      <script type="text/javascript" src="${contextPath}/static/sFreelance/custom/custom.js"></script>
      <!-- Specific Page Scripts Put Here -->
      <script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/stellar/jquery.stellar.js"></script>
      <script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/waypoints/waypoints.min.js"></script>
      <script type="text/javascript" src="${contextPath}/static/sFreelance/plugins/jquery-easing/jquery.easing.1.3.js"></script>
      <script src="${contextPath}/static/sFreelance/plugins/layerslider/js/greensock.js" type="text/javascript"></script>
      <!-- LayerSlider script files -->
      <script src="${contextPath}/static/sFreelance/plugins/layerslider/js/layerslider.transitions.js" type="text/javascript"></script>
      <script src="${contextPath}/static/sFreelance/plugins/layerslider/js/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script>
      <script type="text/javascript">
         $(document).ready(function() {
         	
         	"use strict";
         	
         	var options = { 
         		type: "POST",
         		url:  $("#contact-form-widget").attr('action'),
         		dataType: "json",
         		success: function(data) {
         			if (data.response == "success") {
         				$("#contact-form-result div").addClass('hidden');
         				$("#contact-form-result #success").removeClass('hidden');						
         			} else if (data.response == "error") {
         				$("#contact-form-result div").addClass('hidden');
         				$("#contact-form-result #error").removeClass('hidden');	
         				$("#contact-form-result #error").append(data.message);				
         			} else if (data.response == "empty") {
         				$("#contact-form-result div").addClass('hidden');
         				$("#contact-form-result #empty").removeClass('hidden');						
         			} else if (data.response == "unexpected") {
         				$("#contact-form-result div").addClass('hidden');
         				$("#contact-form-result #unexpected").removeClass('hidden');						
         			}	
         			$("#contact-form-widget").find('#contact-form-submit #spin').remove();
         			$("#contact-form-widget").find('#contact-form-submit').removeClass('disabled').removeAttr('disabled').blur();	 
         
         			$("#contact-form-widget").fadeOut(500, function(){
         				$('#contact-form-result').fadeIn(500);
         			});										
         		},
         		error: function() {
         				$("#contact-form-result div").addClass('hidden');
         				$("#contact-form-result #unexpected").removeClass('hidden');	
         		}
         	}; 	
         	
         	$("#contact-form-widget").validate({
         		submitHandler: function(form) {			
         			$(form).find('#contact-form-submit').prepend('<i id="spin" class="fa fa-spinner fa-spin mgr-10"></i>').addClass('disabled').attr('disabled');			
         			$(form).ajaxSubmit(options);			
         		},
         		success: function(form){	
         		}
         	});	
         	
         	
         	   $(".vd_testimonial .vd_carousel").carouFredSel({
         			prev:{
         				button : function()
         				{
         					return $(this).parent().parent().children('.vd_carousel-control').children('a:first-child')
         				}
         			},
         			next:{
         				button : function()
         				{
         					return $(this).parent().parent().children('.vd_carousel-control').children('a:last-child')
         				}
         			},		
         			scroll: {
         				fx: "crossfade",
         				onBefore: function(){
         						var target = "#front-1-clients";
         						$(target).css("transition","all .5s ease-in-out 0s");				
         					if ($(target).hasClass("vd_bg-soft-yellow")){						
         						$(target).removeClass("vd_bg-soft-yellow");
         						$(target).addClass("vd_bg-soft-red");		
         					} else
         					if ($(target).hasClass("vd_bg-soft-red")){						
         						$(target).removeClass("vd_bg-soft-red");
         						$(target).addClass("vd_bg-soft-blue");		
         					} else
         					if ($(target).hasClass("vd_bg-soft-blue")){						
         						$(target).removeClass("vd_bg-soft-blue");
         						$(target).addClass("vd_bg-soft-green");		
         					} else
         					if ($(target).hasClass("vd_bg-soft-green")){						
         						$(target).removeClass("vd_bg-soft-green");
         						$(target).addClass("vd_bg-soft-yellow");		
         					} 					
         				}
         			},
         			width: "auto",
         			height: "responsive",
         			responsive: true
         			
         		});
         		
         	    var slide = $('.slide-waypoint');		
         		
         		//Setup waypoints plugin
         		slide.waypoint(function(direction) {
         	//  		alert('Direction example triggered scrolling ' + direction);
         			//cache the variable of the data-waypoint attribute associated with each slide
         			var dataslide = $(this).attr('data-waypoint');
         	
         	
         	
         			//If the user scrolls up change the navigation link that has the same data-waypoint attribute as the slide to active and 
         			//remove the active class from the previous navigation link 
         /*			if (direction === 'down') {	
         				resetActive();
         				$('.vc_primary-menu  ul li a[data-waypoint="' + dataslide + '"]').parent().addClass('active');		
         			}
         			else {
         				resetActive();
         				$('.vc_primary-menu  ul li a[data-waypoint="' + dataslide + '"]').parent().prev().addClass('active');						
         			}
         			*/
         	
         		},{offset:0});		
         		
         
         
         		$(".feature-item, .vd_gallery .gallery-item").css("opacity",0);	
         		
         		
         		$("#front-1-services").waypoint(function () {	
         			$(".feature-1").delay().queue(function () {
         				$('.feature-1').addClass("animated fadeInRightBig");				
         			});	
         			$(".feature-3").delay(200).queue(function () {
         				$('.feature-3').addClass("animated fadeInRightBig");				
         			});	
         			$(".feature-5").delay(400).queue(function () {
         				$('.feature-5').addClass("animated fadeInRightBig");				
         			});	
         			$(".feature-2").delay(600).queue(function () {
         				$('.feature-2').addClass("animated fadeInRightBig");				
         			});	
         			$(".feature-4").delay(800).queue(function () {
         				$('.feature-4').addClass("animated fadeInRightBig");				
         			});	
         			$(".feature-6").delay(1000).queue(function () {
         				$('.feature-6').addClass("animated fadeInRightBig");				
         			});																			
         		
         		}, 	{ offset: 600	});
         		
         		$(".vd_gallery").waypoint(function () {
         			$(".vd_gallery .gallery-1").queue(function () {
         				$('.vd_gallery .gallery-1').addClass("animated fadeInUp");				
         			});	
         			$(".vd_gallery .gallery-2").delay(200).queue(function () {
         				$('.vd_gallery .gallery-2').addClass("animated fadeInUp");				
         			});				
         			$(".vd_gallery .gallery-3").delay(400).queue(function () {
         				$('.vd_gallery .gallery-3').addClass("animated fadeInUp");				
         			});	
         			$(".vd_gallery .gallery-4").delay(600).queue(function () {
         				$('.vd_gallery .gallery-4').addClass("animated fadeInUp");				
         			});	
         			$(".vd_gallery .gallery-5").delay(800).queue(function () {
         				$('.vd_gallery .gallery-5').addClass("animated fadeInUp");				
         			});	
         			$(".vd_gallery .gallery-6").delay(1000).queue(function () {
         				$('.vd_gallery .gallery-6').addClass("animated fadeInUp");				
         			});	
         			$(".vd_gallery .gallery-7").delay(1200).queue(function () {
         				$('.vd_gallery .gallery-7').addClass("animated fadeInUp");				
         			});	
         			$(".vd_gallery .gallery-8").delay(1400).queue(function () {
         				$('.vd_gallery .gallery-8').addClass("animated fadeInUp");				
         			});	
         			$(".vd_gallery .gallery-9").delay(1600).queue(function () {
         				$('.vd_gallery .gallery-9').addClass("animated fadeInUp");				
         			});	
         																																							
         		
         		}, 	{ offset: 600	});				
         		
         		
         		//Create a function that will be passed a slide number and then will scroll to that slide using jquerys animate. The Jquery
         		//easing plugin is also used, so we passed in the easing method of 'easeInOutQuint' which is available throught the plugin.
         		function goToByScroll(dataslide) {
         			if (dataslide=="home"){
         				$('html,body').animate({scrollTop:0},1500, 'easeInOutQuint');	
         			} else {
         				$('html,body').animate({
         				   scrollTop: $('.slide-waypoint[data-waypoint="' + dataslide + '"]').offset().top
         				}, 1500, 'easeInOutQuint');				
         			}
         		}	
         
         		
         		$('.vd_top-menu-wrapper .horizontal-menu .nav > li >  a[data-waypoint]').click(function (e) {		
         			e.preventDefault();
         			
         			var dataslide = $(this).attr('data-waypoint');
         			goToByScroll(dataslide);		
         		});		
         		
         		jQuery("#layerslider").layerSlider({
         			responsive: false,
         			responsiveUnder: 1280,
         			layersContainer: 1280,
         			skin: 'noskin',
         			hoverPrevNext: false,
         			skinsPath: '${contextPath}/static/sFreelance/plugins/layerslider/skins/'
         		});		
         });
      </script>
      <!-- Specific Page Scripts END -->
      <!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information. -->
      <script>
         var _gaq = _gaq || [];
         _gaq.push(['_setAccount', 'UA-XXXXX-X']);
         _gaq.push(['_trackPageview']);
         
         (function() {
             var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
             ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
             var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
         })();
      </script> 
</body>
</html>