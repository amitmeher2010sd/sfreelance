<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<c:if test="${not empty successMessage}">
   <script>
      bootbox.alert("${successMessage}");  
   </script>
</c:if>
<c:if test="${not empty failureMessage}">
   <script>
      bootbox.alert("${failureMessage}"); 
   </script>
</c:if>
<!-- Middle Content Start -->
<div class="vd_content-wrapper">
   <div class="vd_container">
      <div class="vd_content clearfix">
         <div class="vd_head-section clearfix">
            <div class="vd_panel-header">
               <ul class="breadcrumb">
                  <li><a href="index.html">Home</a> </li>
                  <li><a href="./">Manage Posts</a> </li>
                  <li class="active">Manage Posts</li>
               </ul>
               <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step=5  data-position="left">
              <!--     <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
                  <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
                  <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>
           -->     </div>
            </div>
         </div>
         <div class="vd_title-section clearfix">
            <div class="vd_panel-header">
               <h1>Manage Posts</h1>
               <small class="subtitle">Look <a href="./">below</a> for more information</small> 
            </div>
         </div>
         <div class="vd_content-section clearfix">
            <div class="row">
               <div class="col-md-12">
                  <div class="panel widget">
                     <c:if test="${not empty successMessage}">
                        <div class="alert alert-success alert-dismissable">
                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cross"></i></button>
                           <i class="fa fa-check-circle append-icon"></i><strong>Well done!</strong> ${successMessage} 
                        </div>
                     </c:if>
                     <c:if test="${not empty failureMessage}">
                        <div class="alert alert-danger alert-dismissable alert-condensed">
                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cross"></i></button>
                           <i class="fa fa-exclamation-circle append-icon"></i>
                           <strong>Oh snap!</strong> ${failureMessage} 
                        </div>
                     </c:if>
                     <div class="panel-heading vd_bg-grey">
                        <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span>List Of Posts</h3>
                     </div>
                     <div class="panel-body table-responsive">
                        <a href="${contextPath}/post" type="button" style="margin-left: 92%;"
                           class="btn vd_btn vd_bg-green vd_white">Add Post</a>
                        <table class="table table-striped" id="data-tables">
                           <thead>
                              <tr>
                                 <th>#</th>
                                 <th>title</th>
                                 <th>posteddate</th>
                                 <th>skills</th>
                                 <th>description</th>
                                 <th>Created On</th>
                                 <th>Updated On</th>
                                 <th>Status</th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody id="tbd">
                              <c:forEach items="${postList}" var="data" varStatus="serialNumber">
                                 <tr>
                                    <td>${serialNumber.count}</td>
                                    <td>${data.title}</td>
                                    <td>
                                       <fmt:formatDate type = "both"   value = "${data.posteddate}" />
                                    </td>
                                    <td>${data.skills}</td>
                                    <td>${data.description}</td>
                                     <td>
                                       <fmt:formatDate type = "both"   value = "${data.createdOn}" />&nbsp;${not empty data.createdBy?'By:':''} ${data.createdBy.userName}
                                    </td>
                                    <td>
                                       <fmt:formatDate type = "both"   value = "${data.updatedOn}" />&nbsp;${not empty data.updatedBy?'By:':''} ${data.updatedBy.userName}
                                    </td>
                                    
                                    <td>${data.posted eq true?'Published':'Un-Published'}</td>
                                    <td>
                                       <c:if test="${data.status eq 'ACTIVE'}">  
                                          ${data.status}
                                       </c:if>
                                       <c:if test="${data.status eq 'INACTIVE'}">  
                                          ${data.status}
                                       </c:if>
                                    </td>
                                    <td>
                                       <c:if test="${data.status eq 'ACTIVE'}">
                                          <c:if test="${data.posted eq false}">
                                             <button class="btn btn-warning btn-sm"
                                                onclick="edit('${data.getId()}')">
                                             <i class=" fa fa-pencil-square-o " aria-hidden="true"></i>
                                             </button>
                                             <button class="btn btn-danger btn-sm"
                                                onclick="del(${data.getId()})">
                                             <i class=" glyphicon glyphicon-trash " aria-hidden="true"></i>
                                             </button>
                                          </c:if>
                                          <button class="btn btn-green btn-sm"
                                             onclick="publish(${data.getId()},${data.posted eq true?false:true})">
                                          <i class="append-icon icon-publish" aria-hidden="true"></i>
                                          </button>
                                       </c:if>
                                       <c:if test="${data.status eq 'INACTIVE'}">
                                          <button class="btn btn-warning btn-sm"
                                             disabled>
                                          <i class=" fa fa-pencil-square-o " aria-hidden="true"></i>
                                          </button>
                                          <button class="btn btn-success btn-sm"
                                             onclick="active(${data.getId()})">
                                          <i class=" glyphicon glyphicon-trash " aria-hidden="true"></i>
                                          </button>
                                          <button class="btn btn-danger btn-sm"
                                             onclick="publish(${data.getId()},${data.posted eq true?false:true})">
                                          <i class="append-icon icon-publish" aria-hidden="true"></i>
                                          </button>
                                       </c:if>
                                    </td>
                                 </tr>
                              </c:forEach>
                           </tbody>
                        </table>
                     </div>
                  </div>
                  <!-- Panel Widget --> 
               </div>
               <!-- col-md-12 --> 
            </div>
            <!-- row --> 
         </div>
         <!-- .vd_content-section --> 
      </div>
      <!-- .vd_content --> 
   </div>
   <!-- .vd_container --> 
</div>
<!-- .vd_content-wrapper --> 
<form action="${contextPath}/edit-post" method="POST" id="edit">
   <input type="hidden" name="${_csrf.parameterName}"
      value="${_csrf.token}" />
   <input type="hidden" name="id_edit" id="id_edit" />
</form>
<form action="${contextPath}/del-post" method="POST"
   id="delete">
   <input type="hidden" name="${_csrf.parameterName}"
      value="${_csrf.token}" />
   <input type="hidden" name="id_del" id="id_del" />
</form>
<form action="${contextPath}/active-post" method="POST"
   id="enabla">
   <input type="hidden" name="${_csrf.parameterName}"
      value="${_csrf.token}" />
   <input type="hidden" name="id_del" id="id_del_en" />
</form>
<form action="${contextPath}/publish-post" method="POST"
   id="publish">
   <input type="hidden" name="${_csrf.parameterName}"
      value="${_csrf.token}" />
   <input type="hidden" name="publish_id" id="publish_id" />
   <input type="hidden" name="publish_flag" id="publish_flag" />
</form>

<script> 
   function edit(id){
   	$("#id_edit").val(id);
   	bootbox.confirm("Do you want to edit ?",
   	function(result) {
   		if (result == true) {
   		   $("#edit").submit();
   		} 
   	});
   }	
   function del(id){
   	$("#id_del").val(id);
   	bootbox.confirm("Do you want to delete?",
       function(result) {
           if (result == true) {
               $("#delete").submit();
           } 
       });
   } 
   function active(id){
   	$("#id_del_en").val(id);
   	bootbox.confirm("Do you want to enable?",
       function(result) {
           if (result == true) {
               $("#enabla").submit();
           } 
       });
   } 
   	function publish(id,flag){
   	$("#publish_id").val(id);
   	$("#publish_flag").val(flag);
   	bootbox.confirm("Do you want to proceeds?",
       function(result) {
           if (result == true) {
               $("#publish").submit();
           } 
       });
   } 
   
</script>