<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<!-- Middle Content Start -->    
<div class="vd_content-wrapper">
   <div class="vd_container">
      <div class="vd_content clearfix">
         <div class="vd_head-section clearfix">
            <div class="vd_panel-header">
               <ul class="breadcrumb">
                  <li><a href="${contextPath}/">Home</a> </li>
                  <li><a href="${contextPath}/">Apply Client Management</a> </li>
                  <li class="active">${not empty applyclient.getId() ?'Modify Apply Client':'Create Apply Client'}</li>
               </ul>
            </div>
         </div>
         <div class="vd_title-section clearfix">
            <div class="vd_panel-header">
               <h1> ${not empty applyclient.getId() ?'Modify Apply Client':'Create Apply Client'}</h1>
               <small class="subtitle">Fill the below form ('*' for Mandetory Field)</small>              
            </div>
         </div>
         <!-- vd_title-section -->          
         <div class="vd_content-section clearfix">
            <div class="row" id="advanced-input">
               <div class="col-md-12">
                  <div class="panel widget">
                     <div class="panel-heading vd_bg-grey">
                        <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-bar-chart-o"></i> </span> Apply Client Details </h3> 
                     </div>
                     <div class="panel-body">
                        <form class="form-horizontal"  action="${contextPath}/applyclient" role="form" id="register-form" method="post" modelAttribute="applyclient" onsubmit="locadingProgress();">
                           <input type="hidden" name="${_csrf.parameterName}"	value="${_csrf.token}" />
                           <input type="hidden" name="Id" value="${applyclient.getId()}" />
                           <div class="alert alert-danger vd_hidden">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-cross"></i></button>
                              <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Oh snap!</strong> Change a few things up and try submitting again. 
                           </div>
                           <div class="alert alert-success vd_hidden">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-cross"></i></button>
                              <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Well done!</strong>.
                           </div>
                           <div class="form-group">
                              <label class="col-sm-2 control-label">Name <span class="vd_red">*</span></label>
                              <div class="col-sm-3 controls">
                                 <input type="text" class="form-control required" id="name" name="name" value="${applyclient.name}"  class="width-70"
                                    >
                              </div>
                              <label class="col-sm-2 control-label">Mob No# <span class="vd_red">*</span> </label>
                              <div class="col-sm-3 controls">
                                 <input type="text" class="form-control required" id="mobile"
                                    name="mobile" value="${applyclient.mobile}"  maxlength="15" >
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-sm-2 control-label">Email Id  </label>
                              <div class="col-sm-3 controls">
                                 <input type="text" class="form-control ignoreValidation" id="email" 
                                    name="email" value="${applyclient.email}" maxlength="100">
                              </div>
                              <label class="col-sm-2 control-label">Gender <span class="vd_red">*</span> </label>
                              <div class="col-sm-3 controls">
                                 <select class="form-control form-control-sm required" id="gender" name="gender" value="${applyclient.gender}">
                                    <option value="">Select</option>
                                    <option ${applyclient.gender eq 'MALE' ?'selected':''} value="MALE">Male</option> 
                                    <option ${applyclient.gender eq 'FEMALE' ?'selected':''} value="FEMALE">Female</option> 
                                    <option ${applyclient.gender eq 'OTHER' ?'selected':''} value="OTHER">Other</option> 
                                 </select>
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-sm-2 control-label">Skills <span class="vd_red">*</span></label>
                              <div class="col-sm-3 controls">
                                 <select class="form-control form-control-sm required" id="skills" name="skills" >
                                    <option value="">-Select-</option>
                                    <c:forEach items="${skillList}" var="data">
                                       <option  value="${data.skillName}" ${applyclient.skills eq data.skillName ?'selected':''}>${data.skillName}</option> 
                                    </c:forEach>
                                 </select>
                              </div>
                              <label class="col-sm-2 control-label"> Requirments Details (If Any)</label>
                              <div class="col-sm-3 controls">
                                 <textarea  rows="4" cols="30" id="requirmentsDetails" name="requirmentsDetails" 
                                    tabindex="2" class="form-control" >${applyclient.requirmentsDetails}</textarea>
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-sm-2 control-label">Freelancer <span class="vd_red">*</span> </label>
                              <div class="col-sm-3 controls">
                                 <select name="freelancerId" id="freelancerId" class="form-control ignoreValidation" >
                                    <option value="">-Select-</option>
                                    <c:forEach items="${freelancerList}" var="data">
                                       <option ${applyclient.freelancer.userId eq data.user.userId ?'selected':''} value="${data.user.userId}">${data.user.name}-${data.user.userName}-${data.user.email}</option> 
                                    </c:forEach>
                                 </select>
                              </div>
                              <label class="col-sm-2 control-label"> Freelancer Details  (If Any)</label>
                              <div class="col-sm-3 controls">
                                 <textarea  rows="4" cols="30" id="freelancerDetails" name="freelancerDetails" 
                                    tabindex="2" class="form-control" >${applyclient.freelancerDetails}</textarea>
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-sm-2 control-label"> Address <span class="vd_red"></span></label>
                              <div class="col-sm-3 controls">
                                 <input type="text" class="form-control" id="address" value="${applyclient.address}"
                                    name="address"  >
                              </div>
                              <label class="col-sm-2 control-label">WhatsApp Group :</label>
                              <div class="col-sm-3 controls">
                                 <input type="text" class="form-control ignoreValidation" id="whatsappgroup" 
                                    name="whatsappgroup" value="${applyclient.whatsappgroup}" maxlength="100">
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-sm-2 control-label"> Meeting Date <span class="vd_red"></span></label>
                              <div class="col-sm-2 controls">
                                 <input type="text" class="form-control  ignoreValidation date" id="meetDate" value="${applyclient.meetDate}"
                                    name="meetDate"  >
                              </div>
                              <div class="col-sm-2 controls">
                                 <div class="input-group bootstrap-timepicker">
                                    <input type="text" placeholder="Date" id="timepicker-default" id="meettime" name="meettime"  value="${applyclient.meettime}">
                                    <span class="input-group-addon" id="timepicker-default-span"><i class="fa fa-clock-o"></i></span> 
                                 </div>
                              </div>
                              <label class="col-sm-1 control-label">Demo Status <span class="vd_red">*</span> </label>
                              <div class="col-sm-2 controls">
                                 <select class="form-control form-control-sm required" id="demostatus" name="demostatus" value="${applyclient.demostatus}">
                                    <option value="">-Select-</option>
                                    <c:forEach items="${demoStatusList}" var="data">
                                       <option  value="${data}" ${applyclient.demostatus eq data?'selected':''}>${data}</option> 
                                    </c:forEach>
                                 </select>
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-sm-2 control-label"> Status <span class="vd_red">*</span></label>
                              <div class="col-sm-3 controls">
                                 <select class="form-control form-control-sm required" id="status" name="status" value="${applyclient.status}" >                                    
                                 <option ${applyclient.status eq 'PENDING' ?'selected':''} value="PENDING">PENDING</option> 
                                 <option ${applyclient.status eq 'CLOSED' ?'selected':''} value="CLOSED">CLOSED</option> 
                                 <option ${applyclient.status eq 'CANCELED' ?'selected':''} value="CANCELED">CANCELED</option> 
                                 <option ${applyclient.status eq 'INACTIVE' ?'selected':''} value="INACTIVE">INACTIVE</option> 
                                 </select>
                              </div>
                              <label class="col-sm-2 control-label"> Remarks <span class="vd_red"></span></label>
                              <div class="col-sm-3 controls">
                                 <textarea  rows="4" cols="30" id="remarks" name="remarks" 
                                    tabindex="2" class="form-control ignoreValidation" maxlength="500">${applyclient.remarks}</textarea>
                              </div>
                           </div>
                           <div class="form-group form-actions">
                              <div class="col-sm-4"> </div>
                              <div class="col-sm-7">
                                 <button class="btn vd_btn vd_bg-green vd_white" type="submit" id="submit-register" name="submit-register"><i class="icon-ok"></i> ${not empty applyclient.getId() ?'Update':'Save'}</button>
                                 <a class="btn vd_btn vd_bg-grey"   href="${contextPath}/applyclients">Back</a>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
                  <!-- Panel Widget --> 
               </div>
               <!-- col-md-12 --> 
            </div>
            <!-- row -->
         </div>
         <!-- .vd_content-section --> 
      </div>
      <!-- .vd_content --> 
   </div>
   <!-- .vd_container --> 
</div>
<!-- .vd_content-wrapper --> 
<!-- Middle Content End --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script type="text/javascript" src="${contextPath}/static/sFreelance/js/jquery.js"></script> 
<script type="text/javascript" src='${contextPath}/static/sFreelance/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js'></script>
<link href="${contextPath}/static/sFreelance/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css">

<script>
   $(function() {	
   	$('#name').keydown(function (e) {	  
   		if (e.ctrlKey || e.altKey) {	    
   			e.preventDefault();	      
   		} else {	    
   			var key = e.keyCode;	      
   			if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {	      
   				e.preventDefault();	
   			} 
   		}	
   	});         
   });	
   function validateNumber() { 
   	if((event.keyCode<48 || event.keyCode>57))
   	{
   		event.returnValue = false;
   	} 
   }  
   function ValidateEmail(email) {
       if (email != "") {
           var reg = /^[A-Za-z0-9]([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
           if (!reg.test(email)) {
           	bootbox.alert('Enter a valid email id'); 
               return false;
           }
           else
               return true;
       }
       else
           return true;
   }; 
</script>
<script type="text/javascript">
   $(document).ready(function() {
   		"use strict";	
   
           var form_register = $('#register-form');
           var error_register = $('.alert-danger', form_register);
           var success_register = $('.alert-success', form_register);
   
           form_register.validate({
               errorElement: 'div', //default input error message container
               errorClass: 'vd_red', // default input error message class
               focusInvalid: false, // do not focus the last invalid input
               ignore: ".ignoreValidation",
               rules: {
                 
               },
   			
   
             	errorPlacement: function(error, element) {
   				if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
   					element.parent().append(error);
   				} else if (element.parent().hasClass("vd_input-wrapper")){
   					error.insertAfter(element.parent());
   				}else {
   					error.insertAfter(element);
   				}
   			}, 
   			
               invalidHandler: function (event, validator) { //display error alert on form submit              
   					success_register.fadeOut();
   					error_register.fadeIn();
   					scrollTo(form_register,-100);
   
               },
   
               highlight: function (element) { // hightlight error inputs
   		
   				$(element).addClass('vd_bd-red');
   				$(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
   
               },
   
               unhighlight: function (element) { // revert the change dony by hightlight
                   $(element)
                       .closest('.control-group').removeClass('error'); // set error class to the control group
               },
   
               success: function (label, element) {
                   label
                       .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                   	.closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
   				$(element).removeClass('vd_bd-red');
   				
               },
   
               submitHandler: function (form) {
   					success_register.fadeIn();
   					error_register.fadeOut();
   					scrollTo(form_register,-100);	
   					$('#register-form').submit();				
               }
           });
   });
   
     $('#timepicker-default').timepicker();
   
     function locadingProgress(){
    	  $("#loading").show();
    	  alert(1);
     }
</script>