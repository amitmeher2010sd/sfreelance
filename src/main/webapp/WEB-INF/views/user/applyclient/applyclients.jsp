<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<c:if test="${not empty successMessage}">
   <script>
      bootbox.alert("${successMessage}");  
   </script>
</c:if>
<c:if test="${not empty failureMessage}">
   <script>
      bootbox.alert("${failureMessage}"); 
   </script>
</c:if>
<!-- Middle Content Start -->
<div class="vd_content-wrapper">
   <div class="vd_container">
      <div class="vd_content clearfix">
         <div class="vd_head-section clearfix">
            <div class="vd_panel-header">
               <ul class="breadcrumb">
                  <li><a href="index.html">Home</a> </li>
                  <li><a href="./">Manage Applied Client</a> </li>
                  <li class="active">Manage Applied Client</li>
               </ul>
               <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step=5  data-position="left">
             <!--      <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
                  <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
                  <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>
         -->       </div>
            </div>
         </div>
         <div class="vd_title-section clearfix">
            <div class="vd_panel-header">
               <h1>Manage  Applied Client</h1>
               <small class="subtitle">Look <a href="./">below</a> for more information</small> 
            </div>
         </div>
         <div class="vd_content-section clearfix">
            <div class="row">
               <div class="col-md-12">
                  <div class="panel widget">
                     <c:if test="${not empty successMessage}">
                        <div class="alert alert-success alert-dismissable">
                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cross"></i></button>
                           <i class="fa fa-check-circle append-icon"></i><strong>Well done!</strong> ${successMessage} 
                        </div>
                     </c:if>
                     <c:if test="${not empty failureMessage}">
                        <div class="alert alert-danger alert-dismissable alert-condensed">
                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cross"></i></button>
                           <i class="fa fa-exclamation-circle append-icon"></i>
                           <strong>Oh snap!</strong> ${failureMessage} 
                        </div>
                     </c:if>
                     <div class="panel-heading vd_bg-grey">
                        <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span>List Of Applied Client</h3>
                     </div>
                     <div class="panel-body table-responsive">
                      <div class="form-group">
                           <label class="col-sm-2 control-label">By Project Status<span class="vd_red"></span> </label>
                           <div class="col-sm-3 controls">
                             <select class="form-control form-control-sm required"   onChange="loadDataByStatus(this.value);">
                                    <option value="ALL">ALL</option>
                                    <c:forEach items="${demoStatusList}" var="data">
                                       <option  value="${data}" ${demostatus eq data?'selected':''}>${data}</option> 
                                    </c:forEach>
                                 </select>                            
                           </div>
                        </div>
                     <a href="${contextPath}/applyclient" type="button" style="margin-left: 92%;" 
                           class="btn vd_btn vd_bg-green vd_white">Add New</a>                    
                           </br> </br> </br>
                        <table class="table table-striped" id="data-tables">
                           <thead>
                              <tr>
                                  <th>#</th>
                                 <th>Client</th>
                                 <th>Skills</th>
                                  <th>Requirments Details</th>
                                 <th>Freelancer</th>
                                 <th>Meeting</th>
                                 <th>WhatsApp Group</th>                                
                                 <th>Remarks</th>
                                 <th>Created On</th>
                                 <th>Updated On</th>
                                 <th>Status</th>
                                 <th>Action</th>
                                 
                                 
                              </tr>
                           </thead>
                           <tbody id="tbd">
                              <c:forEach items="${applyclientList}" var="data" varStatus="serialNumber">
                                 <tr style="color:${data.status ne 'CLOSED'?'black':'#15dc15'}">
                                    <td>${serialNumber.count}</td>
                                    <td> ${data.name}<br>${data.mobile}<br>${data.email} (${data.gender})<br>${data.address}</td>
                                    <td><span  class="txt_field_green">${data.skills}</span></td>
                                     <td>${data.requirmentsDetails}</td>
                                                            
                                    
                                     <td>                                   
                                     ${data.freelancer.name} ${data.freelancer.mobileNo} ${data.freelancer.email}<br>
                                     <hr>More--
                                     ${data.freelancerDetails}
                                     </td>
                                    
                                    
                                    <td>
                                      <br><span  class="label label-danger">${data.demostatus}</span><br>${data.meetDate}&nbsp;&nbsp;&nbsp;${data.meettime} 
                                        
                                       </td>
                                       <td>${data.whatsappgroup}</td>
                                   
                                      <td>${data.remarks}</td>
                                     <td>
                                       <fmt:formatDate type = "both"   value = "${data.createdOn}" />&nbsp;${not empty data.createdBy?'By:':''} ${data.createdBy.userName}
                                    </td>
                                    <td>
                                       <fmt:formatDate type = "both"   value = "${data.updatedOn}" />&nbsp;${not empty data.updatedBy?'By:':''} ${data.updatedBy.userName}
                                    </td>
                                    <td>
                                      
                                          ${data.status}
                                     
                                    </td>
                                    <td>                                      
                                          <button class="btn btn-warning btn-sm"
                                             onclick="edit('${data.getId()}')">
                                          <i class=" fa fa-pencil-square-o " aria-hidden="true"></i>
                                          </button>
                                          
                                    </td>
                                 </tr>
                              </c:forEach>
                           </tbody>
                        </table>
                     </div>
                  </div>
                  <!-- Panel Widget --> 
               </div>
               <!-- col-md-12 --> 
            </div>
            <!-- row --> 
         </div>
         <!-- .vd_content-section --> 
      </div>
      <!-- .vd_content --> 
   </div>
   <!-- .vd_container --> 
</div>
<!-- .vd_content-wrapper --> 
<form action="${contextPath}/edit-applyclient" method="POST" id="edit">
   <input type="hidden" name="${_csrf.parameterName}"
      value="${_csrf.token}" />
   <input type="hidden" name="id_edit" id="id_edit" />
</form>
<form action="${contextPath}/applyclients" method="GET" id="statusForm">  
   <input type="hidden" name="demostatus" id="demostatus" />
</form>

<script> 
   function edit(id){
   	$("#id_edit").val(id);
   	bootbox.confirm("Do you want to edit ?",
   	function(result) {
   		if (result == true) {
   		   $("#edit").submit();
   		} 
   	});
   }	
  function loadDataByStatus(status){
   	$("#demostatus").val(status);
   $("#statusForm").submit();
   }
</script>