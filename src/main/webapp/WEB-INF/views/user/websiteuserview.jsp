<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
      <!-- Middle Content Start -->
    
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <div class="vd_head-section clearfix">
            <div class="vd_panel-header">
              <ul class="breadcrumb">
                <li><a href="index.html">Home</a> </li>
                <li><a href="pages-custom-product.html">Website Users</a> </li>
                <li class="active">User Profile Pages</li>
              </ul>
              <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step=5  data-position="left">
   <!--  <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
      <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
      <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>
 -->      
</div>
 
            </div>
          </div>
          <div class="vd_title-section clearfix">
            <div class="vd_panel-header no-subtitle">
              <h1>User Profile Page</h1>
            </div>
          </div>
          <div class="vd_content-section clearfix">
            <div class="row">
              <div class="col-sm-3" id="imageSection">
                <div class="panel widget light-widget panel-bd-top">
                  <div class="panel-heading no-title"> </div>
                  <div class="panel-body">
                    <div class="text-center vd_info-parent"> <img  id="profilepic" alt="example image" src="${contextPath}/static/sFreelance/img/avatar/big.jpg"> </div>
                    <div class="row">
                      <div class="col-xs-12"> <a class="btn vd_btn vd_bg-green btn-xs btn-block no-br"></a> </div>
                      <div class="col-xs-12"> <a class="btn vd_btn vd_bg-grey btn-xs btn-block no-br"></a> </div>
                    </div>
                    <h2 class="font-semibold mgbt-xs-5">${userInfo.user.name}</h2>
                    <h4>Working at ${userInfo.companyName} Company, Inc.</h4>
                    <p> ${userInfo.description}</p>
                    <div class="mgtp-20">
                      <table class="table table-striped table-hover">
                        <tbody>
                          <tr>
                            <td style="width:60%;">Status</td>
                            <td><span class="label label-success">${userInfo.user.status}</span></td>
                          </tr>
                          <tr>
                            <td>User Rating</td>
                            <td><i class="fa fa-star vd_yellow fa-fw"></i><i class="fa fa-star vd_yellow fa-fw"></i><i class="fa fa-star vd_yellow fa-fw"></i><i class="fa fa-star vd_yellow fa-fw"></i><i class="fa fa-star vd_yellow fa-fw"></i></td>
                          </tr>
                          <tr>
                            <td>Member Since</td>
                            <td> ${userInfo.createdOn} </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <!-- panel widget -->
                
                <!-- panel widget --> 
              </div>
              <div class="col-sm-9">
                <div class="tabs widget">
  <ul class="nav nav-tabs widget">
    <li class="active" id="profile-tab1"> <a data-toggle="tab" href="#profile-tab"> Profile <span class="menu-active"><i class="fa fa-caret-up"></i></span> </a></li>
   
    <li id="resume-tab1"> <a data-toggle="tab" href="#resume-tab">Resume <span class="menu-active"><i class="fa fa-caret-up"></i></span> </a></li>
  </ul>
  <div class="tab-content">
    <div id="profile-tab" class="tab-pane active">
      <div class="pd-20">
<div class="vd_info tr"> <a class="btn vd_btn btn-xs vd_bg-yellow" href="${contextPath}/website-users"> <i class="append-icon fa fa-fw fa-arrow-left"></i> Back </a> </div>      
        <h3 class="mgbt-xs-15 mgtp-10 font-semibold"><i class="icon-user mgr-10 profile-icon"></i> ABOUT</h3>
        <div class="row">
          <div class="col-sm-6">
            <div class="row mgbt-xs-0">
              <label class="col-xs-5 control-label">Name:</label>
              <div class="col-xs-7 controls">${userInfo.user.name}</div>
              <!-- col-sm-10 --> 
            </div>
          </div>
         
         <div class="col-sm-6">
            <div class="row mgbt-xs-0">
              <label class="col-xs-5 control-label">User Code:</label>
              <div class="col-xs-7 controls">${userInfo.userCode}</div>
              <!-- col-sm-10 --> 
            </div>
          </div>
          <div class="col-sm-6">
            <div class="row mgbt-xs-0">
              <label class="col-xs-5 control-label">User Name:</label>
              <div class="col-xs-7 controls">${userInfo.user.username}</div>
              <!-- col-sm-10 --> 
            </div>
          </div>
          <div class="col-sm-6">
            <div class="row mgbt-xs-0">
              <label class="col-xs-5 control-label">Email:</label>
              <div class="col-xs-7 controls">${userInfo.user.email}</div>
              <!-- col-sm-10 --> 
            </div>
          </div>
           <div class="col-sm-6">
            <div class="row mgbt-xs-0">
              <label class="col-xs-5 control-label">Phone:</label>
              <div class="col-xs-7 controls">${userInfo.user.phone}</div>
              <!-- col-sm-10 --> 
            </div>
          </div>
          <div class="col-sm-6">
            <div class="row mgbt-xs-0">
              <label class="col-xs-5 control-label">Address:</label>
              <div class="col-xs-7 controls">${userInfo.address}</div>
              <!-- col-sm-10 --> 
            </div>
          </div>
          <div class="col-sm-6">
            <div class="row mgbt-xs-0">
              <label class="col-xs-5 control-label">DOB:</label>
              <div class="col-xs-7 controls">${userInfo.dateofbirth}</div>
              <!-- col-sm-10 --> 
            </div>
          </div>
          <div class="col-sm-6">
            <div class="row mgbt-xs-0">
              <label class="col-xs-5 control-label">Birthday:</label>
              <div class="col-xs-7 controls">${userInfo.dateofbirth}</div>
              <!-- col-sm-10 --> 
            </div>
          </div>
         
          <div class="col-sm-6">
            <div class="row mgbt-xs-0">
              <label class="col-xs-5 control-label">Website:</label>
              <div class="col-xs-7 controls"><a href="${userInfo.website}" target="_blank">${userInfo.website}</a></div>
              <!-- col-sm-10 --> 
            </div>
          </div>
          <div class="col-sm-6">
            <div class="row mgbt-xs-0">
              <label class="col-xs-5 control-label">Linkedin:</label>
              <div class="col-xs-7 controls"><a href="${userInfo.linkedin}" target="_blank">${userInfo.linkedin}</a></div>
              <!-- col-sm-10 --> 
            </div>
          </div>
        </div>      
        <hr class="pd-10"  />
        
        <div class="row">
          <div class="col-sm-6">
            <div class="row mgbt-xs-0">
              <label class="col-xs-5 control-label">Bank Name:</label>
              <div class="col-xs-7 controls">${userInfo.accountsbankname}</div>
              <!-- col-sm-10 --> 
            </div>
          </div>
          <div class="col-sm-6">
            <div class="row mgbt-xs-0">
              <label class="col-xs-5 control-label">Account Holder Name:</label>
              <div class="col-xs-7 controls">${userInfo.accountsholdername}</div>
              <!-- col-sm-10 --> 
            </div>
          </div>
            <div class="col-sm-6">
            <div class="row mgbt-xs-0">
              <label class="col-xs-5 control-label">IFSC Code:</label>
              <div class="col-xs-7 controls">${userInfo.accountsifsc}</div>
              <!-- col-sm-10 --> 
            </div>
          </div>
           <div class="col-sm-6">
            <div class="row mgbt-xs-0">
              <label class="col-xs-5 control-label">Account No:</label>
              <div class="col-xs-7 controls">${userInfo.accountsaccno}</div>
              <!-- col-sm-10 --> 
            </div>
          </div>
            <div class="col-sm-6">
            <div class="row mgbt-xs-0">
              <label class="col-xs-5 control-label">Any UPI Mode Type:</label>
              <div class="col-xs-7 controls">${userInfo.bankmodetype}</div>
              <!-- col-sm-10 --> 
            </div>
          </div>
            <div class="col-sm-6">
            <div class="row mgbt-xs-0">
              <label class="col-xs-5 control-label">UPI Number:</label>
              <div class="col-xs-7 controls">${userInfo.bankmodenumber}</div>
              <!-- col-sm-10 --> 
            </div>
          </div>
          
          </div>
        
        <!-- row -->
        <hr class="pd-10"  />
        
        
        <div class="row">
          <div class="col-sm-6">
            <div class="row mgbt-xs-0">
              <label class="col-xs-5 control-label">Support Mode:</label>
              <div class="col-xs-7 controls">${userInfo.supportmode}</div>
              <!-- col-sm-10 --> 
            </div>
          </div>
          <div class="col-sm-6">
            <div class="row mgbt-xs-0">
              <label class="col-xs-5 control-label">I can support :</label>
              <div class="col-xs-7 controls">${userInfo.supporttype}</div>
              <!-- col-sm-10 --> 
            </div>
          </div>
            <div class="col-sm-6">
            <div class="row mgbt-xs-0">
              <label class="col-xs-5 control-label">Support Hours ::</label>
              <div class="col-xs-7 controls">${userInfo.supporthour}</div>
              <!-- col-sm-10 --> 
            </div>
          </div>
           <div class="col-sm-6">
            <div class="row mgbt-xs-0">
              <label class="col-xs-5 control-label">Available Time (Job Support/Training):</label>
              <div class="col-xs-7 controls">${userInfo.jobsupporttimeslot}</div>
              <!-- col-sm-10 --> 
            </div>
          </div>
       <div class="col-sm-6">
            <div class="row mgbt-xs-0">
              <label class="col-xs-5 control-label">Other Time Slot If Any ::</label>
              <div class="col-xs-7 controls">${userInfo.othertimeslot}</div>
              <!-- col-sm-10 --> 
            </div>
          </div>
          </div>
        
        <!-- row -->
        <hr class="pd-10"  />
        
        
        <div class="row">
          <div class="col-sm-7">
            <h3 class="mgbt-xs-15 font-semibold"><i class="fa fa-globe mgr-10 profile-icon"></i> EXPERIENCE</h3>
            <div class="">
              <div class="content-list">
                <div data-rel="scroll">
                  <ul  class="list-wrapper">
                    <li> <span class="menu-icon vd_yellow"><i class="fa fa-suitcase"></i></span> <span class="menu-text">${userInfo.experience} Years</span> </span>  </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <!-- col-sm-7 --> 
          <div class="col-sm-5">
            <h3 class="mgbt-xs-15 font-semibold"><i class="fa fa-flask mgr-10 profile-icon"></i> SKILL</h3>
            <div class="skill-list">
              <div class="skill-name"> <strong>${userInfo.primaryskills}</strong> </div>
              <div class="progress  progress-sm">
                <div style="width: 90%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="90" role="progressbar" class="progress-bar progress-bar-success "> <span class="sr-only">90%</span> </div>
              </div>
            </div>
            <div class="skill-list">
              <div class="skill-name"> <strong>${userInfo.secondaryskills}</strong> </div>
              <div class="progress  progress-sm">
                <div style="width: 70%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar" class="progress-bar progress-bar-danger "> <span class="sr-only">70%</span> </div>
              </div>
            </div>
                                       
          </div>
          <!-- col-sm-7 -->           
        </div>
        <!-- row --> 
      </div>
      <!-- pd-20 --> 
    </div>
    <!-- home-tab -->
    

    <div id="resume-tab" class="tab-pane">
    	<div class="pd-20">
        	<div class="row">
            	<div class="col-xs-12 col-sm-12">
                    <div class="content-list content-large menu-action-right">
                       <object>
   <embed id="pdfID" type="text/html" width="1200" height="600" src="data:application/pdf;base64,${userInfo.cvfiles}" />
</object>
                   </div> <!-- content-list -->
                </div> <!-- col-xs-12 col-sm-6 -->


    

                
            </div> <!-- row -->
        </div> <!-- pd-20 --> 
    </div>  <!-- groups tab -->   
    
  </div>
  <!-- tab-content --> 
</div>
<!-- tabs-widget -->              </div>
            </div>
            <!-- row --> 
            
          </div>
          <!-- .vd_content-section --> 
          
        </div>
        <!-- .vd_content --> 
      </div>
      <!-- .vd_container --> 
    </div>
    <!-- .vd_content-wrapper --> 
    
    <!-- Middle Content End --> 
    
    <script>
    var ppic = "${userInfo.profilephoto}";
	if (ppic != "") {
		$("#profilepic").attr("src", "data:image/png;base64, " + ppic);
	}
	$("#profile-tab1").click(function(){
		$("#imageSection").show();
		});
	$("#resume-tab1").click(function(){
		$("#imageSection").hide();
		});
    </script>