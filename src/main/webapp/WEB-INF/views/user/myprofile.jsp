<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
   <!-- Middle Content Start -->
    
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <div class="vd_head-section clearfix">
            <div class="vd_panel-header">
              <ul class="breadcrumb">
                <li><a href="${contextPath}/">Home</a> </li>
                <li><a href="${contextPath}/">Pages</a> </li>
                <li class="active">My Profile</li>
              </ul>
              <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step=5  data-position="left">
  <!--   <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
      <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
      <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>
 -->      
</div>
 
            </div>
          </div>
          <div class="vd_title-section clearfix">
            <div class="vd_panel-header no-subtitle">
              <h1>My Profile</h1>
            </div>
          </div>
          <div class="vd_content-section clearfix">
            <div class="row">
              <div class="col-sm-3">
                <div class="panel widget light-widget panel-bd-top">
                  <div class="panel-heading no-title"> </div>
                  <div class="panel-body">
                    <div class="text-center vd_info-parent"> <img alt="example image" src="${contextPath}/static/sFreelance/img/avtar/${userDTO.userInfo.user.gender eq 'MALE'?'male':'fmale_'}.PNG"> </div>
                    <div class="row">
                      <div class="col-xs-12"> <a class="btn vd_btn vd_bg-green btn-xs btn-block no-br"><i class="fa fa-check-circle append-icon" ></i>Its Me</a> </div>
                     	
                    </div>
                    <h2 class="font-semibold mgbt-xs-5">${userDTO.userInfo.user.name}</h2>
                    <h4>${userDTO.userInfo.organizationName}</h4>
                    <p>${userDTO.userInfo.organizationDetails}</p>
                    <div class="mgtp-20">
                      <table class="table table-striped table-hover">
                        <tbody>
                          <tr>
                            <td style="width:60%;">Status</td>
                            <td><span class="label label-success">Active</span></td>
                          </tr>
                          <tr>
                            <td>User Rating</td>
                            <td><i class="fa fa-star vd_yellow fa-fw"></i><i class="fa fa-star vd_yellow fa-fw"></i><i class="fa fa-star vd_yellow fa-fw"></i><i class="fa fa-star  fa-fw"></i></td>
                          </tr>
                          <tr>
                            <td>Member Since</td>
                            <td> <fmt:formatDate type = "DATE"   value = "${userDTO.userInfo.createdOn}" /> </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
             
              </div>
              <div class="col-sm-9">
                <div class="tabs widget">
  <ul class="nav nav-tabs widget">
    <li class="active"> <a data-toggle="tab" href="#profile-tab"> Profile <span class="menu-active"><i class="fa fa-caret-up"></i></span> </a></li>
    <li> <a data-toggle="tab" href="#projects-tab"> Projects <span class="menu-active"><i class="fa fa-caret-up"></i></span> </a></li>   
    
   
    <li> <a data-toggle="tab" href="#groups-tab"> Groups(Consultancy) <span class="menu-active"><i class="fa fa-caret-up"></i></span> </a></li>
  </ul>
  <div class="tab-content">
    <div id="profile-tab" class="tab-pane active">
      <div class="pd-20">
<div class="vd_info tr"> <a  href="${contextPath}/edit-profile" class="btn vd_btn btn-xs vd_bg-yellow"> <i class="fa fa-pencil append-icon"></i> Edit </a> </div>      
        <h3 class="mgbt-xs-15 mgtp-10 font-semibold"><i class="icon-user mgr-10 profile-icon"></i> ABOUT</h3>
        
        <div class="row">
          <div class="col-sm-6">
            <div class="row mgbt-xs-0">
              <label class="col-xs-5 control-label">Name:</label>
              <div class="col-xs-7 controls">${userDTO.userInfo.user.name}</div>
              <!-- col-sm-10 --> 
            </div>
          </div>
         
          <div class="col-sm-6">
            <div class="row mgbt-xs-0">
              <label class="col-xs-5 control-label">User Name:</label>
              <div class="col-xs-7 controls">${userDTO.userInfo.user.userName}</div>
              <!-- col-sm-10 --> 
            </div>
          </div>
          <div class="col-sm-6">
            <div class="row mgbt-xs-0">
              <label class="col-xs-5 control-label">Email:</label>
              <div class="col-xs-7 controls">${userDTO.userInfo.user.email}</div>
              <!-- col-sm-10 --> 
            </div>
          </div>
          <div class="col-sm-6">
            <div class="row mgbt-xs-0">
              <label class="col-xs-5 control-label">Mobile No:</label>
              <div class="col-xs-7 controls">${userDTO.userInfo.user.mobileNo}</div>
              <!-- col-sm-10 --> 
            </div>
          </div>
          <div class="col-sm-6">
            <div class="row mgbt-xs-0">
              <label class="col-xs-5 control-label">Gender:</label>
              <div class="col-xs-7 controls">${userDTO.userInfo.user.gender}</div>
              <!-- col-sm-10 --> 
            </div>
          </div>
          <div class="col-sm-6">
            <div class="row mgbt-xs-0">
              <label class="col-xs-5 control-label">Address:</label>
              <div class="col-xs-7 controls">${userDTO.userInfo.user.address}</div>
              <!-- col-sm-10 --> 
            </div>
          </div>
          <div class="col-sm-6" style="display:${empty userDTO.userInfo.idProofName?'none':'block'}">
            <div class="row mgbt-xs-0">
              <label class="col-xs-5 control-label">idProof Name:</label>
              <div class="col-xs-7 controls">${userDTO.userInfo.idProofName}</div>
              <!-- col-sm-10 --> 
            </div>
          </div>
           <div class="col-sm-6" style="display:${empty userDTO.userInfo.idProofNumber?'none':'block'}">
            <div class="row mgbt-xs-0">
              <label class="col-xs-5 control-label">idProof Number:</label>
              <div class="col-xs-7 controls">${userDTO.userInfo.idProofNumber}</div>
              <!-- col-sm-10 --> 
            </div>
          </div>
          <div class="col-sm-6">
            <div class="row mgbt-xs-0">
              <label class="col-xs-5 control-label">Facebook:</label>
              <div class="col-xs-7 controls">${userDTO.userInfo.fbLink}</div>
              <!-- col-sm-10 --> 
            </div>
          </div>
          <div class="col-sm-6">
            <div class="row mgbt-xs-0">
              <label class="col-xs-5 control-label">Instagram:</label>
              <div class="col-xs-7 controls">${userDTO.userInfo.instaLink}</div>
              <!-- col-sm-10 --> 
            </div>
          </div>
          <div class="col-sm-6">
            <div class="row mgbt-xs-0">
              <label class="col-xs-5 control-label">Linkdean:</label>
              <div class="col-xs-7 controls">${userDTO.userInfo.linkdeanLink}</div>
              <!-- col-sm-10 --> 
            </div>
          </div>
            <div class="col-sm-6">
            <div class="row mgbt-xs-0">
              <label class="col-xs-5 control-label">Website:</label>
              <div class="col-xs-7 controls">${userDTO.userInfo.websiteLink}</div>
              <!-- col-sm-10 --> 
            </div>
          </div>
          
           <div class="col-sm-6" style="display:${empty userDTO.userInfo.perHours?'none':'block'}">
            <div class="row mgbt-xs-0">
              <label class="col-xs-5 control-label">Per Hours:</label>
              <div class="col-xs-7 controls">${userDTO.userInfo.perHours} ${userDTO.userInfo.currency}</div>
              <!-- col-sm-10 --> 
            </div>
          </div>
          <div class="col-sm-6">
            <div class="row mgbt-xs-0">
              <label class="col-xs-5 control-label">createdOn:</label>
              <div class="col-xs-7 controls">${userDTO.userInfo.createdOn}</div>
              <!-- col-sm-10 --> 
            </div>
          </div>
         <div class="col-sm-6">
            <div class="row mgbt-xs-0">
              <label class="col-xs-5 control-label">createdOn:</label>
              <div class="col-xs-7 controls">${userDTO.userInfo.createdBy.name}</div>
              <!-- col-sm-10 --> 
            </div>
          </div>
         
        </div>
        <hr class="pd-10"  />
     
        <!-- row -->
        <hr class="pd-10"  />
        <div class="row">
          <div class="col-sm-7">
            <h3 class="mgbt-xs-15 font-semibold"><i class="fa fa-globe mgr-10 profile-icon"></i> ACTIVITY</h3>
            <div class="">
              <div class="content-list">
                <div data-rel="scroll">
                  <ul  class="list-wrapper">
                    <li> <span class="menu-icon vd_yellow"><i class="fa fa-suitcase"></i></span> <span class="menu-text"> Someone has give you a surprise <span class="menu-info"><span class="menu-date"> ~ 12 Minutes Ago</span></span> </span>  </li>
                    <li> <span class="menu-icon vd_blue"><i class=" fa fa-user"></i></span> <span class="menu-text"> Change your user profile details <span class="menu-info"><span class="menu-date"> ~ 1 Hour 20 Minutes Ago</span></span> </span> </li>
                    <li> <span class="menu-icon vd_red"><i class=" fa fa-cogs"></i></span> <span class="menu-text"> Your setting is updated <span class="menu-info"><span class="menu-date"> ~ 12 Days Ago</span></span> </span></li>
                    <li>  <span class="menu-icon vd_green"><i class=" fa fa-book"></i></span> <span class="menu-text"> Added new article <span class="menu-info"><span class="menu-date"> ~ 19 Days Ago</span></span> </span>  </li>
                    <li>  <span class="menu-icon vd_green"><img alt="example image" src="${contextPath}/static/sFreelance/${contextPath}/static/sFreelance/img/avatar/avatar.jpg"></span> <span class="menu-text"> Change Profile Pic <span class="menu-info"><span class="menu-date"> ~ 20 Days Ago</span></span> </span> </li>
                    <li>  <span class="menu-icon vd_red"><i class=" fa fa-cogs"></i></span> <span class="menu-text"> Your setting is updated <span class="menu-info"><span class="menu-date"> ~ 12 Days Ago</span></span> </span>  </li>
                    <li>  <span class="menu-icon vd_green"><i class=" fa fa-book"></i></span> <span class="menu-text"> Added new article <span class="menu-info"><span class="menu-date"> ~ 19 Days Ago</span></span> </span> </li>
                    <li>  <span class="menu-icon vd_green"><img alt="example image" src="${contextPath}/static/sFreelance/${contextPath}/static/sFreelance/img/avatar/avatar.jpg"></span> <span class="menu-text"> Change Profile Pic <span class="menu-info"><span class="menu-date"> ~ 20 Days Ago</span></span> </span>  </li>
                  </ul>
                </div>
                <div class="closing text-center" style=""> <a href="#">See All Activities <i class="fa fa-angle-double-right"></i></a> </div>
              </div>
            </div>
          </div>
          <!-- col-sm-7 --> 
          <div class="col-sm-5">
            <h3 class="mgbt-xs-15 font-semibold"><i class="fa fa-flask mgr-10 profile-icon"></i> SKILL</h3>
            <div class="skill-list">
              <div class="skill-name"> Photoshop </div>
              <div class="progress  progress-sm">
                <div style="width: 90%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="90" role="progressbar" class="progress-bar progress-bar-success "> <span class="sr-only">90%</span> </div>
              </div>
            </div>
            <div class="skill-list">
              <div class="skill-name"> Illustrator </div>
              <div class="progress  progress-sm">
                <div style="width: 20%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="20" role="progressbar" class="progress-bar progress-bar-danger "> <span class="sr-only">20%</span> </div>
              </div>
            </div>
            <div class="skill-list">
              <div class="skill-name"> PHP </div>
              <div class="progress  progress-sm">
                <div style="width: 50%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="50" role="progressbar" class="progress-bar progress-bar-warning "> <span class="sr-only">50% Complete</span> </div>
              </div>
            </div>
            <div class="skill-list">
              <div class="skill-name"> Javascript </div>
              <div class="progress  progress-sm">
                <div style="width: 60%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar" class="progress-bar progress-bar-info "> <span class="sr-only">60% Complete</span> </div>
              </div>
            </div>
            <div class="skill-list">
              <div class="skill-name"> Communication </div>
              <div class="progress  progress-sm">
                <div style="width: 95%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="95" role="progressbar" class="progress-bar progress-bar-success "> <span class="sr-only">95% Complete</span> </div>
              </div>
            </div> 
            <div class="skill-list">
              <div class="skill-name"> Writing </div>
              <div class="progress  progress-sm">
                <div style="width: 45%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="45" role="progressbar" class="progress-bar progress-bar-warning "> <span class="sr-only">45% Complete</span> </div>
              </div>
            </div>                                 
          </div>
          <!-- col-sm-7 -->           
        </div>
        <!-- row --> 
      </div>
      <!-- pd-20 --> 
    </div>
    <!-- home-tab -->
    
    <div id="projects-tab" class="tab-pane">
    	<div class="pd-20">
				<div class="vd_info tr">  </div>         
				<h3 class="mgbt-xs-15 mgtp-10 font-semibold"><i class="fa fa-bolt mgr-10 profile-icon"></i> PROJECTS</h3>        
				<table class="table table-striped table-hover">
							  <thead>
								  <tr>
                                  	  <th>Sl No</th>
										<th>Project Code</th>
										<th>Project Name</th>
										<th>Required Skils</th>
										<th>Start Date</th>
										<th>End Date</th>
										<th>Project Cost</th>										
										<th>Project Allocation</th>									
										<th>Project Status</th>
																		
										<th>Status</th>
										                               
								  </tr>
							  </thead>   
							  <tbody>
								
								        <c:forEach items="${userDTO.project}" var="data" varStatus="serialNumber">
										<tr>
											<td>${serialNumber.count}</td>
											<td>${data.projectCode}</td>
											<td>${data.projectName}</td>
											<td>${data.requiredSkils}</td>
											<td><fmt:formatDate type = "DATE"   value = "${data.startDate}" /></td>
											<td><fmt:formatDate type = "DATE"   value = "${data.endDate}" /></td>
											
											<td><b><span class="badge vd_bg-green">${data.projectCost} ${data.currency}</span></b></td>
											<td>
												
															${data.projectAllocation.userFreelancer.name}
																<br>FROM: <fmt:formatDate type = "both"   value = "${data.projectAllocation.startDate}" />  to <fmt:formatDate type = "both"   value = "${data.projectAllocation.endDate}" />
																<br>Allocation Status: ${data.projectAllocation.projectStatus}
																 
														
											</td>
											<td>											
												<c:choose>
														<c:when test="${data.projectStatus eq 'CANCELLED'}">
															<span class="label label-danger">${data.projectStatus}</span>
														</c:when>
														<c:when test="${data.projectStatus eq 'COMPLETED'}">
															<span class="label label-primary">${data.projectStatus}</span>
														</c:when>
														<c:when test="${data.projectStatus eq 'INPROGRESS'}">
															<span class="label label-success">${data.projectStatus}</span>
														</c:when>													
														<c:otherwise>														
																<span class="label label-default">${data.projectStatus}</span>													
														</c:otherwise>
												</c:choose>
											
											</td>
											<td>
												<c:choose>
														<c:when test="${data.status eq 'ACTIVE'}">
															<span class="label label-success">${data.status}</span>
														</c:when>																										
														<c:otherwise>														
															<span class="label label-danger" style="font-size: 14px;">${data.status}</span>													
														</c:otherwise>
												</c:choose>
											
											</td>
											
											
										</tr>
									</c:forEach>                                     
                                                                   
							  </tbody>
						 </table>
                         <div class="">
                         </div>        
        </div>
    </div>
    
    
    
    <!-- photos tab -->  
    <div id="groups-tab" class="tab-pane">
    	<div class="pd-20">
        <h3 class="mgbt-xs-15 mgtp-10 font-semibold"><i class="fa fa-leaf mgr-10 profile-icon"></i> Contact person</h3>
        	<div class="row">
            	<div class="col-xs-12 col-sm-6" style="display:${empty userDTO.userInfo.contractors.name?'none':'block'}">
                    <div class="content-list content-large menu-action-right">
                        <ul class="list-wrapper pd-lr-15">
                        <li>  
                            <div class="menu-icon"><a href="#"><img src="${contextPath}/static/sFreelance/img/groups/logo-01.jpg" alt="example image"></a></div> 
                            <div class="menu-text"> 
                                <h4 class="mgbt-xs-0"><a href="#">${userDTO.userInfo.contractors.name}</a></h4>
                                <div class="menu-info">	
                                    <span class="menu-date"> (Your 1st contact person) </span> 
                                                                                                                                   
                                </div>   
                                <p> ${userDTO.userInfo.contractors.email} </p> 
                                 <p> ${userDTO.userInfo.contractors.mobile} </p>                                                          
                            </div>                                               
                            <div class="menu-action">                                                              
                                <div data-placement="bottom" data-toggle="tooltip" data-original-title="Leave Group" class="menu-action-icon vd_red">
                                    <i class="fa fa-times"></i>
                                </div>                                                                                                                                       
                            </div>                                                	  
                        </li>
                        </ul> 
                   </div> 
                </div> 
                <div class="col-xs-12 col-sm-6" style="display:${empty userDTO.userRole.agentId.name?'none':'block'}">
                    <div class="content-list content-large menu-action-right">
                        <ul class="list-wrapper pd-lr-15">
                        <li>  
                            <div class="menu-icon"><a href="#"><img src="${contextPath}/static/sFreelance/img/groups/logo-01.jpg" alt="example image"></a></div> 
                            <div class="menu-text"> 
                                <h4 class="mgbt-xs-0"><a href="#">${userDTO.userRole.agentId.name}</a></h4>
                                <div class="menu-info">	
                                    <span class="menu-date"> (Your 2nd contact person) </span> 
                                                                                                                                   
                                </div>   
                                <p> ${userDTO.userRole.agentId.email}</p>                                                        
                            </div>                                               
                            <div class="menu-action">                                                              
                                <div data-placement="bottom" data-toggle="tooltip" data-original-title="Leave Group" class="menu-action-icon vd_red">
                                    <i class="fa fa-times"></i>
                                </div>                                                                                                                                       
                            </div>                                                	  
                        </li>
                        </ul> 
                   </div> 
                </div>
                
            </div> <!-- row -->
        </div> <!-- pd-20 --> 
    </div>  <!-- groups tab -->   
    
  </div>
  <!-- tab-content --> 
</div>
<!-- tabs-widget -->              </div>
            </div>
            <!-- row --> 
            
          </div>
          <!-- .vd_content-section --> 
          
        </div>
        <!-- .vd_content --> 
      </div>
      <!-- .vd_container --> 
    </div>
    <!-- .vd_content-wrapper --> 
    
    <!-- Middle Content End --> 
    <%-- <%@include file="/WEB-INF/views/layout/common.jsp" %> --%>