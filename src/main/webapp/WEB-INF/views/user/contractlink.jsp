<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<c:set var="contextPath" value="${pageContext.request.contextPath}"/> 

<style>
footer .vd_bottom {
    background-color: #252525;
    color: #f5f5f5;
    padding-top: 10px;
    padding-bottom: 10px;
    margin-top: 60px;
}
</style>

<body id="pages" class="full-layout no-nav-left no-nav-right  nav-top-fixed background-login     responsive remove-navbar login-layout   clearfix" data-active="pages "  data-smooth-scrolling="1">     
<div class="vd_body">

<div class="content">
  <div class="container"> 
    
    <!-- Middle Content Start -->
    
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <div class="vd_content-section clearfix">
            <div class="vd_login-page">
              <div class="heading clearfix">
                
                <h4 class="text-center font-semibold vd_grey">UPLOAD YOUR SIGNED CONTRACT</h4>
                <p class="text-center font-semibold vd_red">
                ${outcome}
               
                </p>
              </div>
              <div class="panel widget">
                    <c:if test="${not empty successMessage}">
	<div class="alert alert-success alert-dismissable">
             	<button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cross"></i></button>
                    	<i class="fa fa-check-circle append-icon"></i><strong>Well done!</strong> ${successMessage} 
        </div>
</c:if>

<c:if test="${not empty failureMessage}">
<div class="alert alert-danger alert-dismissable alert-condensed">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cross"></i></button>
                        <i class="fa fa-exclamation-circle append-icon"></i>
                        <strong>Oh snap!</strong> ${failureMessage} 
        </div>
</c:if>
                <div class="panel-body">
                  <div class="login-icon entypo-icon"> <i class="icon-key"></i> </div>
                  <c:if test="${invalid eq 'NO'}">

                   <form class="form-horizontal"  action="${contextPath}/link"  method="post" enctype="multipart/form-data">
							<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />
								<input type="hidden" name="cd" id="cd" value="${cd}" required/>
								<input type="hidden" name="type" id="type" value="${type}" required/>
                       
                            <div class="form-group">
                      
                        <div class="col-sm-7 controls">
                           <input id="file" type="file" name="file" required>
                        </div>
                      </div>
                           
                             <div class="modal-footer background-login">
                          <button type="button" class="btn vd_btn vd_bg-grey" data-dismiss="modal">Close</button>
                           <button type="submit" class="btn vd_btn vd_bg-green" >Upload</button>
                         
                        </div>
                            
                          </form>
                          </c:if>
                </div>
              </div>
               </div>
            <!-- vd_login-page --> 
            
          </div>
          <!-- .vd_content-section --> 
          
        </div>
        <!-- .vd_content --> 
      </div>
      <!-- .vd_container --> 
    </div>
    <!-- .vd_content-wrapper --> 
    
    <!-- Middle Content End --> 
    
  </div>
  <!-- .container --> 
</div>
<!-- .content -->

