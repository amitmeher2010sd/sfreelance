<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<% String session_roleCode = (String)request.getSession().getAttribute("roleCode"); %> 
<c:set var = "roleCodeSession" value = "<%=session_roleCode%>" />
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<!-- Middle Content Start -->

<div class="vd_content-wrapper">
   <div class="vd_container">
      <div class="vd_content clearfix">
         <div class="vd_head-section clearfix">
            <div class="vd_panel-header">
               <ul class="breadcrumb">
                  <li><a href="${contextPath}/">Home</a> </li>
                  <li><a href="${contextPath}/">User Management</a> </li>
                  <li class="active">Manage ${rtype eq 'f' ?'Freelancer':''}${rtype eq 'c' ?'Client':''} ${rtype eq 'cst' ?'Consultancy':''}</li>
               </ul>
              <%--  <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step=5  data-position="left">
                  <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
                  <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
                  <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>
               </div> --%>
            </div>
         </div>
         <div class="vd_title-section clearfix">
            <div class="vd_panel-header">
               <h1>Manage ${rtype eq 'f' ?'Freelancers':''}${rtype eq 'c' ?'Clients':''}${rtype eq 'cst' ?'Consultancy':''}</h1>
               <small class="subtitle">Look <a href="http://www.datatables.net">below</a> for more information</small> 
            </div>
         </div>
         <div class="vd_content-section clearfix">
            <div class="row">
               <div class="col-md-12">
                  <div class="panel widget">
                     <c:if test="${not empty successMessage}">
                        <div class="alert alert-success alert-dismissable">
                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cross"></i></button>
                           <i class="fa fa-check-circle append-icon"></i><strong>Well done!</strong> ${successMessage} 
                        </div>
                     </c:if>
                     <c:if test="${not empty failureMessage}">
                        <div class="alert alert-danger alert-dismissable alert-condensed">
                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cross"></i></button>
                           <i class="fa fa-exclamation-circle append-icon"></i>
                           <strong>Oh snap!</strong> ${failureMessage} 
                        </div>
                     </c:if>
                     <div class="panel-heading vd_bg-grey">
                        <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span>List Of ${rtype eq 'f' ?'Freelancers':''}${rtype eq 'c' ?'Clients':''}${rtype eq 'cst' ?'Consultancy':''}</h3>
                     </div>
                     <div class="panel-body table-responsive  clearfix">
                     <div class="form-group">
                           <label class="col-sm-1 control-label">By Status:<span class="vd_red"></span> </label>
                           <div class="col-sm-3 controls">
                              <select class="form-control form-control-sm"  name="status"  onChange="loadDataByStatus(this.value);">   
								    <option ${status eq 'ACTIVE'?'selected':''}  value='ACTIVE'>ACTIVE</option>   
								    <option ${status eq 'INACTIVE'?'selected':''} value='INACTIVE'>INACTIVE</option>
                                  </select> 
                           </div>
                        </div>
                        <a href="${contextPath}/user/${rtype}" type="button" style="margin-left: 92%;" 
                           class="btn vd_btn vd_bg-green vd_white">Add New</a>
                        </br> </br>
                      
                        <table class="table table-striped  clearfix" id="data-tables">
                           <thead>
                              <tr>
                                 <th>#</th>
                                 <th>Name</th>
                                 <th>User Name</th>
                                 <th>Mobile Number</th>
                                 <th>Email Id</th>
                                 <c:if test="${rtype ne 'cst'  and roleCodeSession ne 'ADMIN'}">
                                 <th>Skills</th>
                                 <th>Experience</th>
                                 <th>Standard Payment</th>
                                 <th>Bank</th>
                                 </c:if>
                                 <th>created On</th>
                                 <th>updated On</th>
                                 <th>Status</th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody id="tbd">
                              <c:forEach items="${userInfoList}" var="data" varStatus="serialNumber">
                                 <tr style="background-color:${data.user.status eq 'ACTIVE'?'':'#e6393957'};">
                                    <td>${serialNumber.count}</td>
                                    <td>
                                       <span  class="txt_field_black">${data.user.name} </span> 
                                       <c:if test="${rtype eq 'f'}">
                                          ${data.userType}											
                                       </c:if>
                                       <c:if test="${not empty data.contractors.name}"><br>
                                          <i class="append-icon fa fa-fw fa-users"></i><strong><u>Contractors Details</strong></u>
                                          ${data.contractors.name}-${data.contractors.mobile}-${data.contractors.email}
                                       </c:if>
                                    </td>
                                    <td>${data.user.userName} 
                                    </td>
                                    <td>
                                       <c:set var = "mdata" value = "${fn:split(data.user.mobileNo, '-')}" />
                                       <span  class="txt_field_black">${mdata[0]}${mdata[1]}</span> 
                                    </td>
                                    <td>${data.user.email}</td>
                                     <c:if test="${rtype ne 'cst' and roleCodeSession ne 'ADMIN'}">
                                    <td>
                                    <span  class="txt_field_green">${data.skills}</span> 
                                    <br><strong><u>${not empty data.skillsOther?'Others':''}</u></strong>
                                       ${data.skillsOther}		
                                       
                                       <br><span  class="txt_field_green"><strong> Profession: ${data.profession}</strong></span>
                                       								
                                    </td>
                                    
                                    
                                    
                                    <td>${data.experience} ${empty data.experience?'--':'Yrs'}</td>
                                    <td>${data.standardPayment}</td>
                                     <td>
                                   
                                    <c:choose>
                                          <c:when test="${not empty data.bankInfo}">
                                                 <a href="#" id="showAnc${serialNumber.count}"  onclick="toggleBank(${serialNumber.count},'show')";>Show Bank Details</a>
                                          <a class="lbl_green" href="#" id="hideAnc${serialNumber.count}"  onclick="toggleBank(${serialNumber.count},'hide')"; style="display:none;">Hide Bank Details</a>
                                     <span id="bank${serialNumber.count}" style="display:none;">
                                        
                                      <p>Account Holder Name:- ${data.bankInfo.accountHolderName}</p></br>
                                      <p>Bank Name:- ${data.bankInfo.bankName}</p></br>
                                      <p>Account No:- ${data.bankInfo.accountNo}</p></br>
                                      <p>IFSC Code:- ${data.bankInfo.ifscCode}</p></br>
                                      <p>Contact No:- ${data.bankInfo.accountHolderContactNo}</p></br>
                                      <p>Address:- ${data.bankInfo.accountHolderAddress}</p></br>
                                      <p>GooglePay:- ${data.bankInfo.googlePay}</p></br>
                                      AnyUpi:- ${data.bankInfo.anyUpi}</p>                                     
                                      </span>										
                                          </c:when>
                                          <c:otherwise>
                                    <span class="lbl_red">Pending to Add Account</span>
                                          </c:otherwise>
                                       </c:choose>
                                  
                                   
                                    </td>
                                     </c:if>
                                    <td>
                                       <fmt:formatDate type = "both"   value = "${data.createdOn}" /> ${not empty data.createdBy?'By:':''} ${data.createdBy.userName}
                                    </td>
                                    
                                    <td>
                                       <fmt:formatDate type = "both"   value = "${data.updatedOn}" /> ${not empty data.updatedBy?'By:':''}  ${data.updatedBy.userName}
                                    </td>
                                    <td><span class="label label-success">${data.user.status}</span> </td>
                                    <td> 
                                       <c:choose>
                                          <c:when test="${session_roleCode eq 'ADMIN'}">
                                             <button class="btn btn-success btn-sm"
                                                   onclick="enableUser('${data.user.userId}')" disabled>
                                                <i class="fa fa-check" aria-hidden="true"></i>
                                                </button>
                                                <button class="btn btn-danger btn-sm"
                                                   onclick="disableUser('${data.user.userId}')">
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                                </button>
                                                <button class="btn btn-danger btn-sm" 
                                                   onclick="edit('${data.getId()}')">
                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                                </button>
                                          </c:when>
                                          <c:otherwise>
                                             <c:if test="${data.user.status eq 'ACTIVE'}">
                                                <button class="btn btn-success btn-sm"
                                                   onclick="enableUser('${data.user.userId}')" disabled>
                                                <i class="fa fa-check" aria-hidden="true"></i>
                                                </button>
                                                <button class="btn btn-danger btn-sm"
                                                   onclick="disableUser('${data.user.userId}')">
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                                </button>
                                                <button class="btn btn-danger btn-sm" 
                                                   onclick="edit('${data.getId()}')">
                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                                </button>
                                               
                                             </c:if>
                                             <c:if test="${data.user.status eq 'INACTIVE'}">
                                                <button class="btn btn-success btn-sm"
                                                   onclick="enableUser('${data.user.userId}')">
                                                <i class="fa fa-check" aria-hidden="true"></i>
                                                </button>
                                                <button class="btn btn-danger btn-sm"  disabled>
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                                </button>
                                                <button class="btn btn-danger btn-sm" disabled
                                                   >
                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                                </button>
                                             </c:if>
                                          </c:otherwise>
                                       </c:choose>
                                    </td>
                                 </tr>
                              </c:forEach>
                           </tbody>
                        </table>
                     </div>
                  </div>
                  <!-- Panel Widget --> 
               </div>
               <!-- col-md-12 --> 
            </div>
            <!-- row --> 
         </div>
         <!-- .vd_content-section --> 
      </div>
      <!-- .vd_content --> 
   </div>
   <!-- .vd_container --> 
</div>
<!-- .vd_content-wrapper --> 
<form action="${contextPath}/enableUser.htm" method="GET"
   id="enableUser">
   <input type="hidden" name="userIdForEnableUser"
      id="userIdForEnableUser" />
   <input type="hidden" name="rtype" value="${rtype}" />
</form>
<form action="${contextPath}/disableUser.htm" method="GET"
   id="disableUser">
   <input type="hidden" name="userIdForDisableUser"
      id="userIdForDisableUser" />
   <input type="hidden" name="rtype" value="${rtype}" />
</form>
<form action="${contextPath}/edit-user" method="POST"
   id="editForm">
   <input type="hidden" name="${_csrf.parameterName}"
      value="${_csrf.token}" />
   <input type="hidden" name="id_edit"
      id="id_edit" />
   <input type="hidden" name="rtype" value="${rtype}" />
</form>

<form action="${contextPath}/users/${rtype}" method="GET" id="statusForm">
   <input type="hidden" name="status" id="status" />
</form>
<%-- <%@include file="/WEB-INF/views/layout/common.jsp" %> --%>
<script> 
   function enableUser(userId){
   	$("#userIdForEnableUser").val(userId);
   	bootbox.confirm("Do you want to enable this User?",
   	function(result) {
   		if (result == true) {
   		   $("#enableUser").submit();
   		} 
   	});
   }	
   function disableUser(userId){
   	$("#userIdForDisableUser").val(userId);
   	bootbox.confirm("Do you want to disable this User?",
       function(result) {
           if (result == true) {
               $("#disableUser").submit();
           } 
       });
   } 
   	function edit(userId){
   	$("#id_edit").val(userId);
   	bootbox.confirm("Do you want to Edit this User?",
       function(result) {
           if (result == true) {
               $("#editForm").submit();
           } 
       });
   } 
      	function toggleBank(id,type){
      	if(type=="show")
      	{
      	$("#bank"+id).show();
      	$("#showAnc"+id).hide();
      	$("#hideAnc"+id).show();
      	}
      	else{
      		$("#bank"+id).hide();
      	$("#showAnc"+id).show();
      	$("#hideAnc"+id).hide();
      	}
   	  
   } 
        function loadDataByStatus(status){
           	$("#status").val(status);
           $("#statusForm").submit();
           } 	
      	
</script>