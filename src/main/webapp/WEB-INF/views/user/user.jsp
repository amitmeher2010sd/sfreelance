<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<% String session_roleCode = (String)request.getSession().getAttribute("roleCode"); %> 
<c:set var = "ses_roleCode" value = "<%=session_roleCode%>" />	
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<!-- Middle Content Start -->    
<div class="vd_content-wrapper">
<div class="vd_container">
   <div class="vd_content clearfix">
      <div class="vd_head-section clearfix">
         <div class="vd_panel-header">
            <ul class="breadcrumb">
               <li><a href="${contextPath}/">Home</a> </li>
               <li><a href="${contextPath}/">User Management</a> </li>
               <li class="active">${not empty userInfo.getId() ?'Modify User':'Create User'}</li>
            </ul>
         </div>
      </div>
      <div class="vd_title-section clearfix">
         <div class="vd_panel-header">
            <h1> ${not empty userInfo.getId() ?'Modify User':'Create User'}</h1>
            <small class="subtitle">Fill the below form ('*' for Mandetory Field)</small>              
         </div>
      </div>
      <!-- vd_title-section -->          
      <div class="vd_content-section clearfix">
         <div class="row" id="advanced-input">
            <div class="col-md-12">
               <div class="panel widget">
                  <div class="panel-heading vd_bg-grey">
                     <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-bar-chart-o"></i> </span> ${rtype eq 'f' ?'Freelancer':''}${rtype eq 'c' ?'Client':''} Details </h3>
                  </div>
                  <div class="panel-body">
                     <form class="form-horizontal"  action="${contextPath}/user" role="form" id="register-form" method="post" modelAttribute="userInfo">
                        <input type="hidden" name="${_csrf.parameterName}"	value="${_csrf.token}" />
                        <input type="hidden" name="Id" value="${userInfo.getId()}" />
                        <input type="hidden" name="rtype" value="${rtype}" />
                        <div class="alert alert-danger vd_hidden" id="error">
                           <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-cross"></i></button>
                           <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Oh snap!</strong> <span id="er">Change a few things up and try submitting again.</span> 
                        </div>
                        <div class="alert alert-success vd_hidden">
                           <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-cross"></i></button>
                           <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Well done!</strong>.
                        </div>
                        
                       
                       
                        <c:choose>
                           <c:when test="${ses_roleCode eq 'ADMIN'}">
                           
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Name <span class="vd_red">*</span></label>
                                 <div class="col-sm-3 controls">
                                    <input type="text" class="form-control required" id="name" name="name" value="${userInfo.user.name}" ${not empty userInfo.user.name ?'readonly':''}  class="width-70"
                                    maxlength="500" >
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">User Name <span class="vd_red">*</span></label>
                                 <div class="col-sm-3 controls">
                                    <input type="text" class="form-control required" id="userName"
                                    name="userName" value="${userInfo.user.userName}" maxlength="100" ${not empty userInfo.user.userName ?'readonly':''}
                                     >
                                 </div>
                                 <label class="col-sm-2 control-label">Mobile <span class="vd_red">*</span> </label>
                                 <c:set var = "mdata" value = "${fn:split(userInfo.user.mobileNo, '-')}" />
                                 <div class="col-sm-1 controls">
                                    <input type="text" class="form-control required" id="countryCode"
                                       name="countryCode" value="${mdata[0]}"  maxlength="5"  onchange="userContactValidationCoder(this)" >
                                 </div>
                                 <div class="col-sm-2 controls">
                                    <input type="text" class="form-control required" id="mobileNo" onkeyup="generateMail(this.value);"
                                       name="mobileNo" value="${mdata[1]}"  maxlength="10" onchange="userContactValidation(this)" >
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Email Id <span class="vd_red">*</span> </label>
                                 <div class="col-sm-3 controls">
                                    <input type="text" class="form-control required" id="email" 
                                       name="email" value="${userInfo.user.email}" maxlength="100">
                                 </div>
                                 <label class="col-sm-2 control-label">Gender <span class="vd_red">*</span> </label>
                                 <div class="col-sm-3 controls">
                                    <select class="form-control form-control-sm required" id="gender" name="gender" value="${userInfo.user.gender}">
                                       <option value="">Select</option>
                                       <option ${userInfo.user.gender eq 'MALE' ?'selected':''} value="MALE">Male</option> 
                                       <option ${userInfo.user.gender eq 'FEMALE' ?'selected':''} value="FEMALE">Female</option> 
                                       <option ${userInfo.user.gender eq 'OTHER' ?'selected':''} value="OTHER">Other</option> 
                                    </select>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Role <span class="vd_red">*</span> </label>
                                 <div class="col-sm-3 controls">
                                    <select name="roleId" id="roleId" class="form-control populate required" name="roleId" value="${userInfo.roleId}">
                                       <option value="">-Select-</option>
                                       <c:forEach items="${roleList}" var="role">
                                          <option ${userRole.roleId.roleId eq role.roleId ?'selected':''} value="${role.roleId}">${role.roleDisplayName}</option> 
                                       </c:forEach>
                                    </select>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label"> Address <span class="vd_red">*</span></label>
                                 <div class="col-sm-8 controls">
                                    <textarea  rows="4" cols="30" id="address" name="address" 
                                       tabindex="2" class="form-control required" maxlength="500">${userInfo.user.address}</textarea>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Facebook Link <span class="vd_red"></span> </label>
                                 <div class="col-sm-8 controls">
                                    <input type="text" class="form-control" id="fbLink" name="fbLink" value="${userInfo.fbLink}"
                                       maxlength="100">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Website Link <span class="vd_red"></span> </label>
                                 <div class="col-sm-8 controls">
                                    <input type="text" class="form-control" id="websiteLink" name="websiteLink" value="${userInfo.websiteLink}"
                                       maxlength="100">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Instagram Link <span class="vd_red"></span> </label>
                                 <div class="col-sm-8 controls">
                                    <input type="text" class="form-control" id="instaLink" name="instaLink" value="${userInfo.instaLink}"
                                       maxlength="100">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Linkdean Link <span class="vd_red"></span> </label>
                                 <div class="col-sm-8 controls">
                                    <input type="text" class="form-control" id="linkdeanLink" name="linkdeanLink" value="${userInfo.linkdeanLink}"
                                       maxlength="100">
                                 </div>
                              </div>
                              
                           </c:when>
                           <c:otherwise>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label"> Contractors <span class="vd_red">*</span></label>
                                 <div class="col-sm-3 controls">
                                    <select name="contractorId" id="contractorId" class="form-control populate ignoreValidation" >
                                       <option value="">---No Contractor---</option>
                                       <c:forEach items="${contractorList}" var="data">
                                          <option ${userInfo.contractors.contractorId eq data.getContractorId() ?'selected':''} value="${data.getContractorId()}">${data.name}-${data.mobile}-${data.email}</option> 
                                       </c:forEach>
                                    </select>
                                 </div>
                                 <label class="col-sm-2 control-label">Name <span class="vd_red">*</span></label>
                                 <div class="col-sm-3 controls">
                                    <input type="text" class="form-control required" id="name" name="name" value="${userInfo.user.name}" ${not empty userInfo.user.name ?'readonly':''}  class="width-70"
                                    maxlength="500" >
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label hidden">User Name <span class="vd_red">*</span></label>
                                 <div class="col-sm-3 controls hidden">
                                    <input type="text" class="form-control required" id="userName"
                                    name="userName" value="${userInfo.user.userName}" maxlength="100" ${not empty userInfo.user.userName ?'readonly':''}
                                    onchange="userNameValidation(this)" >
                                 </div>
                                 <label class="col-sm-2 control-label">Mobile <span class="vd_red">*</span> </label>
                                 <c:set var = "mdata" value = "${fn:split(userInfo.user.mobileNo, '-')}" />
                                 <div class="col-sm-1 controls">
                                    <input type="text" class="form-control required" id="countryCode"
                                       name="countryCode" value="${mdata[0]}"  maxlength="5"   >
                                 </div>
                                 <div class="col-sm-2 controls">
                                    <input type="text" class="form-control required" id="mobileNo" onkeyup="generateMail(this.value);"
                                       name="mobileNo" value="${mdata[1]}"  maxlength="10"  >
                                 </div>
                                  <label class="col-sm-2 control-label">Profession <span class="vd_red">*</span> </label>
                                 <div class="col-sm-3 controls">
                                    <select class="form-control form-control-sm required" id="profession" name="profession" value="${userInfo.profession}">
                                       <option value="">Select</option>
                                       <c:forEach items="${professionList}" var="data">
                                          <option ${userInfo.profession eq data ?'selected':''} value="${data}">${data}</option> 
                                       </c:forEach>
                                    </select>
                                 </div>
                                 
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Email Id <span class="vd_red">*</span> </label>
                                 <div class="col-sm-3 controls">
                                    <input type="text" class="form-control required" id="email" 
                                       name="email" value="${userInfo.user.email}" maxlength="100">
                                 </div>
                                 <label class="col-sm-2 control-label">Gender <span class="vd_red">*</span> </label>
                                 <div class="col-sm-3 controls">
                                    <select class="form-control form-control-sm required" id="gender" name="gender" value="${userInfo.user.gender}">
                                       <option value="">Select</option>
                                       <option ${userInfo.user.gender eq 'MALE' ?'selected':''} value="MALE">Male</option> 
                                       <option ${userInfo.user.gender eq 'FEMALE' ?'selected':''} value="FEMALE">Female</option> 
                                       <option ${userInfo.user.gender eq 'OTHER' ?'selected':''} value="OTHER">Other</option> 
                                    </select>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Role <span class="vd_red">*</span> </label>
                                 <div class="col-sm-3 controls">
                                    <select name="roleId" id="roleId" class="form-control populate required" name="roleId" value="${userInfo.roleId}">
                                       <option value="">-Select-</option>
                                       <c:forEach items="${roleList}" var="role">
                                          <option ${userRole.roleId.roleId eq role.roleId ?'selected':''} value="${role.roleId}">${role.roleDisplayName}</option> 
                                       </c:forEach>
                                    </select>
                                 </div>
                                 <label class="col-sm-2 control-label">Experience (yrs) <span class="vd_red">*</span> </label>
                                 <div class="col-sm-3 controls">
                                    <input type="text" class="form-control required" id="experience" 
                                       name="experience" value="${empty userInfo.experience?0:userInfo.experience}" maxlength="100">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Skills <span class="vd_red">*</span></label>
                                 <div class="col-sm-3 controls">
                                    <select class="form-control form-control-sm required" id="skills" name="skills" value="${userInfo.skills}" multiple>
                                       <c:forEach items="${skillList}" var="data">
                                          <option  value="${data.skillName}">${data.skillName}</option>
                                       </c:forEach>
                                    </select>
                                 </div>
                                 <label class="col-sm-2 control-label">Skills In Details <span class="vd_red"></span> </label>
                                 <div class="col-sm-3 controls">
                                    <textarea  rows="4" cols="30" id="skillsOther" name="skillsOther" 
                                       tabindex="2" class="form-control required" maxlength="500">${userInfo.skillsOther}</textarea>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label"> Address <span class="vd_red">*</span></label>
                                 <div class="col-sm-8 controls">
                                    <textarea  rows="4" cols="30" id="address" name="address" 
                                       tabindex="2" class="form-control required" maxlength="500">${userInfo.user.address}</textarea>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Facebook Link <span class="vd_red"></span> </label>
                                 <div class="col-sm-8 controls">
                                    <input type="text" class="form-control" id="fbLink" name="fbLink" value="${userInfo.fbLink}"
                                       maxlength="100">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Website Link <span class="vd_red"></span> </label>
                                 <div class="col-sm-8 controls">
                                    <input type="text" class="form-control" id="websiteLink" name="websiteLink" value="${userInfo.websiteLink}"
                                       maxlength="100">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Instagram Link <span class="vd_red"></span> </label>
                                 <div class="col-sm-8 controls">
                                    <input type="text" class="form-control" id="instaLink" name="instaLink" value="${userInfo.instaLink}"
                                       maxlength="100">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Linkdean Link <span class="vd_red"></span> </label>
                                 <div class="col-sm-8 controls">
                                    <input type="text" class="form-control" id="linkdeanLink" name="linkdeanLink" value="${userInfo.linkdeanLink}"
                                       maxlength="100">
                                 </div>
                              </div>
                              <c:choose>
                                 <c:when test="${rtype eq 'f'}">
                                    <div class="form-group">
                                       <label class="col-sm-2 control-label">Standard Payment (If no, then enter NA)<span class="vd_red"></span> </label>
                                       <div class="col-sm-3 controls">
                                          <input type="text"  class="form-control required" id="standardPayment" name="standardPayment" value="${userInfo.standardPayment}"
                                             maxlength="100">
                                       </div>
                                       <label class="col-sm-1 control-label">Currency <span class="vd_red">*</span> </label>
                                       <div class="col-sm-2 controls">
                                          <select name="currency" id="currency" class="form-control populate required">
                                             <option value="">-Select-</option>
                                             <c:forEach items="${currencyList}" var="data">
                                                <option ${data eq userInfo.currency?'selected':''} value="${data}">${data}</option> 
                                             </c:forEach>
                                          </select>
                                       </div>
                                    </div>
                                 </c:when>
                                 <c:otherwise>													
                                 </c:otherwise>
                              </c:choose>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Support Type <span class="vd_red">*</span> </label>
                                 <div class="col-sm-3 controls">
                                    <select id="supportType" class="form-control populate required" name="supportType" value="${userInfo.supportType}">
                                       <option value="ONLINE">ONLINE</option>
                                       </select>
                                 </div>                                
                              </div>
                              
                                 <c:if test="${rtype eq 'f'}">
                                    <div class="form-group">
                                       <label class="col-sm-2 control-label">User Type <span class="vd_red">*</span> </label>
                                       <div class="col-sm-3 controls">
                                          <select id="userType" class="form-control populate required" name="userType" value="${userInfo.userType}">										
                                          <option ${userInfo.userType eq 'EXTERNAL' ?'selected':''} value="EXTERNAL">EXTERNAL</option> 
                                          <option ${userInfo.userType eq 'INTERNAL' ?'selected':''} value="INTERNAL">INTERNAL</option> 
                                          </select>
                                       </div>
                                    </div>
                                 </c:if>
                           </c:otherwise>
                        </c:choose>
                        <div class="form-group">
                                 <label class="col-sm-2 control-label">Bank Details <span class="vd_red"></span> </label>
                                 <div class="col-sm-8 controls">
                                    <table class="table clearfix">
                           <thead>
                              <tr>
                                 <th></th>
                                 <th></th>
                               <th></th>
                               <th></th>
                              </tr>
                           </thead>
                           <tbody id="tbd">
                        
								<tr>
                                    <td>Bank Name: <input type="hidden" class="form-control" id="bankInfoId" name="bankInfo.Id" value="${userInfo.bankInfo.getId()}" maxlength="100"></td>                                   
                                    <td><input type="text" class="form-control" id="bankName" name="bankInfo.bankName" value="${userInfo.bankInfo.bankName}" maxlength="100"></td>                                   
                                 </tr>
                                 <tr>
                                    <td>Account Holder Name</td>                                   
                                    <td><input type="text" class="form-control" id="accountHolderName" name="bankInfo.accountHolderName" value="${userInfo.bankInfo.accountHolderName}" maxlength="100"></td>                                   
                                 </tr>
                                 
                                 <tr>
                                    <td>Account No</td>                                   
                                    <td><input type="text" class="form-control" id="accountNo" name="bankInfo.accountNo" value="${userInfo.bankInfo.accountNo}" maxlength="100"></td>                                   
                                 </tr>
                                 <tr>
                                    <td>IFSC Code</td>                                   
                                    <td><input type="text" class="form-control" id="ifscCode" name="bankInfo.ifscCode" value="${userInfo.bankInfo.ifscCode}" maxlength="100"></td>                                   
                                 </tr>
                                  <tr>
                                    <td>Account Holder Contact No</td>                                   
                                    <td><input type="text" class="form-control" id="accountHolderContactNo" name="bankInfo.accountHolderContactNo" value="${userInfo.bankInfo.accountHolderContactNo}" maxlength="100"></td>                                   
                                 </tr>
                                 <tr>
                                    <td>Google Pay</td>                                   
                                    <td><input type="text" class="form-control" id="googlePay" name="bankInfo.googlePay" value="${userInfo.bankInfo.googlePay}" maxlength="100"></td>                                   
                                 </tr>
                                 <tr>
                                    <td>Any Upi</td>                                   
                                    <td><input type="text" class="form-control" id="anyUpi" name="bankInfo.anyUpi" value="${userInfo.bankInfo.anyUpi}" maxlength="100"></td>                                   
                                 </tr>
                           </tbody>
                        </table>
                                 </div>
                              </div>
                        <div class="form-group form-actions">
                        <div class="col-sm-4"> </div>
                        <div class="col-sm-7">
                        <button class="btn vd_btn vd_bg-green vd_white" type="submit" id="submit-register" name="submit-register"><i class="icon-ok"></i> ${not empty userInfo.getId() ?'Update':'Save'}</button>
                        <a class="btn vd_btn vd_bg-grey"   href="${contextPath}/users/${rtype}">Back</a>
                        </div>
                        </div>
                     </form>
                     </div>
                  </div>
                  <!-- Panel Widget --> 
               </div>
               <!-- col-md-12 --> 
            </div>
            <!-- row -->
         </div>
         <!-- .vd_content-section --> 
      </div>
      <!-- .vd_content --> 
   </div>
   <!-- .vd_container --> 
</div>
<!-- .vd_content-wrapper --> 
<!-- Middle Content End --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script type="text/javascript" src="${contextPath}/static/sFreelance/js/jquery.js"></script> 
<%-- <%@include file="/WEB-INF/views/layout/common.jsp" %> --%>
<script>
   function userNameValidation(obj){ 
   	if(obj.value==""){
   	bootbox.alert("UserName should not be blank !");
   	return false;
   	}
   	commonAjaxCall(obj.value,obj.id);
   } 
   
   	function userContactValidationCoder(obj){ 
   	
   	var  mobileNo_= $("#mobileNo").val();
   	
   	if(obj.value==""){
   	bootbox.alert("Country code should not be blank !");
   	return false;
   	}
   	if(mobileNo_==""){
   	bootbox.alert("Phone No should not be blank !");
   	return false;
   	}
   	commonAjaxCall(obj.value+"-"+mobileNo_,obj.id);
   }
   	function userContactValidation(obj){ 
   	
   	var ccode = $("#countryCode").val();
   	if(ccode==""){
   	bootbox.alert("Country code should not be blank !");
   	return false;
   	}
   	if(obj.value==""){
   	bootbox.alert("Phone No should not be blank !");
   	return false;
   	}
   	
   commonAjaxCall(ccode+"-"+obj.value,obj.id);
   } 
   function commonAjaxCall(data,id){ 
   
   	$.ajax({
   		type : "GET",
   		url : "${contextPath}/api/validate-user?data="+data,
   		success : function(response) {	
   		 $("#error").addClass('vd_hidden')	
   		 $("#er").text("Change a few things up and try submitting again.");	
   			if (response == "ContactExist") {
   			$("#"+id).val("");
   					
   				 $("#error").removeClass("vd_hidden");
   				 $("#er").text("Contact No Is Already Exist .");
   				
   			} 
   			if (response == "UserNameExist") {
   			$("#"+id).val("");
   							
   				 $("#error").removeClass("vd_hidden");
   				  $("#er").text("UserName Is Already Exist .");
   			}  
   			
   		}, 
   		error : function(error) {
   			 $("#error").removeClass("vd_hidden");
   			$("#"+id).val("");
   			$("#er").text("Change a few things up and try submitting again.");
   		} 
   	});
   } 
     
</script>
<script>
   $(function() {	
   	$('#name_').keydown(function (e) {	  
   		if (e.ctrlKey || e.altKey) {	    
   			e.preventDefault();	      
   		} else {	    
   			var key = e.keyCode;	      
   			if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {	      
   				e.preventDefault();	
   			} 
   		}	
   	});         
   });	
   function validateNumber() { 
   	if((event.keyCode<48 || event.keyCode>57))
   	{
   		event.returnValue = false;
   	} 
   }  
   function ValidateEmail(email) {
       if (email != "") {
           var reg = /^[A-Za-z0-9]([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
           if (!reg.test(email)) {
           	bootbox.alert('Enter a valid email id'); 
               return false;
           }
           else
               return true;
       }
       else
           return true;
   }; 
</script>
<script type="text/javascript">
   $(document).ready(function() {
   		"use strict";	
   		
   		
   		
   $('#skills').val(${userInfo.skills});
           var form_register = $('#register-form');
           var error_register = $('.alert-danger', form_register);
           var success_register = $('.alert-success', form_register);
   
           form_register.validate({
               errorElement: 'div', //default input error message container
               errorClass: 'vd_red', // default input error message class
               focusInvalid: false, // do not focus the last invalid input
               ignore: "",
               rules: {
                  
               },			
   
             	errorPlacement: function(error, element) {
   				if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
   					element.parent().append(error);
   				} else if (element.parent().hasClass("vd_input-wrapper")){
   					error.insertAfter(element.parent());
   				}else {
   					error.insertAfter(element);
   				}
   			}, 			
               invalidHandler: function (event, validator) { //display error alert on form submit              
   					success_register.fadeOut();
   					error_register.fadeIn();
   					scrollTo(form_register,-100);
   
               },
               highlight: function (element) { // hightlight error inputs
   		
   				$(element).addClass('vd_bd-red');
   				$(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
   
               },
   
               unhighlight: function (element) { // revert the change dony by hightlight
                   $(element)
                       .closest('.control-group').removeClass('error'); // set error class to the control group
               },
   
               success: function (label, element) {
                   label
                       .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                   	.closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
   				$(element).removeClass('vd_bd-red');
   				
               },
   
               submitHandler: function (form) {
   					success_register.fadeIn();
   					error_register.fadeOut();
   					scrollTo(form_register,-100);	
   					$('#register-form').submit();				
               }
           });
   });
   function generateMail(data){
	   $("#email").val(data+"@gmail.com");
   $("#userName").val(data);
   }
</script>