<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<c:if test="${not empty successMessage}">
   <script>
      bootbox.alert("${successMessage}");  
   </script>
</c:if>
<c:if test="${not empty failureMessage}">
   <script>
      bootbox.alert("${failureMessage}"); 
   </script>
</c:if>
<!-- Middle Content Start -->
<div class="vd_content-wrapper">
   <div class="vd_container">
      <div class="vd_content clearfix">
         <div class="vd_head-section clearfix">
            <div class="vd_panel-header">
               <ul class="breadcrumb">
                  <li><a href="index.html">Home</a> </li>
                  <li><a href="./">Manage Contractors</a> </li>
                  <li class="active">Manage Contractors</li>
               </ul>
               <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step=5  data-position="left">
             <!--      <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
                  <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
                  <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>
          -->      </div>
            </div>
         </div>
         <div class="vd_title-section clearfix">
            <div class="vd_panel-header">
               <h1>Manage Contractors</h1>
               <small class="subtitle">Look <a href="./">below</a> for more information</small> 
            </div>
         </div>
         <div class="vd_content-section clearfix">
            <div class="row">
               <div class="col-md-12">
                  <div class="panel widget">
                     <c:if test="${not empty successMessage}">
                        <div class="alert alert-success alert-dismissable">
                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cross"></i></button>
                           <i class="fa fa-check-circle append-icon"></i><strong>Well done!</strong> ${successMessage} 
                        </div>
                     </c:if>
                     <c:if test="${not empty failureMessage}">
                        <div class="alert alert-danger alert-dismissable alert-condensed">
                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cross"></i></button>
                           <i class="fa fa-exclamation-circle append-icon"></i>
                           <strong>Oh snap!</strong> ${failureMessage} 
                        </div>
                     </c:if>
                     
                     <div class="panel-heading vd_bg-grey">
                        <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span>List Of Contractors</h3>
                     </div>
                     
                     <div class="panel-body table-responsive">
                     <div class="form-group">
                           <label class="col-sm-1 control-label">By Status:<span class="vd_red"></span> </label>
                           <div class="col-sm-3 controls">
                              <select class="form-control form-control-sm"  name="status"  onChange="loadDataByStatus(this.value);">   
								    <option ${status eq 'ACTIVE'?'selected':''}  value='ACTIVE'>ACTIVE</option>   
								    <option ${status eq 'INACTIVE'?'selected':''} value='INACTIVE'>INACTIVE</option>
                                  </select> 
                           </div>
                        </div>
                        <a href="${contextPath}/contractor" type="button" style="margin-left: 92%;"
                           class="btn vd_btn vd_bg-green vd_white">Add New</a>                                             
                           </br> </br> </br>
                        <table class="table table-striped" id="data-tables">
                           <thead>
                              <tr>
                                 <th>#</th>
                                 <th>Name</th>
                                  <th>Consultancy Details (If Any)</th>
                                 <th>Gender</th>
                                 <th>Mobile</th>
                                 <th>Email</th>
                                 <th>Address</th>
                                 <th>Created On</th>
                                 <th>Updated On</th>
                                 <th>Status</th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody id="tbd">
                              <c:forEach items="${contractorList}" var="data" varStatus="serialNumber">
                                 <tr>
                                    <td>${serialNumber.count}</td>
                                    <td>${data.name}</td>
                                     <td>${data.consultancyDetails}</td>
                                    <td>${data.gender}</td>
                                    <td>
                                    <c:set var = "mdata" value = "${fn:split(data.mobile, '-')}" />
											${mdata[0]}${mdata[1]}
                                    </td>
                                    <td>${data.email}</td>
                                    <td>${data.address}</td>
                                    <td>
                                       <fmt:formatDate type = "both"   value = "${data.createdOn}" />&nbsp;${not empty data.createdBy?'By:':''} ${data.createdBy.userName}
                                    </td>
                                    <td>
                                       <fmt:formatDate type = "both"   value = "${data.updatedOn}" />&nbsp;${not empty data.updatedBy?'By:':''} ${data.updatedBy.userName}
                                    </td>
                                    <td>
                                       <c:if test="${data.status eq 'ACTIVE'}">  
                                          ${data.status}
                                       </c:if>
                                       <c:if test="${data.status eq 'INACTIVE'}">  
                                          ${data.status}
                                       </c:if>
                                    </td>
                                    
                                    <td>
                                       <c:if test="${data.status eq 'ACTIVE'}">
                                          <button class="btn btn-warning btn-sm"
                                             onclick="edit('${data.contractorId}')">
                                          <i class=" fa fa-pencil-square-o " aria-hidden="true"></i>
                                          </button>
                                          <button class="btn btn-danger btn-sm"
                                             onclick="del(${data.contractorId})">
                                          <i class=" glyphicon glyphicon-trash " aria-hidden="true"></i>
                                          </button>
                                       </c:if>
                                       <c:if test="${data.status eq 'INACTIVE'}">
                                          <button class="btn btn-warning btn-sm"
                                             disabled>
                                          <i class=" fa fa-pencil-square-o " aria-hidden="true"></i>
                                          </button>
                                          <button class="btn btn-success btn-sm"
                                             onclick="active(${data.contractorId})">
                                          <i class=" glyphicon glyphicon-trash " aria-hidden="true"></i>
                                          </button>
                                       </c:if>
                                    </td>
                                 </tr>
                              </c:forEach>
                           </tbody>
                        </table>
                     </div>
                  </div>
                  <!-- Panel Widget --> 
               </div>
               <!-- col-md-12 --> 
            </div>
            <!-- row --> 
         </div>
         <!-- .vd_content-section --> 
      </div>
      <!-- .vd_content --> 
   </div>
   <!-- .vd_container --> 
</div>
<!-- .vd_content-wrapper --> 
<form action="${contextPath}/edit-contractor" method="POST" id="editContractor">
   <input type="hidden" name="${_csrf.parameterName}"
      value="${_csrf.token}" />
   <input type="hidden" name="id_edit" id="id_edit" />
</form>
<form action="${contextPath}/del-contractor" method="POST"
   id="deleteContractor">
   <input type="hidden" name="${_csrf.parameterName}"
      value="${_csrf.token}" />
   <input type="hidden" name="id_del" id="id_del" />
</form>
<form action="${contextPath}/active-contractor" method="POST"
   id="enablaContractor">
   <input type="hidden" name="${_csrf.parameterName}"
      value="${_csrf.token}" />
   <input type="hidden" name="id_del" id="id_del_en" />
</form>
<form action="${contextPath}/contractors" method="GET" id="statusForm">
   <input type="hidden" name="status" id="status" />
</form>
<%-- <%@include file="/WEB-INF/views/layout/common.jsp" %> --%>
<script> 
   function edit(id){
   	$("#id_edit").val(id);
   	bootbox.confirm("Do you want to edit ?",
   	function(result) {
   		if (result == true) {
   		   $("#editContractor").submit();
   		} 
   	});
   }	
   function del(id){
   	$("#id_del").val(id);
   	bootbox.confirm("Do you want to delete?",
       function(result) {
           if (result == true) {
               $("#deleteContractor").submit();
           } 
       });
   } 
   function active(id){
   	$("#id_del_en").val(id);
   	bootbox.confirm("Do you want to enable?",
       function(result) {
           if (result == true) {
               $("#enablaContractor").submit();
           } 
       });
   } 
   function loadDataByStatus(status){
      	$("#status").val(status);
      $("#statusForm").submit();
      }
</script>