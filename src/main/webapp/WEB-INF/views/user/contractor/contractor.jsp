<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<!-- Middle Content Start -->    
<div class="vd_content-wrapper">
   <div class="vd_container">
      <div class="vd_content clearfix">
         <div class="vd_head-section clearfix">
            <div class="vd_panel-header">
               <ul class="breadcrumb">
                  <li><a href="${contextPath}/">Home</a> </li>
                  <li><a href="${contextPath}/">User Management</a> </li>
                  <li class="active">${not empty contractor.getContractorId() ?'Modify Contractor':'Create Contractor'}</li>
               </ul>
            </div>
         </div>
         <div class="vd_title-section clearfix">
            <div class="vd_panel-header">
               <h1> ${not empty contractor.getContractorId() ?'Modify Contractor':'Create Contractor'}</h1>
               <small class="subtitle">Fill the below form ('*' for Mandetory Field)</small>              
            </div>
         </div>
         <!-- vd_title-section -->          
         <div class="vd_content-section clearfix">
            <div class="row" id="advanced-input">
               <div class="col-md-12">
                  <div class="panel widget">
                     <div class="panel-heading vd_bg-grey">
                        <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-bar-chart-o"></i> </span> Contractor Details </h3>
                     </div>
                     <div class="panel-body">
                        <form class="form-horizontal"  action="${contextPath}/contractor" role="form" id="register-form" method="post" modelAttribute="contractor">
                           <input type="hidden" name="${_csrf.parameterName}"	value="${_csrf.token}" />
                           <input type="hidden" name="contractorId" value="${contractor.getContractorId()}" />
                           <div class="alert alert-danger vd_hidden">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-cross"></i></button>
                              <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Oh snap!</strong> Change a few things up and try submitting again. 
                           </div>
                           <div class="alert alert-success vd_hidden">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-cross"></i></button>
                              <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Well done!</strong>.
                           </div>
                           <div class="form-group">
                              <label class="col-sm-2 control-label">Name <span class="vd_red">*</span></label>
                              <div class="col-sm-3 controls">
                                 <input type="text" class="form-control required" id="name" name="name" value="${contractor.name}" ${not empty contractor.name ?'readonly':''}  class="width-70"
                                 maxlength="500" >
                              </div>
                              <label class="col-sm-2 control-label">Mobile Number <span class="vd_red">*</span> </label>
                             
                               <c:set var = "mdata" value = "${fn:split(contractor.mobile, '-')}" />
											
                        <div class="col-sm-1 controls">
                        <input type="text" class="form-control required" id="countryCode"
												name="countryCode" value="${mdata[0]}"  maxlength="5" >
                          </div>
                        <div class="col-sm-2 controls">
                          <input type="text" class="form-control required" id="mobile"
												name="mobile" value="${mdata[1]}"  maxlength="10" >
                        </div>
                           </div>
                           <div class="form-group">
                              <label class="col-sm-2 control-label"> Consultancy Details (If Any)</label>
                              <div class="col-sm-8 controls">
                                 <textarea  rows="4" cols="30" id="consultancyDetails" name="consultancyDetails" 
                                    tabindex="2" class="form-control" maxlength="500">${contractor.consultancyDetails}</textarea>
                              </div>
                           </div>
                           
                           
                           <div class="form-group">
                              <label class="col-sm-2 control-label">Email Id <span class="vd_red">*</span> </label>
                              <div class="col-sm-3 controls">
                                 <input type="text" class="form-control required" id="email" 
                                    name="email" value="${contractor.email}" maxlength="100">
                              </div>
                              <label class="col-sm-2 control-label">Gender <span class="vd_red">*</span> </label>
                              <div class="col-sm-3 controls">
                                 <select class="form-control form-control-sm required" id="gender" name="gender" value="${contractor.gender}">
                                    <option value="">Select</option>
                                    <option ${contractor.gender eq 'MALE' ?'selected':''} value="MALE">Male</option> 
                                    <option ${contractor.gender eq 'FEMALE' ?'selected':''} value="FEMALE">Female</option> 
                                    <option ${contractor.gender eq 'OTHER' ?'selected':''} value="OTHER">Other</option> 
                                 </select>
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-sm-2 control-label"> Address <span class="vd_red">*</span></label>
                              <div class="col-sm-8 controls">
                                 <textarea  rows="4" cols="30" id="address" name="address" 
                                    tabindex="2" class="form-control required" maxlength="500">${contractor.address}</textarea>
                              </div>
                           </div>
                           <div class="form-group form-actions">
                              <div class="col-sm-4"> </div>
                              <div class="col-sm-7">
                                 <button class="btn vd_btn vd_bg-green vd_white" type="submit" id="submit-register" name="submit-register"><i class="icon-ok"></i> ${not empty contractor.getContractorId() ?'Update':'Save'}</button>
                                 <a class="btn vd_btn vd_bg-grey"   href="${contextPath}/contractors">Back</a>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
                  <!-- Panel Widget --> 
               </div>
               <!-- col-md-12 --> 
            </div>
            <!-- row -->
         </div>
         <!-- .vd_content-section --> 
      </div>
      <!-- .vd_content --> 
   </div>
   <!-- .vd_container --> 
</div>
<!-- .vd_content-wrapper --> 
<!-- Middle Content End --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script type="text/javascript" src="${contextPath}/static/sFreelance/js/jquery.js"></script> 
<%-- <%@include file="/WEB-INF/views/layout/common.jsp" %> --%>
<script>
   $(function() {	
   	$('#name').keydown(function (e) {	  
   		if (e.ctrlKey || e.altKey) {	    
   			e.preventDefault();	      
   		} else {	    
   			var key = e.keyCode;	      
   			if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {	      
   				e.preventDefault();	
   			} 
   		}	
   	});         
   });	
   function validateNumber() { 
   	if((event.keyCode<48 || event.keyCode>57))
   	{
   		event.returnValue = false;
   	} 
   }  
   function ValidateEmail(email) {
       if (email != "") {
           var reg = /^[A-Za-z0-9]([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
           if (!reg.test(email)) {
           	bootbox.alert('Enter a valid email id'); 
               return false;
           }
           else
               return true;
       }
       else
           return true;
   }; 
</script>
<script type="text/javascript">
   $(document).ready(function() {
   		"use strict";	
   
           var form_register = $('#register-form');
           var error_register = $('.alert-danger', form_register);
           var success_register = $('.alert-success', form_register);
   
           form_register.validate({
               errorElement: 'div', //default input error message container
               errorClass: 'vd_red', // default input error message class
               focusInvalid: false, // do not focus the last invalid input
               ignore: "",
               rules: {
                   email: {
                       required: true,
                       email: true
                   },
                    name: {
                       required: true,                 
                   },
                    mobile: {
                       required: true,                 
                   },
                    gender: {
                       required: true,                 
                   }
               },
   			
   
             	errorPlacement: function(error, element) {
   				if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
   					element.parent().append(error);
   				} else if (element.parent().hasClass("vd_input-wrapper")){
   					error.insertAfter(element.parent());
   				}else {
   					error.insertAfter(element);
   				}
   			}, 
   			
               invalidHandler: function (event, validator) { //display error alert on form submit              
   					success_register.fadeOut();
   					error_register.fadeIn();
   					scrollTo(form_register,-100);
   
               },
   
               highlight: function (element) { // hightlight error inputs
   		
   				$(element).addClass('vd_bd-red');
   				$(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
   
               },
   
               unhighlight: function (element) { // revert the change dony by hightlight
                   $(element)
                       .closest('.control-group').removeClass('error'); // set error class to the control group
               },
   
               success: function (label, element) {
                   label
                       .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                   	.closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
   				$(element).removeClass('vd_bd-red');
   				
               },
   
               submitHandler: function (form) {
   					success_register.fadeIn();
   					error_register.fadeOut();
   					scrollTo(form_register,-100);	
   					$('#register-form').submit();				
               }
           });
   });
</script>