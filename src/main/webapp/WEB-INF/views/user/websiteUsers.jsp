<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<!-- Middle Content Start -->

<div class="vd_content-wrapper">
   <div class="vd_container">
      <div class="vd_content clearfix">
         <div class="vd_head-section clearfix">
            <div class="vd_panel-header">
               <ul class="breadcrumb">
                  <li><a href="${contextPath}/">Home</a> </li>
                  <li><a href="${contextPath}/">User Management</a> </li>
                  <li class="active">Manage Website Freelancers</li>
               </ul>
               <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step=5  data-position="left">
           <!--        <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
                  <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
                  <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>
        -->        </div>
            </div>
         </div>
         <div class="vd_title-section clearfix">
            <div class="vd_panel-header">
               <h1>ALL Website Freelancers</h1>
               <small class="subtitle">Look <a href="http://www.datatables.net">below</a> for more information</small> 
            </div>
         </div>
         <div class="vd_content-section clearfix">
            <div class="row">
               <div class="col-md-12">
                  <div class="panel widget">
                     <c:if test="${not empty successMessage}">
                        <div class="alert alert-success alert-dismissable">
                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cross"></i></button>
                           <i class="fa fa-check-circle append-icon"></i><strong>Well done!</strong> ${successMessage} 
                        </div>
                     </c:if>
                     <c:if test="${not empty failureMessage}">
                        <div class="alert alert-danger alert-dismissable alert-condensed">
                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cross"></i></button>
                           <i class="fa fa-exclamation-circle append-icon"></i>
                           <strong>Oh snap!</strong> ${failureMessage} 
                        </div>
                     </c:if>
                     <div class="panel-heading vd_bg-grey">
                        <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span>List Of Freelancers</h3>
                     </div>
                     <div class="panel-body table-responsive  clearfix">
                        <a href="${contextPath}/website-user" type="button" style="margin-left: 92%;" 
                           class="btn vd_btn vd_bg-green vd_white">Add New</a>
                        </br> </br> </br> 
                        <table class="table table-striped  clearfix" id="data-tables">
                           <thead>
                              <tr>  
                                 <th>#</th>
                                 <th>Name</th>
                                 <th>User Name</th>
                                 <th>Phone <br>OR<br> Email</th>
                                 <th>Skills</th>
                                 <th>created On</th>
                                 <th>updated On</th>
                                 <th>Status</th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody id="tbd">
                              <c:forEach items="${uinfoList}" var="data" varStatus="serialNumber">
                                <tr>
                                <td>${serialNumber.count}</td>
                                <td><span class="txt_field_black">${data.user.name} (Exp. : ${data.experience} yrs)</span></td>
                                <td>${data.user.username}</td>
                                <td>${data.user.phone}<br><span class="txt_field_black"> OR </span><br>${data.user.email}</td>
                                <td>Primary : ${data.primaryskills} <br> Secondary :${data.secondaryskills}</td>
                                <td>${data.createdOn}</td>
                                <td>${data.updatedOn}</td>
                                 <td><span class="label label-${data.user.status eq 'ACTIVE'?'success':'danger'}">${data.user.status}</span> </td>
                                 
                                 <td>
                                   <button class="btn menu-icon vd_bd-green vd_green btn-sm"
                                             onclick="view('${data.user.username}')">
                                         <i class="fa fa-eye"></i>
                                          </button>
                                  <button class="btn btn-warning btn-sm"
                                             onclick="edit('${data.getId()}')">
                                          <i class=" fa fa-pencil-square-o " aria-hidden="true"></i>
                                          </button>
                           </td>
                                </tr>
                              
                              </c:forEach>
                           </tbody>
                        </table>
                     </div>
                  </div>
                  <!-- Panel Widget --> 
               </div>
               <!-- col-md-12 --> 
            </div>
            <!-- row --> 
         </div>
         <!-- .vd_content-section --> 
      </div>
      <!-- .vd_content --> 
   </div>
   <!-- .vd_container --> 
</div>
<!-- .vd_content-wrapper --> 
<form action="${contextPath}/website-user-view" method="GET"
   id="viewForm">
   <input type="hidden" name="username"
      id="username" />
</form>
<form action="${contextPath}/website-user-edit" method="POST"
   id="editForm">
   <input type="hidden" name="${_csrf.parameterName}"
      value="${_csrf.token}" />
   <input type="hidden" name="id_edit"
      id="id_edit" />
</form>
<script>
	function edit(id){
	   	$("#id_edit").val(id);
	   	bootbox.confirm("Do you want to Edit this User?",
	       function(result) {
	           if (result == true) {
	               $("#editForm").submit();
	           } 
	       });
	   } 
	function view(uname){
		$("#username").val(uname);
		 $("#viewForm").submit();
	   } 
</script>