<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<!-- Middle Content Start -->    
<div class="vd_content-wrapper">
   <div class="vd_container">
      <div class="vd_content clearfix">
         <div class="vd_head-section clearfix">
            <div class="vd_panel-header">
               <ul class="breadcrumb">
                  <li><a href="${contextPath}/">Home</a> </li>
                  <li><a href="${contextPath}/">Change Password</a> </li>
                  <li class="active">Change Password</li>
               </ul>
            </div>
         </div>
         <div class="vd_title-section clearfix">
            <div class="vd_panel-header">
               <h1>Change Password</h1>
               <small class="subtitle">Fill the below form ('*' for Mandetory Field)</small>              
            </div>
         </div>
         <!-- vd_title-section -->          
         <div class="vd_content-section clearfix">
            <div class="row" id="advanced-input">
               <div class="col-md-12">
                  <div class="panel widget">
                   <c:if test="${not empty successMessage}">
                        <div class="alert alert-success alert-dismissable">
                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cross"></i></button>
                           <i class="fa fa-check-circle append-icon"></i><strong>Well done!</strong> ${successMessage} 
                        </div>
                     </c:if>
                     <c:if test="${not empty failureMessage}">
                        <div class="alert alert-danger alert-dismissable alert-condensed">
                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cross"></i></button>
                           <i class="fa fa-exclamation-circle append-icon"></i>
                           <strong>Oh snap!</strong> ${failureMessage} 
                        </div>
                     </c:if>
                     <div class="panel-heading vd_bg-grey">
                        <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-bar-chart-o"></i> </span> Change Password </h3>
                     </div>
                     <div class="panel-body">
                        <form action="${contextPath}/pwd-change" id="changePassword" method="post"> 
							<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />  
							<input type="hidden" name="userId" value="${userId}" />  
							<div class="col-md-6">
						        <div class="label-wrapper">
						          <label class="control-label">Old Password <span class="vd_red">*</span></label>
						        </div>
						        <div class="vd_input-wrapper" id="last-name-input-wrapper"> <span class="menu-icon"> </span>
						          <input type="password" class="form-control" id="oldPassword" maxlength="50" name="oldPassword"> 
						        </div>
						     </div>
							 <div class="col-md-6">
						        <div class="label-wrapper">
						          <label class="control-label">New Password <span class="vd_red">*</span></label>
						        </div>
						        <div class="vd_input-wrapper" id="last-name-input-wrapper"> <span class="menu-icon"> </span>
						          <input type="password" class="form-control" id="newPassword" maxlength="50" name="newPassword"> 
						        </div>
						     </div>
							<div class="col-md-6">
						        <div class="label-wrapper">
						          <label class="control-label">Confirm New Password <span class="vd_red">*</span></label>
						        </div>
						        <div class="vd_input-wrapper" id="last-name-input-wrapper"> <span class="menu-icon"> </span>
						          <input type="password" class="form-control" id="confirmNewPassword" maxlength="50" name="confirmNewPassword"> 
						        </div>
						     </div>
							 <div class="col-md-12" style="margin-top: 20px;"> 
								<div class="text-center">				
									<input type="button" name="updatePwd" value="Update Password" id="updatePwd" class="btn btn-success" onclick="updatePassword();">&nbsp;&nbsp;  								
									<a href="${contextPath}/" type="button" class="btn btn-warning">Back</a> 
								</div> 
							 </div>
						</form> 
                     </div>
                  </div>
                  <!-- Panel Widget --> 
               </div>
               <!-- col-md-12 --> 
            </div>
            <!-- row -->
         </div>
         <!-- .vd_content-section --> 
      </div>
      <!-- .vd_content --> 
   </div>
   <!-- .vd_container --> 
</div>
<%-- <%@include file="/WEB-INF/views/layout/common.jsp" %> --%>
<script>
	function updatePassword() {
		var newPassword = $("#newPassword").val();
		var confirmNewPassword = $("#confirmNewPassword").val(); 
		if ($("#oldPassword").val() == "") {
			bootbox.alert("Please Enter Old Password"); 
			return false;
		}
		else if ($("#newPassword").val() == "") {
			bootbox.alert("Please Enter New Password"); 
			return false;
		}
		else if ($("#confirmNewPassword").val() == "") {
			bootbox.alert("Please Enter Confirm New Password"); 
			return false;
		}
		else if (newPassword != confirmNewPassword) { 
			bootbox.alert("Password Mismatch"); 
			$("#newPassword").val("");
			$("#confirmNewPassword").val("");  
			return false;
		} 
		else
		{
			bootbox.confirm("Do you want to change the password?",
		    function(result) {
		       if (result == true) {
		          $("#changePassword").submit();
		       }
		    }); 
		}
	}
</script>
	