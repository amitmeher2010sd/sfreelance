<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

 <!-- Middle Content Start -->
    
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <div class="vd_head-section clearfix">
            <div class="vd_panel-header">
              <ul class="breadcrumb">
                <li><a href="index.html">Home</a> </li>
                <li><a href="pages-custom-product.html">Pages</a> </li>
                <li class="active">User Profile Form</li>
              </ul>
              <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step=5  data-position="left">
   <!--  <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
      <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
      <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>
 -->      
</div>
 
            </div>
          </div>
          <div class="vd_title-section clearfix">
            <div class="vd_panel-header">
              <h1>Edit Profile</h1>
              <small class="subtitle">Form for user profile</small> </div>
          </div>
          <div class="vd_content-section clearfix">
            <div class="row">
              <div class="col-sm-12">
                <div class="panel widget light-widget">
                 <c:if test="${not empty successMessage}">
                        <div class="alert alert-success alert-dismissable">
                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cross"></i></button>
                           <i class="fa fa-check-circle append-icon"></i><strong>Well done!</strong> ${successMessage} 
                        </div>
                     </c:if>
                     <c:if test="${not empty failureMessage}">
                        <div class="alert alert-danger alert-dismissable alert-condensed">
                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cross"></i></button>
                           <i class="fa fa-exclamation-circle append-icon"></i>
                           <strong>Oh snap!</strong> ${failureMessage} 
                        </div>
                     </c:if>
                  <div class="panel-heading no-title"> </div>
                	<form action="${contextPath}/edit-profile"  method="post" modelAttribute="userInfo"> 
                	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />   
							<input type="hidden" name="userId" value="${userInfo.user.userId}" />  
                    <div  class="panel-body">
                      <h2 class="mgbt-xs-20"> Profile: <span class="font-semibold">I am ${userInfo.user.name} as a ${userInfo.userRoleTest.roleId.roleDisplayName} Role</span> </h2>
                      <br/>
                      <div class="row">
                       
                        <div class="col-sm-6">
                        
                         <c:if test="${userInfo.userRoleTest.roleId.roleName eq 'Agent'}">  
                                      
                         <h3 class="mgbt-xs-15">Company Setting</h3>
                          <div class="form-group">
                            <label class="col-sm-3 control-label">Organization Name</label>
                            <div class="col-sm-9 controls">
                              <div class="row mgbt-xs-0">
                                <div class="col-xs-9">
                                  <input type="text" class="form-control" id="organizationName" name="organizationName" maxlength="500" value="${userInfo.organizationName}"> 
                                </div>
                                <!-- col-xs-9 -->
                               
                              </div>
                              <!-- row --> 
                            </div>
                            <!-- col-sm-10 --> 
                          </div>
                          <!-- form-group -->
                          
                         
                          
                          <div class="form-group">
                            <label class="col-sm-3 control-label">Organization Details</label>
                            <div class="col-sm-9 controls">
                              <div class="row mgbt-xs-0">
                                <div class="col-xs-9">
                                       <textarea name="organizationDetails" rows="2" cols="20" id="organizationDetails" tabindex="2" class="form-control" maxlength="500">${userInfo.organizationDetails}</textarea>   
							    
                                  
                                </div>
                                <!-- col-xs-9 -->
                                
                              </div>
                              <!-- row --> 
                            </div>
                            <!-- col-sm-10 --> 
                          </div>
                          <!-- form-group -->
                          </c:if>
                           </div>
                           
                        <div class="col-sm-6">
                          <h3 class="mgbt-xs-15">Account Setting</h3>
                          
                          
                          <div class="form-group">
                            <label class="col-sm-3 control-label">UserName</label>
                            <div class="col-sm-9 controls">
                              <div class="row mgbt-xs-0">
                                <div class="col-xs-9">                                  
                                 
                                   <input type="text" class="form-control"  maxlength="20" value=" ${userInfo.user.userName}"  disabled> 
                                </div>
                                <!-- col-xs-9 -->
                                
                              </div>
                              <!-- row --> 
                            </div>
                            <!-- col-sm-10 --> 
                          </div>
                          <!-- form-group -->
                          
                          <div class="form-group">
                            <label class="col-sm-3 control-label">Password</label>
                            <div class="col-sm-9 controls">
                              <div class="row mgbt-xs-0">
                                <div class="col-xs-9">
                                 <input type="text" class="form-control"  maxlength="20" value="******"  disabled>   
							         </div>
                                <!-- col-xs-12 --> 
                              </div>
                              <!-- row --> 
                            </div>
                            <!-- col-sm-10 --> 
                          </div>
                          <!-- form-group -->
                          
                           <div class="form-group">
                            <label class="col-sm-3 control-label">Profession</label>
                            <div class="col-sm-9 controls">
                              <div class="row mgbt-xs-0">
                                <div class="col-xs-9">
                                 <input type="text" class="form-control" name="profession" value="${userInfo.profession}"  maxlength="20"   >   
							         </div>
                                <!-- col-xs-12 --> 
                              </div>
                              <!-- row --> 
                            </div>
                            <!-- col-sm-10 --> 
                          </div>
                          <!-- form-group -->
                          
                           </div>
                          
                        <div class="col-sm-6">
                          <hr />
                          <h3 class="mgbt-xs-15">Profile Setting</h3>
                        
                          
                         
                          
                          <div class="form-group">
                            <label class="col-sm-3 control-label">ID Proof Name</label>
                            <div class="col-sm-9 controls">
                              <div class="row mgbt-xs-0">
                                <div class="col-xs-9">
                                  <input type="text" class="form-control" id="idProofName" name="idProofName" maxlength="500" value="${userInfo.idProofName}" value=""> 
                                  
                                </div>
                               
                              </div>
                              <!-- row --> 
                            </div>
                            <!-- col-sm-10 --> 
                          </div>
                          <!-- form-group -->
                          
                           <div class="form-group">
                            <label class="col-sm-3 control-label">ID Proof Number</label>
                            <div class="col-sm-9 controls">
                              <div class="row mgbt-xs-0">
                                <div class="col-xs-9">
                                  <input type="text" class="form-control" id="idProofNumber" name="idProofNumber" maxlength="500" value="${userInfo.idProofNumber}" > 
                                  
                                </div>
                               
                              </div>
                              <!-- row --> 
                            </div>
                            <!-- col-sm-10 --> 
                          </div>
                          <!-- form-group -->
                          
                          <div class="form-group">
                            <label class="col-sm-3 control-label">ID Proof Document</label>
                            <div class="col-sm-9 controls">
                              <div class="row mgbt-xs-0">
                                <div class="col-xs-9">
                                  
                                  <input type="file" class="form-control"> 
                                </div>
                               
                              </div>
                              <!-- row --> 
                            </div>
                            <!-- col-sm-10 --> 
                          </div>
                          <!-- form-group -->
                          
                         
                          
                          <div class="form-group">
                            <label class="col-sm-3 control-label">Address</label>
                            <div class="col-sm-9 controls">
                              <div class="row mgbt-xs-0">
                                <div class="col-xs-9">
                                      <textarea name="address" rows="2" cols="20" id="address" tabindex="2" class="form-control" maxlength="500">${userInfo.user.address}</textarea>   
							      
                                </div>
                                <!-- col-xs-12 -->
                                
                              </div>
                              <!-- row --> 
                            </div>
                            <!-- col-sm-10 --> 
                          </div>
                          <!-- form-group -->
                           </div>
                          
                        <div class="col-sm-6">
                          <hr/>
                          <h3 class="mgbt-xs-15">Contact Setting</h3>
                          <div class="form-group">
                            <label class="col-sm-3 control-label">Mobile Phone</label>
                            <div class="col-sm-9 controls">
                              <div class="row mgbt-xs-0">
                                <div class="col-xs-9">
                                    <input type="text" class="form-control"  readonly value="${userInfo.user.mobileNo}" >  
							    
                                </div>
                               
                              </div>
                              <!-- row --> 
                            </div>
                            <!-- col-sm-10 --> 
                          </div>
                          <!-- form-group -->
                          
                          <div class="form-group">
                            <label class="col-sm-3 control-label">Website</label>
                            <div class="col-sm-9 controls">
                              <div class="row mgbt-xs-0">
                                <div class="col-xs-9">
                                  <input type="text"  placeholder="website" id="websiteLink" name="websiteLink"  value="${userInfo.websiteLink}">
                                </div>
                               
                              </div>
                              <!-- row --> 
                            </div>
                            <!-- col-sm-10 --> 
                          </div>
                          <!-- form-group -->
                          
                          <div class="form-group">
                            <label class="col-sm-3 control-label">Facebook</label>
                            <div class="col-sm-9 controls">
                              <div class="row mgbt-xs-0">
                                <div class="col-xs-9">
                                  <input type="text"  placeholder="facebook" id="fbLink" name="fbLink"  value="${userInfo.fbLink}">
                                </div>
                               
                              </div>
                              <!-- row --> 
                            </div>
                            <!-- col-sm-10 --> 
                          </div>
                          <!-- form-group -->
                          
                          <div class="form-group">
                            <label class="col-sm-3 control-label">Linkdean</label>
                            <div class="col-sm-9 controls">
                              <div class="row mgbt-xs-0">
                                <div class="col-xs-9">
                                  <input type="text" placeholder="twitter" id="linkdeanLink" name="linkdeanLink"  value="${userInfo.linkdeanLink}">
                                </div>
                               
                              </div>
                              <!-- row --> 
                            </div>
                            <!-- col-sm-10 --> 
                          </div>
                          <!-- form-group --> 
                          
                        </div>
                        <!-- col-sm-12 --> 
                      </div>
                      <!-- row --> 
                      
                   
                    <!-- panel-body -->
                    <div class="pd-20">
                      <button class="btn vd_btn vd_bg-green col-md-offset-6" type="submit"><span class="menu-icon"><i class="fa fa-fw fa-check"></i></span> Finish</button>
                   
                      <a href="${contextPath}/" class="btn vd_btn vd_bg-default"><span class="menu-icon"><i class="fa fa-fw fa-check"></i></span>Back</a>
                    </div>
                     </div>
                  </form>
                </div>
                <!-- Panel Widget --> 
              </div>
              <!-- col-sm-12--> 
            </div>
            <!-- row --> 
            
          </div>
          <!-- .vd_content-section --> 
          
        </div>
        <!-- .vd_content --> 
      </div>
      <!-- .vd_container --> 
    </div>
    <!-- .vd_content-wrapper --> 
    
    <!-- Middle Content End --> 
			<%-- <%@include file="/WEB-INF/views/layout/common.jsp" %> --%>		
<script>
	function updateProfile() {
		var email = $("#emailId").val(); 		
		if ($("#name").val() == "") {
			bootbox.alert("Please Enter Name");
			return false;
		}
		else if ($("#mobileNumber").val() == "") {
			bootbox.alert("Please Enter Mobile Number");
			return false;
		}	
		else if ($("#emailId").val() == "") {
			bootbox.alert("Please Enter Email Id");
			return false;
		}	
		else if (!ValidateEmail(email)) { 
            return false;
        } 
		var gender = $("#gender").val();
		if (gender.length == 0 || gender == "0") {
			bootbox.alert("Please Choose Gender"); 
			return false;
		}
        else if ($("#address").val() == "") {
			bootbox.alert("Please Enter Address");
			return false;
		}	
		else
		{
			bootbox.confirm("Do you want to update the User details?",
		    function(result) {
		       if (result == true) {
		          $("#editProfile").submit();
		       }
		    }); 
		}
	}
</script>
<script>
	function userNameValidation(){ 
		var userName = $("#userName").val();
		$.ajax({
			type : "GET",
			url : "validate-user-name.htm",
			data : {
				"userName" : userName,
			},
			success : function(response) {
				var val = JSON.parse(response); 
				if (val[0].duplicateUserName == "User Having This Name Is Already Exist") {
					bootbox.alert("User Having This Name Is Already Exist"); 				
					$("#userName").val("");   
				}  
			}, 
			error : function(error) {
				//alert("Failure");
			} 
		});
	} 
</script>  
<script>
	$(function() {	
		$('#name').keydown(function (e) {	  
			if (e.ctrlKey || e.altKey) {	    
				e.preventDefault();	      
			} else {	    
				var key = e.keyCode;	      
				if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {	      
					e.preventDefault();	
				} 
			}	
		});     
	});	
	function validateNumber() { 
		if((event.keyCode<48 || event.keyCode>57))
		{
			event.returnValue = false;
		} 
	}  
	function ValidateEmail(email) {
	    if (email != "") {
	        var reg = /^[A-Za-z0-9]([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
	        if (!reg.test(email)) {
	        	bootbox.alert('Enter a valid email id'); 
	            return false;
	        }
	        else
	            return true;
	    }
	    else
	        return true;
	}; 
</script>
	