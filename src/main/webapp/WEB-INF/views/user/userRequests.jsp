<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
   <!-- Middle Content Start -->
    
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <div class="vd_head-section clearfix">
            <div class="vd_panel-header">
              <ul class="breadcrumb">
                <li><a href="${contextPath}/">Home</a> </li>
                <li><a href="${contextPath}/">Online Requests</a> </li>
                <li class="active">Manage Online Requests</li>
              </ul>
              <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step=5  data-position="left">
    <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
      <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
      <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>
      
</div>
 
            </div>
          </div>
          <div class="vd_title-section clearfix">
            <div class="vd_panel-header">
              <h1>Manage Online Requests</h1>
              <small class="subtitle">Look <a href="http://www.datatables.net">below</a> for more information</small> </div>
          </div>
          <div class="vd_content-section clearfix">
            <div class="row">
            
              <div class="col-md-12">
          
                      <div class="panel widget">
<c:if test="${not empty successMessage}">
	<div class="alert alert-success alert-dismissable">
             	<button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cross"></i></button>
                    	<i class="fa fa-check-circle append-icon"></i><strong>Well done!</strong> ${successMessage} 
        </div>
</c:if>
<c:if test="${not empty failureMessage}">
<div class="alert alert-danger alert-dismissable alert-condensed">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cross"></i></button>
                        <i class="fa fa-exclamation-circle append-icon"></i>
                        <strong>Oh snap!</strong> ${failureMessage} 
        </div>
</c:if>                      
                      
  		
		
                  <div class="panel-heading vd_bg-grey">
                    <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span>List Of Online Requests</h3>
                  </div>
                  <div class="panel-body table-responsive  clearfix">
                  
											</br> </br> </br> 
											  
                    <table class="table table-striped  clearfix" id="data-tables">
                     <thead>
									<tr>
										 <th>#</th>	
										<th>User Type</th>												
										<th>Name</th>	
										<th>Organization Name</th>									
										<th>Mobile Number</th>
										<th>Email Id</th>										
										<th>created On</th>									
										<th>Status</th>	
										<th>Remarks</th>										
										<th>Action</th>																				
									</tr>
								</thead>
                    
                      <tbody id="tbd">
									<c:forEach items="${userRequestList}" var="data" varStatus="serialNumber">
										<tr>
											<td>${serialNumber.count}</td>
											<td>${data.userType}</td>											
											<td>${data.name}</td>
											<td>${data.organizationName}</td>
											<td>${data.mobile}</td>
											<td>${data.email}</td>
											<td>${data.createdOn}</td>
											<td>${data.status}</td>
											<td>${data.remark}</td>
											<td></td>
											
										</tr>
									</c:forEach>
								</tbody>
                    </table>
                  </div>
                </div>
                <!-- Panel Widget --> 
              </div>
              <!-- col-md-12 --> 
            </div>
            <!-- row --> 
            
          </div>
          <!-- .vd_content-section --> 
          
        </div>
        <!-- .vd_content --> 
      </div>
      <!-- .vd_container --> 
    </div>
    <!-- .vd_content-wrapper --> 


<form action="${contextPath}/enableUser.htm" method="GET"
	id="enableUser">
	<input type="hidden" name="userIdForEnableUser"
		id="userIdForEnableUser" />
		<input type="hidden" name="rtype" value="${rtype}" />
</form>
<form action="${contextPath}/disableUser.htm" method="GET"
	id="disableUser">
	<input type="hidden" name="userIdForDisableUser"
		id="userIdForDisableUser" />
		<input type="hidden" name="rtype" value="${rtype}" />
</form>
<form action="${contextPath}/edit-user" method="POST"
	id="editForm">
	<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />
	<input type="hidden" name="id_edit"
		id="id_edit" />
		<input type="hidden" name="rtype" value="${rtype}" />
		
		
</form>

<%-- <%@include file="/WEB-INF/views/layout/common.jsp" %> --%>
<script> 
	function enableUser(userId){
		$("#userIdForEnableUser").val(userId);
		bootbox.confirm("Do you want to enable this User?",
		function(result) {
			if (result == true) {
			   $("#enableUser").submit();
			} 
		});
	}	
	function disableUser(userId){
		$("#userIdForDisableUser").val(userId);
		bootbox.confirm("Do you want to disable this User?",
	    function(result) {
	        if (result == true) {
	            $("#disableUser").submit();
	        } 
	    });
	} 
		function edit(userId){
		$("#id_edit").val(userId);
		bootbox.confirm("Do you want to Edit this User?",
	    function(result) {
	        if (result == true) {
	            $("#editForm").submit();
	        } 
	    });
	} 

</script>
	