<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" /> 



<!-- End of Data -->
  

<body id="pages" class="full-layout no-nav-left no-nav-right  nav-top-fixed background-login     responsive remove-navbar login-layout   clearfix" data-active="pages "  data-smooth-scrolling="1">     
<div class="vd_body">
<!-- Header Start -->



<!-- Header Ends --> 
<div class="content"><div class="container">


<!-- Middle Content Start -->

<div class="vd_content-wrapper">
    <div class="vd_container">
    
        <div class="vd_content clearfix">               
            <div class="vd_content-section clearfix">  

            <div class="vd_login-page">     
            	<div class="heading clearfix">
                	<div class="logo"><h2 class="mgbt-xs-5"><img class="logo_img" src="${contextPath}/static/sFreelance/img/logo.png" alt="logo"></h2></div>
                                        
                </div>
               
                <div class="panel widget">
                    <div class="panel-body">
                    
                          <div class="login-icon">
                                <i class="fa fa-lock"></i>
                          </div>      
                                      
                          <form class="form-horizontal"  role="form" id="forgotPassword" action="${contextPath}/forgot-pass"  method="post">
                          <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" /> 
                  <div class="alert alert-danger ${not empty failureMessage?'':'vd_hidden'}">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-cross"></i></button>
                    <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Oh snap!</strong> ${failureMessage}</div>
                  <div class="alert alert-success ${not empty successMessage?'':'vd_hidden'}">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-cross"></i></button>
                    <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span>${successMessage} </div>                            
                             <div class="form-group mgbt-xs-20">
                                 <div class="col-md-12">
                                 	<p class="text-center"><strong>To reset your password, enter the email address you use to sign in to Vendroid.</strong> </p>
                                 	<p class="text-center"><strong>${noDataFound}${successOtpMsg}${failureOtpMsg}</strong> </p>
                                 
                                 	
                                    <div class="vd_input-wrapper" id="email-input-wrapper">
                                        <span class="menu-icon">
                                            <i class="fa fa-envelope"></i>
                                        </span>
                                         <input type="text" class="form-control" placeHolder="username or mobile no"  maxlength="20" name="unameOrmob" required> 
                                    </div>   
                                
                                  </div>   
                                                           
                            </div>   
                            
                              <div class="form-group" id="submit-password-wrapper">
                              <div class="col-md-12 text-center mgbt-xs-5">
                                  <button class="btn vd_bg-green vd_white width-100" type="submit" id="submit-password" name="submit-password">Send me my password</button>   
                              </div>

                            </div>
                          </form>

                          
                    </div>
                </div> <!-- Panel Widget --> 
                <div class="register-panel text-center font-semibold">	
                	<a href="${contextPath}/user-login">BACK TO LOGIN PAGE</a>
                   
                </div>
                </div> <!-- vd_login-page -->

                
                                                           
            </div>   
          <!-- .vd_content-section --> 
          
        </div>
        <!-- .vd_content --> 
      </div>
      <!-- .vd_container --> 
    </div>
    <!-- .vd_content-wrapper --> 
    
    <!-- Middle Content End --> 
    
  </div>
  <!-- .container --> 
</div>
<!-- .content -->
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  

</div>


</body>
</html>
<%-- <%@include file="/WEB-INF/views/layout/common.jsp" %> --%>