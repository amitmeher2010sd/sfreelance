<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<% String session_roleCode = (String)request.getSession().getAttribute("roleCode"); %>
<c:set var="ses_roleCode" value="<%=session_roleCode%>" />
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<!-- Middle Content Start -->
<div class="vd_content-wrapper">
	<div class="vd_container">
		<div class="vd_content clearfix">
			<div class="vd_head-section clearfix">
				<div class="vd_panel-header">
					<ul class="breadcrumb">
						<li><a href="${contextPath}/">Home</a></li>
						<li><a href="${contextPath}/">AmyOhm</a></li>
						<li class="active">Website User</li>
					</ul>
				</div>
			</div>
			<div class="vd_title-section clearfix">
				<div class="vd_panel-header">
					<h1>${not empty userInfo.getId() ?'Modify Website User':'New Website User'}</h1>
					<small class="subtitle">Fill the below form ('*' for
						Mandetory Field)</small>
				</div>
			</div>
			<!-- vd_title-section -->
			<div class="vd_content-section clearfix">
				<div class="row" id="advanced-input">
					<div class="col-md-12">
						<div class="panel widget">
							<div class="panel-heading vd_bg-grey">
								<h3 class="panel-title">
									<span class="menu-icon"> <i class="fa fa-bar-chart-o"></i>
									</span> Website User Details
								</h3>
							</div>
							<div class="panel-body">
								<form class="form-horizontal"
									action="${contextPath}/website-user" role="form"
									id="register-form" method="post" modelAttribute="userInfo">
									<input type="hidden" name="${_csrf.parameterName}"
										value="${_csrf.token}" /> <input type="hidden" name="Id"
										value="${userInfo.getId()}" />
									<div class="alert alert-danger vd_hidden" id="error">
										<button type="button" class="close" data-dismiss="alert"
											aria-hidden="true">
											<i class="icon-cross"></i>
										</button>
										<span class="vd_alert-icon"><i
											class="fa fa-exclamation-circle vd_red"></i></span><strong>Oh
											snap!</strong> <span id="er">Change a few things up and try
											submitting again.</span>
									</div>
									<div class="alert alert-success vd_hidden">
										<button type="button" class="close" data-dismiss="alert"
											aria-hidden="true">
											<i class="icon-cross"></i>
										</button>
										<span class="vd_alert-icon"><i
											class="fa fa-check-circle vd_green"></i></span><strong>Well
											done!</strong>.
									</div>


									<fieldset>
										<legend>Basic Information:</legend>
											<div class="form-group">
										<label class="col-sm-2 control-label">Status <span
												class="vd_red">*</span>
											</label>
											<div class="col-sm-3 controls">
												<select class="form-control form-control-sm required"
													id="status" name="status">
													<option value="">Select</option>
													<option ${userInfo.user.status eq 'ACTIVE' ?'selected':''}
														value="ACTIVE">ACTIVE</option>
													<option ${userInfo.user.status eq 'INACTIVE' ?'selected':''}
														value="INACTIVE">INACTIVE</option>
													
												</select>
											</div>
										
											
										</div>
										<div class="form-group">
									
											<label class="col-sm-2 control-label">Your Name <span
												class="vd_red">*</span></label>
											<div class="col-sm-3 controls">
												<input type="text" class="form-control required" id="name"
													name="name" value="${userInfo.user.name}"
													
													class="width-70" maxlength="500">
											</div>
											<label class="col-sm-2 control-label">Email <span
												class="vd_red">*</span>
											</label>
											<div class="col-sm-3 controls">
												<input type="text" class="form-control required" id="email"
													name="email" value="${userInfo.user.email}" maxlength="100">
											</div>
											
										</div>



										<div class="form-group">

											<label class="col-sm-2 control-label">Date of birth
												(MM-DD-YYYY) <span class="vd_red">*</span>
											</label>

											<div class="col-sm-3 controls">
												<input type="text" class="form-control required   ignoreValidation date" id="dateofbirth"
													name="dateofbirth" value="${userInfo.dateofbirth}" maxlength="15">
											</div>

											<label class="col-sm-2 control-label">Phone (Primary)
												<span class="vd_red">*</span>
											</label>
											<div class="col-sm-3 controls">
												<input type="text" class="form-control required" id="phone"
													name="phone" value="${userInfo.user.phone}" maxlength="100">
											</div>
										</div>



										<div class="form-group">

											<label class="col-sm-2 control-label">Phone
												(Secondary) <span class="vd_red">*</span>
											</label>

											<div class="col-sm-3 controls">
												<input type="text" class="form-control required" id="secondaryphone"
													name="secondaryphone" value="${userInfo.secondaryphone}" maxlength="15">
											</div>
											<label class="col-sm-2 control-label">Gender <span
												class="vd_red">*</span>
											</label>
											<div class="col-sm-3 controls">
												<select class="form-control form-control-sm required"
													id="gender" name="gender" value="${userInfo.gender}">
													<option value="">Select</option>
													<option ${userInfo.gender eq 'MALE' ?'selected':''}
														value="MALE">Male</option>
													<option ${userInfo.gender eq 'FEMALE' ?'selected':''}
														value="FEMALE">Female</option>
													<option ${userInfo.gender eq 'OTHER' ?'selected':''}
														value="OTHER">Other</option>
												</select>
											</div>

										</div>

										<div class="form-group">
											<label class="col-sm-2 control-label">
												Description(Bio) <span class="vd_red">*</span>
											</label>
											<div class="col-sm-8 controls">
												<textarea rows="4" cols="30" id="description" name="description"
													tabindex="2" class="form-control required" maxlength="500">${userInfo.description}</textarea>
											</div>
										</div>
									</fieldset>

									<fieldset>
										<legend>Professonal Details:</legend>
										<div class="form-group">
											<label class="col-sm-2 control-label">Company Name <span
												class="vd_red"></span>
											</label>
											<div class="col-sm-3 controls">
												<input type="text" class="form-control" id="companyName"
													name="companyName" value="${userInfo.companyName}"
													maxlength="100">
											</div>
											<label class="col-sm-2 control-label">Experience <span
												class="vd_red"></span>
											</label>
											<div class="col-sm-3 controls">
												<select maxlength="10" class="form-control" id="experience" name="experience">
													<c:forEach var="data" begin="0" end="15" varStatus="outer">
														<option value="${data}"
															${userInfo.experience eq data?'selected':''}>${data}
															Years</option>
													</c:forEach>
												</select>
											</div>
										</div>


										<div class="form-group">
											<label class="col-sm-2 control-label"> Primary Skills
												<span class="vd_red">*</span>
											</label>
											<div class="col-sm-8 controls">
												<select class="js-example-basic-multiple" id="primarySkillArray" name="primarySkillArray"
													multiple="multiple">
													<c:forEach items="${skillList}" var="data">
														<option value="${data.skillName}"
															${fn:contains(userInfo.primaryskills, data.skillName)?'selected':''}>${data.skillName}</option>
													</c:forEach>
												</select>
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-2 control-label"> Secondary
												Skills <span class="vd_red">*</span>
											</label>
											<div class="col-sm-8 controls">
												<select class="js-example-basic-multiple"
													id="secondaryskillsArray" name="secondaryskillsArray" multiple="multiple">
													<c:forEach items="${skillList}" var="data">
														<option value="${data.skillName}"
															${fn:contains(userInfo.secondaryskills, data.skillName)?'selected':''}>${data.skillName}</option>
													</c:forEach>
												</select>
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-2 control-label">Other skills <span
												class="vd_red"></span>
											</label>
											<div class="col-sm-3 controls">
												<input type="text" class="form-control" id="otherskills"
													name="otherskills" value="${userInfo.otherskills}" maxlength="100">
											</div>
											
										</div>
									</fieldset>

									<fieldset>
										<legend>Work Support Details:</legend>

										<div class="form-group" style="margin-left: 6%;">

											<div class="form-group mb-3 col-md-6">
												<label class="form-label">Support Mode:</label>
												<div class="form-check">
													<input class="form-check-input supportmode1"
														type="checkbox" maxlength="30" name="supportmode"
														value="Online"
														${fn:contains(userInfo.supportmode,'Online')?'checked':''}>
													<label class="form-check-label" for="chk-policy">Online
														(i.e call/chat/any meeting app like zoom,team etc)</label>
												</div>
												<div class="form-check">
													<input class="form-check-input supportmode1"
														type="checkbox" maxlength="30" name="supportmode"
														value="Offline"
														${fn:contains(userInfo.supportmode,'Offline')?'checked':''}>
													<label class="form-check-label" for="chk-policy">Offline
														(i.e candidate will visit to your location)</label>
												</div>
											</div>
											<div class="form-group mb-3 col-md-6">
												<label class="form-label">I can support :</label>
												<div class="form-check">
													<input class="form-check-input supporttype1"
														type="checkbox" maxlength="250" name="supporttype"
														value="Job Support"
														${fn:contains(userInfo.supporttype,'Job Support')?'checked':''}>
													<label class="form-check-label" for="chk-policy">Job
														Support</label>
												</div>
												<div class="form-check">
													<input class="form-check-input supporttype1"
														type="checkbox" maxlength="250" name="supporttype"
														value="Training Support"
														${fn:contains(userInfo.supporttype,'Training Support')?'checked':''}>
													<label class="form-check-label" for="chk-policy">Training
														Support</label>
												</div>
												<div class="form-check">
													<input class="form-check-input supporttype1"
														type="checkbox" maxlength="250" name="supporttype"
														value="Interview Support"
														${fn:contains(userInfo.supporttype,'Interview Support')?'checked':''}>
													<label class="form-check-label" for="chk-policy">Interview
														Support</label>
												</div>
											</div>
											<hr>
											<div class="form-group mb-3 col-md-6">
												<label class="form-label">Support Hours :</label>
												<div class="form-check">
													<input class="form-check-input supporthour1"
														type="checkbox" maxlength="250" name="supporthour"
														value="2 Hours"
														${fn:contains(userInfo.supporthour,'2 Hours')?'checked':''}>
													<label class="form-check-label" for="chk-policy">2
														Hours/days </label>
												</div>
												<div class="form-check">
													<input class="form-check-input supporthour1"
														type="checkbox" maxlength="250" name="supporthour"
														value="3 Hours"
														${fn:contains(userInfo.supporthour,'3 Hours')?'checked':''}>
													<label class="form-check-label" for="chk-policy">3
														Hours/days</label>
												</div>
												<div class="form-check">
													<input class="form-check-input supporthour1"
														type="checkbox" maxlength="250" name="supporthour"
														value="4 Hours"
														${fn:contains(userInfo.supporthour,'4 Hours')?'checked':''}>
													<label class="form-check-label" for="chk-policy">4
														Hours/days</label>
												</div>
												<div class="form-check">
													<input class="form-check-input supporthour1"
														type="checkbox" maxlength="250" name="supporthour"
														value="5 Hours"
														${fn:contains(userInfo.supporthour,'5 Hours')?'checked':''}>
													<label class="form-check-label" for="chk-policy">5
														Hours/days</label>
												</div>
												<hr>
												<div class="form-check">
													<input class="form-check-input supporthour1"
														type="checkbox" maxlength="250" name="supporthour"
														value="Per Hour Basis"
														${fn:contains(userInfo.supporthour,'Per Hour Basis')?'checked':''}>
													<label class="form-check-label" for="chk-policy">Per
														Hour Basis</label>
												</div>
												<div class="form-check">
													<input class="form-check-input supporthour1"
														type="checkbox" maxlength="250" name="supporthour"
														value="Task Basis"
														${fn:contains(userInfo.supporthour,'Task Basis')?'checked':''}>
													<label class="form-check-label" for="chk-policy">Task
														Basis</label>
												</div>
											</div>


											<div class="form-group mb-3 col-md-6">
												<label class="form-label">Available Time (Job
													Support/Training) :</label>

												<div class="form-check">
													<input class="form-check-input jobsupporttimeslot1"
														maxlength="250" name="jobsupporttimeslot" type="checkbox"
														value="7.00AM - 9.00AM (IST)"
														${fn:contains(userInfo.jobsupporttimeslot,'7.00AM - 9.00AM (IST)')?'checked':''}>
													<label class="form-check-label bllpink" for="chk-policy">Morning
														Time : 7.00AM - 9.00AM (IST)</label>
												</div>
												<div class="form-check">
													<input class="form-check-input jobsupporttimeslot1"
														maxlength="250" name="jobsupporttimeslot" type="checkbox"
														value="7.30AM - 9.30AM (IST)"
														${fn:contains(userInfo.jobsupporttimeslot,'7.30AM - 9.30AM (IST)')?'checked':''}>
													<label class="form-check-label bllpink" for="chk-policy">Morning
														Time : 7.30AM - 9.30AM (IST)</label>
												</div>
												<div class="form-check">
													<input class="form-check-input jobsupporttimeslot1"
														maxlength="250" name="jobsupporttimeslot" type="checkbox"
														value="8.00AM - 10.00AM (IST)"
														${fn:contains(userInfo.jobsupporttimeslot,'8.00AM - 10.00AM (IST)')?'checked':''}>
													<label class="form-check-label bllpink" for="chk-policy">Morning
														Time : 8.00AM - 10.00AM (IST)</label>
												</div>
												<div class="form-check">
													<input class="form-check-input jobsupporttimeslot1"
														maxlength="250" name="jobsupporttimeslot" type="checkbox"
														value="8.30AM - 10.30AM (IST)"
														${fn:contains(userInfo.jobsupporttimeslot,'8.30AM - 10.30AM (IST)')?'checked':''}>
													<label class="form-check-label bllpink" for="chk-policy">Morning
														Time : 8.30AM - 10.30AM (IST)</label>
												</div>
												<div class="form-check">
													<input class="form-check-input jobsupporttimeslot1"
														maxlength="250" name="jobsupporttimeslot" type="checkbox"
														value="9.00AM - 11.00AM (IST)"
														${fn:contains(userInfo.jobsupporttimeslot,'9.00AM - 11.00AM (IST)')?'checked':''}>
													<label class="form-check-label bllpink" for="chk-policy">Morning
														Time : 9.00AM - 11.00AM (IST)</label>
												</div>
												<div class="form-check">
													<input class="form-check-input jobsupporttimeslot1"
														maxlength="250" name="jobsupporttimeslot" type="checkbox"
														value="2.30PM - 4.30PM (IST)"
														${fn:contains(userInfo.jobsupporttimeslot,'2.30PM - 4.30PM (IST)')?'checked':''}>
													<label class="form-check-label bllblue" for="chk-policy">AfterNoon
														Time : 2.30PM - 4.30PM (IST)</label>
												</div>

												<div class="form-check">
													<input class="form-check-input jobsupporttimeslot1"
														maxlength="250" name="jobsupporttimeslot" type="checkbox"
														value="6.30PM - 8.30PM (IST)"
														${fn:contains(userInfo.jobsupporttimeslot,'6.30PM - 8.30PM (IST)')?'checked':''}>
													<label class="form-check-label bllgreen" for="chk-policy">Evening
														Time : 6.30PM - 8.30PM (IST)</label>
												</div>
												<div class="form-check">
													<input class="form-check-input jobsupporttimeslot1"
														maxlength="250" name="jobsupporttimeslot" type="checkbox"
														value="7.00PM - 9.00PM (IST)"
														${fn:contains(userInfo.jobsupporttimeslot,'7.00PM - 9.00PM (IST)')?'checked':''}>
													<label class="form-check-label bllgreen" for="chk-policy">Evening
														Time : 7.00PM - 9.00PM (IST)</label>
												</div>
												<div class="form-check">
													<input class="form-check-input jobsupporttimeslot1"
														maxlength="250" name="jobsupporttimeslot" type="checkbox"
														value="8PM - 10PM (IST)"
														${fn:contains(userInfo.jobsupporttimeslot,'8PM - 10PM (IST)')?'checked':''}>
													<label class="form-check-label bllgreen" for="chk-policy">Evening
														Time : 8PM - 10PM (IST)</label>
												</div>
												<div class="form-check">
													<input class="form-check-input jobsupporttimeslot1"
														maxlength="250" name="jobsupporttimeslot" type="checkbox"
														value="9PM - 11PM (IST)"
														${fn:contains(userInfo.jobsupporttimeslot,'9PM - 11PM (IST)')?'checked':''}>
													<label class="form-check-label bllgreen" for="chk-policy">Evening
														Time : 9PM - 11PM (IST)</label>
												</div>
												<div class="form-check">
													<input class="form-check-input jobsupporttimeslot1"
														maxlength="250" name="jobsupporttimeslot" type="checkbox"
														value="9.30PM - 11.30PM (IST)"
														${fn:contains(userInfo.jobsupporttimeslot,'9.30PM - 11.30PM (IST)')?'checked':''}>
													<label class="form-check-label bllgreen" for="chk-policy">Evening
														Time : 9.30PM - 11.30PM (IST)</label>
												</div>
												<div class="form-check">
													<input class="form-check-input jobsupporttimeslot1"
														maxlength="250" name="jobsupporttimeslot" type="checkbox"
														value="10.00PM - 12.00PM (IST)"
														${fn:contains(userInfo.jobsupporttimeslot,'10.00PM - 12.00PM (IST)')?'checked':''}>
													<label class="form-check-label bllgreen" for="chk-policy">Evening
														Time : 10.00PM - 12.00PM (IST)</label>
												</div>
												<div class="form-check">
													<input class="form-check-input jobsupporttimeslot1"
														maxlength="250" name="jobsupporttimeslot" type="checkbox"
														value="10.30PM - 12.30PM (IST)"
														${fn:contains(userInfo.jobsupporttimeslot,'10.30PM - 12.30PM (IST)')?'checked':''}>
													<label class="form-check-label bllgreen" for="chk-policy">Evening
														Time : 10.30PM - 12.30PM (IST)</label>
												</div>
											</div>


										</div>

										<div class="form-group">
											<label class="col-sm-2 control-label"> Other Time
												Slot If Any <span class="vd_red">*</span>
											</label>
											<div class="col-sm-3 controls">
												<input type="text" class="form-control" id="othertimeslot"
													name="othertimeslot" value="${userInfo.othertimeslot}"
													maxlength="100">
											</div>
										</div>



									</fieldset>

									<fieldset>
										<legend>Bank Details:</legend>

										<div class="form-group">

											<label class="col-sm-2 control-label">Bank Name <span
												class="vd_red">*</span>
											</label>

											<div class="col-sm-3 controls">
												<input type="text" class="form-control required"
													id="accountsbankname" name="accountsbankname"
													value="${userInfo.accountsbankname}" maxlength="15">
											</div>

											<label class="col-sm-2 control-label">Account Holder
												Name <span class="vd_red">*</span>
											</label>
											<div class="col-sm-3 controls">
												<input type="text" class="form-control required" id="accountsholdername"
													name="accountsholdername" value="${userInfo.accountsholdername}"
													maxlength="100">
											</div>
										</div>

										<div class="form-group">

											<label class="col-sm-2 control-label">IFSC Code <span
												class="vd_red">*</span>
											</label>

											<div class="col-sm-3 controls">
												<input type="text" class="form-control required"
													id="accountsifsc" name="accountsifsc"
													value="${userInfo.accountsifsc}" maxlength="15">
											</div>

											<label class="col-sm-2 control-label">Account No<span
												class="vd_red">*</span>
											</label>
											<div class="col-sm-3 controls">
												<input type="text" class="form-control required"
													id="accountsaccno" name="accountsaccno"
													value="${userInfo.accountsaccno}" maxlength="100">
											</div>
										</div>


										<div class="form-group">

											<label class="col-sm-2 control-label">Any UPI Mode
												Type <span class="vd_red">*</span>
											</label>

											<div class="col-sm-3 controls">
												<select maxlength="30" class="form-control" name="bankmodetype"
													id="bankmodetype">
													<option value="">---select---</option>
													<option value="Google Pay"
														${userInfo.bankmodetype eq 'Google Pay'?'selected':''}>Google
														Pay</option>
													<option value="Phone Pay"
														${userInfo.bankmodetype eq 'Phone Pay'?'selected':''}>Phone
														Pay</option>
												</select>
											</div>

											<label class="col-sm-2 control-label">Number<span
												class="vd_red">*</span>
											</label>
											<div class="col-sm-3 controls">
												<input type="text" class="form-control required"
													id="bankmodenumber" name="bankmodenumber"
													value="${userInfo.bankmodenumber}" maxlength="100">
											</div>
										</div>





									</fieldset>


									<fieldset>
										<legend>Social Links:</legend>

										<div class="form-group">
											<label class="col-sm-2 control-label">Facebook <span
												class="vd_red"></span>
											</label>
											<div class="col-sm-8 controls">
												<input type="text" class="form-control" id="facebook"
													name="facebook" value="${userInfo.facebook}" maxlength="100">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">Twitter <span
												class="vd_red"></span>
											</label>
											<div class="col-sm-8 controls">
												<input type="text" class="form-control" id="twitter"
													name="twitter" value="${userInfo.twitter}" maxlength="100">
											</div>
										</div>
										
										<div class="form-group">
											<label class="col-sm-2 control-label">Website <span
												class="vd_red"></span>
											</label>
											<div class="col-sm-8 controls">
												<input type="text" class="form-control" id="website"
													name="website" value="${userInfo.website}" maxlength="100">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">Linkdean <span
												class="vd_red"></span>
											</label>
											<div class="col-sm-8 controls">
												<input type="text" class="form-control" id="linkedin"
													name="linkedin" value="${userInfo.linkedin}"
													maxlength="100">
											</div>
										</div>
									</fieldset>


									<fieldset>
										<legend>Address:</legend>
										<div class="form-group">
											<label class="col-sm-2 control-label"> Enter Your
												Location <span class="vd_red">*</span>
											</label>
											<div class="col-sm-8 controls">
												<textarea rows="4" cols="30" id="address" name="address"
													tabindex="2" class="form-control required" maxlength="500">${userInfo.address}</textarea>
											</div>
										</div>

									</fieldset>





									<div class="form-group form-actions">
										<div class="col-sm-4"></div>
										<div class="col-sm-7">
											<button class="btn vd_btn vd_bg-green vd_white" type="submit"
												id="submit-register" name="submit-register">
												<i class="icon-ok"></i> ${not empty userInfo.getId() ?'Update':'Save'}
											</button>
											<a class="btn vd_btn vd_bg-grey"
												href="${contextPath}/website-users">Back</a>
										</div>
									</div>
								</form>
							</div>
						</div>
						<!-- Panel Widget -->
					</div>
					<!-- col-md-12 -->
				</div>
				<!-- row -->
			</div>
			<!-- .vd_content-section -->
		</div>
		<!-- .vd_content -->
	</div>
	<!-- .vd_container -->
</div>
<!-- .vd_content-wrapper -->
<!-- Middle Content End -->
<!-- Placed at the end of the document so the pages load faster -->
<%-- <script type="text/javascript" src="${contextPath}/static/sFreelance/js/jquery.js"></script>  --%>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script> -->
<script type="text/javascript"
	src="${contextPath}/static/sFreelance/js/select2.min.js" defer></script>
<link href="${contextPath}/static/sFreelance/css/select2.min.css"
	rel="stylesheet" type="text/css">
<script>
$(document).ready(function() {
	$('.js-example-basic-multiple').select2();
});
     
</script>
