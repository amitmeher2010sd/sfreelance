<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<c:if test="${not empty successMessage}">
   <script>
      bootbox.alert("${successMessage}");  
   </script>
</c:if>
<c:if test="${not empty failureMessage}">
   <script>
      bootbox.alert("${failureMessage}"); 
   </script>
</c:if>
<!-- Middle Content Start -->
<div class="vd_content-wrapper">
   <div class="vd_container">
      <div class="vd_content clearfix">
         <div class="vd_head-section clearfix">
            <div class="vd_panel-header">
               <ul class="breadcrumb">
                  <li><a href="index.html">Home</a> </li>
                  <li><a href="./">Manage JK Freelance</a> </li>
                  <li class="active">JKfreelancers</li>
               </ul>
               <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step=5  data-position="left">
                  <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
                  <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
                  <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>
               </div>
            </div>
         </div>
         <div class="vd_title-section clearfix">
            <div class="vd_panel-header">
               <h1>JK freelancers</h1>
               <small class="subtitle">Look <a href="./">below</a> for more information</small> 
            </div>
         </div>
         <div class="vd_content-section clearfix">
            <div class="row">
               <div class="col-md-12">
                  <div class="panel widget">
                     <c:if test="${not empty successMessage}">
                        <div class="alert alert-success alert-dismissable">
                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cross"></i></button>
                           <i class="fa fa-check-circle append-icon"></i><strong>Well done!</strong> ${successMessage} 
                        </div>
                     </c:if>
                     <c:if test="${not empty failureMessage}">
                        <div class="alert alert-danger alert-dismissable alert-condensed">
                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cross"></i></button>
                           <i class="fa fa-exclamation-circle append-icon"></i>
                           <strong>Oh snap!</strong> ${failureMessage} 
                        </div>
                     </c:if>
                     <div class="panel-heading vd_bg-grey">
                        <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span>List Of Freelancer</h3>
                     </div>
                     <div class="panel-body table-responsive">
                      <div class="form-group">
                           <label class="col-sm-2 control-label">By Project Status<span class="vd_red"></span> </label>
                           <div class="col-sm-3 controls">
                             <select class="form-control form-control-sm required"   onChange="loadDataByStatus(this.value);">
                                    <option value="ALL">ALL</option>
                                    <c:forEach items="${demoStatusList}" var="data">
                                       <option  value="${data}" ${demostatus eq data?'selected':''}>${data}</option> 
                                    </c:forEach>
                                 </select>                            
                           </div>
                        </div>
                    
                        <table class="table table-striped" id="data-tables">
                           <thead>
                              <tr>
                                  <th>#</th>
                                 <th>Name</th>  
                                  <th>Gender</th>                               
                                
                                    <th>Contact Info</th>
                                     <th>Designation</th>
                                      <th>Experience</th>
                                     <th>Primary Skills</th>
                                          <th>Secondary Skills</th>
                                           <th>Address</th>
                                 <th>Action</th>
                                 
                              </tr>
                           </thead>
                           <tbody id="tbd">
                              <c:forEach items="${userList}" var="data" varStatus="serialNumber">
                                 <tr>
                                    <td>${serialNumber.count}</td>
                                    <td> ${data.user.name}-${data.user.username}<br><span class="label label-${data.user.status eq 'ACTIVE'?'success':'danger'}">${data.user.status}</span>
                                    <br><span class="label label-${data.profileverification eq 'Verified'?'success':'danger'}">${data.profileverification}</span>
                                    
                                    <br>
                                   <p>Resend Activation Link: <a href="${JKFREELANCEURL}/public/resend-account-verification-link?toEmail=${data.user.email}&name=${data.user.name}&activationcode=${data.user.activationcode}&username=${data.user.username}">Click Me</a></p>
                                   
                                      <p>Activate & Send Accounts Details: <a href="${JKFREELANCEURL}/public/account-activation/${data.user.activationcode}">Click Me</a></p>
                                 
                                        <p>Change to ${data.profileverification eq 'Verified'?'Pending':'Verified'}: <a href="${JKFREELANCEURL}/public/verify-account?userId=${data.user.id}&profileverification=${data.profileverification eq 'Verified'?'Pending':'Verified'}">Click Me</a></p>
                                   </td> 
                                    <td> ${data.gender}</td> 
                                    <td>(P:-) ${data.user.phone}<br>(S:-)${data.secondaryphone}<br>${data.user.email}<br>${data.website}<br>${data.linkedin}</td> 
                                    
                                    <td> ${data.designation}</td> 
                                    <td> ${data.experience}</td>
                                    <td> ${data.primaryskills}</td> 
                                     <td> ${data.secondaryskills}</td> 
                                      
                                       
                                        <td> ${data.address}</td>  
                                         <td>
                                          <button class="btn btn-danger btn-sm"
                                                   onclick="edit('${data.id}')">
                                                <i class="fa fa-edit" aria-hidden="true"></i>
                                                </button>
                                                 <button class="btn menu-icon vd_bd-green vd_green btn-sm"
                                             onclick="view('${data.user.username}')">
                                         <i class="fa fa-eye"></i>
                                          </button>
                                             
                                         </td>                                       
                                                                      
                                 </tr>
                              </c:forEach>
                           </tbody>
                        </table>
                     </div>
                  </div>
                  <!-- Panel Widget --> 
               </div>
               <!-- col-md-12 --> 
            </div>
            <!-- row --> 
         </div>
         <!-- .vd_content-section --> 
      </div>
      <!-- .vd_content --> 
   </div>
   <!-- .vd_container --> 
</div>
<!-- .vd_content-wrapper --> 
<form action="${contextPath}/edit-jkfreelancer" method="POST" id="edit">
   <input type="hidden" name="${_csrf.parameterName}"
      value="${_csrf.token}" />
   <input type="hidden" name="userId" id="userId" />
</form>
<form action="${contextPath}/user-view" method="GET"
   id="viewForm">
   <input type="hidden" name="username"
      id="username" />
</form>
<script> 
   function edit(id){
   	$("#userId").val(id);
   	bootbox.confirm("Do you want to edit ?",
   	function(result) {
   		if (result == true) {
   		   $("#edit").submit();
   		} 
   	});
   }	
   function view(uname){
		$("#username").val(uname);
		 $("#viewForm").submit();
	   } 
</script>