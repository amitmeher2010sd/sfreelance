<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<!-- Middle Content Start -->    
<div class="vd_content-wrapper">
   <div class="vd_container">
      <div class="vd_content clearfix">
         <div class="vd_head-section clearfix">
            <div class="vd_panel-header">
               <ul class="breadcrumb">
                  <li><a href="${contextPath}/">Home</a> </li>
                  <li><a href="${contextPath}/">Manage JK Freelance</a> </li>
                  <li class="active">Modify JKfreelancer</li>
               </ul>
            </div>
         </div>
         <div class="vd_title-section clearfix">
            <div class="vd_panel-header">
               <h1> Modify JKfreelancer</h1>
               <small class="subtitle">Fill the below form ('*' for Mandetory Field)</small>              
            </div>
         </div>
         <!-- vd_title-section -->          
         <div class="vd_content-section clearfix">
            <div class="row" id="advanced-input">
               <div class="col-md-12">
                  <div class="panel widget">
                     <div class="panel-heading vd_bg-grey">
                        <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-bar-chart-o"></i> </span>  JKfreelancer Details </h3> 
                     </div>
                     <div class="panel-body">
                        <form class="form-horizontal"  action="${contextPath}/jkfreelancer" role="form" id="register-form" method="post" modelAttribute="applyclient" onsubmit="locadingProgress();">
                           <input type="hidden" name="${_csrf.parameterName}"	value="${_csrf.token}" />
                           <input type="hidden" name="Id" value="${userInfo.getId()}" />
                           <div class="alert alert-danger vd_hidden">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-cross"></i></button>
                              <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Oh snap!</strong> Change a few things up and try submitting again. 
                           </div>
                           <div class="alert alert-success vd_hidden">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-cross"></i></button>
                              <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Well done!</strong>.
                           </div>
                         
                           <div class="form-group">
                              <label class="col-sm-2 control-label">Name </label>
                              <div class="col-sm-3 controls">
                                 <input type="text" class="form-control ignoreValidation" id="email" 
                                    name="email" value="${userInfo.user.name}" maxlength="100">
                              </div>
                              <label class="col-sm-2 control-label">Status <span class="vd_red">*</span> </label>
                              <div class="col-sm-3 controls">
                                 <select class="form-control form-control-sm required" id="status" name="status" value="${userInfo.user.status}">
                                 
                                    <option ${userInfo.user.status eq 'INACTIVE' ?'selected':''} value="INACTIVE">INACTIVE</option> 
                                       <option ${userInfo.user.status eq 'ACTIVE' ?'selected':''} value="ACTIVE">ACTIVE</option> 
                                 </select>
                              </div>
                           </div>
                          
                           <div class="form-group form-actions">
                              <div class="col-sm-4"> </div>
                              <div class="col-sm-7">
                                 <button class="btn vd_btn vd_bg-green vd_white" type="submit" id="submit-register" name="submit-register"><i class="icon-ok"></i> ${not empty applyclient.getId() ?'Update':'Save'}</button>
                                 <a class="btn vd_btn vd_bg-grey"   href="${contextPath}/jkfreelancers">Back</a>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
                  <!-- Panel Widget --> 
               </div>
               <!-- col-md-12 --> 
            </div>
            <!-- row -->
         </div>
         <!-- .vd_content-section --> 
      </div>
      <!-- .vd_content --> 
   </div>
   <!-- .vd_container --> 
</div>
<!-- .vd_content-wrapper --> 
<!-- Middle Content End --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script type="text/javascript" src="${contextPath}/static/sFreelance/js/jquery.js"></script> 
<script type="text/javascript" src='${contextPath}/static/sFreelance/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js'></script>
<link href="${contextPath}/static/sFreelance/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css">

<script type="text/javascript">
   $(document).ready(function() {
   		"use strict";	
   
           var form_register = $('#register-form');
           var error_register = $('.alert-danger', form_register);
           var success_register = $('.alert-success', form_register);
   
           form_register.validate({
               errorElement: 'div', //default input error message container
               errorClass: 'vd_red', // default input error message class
               focusInvalid: false, // do not focus the last invalid input
               ignore: ".ignoreValidation",
               rules: {
                 
               },
   			
   
             	errorPlacement: function(error, element) {
   				if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
   					element.parent().append(error);
   				} else if (element.parent().hasClass("vd_input-wrapper")){
   					error.insertAfter(element.parent());
   				}else {
   					error.insertAfter(element);
   				}
   			}, 
   			
               invalidHandler: function (event, validator) { //display error alert on form submit              
   					success_register.fadeOut();
   					error_register.fadeIn();
   					scrollTo(form_register,-100);
   
               },
   
               highlight: function (element) { // hightlight error inputs
   		
   				$(element).addClass('vd_bd-red');
   				$(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
   
               },
   
               unhighlight: function (element) { // revert the change dony by hightlight
                   $(element)
                       .closest('.control-group').removeClass('error'); // set error class to the control group
               },
   
               success: function (label, element) {
                   label
                       .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                   	.closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
   				$(element).removeClass('vd_bd-red');
   				
               },
   
               submitHandler: function (form) {
   					success_register.fadeIn();
   					error_register.fadeOut();
   					scrollTo(form_register,-100);	
   					$('#register-form').submit();				
               }
           });
   });
    
     function locadingProgress(){
    	  $("#loading").show();
    	
     }
</script>