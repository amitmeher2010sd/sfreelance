<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

   <!-- Middle Content Start -->    
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <div class="vd_head-section clearfix">
            <div class="vd_panel-header">
              <ul class="breadcrumb">
                <li><a href="index.html">Home</a> </li>
                <li><a href="forms-elements.html">Forms</a> </li>
                <li class="active">                
                 <c:if test="${empty role}">
								<a href="#">Create Role</a>
							</c:if>
							<c:if test="${not empty role}">
								<a href="#">Update Role</a>
							</c:if>   
                </li>
              </ul>
              <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step=5  data-position="left">
    			<div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
      			<div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
      			<div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>
      
			 </div> 
            </div>
          </div>
          <div class="vd_title-section clearfix">
            <div class="vd_panel-header">
              <h1><c:if test="${empty role}">
								<a href="#">Create Role</a>
							</c:if>
							<c:if test="${not empty role}">
								<a href="#">Update Role</a>
							</c:if> </h1>
              <small class="subtitle">Form Basic, Form Advanced</small>              
            </div>
          </div>
          <!-- vd_title-section -->
          
          <div class="vd_content-section clearfix">        
            <div class="row" id="advanced-input">
              <div class="col-md-12">
                <div class="panel widget">
                  <div class="panel-heading vd_bg-grey">
                    <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-bar-chart-o"></i> </span>
                    <c:if test="${empty role}">
								<a href="#">Create Role</a>
							</c:if>
							<c:if test="${not empty role}">
								<a href="#">Update Role</a>
							</c:if>                    
                    </h3>
                  </div>
                  <div class="panel-body">
                    <form class="form-horizontal" modelAttribute="role" action="./role" id="addRole" method="post">
                    <input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />
								<input type="hidden" name="roleId"
								value="${role.roleId}" />
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Role Name</label>
                        <div class="col-sm-7 controls">
                       		<c:if test="${empty role}">
										<input type="text" class="form-control width-50" id="roleName"
												maxlength="50" name="roleName">
										</c:if>
										<c:if test="${not empty role}">
											<input type="text" class="form-control width-50" id="roleName"
												maxlength="50" name="roleName" value="${role.roleName}" readonly>
										</c:if>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Role Display Name</label>
                        <div class="col-sm-7 controls">
                         <input type="text" class="form-control width-50" id="roleDisplayName"
												maxlength="100" name="roleDisplayName" value="${role.roleDisplayName}">
                      
                        </div>
                      </div>                    
                  
                       <div class="form-group form-actions">
                        <div class="col-sm-4"> </div>
                        <div class="col-sm-7">                         
                          <c:if test="${not empty role}">
											<input type="button" name="update" value="Update" id="update"
												class="btn vd_btn vd_bg-green vd_white" onclick="addUpdateRole();">&nbsp;&nbsp; 
													</c:if>
										<c:if test="${empty role}">
											<input type="button" name="add" value="Add" id="add"
												class="btn vd_btn vd_bg-green vd_white" onclick="addUpdateRole();">&nbsp;&nbsp;  
													</c:if>
										<a href="${contextPath}/home" type="button"
											class="btn vd_btn">Back</a>
											
											
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
                <!-- Panel Widget --> 
              </div>
              <!-- col-md-12 --> 
            </div>
            <!-- row -->
            
          </div>
          <!-- .vd_content-section --> 
          
        </div>
        <!-- .vd_content --> 
      </div>
      <!-- .vd_container --> 
    </div>
    <!-- .vd_content-wrapper --> 
    
    <!-- Middle Content End --> 



<script>
	function addUpdateRole() {
		if ($("#roleName").val() == "") {
			bootbox.alert("Please Enter Role Code");
			return false;
		}
		else if ($("#roleDisplayName").val() == "") {
			bootbox.alert("Please Enter Role Display Name");
			return false;
		}
		else
		{
			bootbox.confirm("Do you want to save/update the Role?",
		    function(result) {
		       if (result == true) {
		          $("#addRole").submit();
		       }
		    }); 
		}
	}
</script>
<script>
	$(function() {	
		$('#roleName').keydown(function (e) {	  
			if (e.ctrlKey || e.altKey) {	    
				e.preventDefault();	      
			} else {	    
				var key = e.keyCode;	      
				if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {	      
					e.preventDefault();	
				} 
			}	
		});	     
	});	
</script>
	