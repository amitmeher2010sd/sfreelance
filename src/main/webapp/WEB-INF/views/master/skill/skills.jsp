<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<c:if test="${not empty successMessage}">
	<script>
		bootbox.alert("${successMessage}");  
	</script>
</c:if>
<c:if test="${not empty failureMessage}">
	<script>
		bootbox.alert("${failureMessage}"); 
	</script>
</c:if>

  <!-- Middle Content Start -->
    
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <div class="vd_head-section clearfix">
            <div class="vd_panel-header">
              <ul class="breadcrumb">
                <li><a href="index.html">Home</a> </li>
                <li><a href="./">Skill Management</a> </li>
                <li class="active">Manage Skills</li>
              </ul>
              <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step=5  data-position="left">
   <!--  <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
      <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
      <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>
 -->      
</div>
 
            </div>
          </div>
          <div class="vd_title-section clearfix">
            <div class="vd_panel-header">
              <h1>Manage Skills</h1>
              <small class="subtitle">Look <a href="./">below</a> for more information</small> </div>
          </div>
          <div class="vd_content-section clearfix">
            <div class="row">
              <div class="col-md-12">
                <div class="panel widget">
                      <c:if test="${not empty successMessage}">
	<div class="alert alert-success alert-dismissable">
             	<button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cross"></i></button>
                    	<i class="fa fa-check-circle append-icon"></i><strong>Well done!</strong> ${successMessage} 
        </div>
</c:if>
<c:if test="${not empty failureMessage}">
<div class="alert alert-danger alert-dismissable alert-condensed">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cross"></i></button>
                        <i class="fa fa-exclamation-circle append-icon"></i>
                        <strong>Oh snap!</strong> ${failureMessage} 
        </div>
</c:if>
                  <div class="panel-heading vd_bg-grey">
                    <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span>List Of Skills</h3>                    
                  </div>
                  
                  <div class="panel-body table-responsive">
                   <a href="${contextPath}/master/skill" type="button" style="margin-left: 92%;"
											class="btn vd_btn vd_bg-green vd_white">Add Skill</a>
                 
                    <table class="table table-striped" id="data-tables">
                     <thead>
									<tr>
										<th>Serial Number</th>
										<th>Skill Name</th>
										<th>Description</th>
										<th>Created On</th>
										<th>Updated On</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody id="tbd">
									<c:forEach items="${skillList}" var="data" varStatus="serialNumber">
										<tr>
											<td>${serialNumber.count}</td>
											<td>${data.skillName}</td>
											<td>${data.description}</td>
											<td><fmt:formatDate type = "both"   value = "${data.createdOn}" /></td>
											<td><fmt:formatDate type = "both"   value = "${data.updatedOn}" /></td>
											<td><c:if test="${data.status eq 'ACTIVE'}">  
												${data.status}
											</c:if> <c:if test="${data.status eq 'INACTIVE'}">  
												${data.status}
											</c:if></td>
											<td><c:if test="${data.status eq 'ACTIVE'}">
													<button class="btn btn-warning btn-sm"
														onclick="edit('${data.skillId}')">
														<i class=" fa fa-pencil-square-o " aria-hidden="true"></i>
													</button>
													<button class="btn btn-danger btn-sm"
														onclick="del(${data.skillId})">
														<i class=" glyphicon glyphicon-trash " aria-hidden="true"></i>
													</button>
												</c:if> <c:if test="${data.status eq 'INACTIVE'}">
													<button class="btn btn-warning btn-sm"
														 disabled>
														<i class=" fa fa-pencil-square-o " aria-hidden="true"></i>
													</button>
													<button class="btn btn-success btn-sm"
														onclick="active(${data.skillId})">
														<i class=" glyphicon glyphicon-trash " aria-hidden="true"></i>
													</button>
												</c:if></td>
										</tr>
									</c:forEach>
								</tbody>
                    </table>
                  </div>
                </div>
                <!-- Panel Widget --> 
              </div>
              <!-- col-md-12 --> 
            </div>
            <!-- row --> 
            
          </div>
          <!-- .vd_content-section --> 
          
        </div>
        <!-- .vd_content --> 
      </div>
      <!-- .vd_container --> 
    </div>
    <!-- .vd_content-wrapper --> 



 					














<form action="${contextPath}/master/edit-skill" method="POST" id="editSkill">
<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />
	<input type="hidden" name="id_edit" id="id_edit" />
</form>
<form action="${contextPath}/master/del-skill" method="POST"
	id="deleteSkill">
	<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />
	<input type="hidden" name="id_del" id="id_del" />
</form>
<form action="${contextPath}/master/active-skill" method="POST"
	id="enablaSkill">
	<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />
	<input type="hidden" name="id_del" id="id_del_en" />
</form>


<script> 
	function edit(id){
		$("#id_edit").val(id);
		bootbox.confirm("Do you want to edit ?",
		function(result) {
			if (result == true) {
			   $("#editSkill").submit();
			} 
		});
	}	
	function del(id){
		$("#id_del").val(id);
		bootbox.confirm("Do you want to delete?",
	    function(result) {
	        if (result == true) {
	            $("#deleteSkill").submit();
	        } 
	    });
	} 
	function active(id){
		$("#id_del_en").val(id);
		bootbox.confirm("Do you want to enable?",
	    function(result) {
	        if (result == true) {
	            $("#enablaSkill").submit();
	        } 
	    });
	} 
	
</script>
	