<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

   <!-- Middle Content Start -->    
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <div class="vd_head-section clearfix">
            <div class="vd_panel-header">
              <ul class="breadcrumb">
                <li><a href="index.html">Home</a> </li>
                <li><a href="forms-elements.html">Forms</a> </li>
                <li class="active">  
                ${empty skill?'Create Skill':'Update Skill'}              
               
                </li>
              </ul>
              <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step=5  data-position="left">
    			<div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
      			<div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
      			<div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>
      
			 </div> 
            </div>
          </div>
          <div class="vd_title-section clearfix">
            <div class="vd_panel-header">
              <h1>${empty skill?'Create Skill':'Update Skill'}</h1>
              <small class="subtitle">Form Basic, Form Advanced</small>              
            </div>
          </div>
          <!-- vd_title-section -->
          
          <div class="vd_content-section clearfix">        
            <div class="row" id="advanced-input">
              <div class="col-md-12">
                <div class="panel widget">
                
                  <div class="panel-heading vd_bg-grey">
                    <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-bar-chart-o"></i> </span>
                    ${empty skill?'Create Skill':'Update Skill'}                 
                    </h3>
                  </div>
                  <div class="panel-body">
                    <form class="form-horizontal" modelAttribute="skill" action="${contextPath}/admin/skill" id="addSkill" method="post">
                    <input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />
								<input type="hidden" name="skillId"
								value="${skill.skillId}" />
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Skill Name</label>
                        <div class="col-sm-7 controls">
                       		
										<input type="text" class="form-control width-50" id="skillName"
												maxlength="50" name="skillName" value="${skill.skillName}">
										
										
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-7 controls">
                         <input type="text" class="form-control width-50" id="description"
												maxlength="100" name="description" value="${skill.description}">
                      
                        </div>
                      </div>                    
                  
                       <div class="form-group form-actions">
                        <div class="col-sm-4"> </div>
                        <div class="col-sm-7">                         
                        
											<input type="button" name="update" value="${empty skill?'Save':'Update'}" 
												class="btn vd_btn vd_bg-green vd_white" onclick="addUpdateSkill();">&nbsp;&nbsp; 
												
										<a href="${contextPath}/skills" type="button"
											class="btn vd_btn">Back</a>
											
											
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
                <!-- Panel Widget --> 
              </div>
              <!-- col-md-12 --> 
            </div>
            <!-- row -->
            
          </div>
          <!-- .vd_content-section --> 
          
        </div>
        <!-- .vd_content --> 
      </div>
      <!-- .vd_container --> 
    </div>
    <!-- .vd_content-wrapper --> 
    
    <!-- Middle Content End --> 



<script>
	function addUpdateSkill() {
		if ($("#skillName").val() == "") {
			bootbox.alert("Please Enter Skill Name");
			return false;
		}
		else if ($("#roleDisplayName").val() == "") {
			bootbox.alert("Please Enter Description");
			return false;
		}
		else
		{
			bootbox.confirm("Do you want to save/update the Skill?",
		    function(result) {
		       if (result == true) {
		          $("#addSkill").submit();
		       }
		    }); 
		}
	}
</script>
<
	