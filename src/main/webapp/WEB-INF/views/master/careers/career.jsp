<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

   <!-- Middle Content Start -->    
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <div class="vd_head-section clearfix">
            <div class="vd_panel-header">
              <ul class="breadcrumb">
                <li><a href="index.html">Home</a> </li>
                <li><a href="forms-elements.html">Forms</a> </li>
                <li class="active">  
                ${empty data?'Create Creer':'Update Creers'}              
               
                </li>
              </ul>
              <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step=5  data-position="left">
    			<div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
      			<div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
      			<div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>
      
			 </div> 
            </div>
          </div>
          <div class="vd_title-section clearfix">
            <div class="vd_panel-header">
              <h1>${empty data?'Create Creer':'Update Creer'}</h1>
              <small class="subtitle">Form Basic, Form Advanced</small>              
            </div>
          </div>
          <!-- vd_title-section -->
          
          <div class="vd_content-section clearfix">        
            <div class="row" id="advanced-input">
              <div class="col-md-12">
                <div class="panel widget">
                
                  <div class="panel-heading vd_bg-grey">
                    <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-bar-chart-o"></i> </span>
                    ${empty data?'Create Creer':'Update Creer'}                 
                    </h3>
                  </div>
                  <div class="panel-body">
                    <form class="form-horizontal" modelAttribute="career" action="${contextPath}/master/career" id="add" method="post">
                    <input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />
								<input type="hidden" name="Id"
								value="${data.getId()}" />
								 <div class="form-group">
                        <label class="col-sm-2 control-label">Title</label>
                        <div class="col-sm-7 controls">
                       		
										<input type="file" class="form-control width-50" id="imageInput" accept="image/*"
												maxlength="50" >
												${not empty data.logo ?'Uploaded':'Pending'}
									<br><br>
    <img id="imageDisplay" alt="Image will appear here" style="width:100px;height:100px;margin-top: 0px;" src="${data.logo}"  />
   	
										
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Title</label>
                        <div class="col-sm-7 controls">
                       		
										<input type="text" class="form-control width-50" id="title"
												maxlength="50" name="title" value="${data.title}">
										
										
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Location</label>
                        <div class="col-sm-7 controls">
                         <input type="text" class="form-control width-50" id="location"
												maxlength="100" name="location" value="${data.location}">
                      
                        </div>
                      </div>                    
                   <div class="form-group">
                        <label class="col-sm-2 control-label">Total Vacancy</label>
                        <div class="col-sm-7 controls">
                         <input type="text" class="form-control width-50" id="totalvacancy"
												maxlength="100" name="totalvacancy" value="${data.totalvacancy}">
                      
                        </div>
                      </div> 
                       <div class="form-group">
                        <label class="col-sm-2 control-label">Experience</label>
                        <div class="col-sm-7 controls">
                         <input type="text" class="form-control width-50" id="experience"
												maxlength="10" name="experience" value="${data.experience}">
                      
                        </div>
                      </div> 
                        <div class="form-group">                                 
                                 <label class="col-sm-2 control-label">Publish<span class="vd_red">*</span> </label>
                                 <div class="col-sm-3 controls">
                                    <select class="form-control form-control-sm required" id="publishedStr" name="publishedStr" >
                                       <option ${data.published eq false ?'selected':''} value="Pending">Pending</option> 
                                       <option ${data.published eq true ?'selected':''} value="Publish">Publish</option> 
                                    </select>
                                 </div>
                              </div>
                       <div class="form-group form-actions">
                        <div class="col-sm-4"> </div>
                        <div class="col-sm-7">                         
                        
											<input type="button" name="update" value="${empty data?'Save':'Update'}" 
												class="btn vd_btn vd_bg-green vd_white" onclick="addUpdate();">&nbsp;&nbsp; 
												
										<a href="${contextPath}/master/careers" type="button"
											class="btn vd_btn">Back</a>
											
											
                        </div>
                      </div>
                       <input type="hidden"  id="logo" name="logo" value="${data.logo}">
                    </form>
                  </div>
                </div>
                <!-- Panel Widget --> 
              </div>
              <!-- col-md-12 --> 
            </div>
            <!-- row -->
            
          </div>
          <!-- .vd_content-section --> 
          
        </div>
        <!-- .vd_content --> 
      </div>
      <!-- .vd_container --> 
    </div>
    <!-- .vd_content-wrapper --> 
    
    <!-- Middle Content End --> 



<script>
	function addUpdate() {
		if ($("#title").val() == "") {
			bootbox.alert("Please Enter title");
			return false;
		}
		else if ($("#logo").val() == "" && $("#imageInput").val() == "") {
			bootbox.alert("Please Enter logo");
			return false;
		}
		else if ($("#location").val() == "") {
			bootbox.alert("Please Enter location");
			return false;
		}
		else if ($("#totalvacancy").val() == "") {
			bootbox.alert("Please Enter totalvacancy");
			return false;
		}
		else if ($("#experience").val() == "") {
			bootbox.alert("Please Enter experience");
			return false;
		}else if ($("#publishedStr").val() == "") {
			bootbox.alert("Please Enter publish");
			return false;
		}
		else
		{
			bootbox.confirm("Do you want to save/update?",
		    function(result) {
		       if (result == true) {
		          $("#add").submit();
		       }
		    }); 
		}
	}

</script>
<script>
        const imageInput = document.getElementById('imageInput');
        const imageDisplay = document.getElementById('imageDisplay');

        imageInput.addEventListener('change', function(event) {
            const file = event.target.files[0];
            if (file) {
                const reader = new FileReader();

                reader.onload = function(e) {
                    const base64String = e.target.result;
                    imageDisplay.src = base64String; // Display the image
                    //console.log("Base64 Image String: ", base64String); // For debugging
                    $("#logo").val(base64String);
                    
                };

                reader.readAsDataURL(file); // Convert image to Base64
            }
        });
    </script>
	