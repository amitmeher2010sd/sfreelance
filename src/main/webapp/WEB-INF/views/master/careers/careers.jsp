<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<c:if test="${not empty successMessage}">
	<script>
		bootbox.alert("${successMessage}");  
	</script>
</c:if>
<c:if test="${not empty failureMessage}">
	<script>
		bootbox.alert("${failureMessage}"); 
	</script>
</c:if>

  <!-- Middle Content Start -->
    
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <div class="vd_head-section clearfix">
            <div class="vd_panel-header">
              <ul class="breadcrumb">
                <li><a href="index.html">Home</a> </li>
                <li><a href="./">Careers Management</a> </li>
                <li class="active">Manage Careers</li>
              </ul>
              <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step=5  data-position="left">
   <!--  <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
      <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
      <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>
 -->      
</div>
 
            </div>
          </div>
          <div class="vd_title-section clearfix">
            <div class="vd_panel-header">
              <h1>Manage Careers</h1>
              <small class="subtitle">Look <a href="./">below</a> for more information</small> </div>
          </div>
          <div class="vd_content-section clearfix">
            <div class="row">
              <div class="col-md-12">
                <div class="panel widget">
                      <c:if test="${not empty successMessage}">
	<div class="alert alert-success alert-dismissable">
             	<button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cross"></i></button>
                    	<i class="fa fa-check-circle append-icon"></i><strong>Well done!</strong> ${successMessage} 
        </div>
</c:if>
<c:if test="${not empty failureMessage}">
<div class="alert alert-danger alert-dismissable alert-condensed">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cross"></i></button>
                        <i class="fa fa-exclamation-circle append-icon"></i>
                        <strong>Oh snap!</strong> ${failureMessage} 
        </div>
</c:if>
                  <div class="panel-heading vd_bg-grey">
                    <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span>List Of Careers</h3>                    
                  </div>
                  
                  <div class="panel-body table-responsive">
                   <a href="${contextPath}/master/career" type="button" style="margin-left: 92%;"
											class="btn vd_btn vd_bg-green vd_white">Add Career</a>
                 
                    <table class="table table-striped" id="data-tables">
                     <thead>
									<tr>
										<th>Serial Number</th>
										<th>Logo</th>
										<th>Title</th>
										<th>Location</th>
										<th>Total Vacancy</th>
										<th>Experience</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody id="tbd">
									<c:forEach items="${dataList}" var="data" varStatus="serialNumber">
										<tr>
											<td>${serialNumber.count}</td>
											<td> <img id="imageDisplay" alt="Image will appear here" style="width:100px;height:100px;margin-top: 0px;" src="${data.logo}"  /></td>
											<td>${data.title}</td>
											<td>${data.location}</td>
											<td>${data.totalvacancy}</td>
											<td>${data.experience}</td>
											<td><c:if test="${data.published eq true}">  
												
												<span class="label label-success">Published</span> 
											</c:if> <c:if test="${data.published eq false}">  
												<span class="label label-danger">Pending</span> 
											</c:if></td>
											<td>
													<button class="btn btn-warning btn-sm"
														onclick="edit('${data.getId()}')">
														<i class=" fa fa-pencil-square-o " aria-hidden="true"></i>
													</button>
													</td>
										</tr>
									</c:forEach>
								</tbody>
                    </table>
                  </div>
                </div>
                <!-- Panel Widget --> 
              </div>
              <!-- col-md-12 --> 
            </div>
            <!-- row --> 
            
          </div>
          <!-- .vd_content-section --> 
          
        </div>
        <!-- .vd_content --> 
      </div>
      <!-- .vd_container --> 
    </div>
    <!-- .vd_content-wrapper --> 



 					














<form action="${contextPath}/master/edit-career" method="POST" id="editSkill">
<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />
	<input type="hidden" name="id_edit" id="id_edit" />
</form>

<script> 
	function edit(id){
		$("#id_edit").val(id);
		bootbox.confirm("Do you want to edit ?",
		function(result) {
			if (result == true) {
			   $("#editSkill").submit();
			} 
		});
	}	
	
	
</script>
	