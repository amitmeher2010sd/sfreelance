<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<!-- Middle Content Start -->
<div class="vd_content-wrapper">
<div class="vd_container">
   <div class="vd_content clearfix">
      <div class="vd_head-section clearfix">
         <div class="vd_panel-header">
            <ul class="breadcrumb">
               <li><a href="index.html">Home</a> </li>
               <li><a href="pages-custom-product.html">Pages</a> </li>
               <li class="active">Calendar</li>
            </ul>
            <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step=5  data-position="left">
         <!--       <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
               <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
               <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>
     -->        </div>
         </div>
      </div>
      <div class="vd_title-section clearfix">
         <div class="vd_panel-header no-menu">
            <h1>Payment Calendar</h1>
            <small class="subtitle">Calendar page with drag and drop events. See <a href="http://arshaw.com/fullcalendar/">fullcalendar</a> for detail</small> 
         </div>
      </div>
      <div class="vd_content-section clearfix">
         <div class="row">
            <div class="col-sm-12">
               <div class="panel widget light-widget panel-bd-top">
                  <div class="panel-body">
                     <div id="fullcalendar" class="mgtp-10"> </div>
                  </div>
               </div>
            </div>
            <!-- col-sm-9-->
            <!-- row --> 
         </div>
         <!-- .vd_content-section --> 
      </div>
      <!-- .vd_content --> 
   </div>
   <!-- .vd_container --> 
</div>
<!-- .vd_content-wrapper --> 
<!-- Middle Content End --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script type="text/javascript" src="${contextPath}/static/sFreelance/js/jquery.js"></script> 
<script type="text/javascript">
   $(window).load(function() 
   	{
   	"use strict";
   
       function renderCalendar(){
            
           if (!jQuery().fullCalendar) {
               return;
           }
   
           var date = new Date();
           var d = date.getDate();
           var m = date.getMonth();
           var y = date.getFullYear();
   
   
   		var h = {
   			left: 'title, prev,next',
   			left: 'title',
   			center: '',
   			right: 'month'
   			//right: 'today,month,agendaWeek,agendaDay'
   		};
   
   
           function initDragObject(element) {
               // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
               // it doesn't need to have a start or end
               var eventObject = {
                   title: $.trim(element.text()) // use the element's text as the event title
               };
               // store the Event Object in the DOM element so we can get to it later
               element.data('eventObject', eventObject);
               // make the event draggable using jQuery UI
               element.draggable({
                   zIndex: 999,
                   revert: true, // will cause the event to go back to its
                   revertDuration: 0 //  original position after the drag
               });
           }
   
              
   
           $('#fullcalendar').html("");
           $('#fullcalendar').fullCalendar({
               header: h,
               editable: true,
               droppable: true, // this allows things to be dropped onto the calendar !!!
               drop: function (date, allDay) { // this function is called when something is dropped
   
                   // retrieve the dropped element's stored Event Object
                   var originalEventObject = $(this).data('eventObject');
                   // we need to copy it, so that multiple events don't have a reference to the same object
                   var copiedEventObject = $.extend({}, originalEventObject);
   
                   // assign it the date that was reported
                   copiedEventObject.start = date;
                   copiedEventObject.allDay = allDay;
                   copiedEventObject.className = $(this).attr("data-class");
   
                   // render the event on the calendar
                   // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                   $('#fullcalendar').fullCalendar('renderEvent', copiedEventObject, true);
   
                   // is the "remove after drop" checkbox checked?
                   if ($('#drop-remove').is(':checked')) {
                       // if so, remove the element from the "Draggable Events" list
                       $(this).remove();
                   }
               },
               events: getPaymentInfo(d,m,y),
               
   eventMouseover: function(calEvent, jsEvent) {
   var data=(calEvent.desc).split("||");
       var tooltip = '<div class="tooltipevent" style="width:400px;height:200px;background:#ccc;position:absolute;z-index:10001;"><div class="table-responsive-xl"><table class="table table-borderless"><thead><tr><th scope="col">#</th><th scope="col">Freelancer</th><th scope="col">Client</th><th scope="col">Project</th></tr></thead><tbody><tr scope="row"><td>1</td><td>'+data[0]+'</td><td>'+data[1]+'</td> <td>'+data[2]+'</td></tr></tbody></table></div></div>';
       var $tooltip = $("").appendTo('body');
   
       $(this).mouseover(function(e) {
           $(this).css('z-index', 10000);
           $tooltip.fadeIn('500');
           $tooltip.fadeTo('10', 1.9);
       }).mousemove(function(e) {
           $tooltip.css('top', e.pageY + 20);
          $tooltip.css('left', e.pageX + 20);     
       });
   },
   
   eventMouseout: function(calEvent, jsEvent) {
       $(this).css('z-index', 8);
       $('.tooltipevent').remove();
   }
            
           
           });
           
           
          
   
       }
   
   	renderCalendar();
   
   
   });
   
   
   function getPaymentInfo(d,m,y){
   var eventsdata= [];
   <c:forEach items="${payResList}" var="res"  varStatus="i">//y, m, d - 3, 16, 0
   var start="${res.start}";
   var dt = (start).split(",");
   eventsdata.push({title: '${res.title}',start:new Date(dt[0], dt[1], dt[2]),allDay: ${res.allDay},url:'${res.url}'});
   </c:forEach>
   
   return eventsdata;
   }
   
</script>
<!-- Specific Page Scripts END -->
