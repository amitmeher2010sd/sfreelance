<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<c:if test="${not empty successMessage}">
   <script>
      bootbox.alert("${successMessage}");  
   </script>
</c:if>
<c:if test="${not empty failureMessage}">
   <script>
      bootbox.alert("${failureMessage}"); 
   </script>
</c:if>
<!-- Middle Content Start -->
<div class="vd_content-wrapper">
   <div class="vd_container">
      <div class="vd_content clearfix">
         <div class="vd_head-section clearfix">
            <div class="vd_panel-header">
               <ul class="breadcrumb">
                  <li><a href="index.html">Home</a> </li>
                  <li><a href="${contextPath}/">Manage Payment</a> </li>
                  <li class="active">Payment History</li>
               </ul>
            </div>
         </div>
         <div class="vd_title-section clearfix">
            <div class="vd_panel-header">
               <h1>Payment History</h1>
               <small class="subtitle">Look <a href="http://www.datatables.net">below</a> for more information</small> 
            </div>
         </div>
         <c:if test="${not empty project}">
            <div class="vd_content-section clearfix">
               <div class="row" id="advanced-input">
                  <div class="col-md-12">
                     <div class="panel widget">
                        <div class="panel-heading vd_bg-grey">
                           <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-bar-chart-o"></i> </span> Payment Details </h3>
                        </div>
                        <div class="panel-body">
                           <form class="form-horizontal"  action="${contextPath}/project-payment" id="register-form" method="post" modelAttribute="projectPayment">
                              <div class="modal-content">
                                 <div class="modal-header vd_bg-blue vd_white">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                                    <h4 class="modal-title" id="myModalLabel1">Add Payment Details</h4>
                                 </div>
                                 <div class="modal-body">
                                    <input type="hidden" name="${_csrf.parameterName}"
                                       value="${_csrf.token}" />
                                    <div class="alert alert-danger vd_hidden">
                                       <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-cross"></i></button>
                                       <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Oh snap!</strong> Change a few things up and try submitting again. 
                                    </div>
                                    <div class="alert alert-success vd_hidden">
                                       <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-cross"></i></button>
                                       <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Well done!</strong>. 
                                    </div>
                                    <div class="form-group">
                                          
                                     
                                     <label class="col-sm-2 control-label">Project Code </label>
                                       <div class="col-sm-3 controls">
                                          <input type="hidden" class="form-control" required id="projectCode" name="projectCode" value="${project.projectCodeInternal}"   class="width-70"
                                             maxlength="500">	
                                          <span class="lbl_black">${project.projectCode} (${project.projectName}) </span>
                                       </div>
                                       
                                        <label class="col-sm-2 control-label">Hourly/Other </label>
                                       
                                          <input type="checkbox" class="form-control" id="hourlyOrProjectWise" name="hourlyOrProjectWise" style="width:20px;"	>	
                                         
                                  
                                  
                                  
                                       <label class="col-sm-2 control-label">Payment Type:</label>
                                       <div class="col-sm-3 controls">
                                          <select name="paymentfor" class="form-control" required>
                                             <option value="">--select--</option>
                                             <c:forEach items="${paymentTypeList}" var="data">
														<option  value="${data}">${data}</option> 
													</c:forEach>
                                          </select>
                                       </div>
                                  
                                    </div>
                                    
                                      <div class="form-group">
                                       <label class="col-sm-2 control-label"> Payment Date <span class="vd_red">*</span></label>
                                       <div class="col-sm-3 controls">
                                          <input type="text" class="form-control ignoreValidation date" id="paymentDateStr"
                                             name="paymentDateStr"  >
                                       </div>
                                          <label class="col-sm-2 control-label">By Project Status<span class="vd_red"></span> </label>
                              <div class="col-sm-3 controls">
                                 <select class="form-control form-control-sm"  name="paymentStatus">   
                                <c:forEach items="${paymentStatusList}" var="data">
														<option  value="${data}">${data}</option> 
													</c:forEach>
                                								
                                 </select> 
                              </div>
                                    </div>
                                    
                                    
                                    
                                    <div class="form-group">
                                       <label class="col-sm-2 control-label"> Start Date <span class="vd_red">*</span></label>
                                       <div class="col-sm-3 controls">
                                          <input type="text" class="form-control  ignoreValidation date" id="startDateStr"
                                             name="startDateStr"  >
                                       </div>
                                       <label class="col-sm-2 control-label"> End Date <span class="vd_red">*</span></label>
                                       <div class="col-sm-3 controls">
                                          <input type="text" class="form-control  ignoreValidation date" id="endDateStr"
                                             name="endDateStr"  >
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label class="col-sm-2 control-label">Payment Amount <span class="vd_red"></span> </label>
                                       <div class="col-sm-2 controls">
                                          <input type="number" step="0.01" class="form-control required" id="paymentAmount" name="paymentAmount"
                                             maxlength="100">
                                       </div>
                                        
                                       <label class="col-sm-2 control-label">Currency <span class="vd_red">*</span> </label>
                                       <div class="col-sm-1 controls">
                                          <select name="currency" id="currency" class="form-control populate required">
                                             <option value="">-Select-</option>
                                             <c:forEach items="${currencyList}" var="data">
                                                <option value="${data}">${data}</option> 
                                             </c:forEach>
                                          </select>
                                       </div>
                                        <label class="col-sm-2 control-label">Advence? </label>
                                       <div class="col-sm-1 controls">
                                          <input type="checkbox" class="form-control" id="advance" name="advance" style="width:20px;"	>	
                                           </div>
                                    </div>
                                    <div class="form-group">
                                       <label class="col-sm-2 control-label">Remarks <span class="vd_red">*</span> </label>
                                       <div class="col-sm-8 controls">
                                          <textarea  rows="3" cols="20" id="remarks required" name="remarks" 
                                             tabindex="2" class="form-control" maxlength="500">just created</textarea>
                                       </div>
                                    
                                 </div>
                                 <div class="modal-footer background-login">
                                    <button type="submit" class="btn vd_btn vd_bg-green">Save changes</button>
                                 </div>
                              </div>
                              <!-- /.modal-content --> 
                           </form>
                        </div>
                     </div>
                     <!-- Panel Widget --> 
                  </div>
                  <!-- col-md-12 --> 
               </div>
               <!-- row -->
            </div>
            <!-- .vd_content-section --> 
         </c:if>
         <div class="vd_content-section clearfix">
            <div class="row">
               <div class="col-md-12">
                  <div class="panel widget">
                     <c:if test="${not empty successMessage}">
                        <div class="alert alert-success alert-dismissable">
                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cross"></i></button>
                           <i class="fa fa-check-circle append-icon"></i><strong>Well done!</strong> ${successMessage} 
                        </div>
                     </c:if>
                     <c:if test="${not empty failureMessage}">
                        <div class="alert alert-danger alert-dismissable alert-condensed">
                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cross"></i></button>
                           <i class="fa fa-exclamation-circle append-icon"></i>
                           <strong>Oh snap!</strong> ${failureMessage} 
                        </div>
                     </c:if>
                     <div class="panel-heading vd_bg-grey">
                        <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span>List Of Payment History</h3>
                     </div>
                     <div class="panel-body table-responsive">
                        
                           <div class="form-group">
                              <label class="col-sm-2 control-label">By Project Status<span class="vd_red"></span> </label>
                              <div class="col-sm-3 controls">
                                 <select class="form-control form-control-sm"  name="paystatus1"  onChange="loadByPAllocationStatus(this.value);">   
                                <c:forEach items="${paymentStatusList}" var="data">
														<option  value="${data}" ${paystatus eq data?'selected':''}>${data}</option> 
													</c:forEach>
                                								
                                 </select> 
                              </div>
                           </div>
                    
                        <c:set var = "credit" value = "${0}"/>
                        <c:set var = "debit" value = "${0}"/>
                        <c:set var = "currency" value = "INR"/>
                        <c:set var = "usdInInr" value = "${USD_TO_INR}"/>
                        <table class="table table-striped" id="data-tables">
                           <thead>
                              <tr>
                                 <th>Serial Number</th>
                                 <th>Payment for</th>
                                 <th>Hourly Or ProjectWise</th>
                                 <th>Project Details</th>
                                 <th>Freelancer Details</th>
                                 <th>Payment Date</th>
                                 <th>Start Date</th>
                                 <th>End Date</th>
                                 <th>Payment Amount</th>
                                 <th>Payment Status</th>
                                 <th>Remarks</th>
                               
                                 <th>Created On</th>
                                 <th>Updated On</th>
                                 <th>Status</th>d
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody id="tbd">
                           <c:set var = "count" value = "${responseList.size()}"/>
                              <c:forEach items="${responseList}" var="data" varStatus="serialNumber">
                               <tr style="color:${data.projectAllocation.projectStatus eq 'INPROGRESS'?'black':'red'};">
                                
                                    <td>${count}  </td>
                                    <td style="font-weight: bold;color: ${data.paymentfor eq 'CREDIT'?'green':'red'};">${data.paymentfor eq 'CREDIT'?'Payment Received':'Payment Released'}  </td>
                                    <td><span class="${data.hourlyOrProjectWise eq true?'label label-success':''}">${data.hourlyOrProjectWise eq true?'YES':''}</span></td>
                                    <td>
                                       ${data.projectAllocation.project.projectName} (${data.projectAllocation.project.projectCode})
                                    Allocation Status: ${data.projectAllocation.projectStatus}
                                    </td>
                                    <td>${data.projectAllocation.userFreelancer.name}(${data.projectAllocation.userFreelancer.mobileNo})</td>
                                    <td>
                                       <fmt:formatDate type = "DATE" pattern="dd-MMM-yyyy"    value = "${data.paymentDate}" />
                                    </td>
                                    <td>
                                       <fmt:formatDate type = "DATE" pattern="dd-MMM-yyyy"   value = "${data.startDate}" />
                                    </td>
                                    <td>
                                       <fmt:formatDate type = "DATE"   value = "${data.endDate}" />
                                    </td>
                                    <td>${data.paymentAmount}&nbsp;&nbsp;${data.currency} 
                                    <c:if test="${data.currency eq 'USD'}">
                                          => ${(data.paymentAmount)*(usdInInr)} INR
                                       </c:if>
                                    
                                    <span class="label label-success">${data.advance eq true?'Advanced Paid':''}</span>
                                    </td>
                                    <td><span class="label label-${data.paymentStatus eq 'PAID'?'success':'danger'}">${data.paymentStatus}</span></td>
                                    <td>${data.remarks}</td>
                                   
                                   <td>
                                       <fmt:formatDate type = "both"   value = "${data.createdOn}" />&nbsp;${not empty data.createdBy?'By:':''} ${data.createdBy.userName}
                                    </td>
                                    <td>
                                       <fmt:formatDate type = "both"   value = "${data.updatedOn}" />&nbsp;${not empty data.updatedBy?'By:':''} ${data.updatedBy.userName}
                                    </td>
                                    <td>${data.status}</td>
                                    <td>
                                    <fmt:formatDate var="month" value="${data.createdOn}" pattern="MM" />
                                      
                                      <c:if test="${(data.paymentStatus eq 'PENDING' or data.paymentStatus eq 'WAITING_PAYMENT') and data.projectAllocation.projectStatus eq 'INPROGRESS'}">  
                                          <button class="btn btn-primary btn-sm"  onclick="psmodal('${data.getId()}')">
                                          <i class="append-icon fa fa-fw fa-exchange"></i>  Update
                                          </button>
                                          <button class="btn btn-danger btn-sm"
														onclick="deletePay('${data.getId()}')">
														<i class=" glyphicon glyphicon-trash " aria-hidden="true"></i>
													</button>
                                     </c:if>    
                                             <c:if test="${month eq  currentMonth}">                           
                                    <%--    <c:if test="${data.paymentStatus eq 'PENDING' and data.projectAllocation.projectStatus eq 'CLOSED'}">--%>  
                                          <button class="btn btn-primary btn-sm"  onclick="psmodal('${data.getId()}')">
                                          <i class="append-icon fa fa-fw fa-exchange"></i>  Update
                                          </button>
                                       </c:if>   
                                   
                                       <c:if test="${data.paymentfor eq 'CREDIT'}">                                      
                                        <c:if test="${data.currency eq 'USD'}">
                                         <c:set var = "inr" value = "${(data.paymentAmount)*(usdInInr)}"/>  
                                        <c:set var = "credit" value = "${credit+inr}"/>
                                        </c:if>
                                         <c:if test="${data.currency eq 'INR'}">
                                          <c:set var = "credit" value = "${credit+data.paymentAmount}"/>
                                       </c:if>
                                     
                                      <c:set var = "inr" value = "${0}"/>  
                                        </c:if>
                                       <c:if test="${data.paymentfor eq 'DEBIT'}">
                                        <c:if test="${data.currency eq 'USD'}">     
                                         <c:set var = "inr" value = "${(data.paymentAmount)*(usdInInr)}"/>                                  
                                        <c:set var = "debit" value = "${debit+inr}"/>
                                        </c:if>
                                         <c:if test="${data.currency eq 'INR'}">
                                          <c:set var = "debit" value = "${debit+data.paymentAmount}"/>
                                       </c:if>
                                          
                                           <c:set var = "inr" value = "${0}"/>  
                                       </c:if>
                                       
                                       <c:set var = "count" value = "${count-1}"/>
                                    </td>
                                 </tr>
                              </c:forEach>
                           </tbody>
                        </table>
                        <p style="font-weight: bold;color: green;"><strong>CREDIT: <fmt:formatNumber type="number" maxFractionDigits="2" value="${credit}"/></strong>&nbsp;${currency} </p>
                        <p style="font-weight: bold;color: red;"><strong>DEBIT: <fmt:formatNumber type="number" maxFractionDigits="2" value="${debit}"/></strong>&nbsp;${currency} </p>
                        </hr>------------------------------</hr>
                        <p style="font-weight: bold;color: blue;"><strong> ${credit-debit>0?'PROFIT':'LOSS'}: <fmt:formatNumber type="number" maxFractionDigits="2" value="${credit-debit}"/>&nbsp;${currency}</strong> </p>
                     </div>
                  </div>
                  <!-- Panel Widget --> 
               </div>
               <!-- col-md-12 --> 
            </div>
            <!-- row --> 
         </div>
         <!-- .vd_content-section --> 
      </div>
      <!-- .vd_content --> 
   </div>
   <!-- .vd_container--> 
</div>
<!-- .vd_content-wrapper --> 
<!-- Modal -->
<div class="modal fade" id="myModal_statusChange"  role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog">
      <form class="form-horizontal"  action="${contextPath}/payment-status-change"  method="post">
         <div class="modal-content">
            <div class="modal-header vd_bg-blue vd_white">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
               <h4 class="modal-title" id="myModalLabel">Payment Status Change For : <i id="pcode"></i></h4>
            </div>
            <div class="modal-body">
               <input type="hidden" name="${_csrf.parameterName}"
                  value="${_csrf.token}" />
               <input type="hidden" name="paymentId" id="paymentId" />
                <input type="hidden" name="projectcodeStatusChange"  value="${project.projectCodeInternal}"/>
               <div class="form-group">
                  <label class="col-sm-4 control-label">Status</label>
                  <div class="col-sm-7 controls">
                     <select name="pstatus" class="width-40" required>
                      <option value="">--select--</option>
                        <c:forEach items="${paymentStatusList}" var="data">
														<option  value="${data}" ${paystatus eq data?'selected':''}>${data}</option> 
													</c:forEach>
													
                     </select>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-4 control-label">Remarks:</label>
                  <div class="col-sm-7 controls">
                     <textarea  rows="2" cols="20" id="remarks" name="remarks" 
                        tabindex="2" class="form-control required" maxlength="500" required></textarea>
                  </div>
               </div>
               <div class="form-group">
               <label class="col-sm-2 control-label"> Edit <span class="vd_red">*</span></label>
                        <div class="col-sm-3 controls">
                           <input type="checkbox" class="form-control" onchange="editPayment();"   name="isedit" id="isedit" style="width: 20px;">
                         </div>
                
              </div>
               <div class="form-group" id="pdetails" style="display:none;">              
                   <label class="col-sm-2 control-label"> Payment Date <span class="vd_red">*</span></label>
                        <div class="col-sm-3 controls">
                            <input type="text" class="form-control ignoreValidation date"   name="paymentDateStrM"  id="paymentDateStrM" >
                         </div>
                    <label class="col-sm-2 control-label">Payment Amount <span class="vd_red"></span> </label>
                      <div class="col-sm-3 controls">
                             <input type="number" step="0.01" class="form-control required" id="paymentAmount_" name="paymentAmount_"  maxlength="100">                                 
               			</div>
               			            			
              </div>
               <div class="form-group" id="pdetails1" style="display:none;">              
                 
               			 <label class="col-sm-2 control-label">Currency:<span class="vd_red"></span> </label>
               			 <div class="col-sm-3 controls">
                              <select name="currency_" id="currency_" class="form-control populate required">
                                             <option value="">-Select-</option>
                                             <c:forEach items="${currencyList}" var="data">
                                                <option value="${data}">${data}</option> 
                                             </c:forEach>
                                          </select>                                
               			</div>
               			
              </div>
               <div class="form-group" id="pstartendDt" style="display:none;">
                                       <label class="col-sm-2 control-label"> Start Date <span class="vd_red">*</span></label>
                                       <div class="col-sm-3 controls">
                                          <input type="text" class="form-control  ignoreValidation date" id="startDateStr_"
                                             name="startDateStr_"  >
                                       </div>
                                       <label class="col-sm-2 control-label"> End Date <span class="vd_red">*</span></label>
                                       <div class="col-sm-3 controls">
                                          <input type="text" class="form-control  ignoreValidation date" id="endDateStr_"
                                             name="endDateStr_"  >
                                       </div>
                                    </div>
            <div class="modal-footer background-login">
               <button type="reset" class="btn vd_btn vd_bg-grey" data-dismiss="modal">Close</button>
               <button type="submit" class="btn vd_btn vd_bg-green">Save changes</button>
            </div>
         </div>
         <!-- /.modal-content --> 
      </form>
   </div>
   <!-- /.modal-dialog --> 
</div>
<!-- /.modal --> 
<form action="${contextPath}/payments" method="GET" id="statusForm">
   <input type="hidden" name="${_csrf.parameterName}"
      value="${_csrf.token}" />
   <input type="hidden" name="paystatus" id="paystatus" />
    <input type="hidden" name="pcode" id="pcode"  value="${project.projectCodeInternal}"/>
   	
   
</form>
<form action="${contextPath}/del-payment" method="POST"
	id="delete">
	<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />
	<input type="hidden" name="id_del" id="id_del" />
	<input type="hidden" name="pcodedel" id="pcodedel"  value="${project.projectCodeInternal}"/>
	
</form>

<script> 
   function loadByPAllocationStatus(status){
   	$("#paystatus").val(status);
   	$("#statusForm").submit();
   }
   function psmodal(paymentId){		
   $("#paymentId").val(paymentId);		
   $('#myModal_statusChange').modal('show');
   }
   function editPayment(){	
 let checkState = $("#isedit").is(":checked") ? "true" : "false";
 
     $("#paymentAmount_").val("");	
   $("#paymentDateStrM").val("");
    $("#startDateStr_").val("");
     $("#endDateStr_").val("");
     $("#currency_").val("");
   if(checkState=="true"){
 
   $('#pdetails').show('show');	
   $('#pdetails1').show('show');
   $('#pstartendDt').show('show');   	
   }
   else{
    $('#pdetails').hide('show');	
    $('#pdetails1').hide('show');	
     $('#pstartendDt').hide('show');	
    
   }
   
   
   }
  function deletePay(id){
		$("#id_del").val(id);
		bootbox.confirm("Do you want to delete ?",
	    function(result) {
	        if (result == true) {
	            $("#delete").submit();
	        } 
	    });
	} 
</script>