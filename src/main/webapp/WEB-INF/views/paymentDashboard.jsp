<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<!-- Specific CSS -->
  <script src="https://cdn.canvasjs.com/canvasjs.min.js"></script>
<!-- Middle Content Start -->
<div class="vd_content-wrapper">
	<div class="vd_container">
		<div class="vd_content clearfix">
			<div class="vd_head-section clearfix">
				<div class="vd_panel-header">
					<ul class="breadcrumb">
						<li><a href="index.html">Home</a></li>
						<li class="active">Payment Dashboard</li>
					</ul>
					<div class="vd_panel-menu hidden-sm hidden-xs"
						data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code."
						data-step=5 data-position="left">
						<div data-action="remove-navbar"
							data-original-title="Remove Navigation Bar Toggle"
							data-toggle="tooltip" data-placement="bottom"
							class="remove-navbar-button menu">
							<i class="fa fa-arrows-h"></i>
						</div>
						<div data-action="remove-header"
							data-original-title="Remove Top Menu Toggle"
							data-toggle="tooltip" data-placement="bottom"
							class="remove-header-button menu">
							<i class="fa fa-arrows-v"></i>
						</div>
						<div data-action="fullscreen"
							data-original-title="Remove Navigation Bar and Top Menu Toggle"
							data-toggle="tooltip" data-placement="bottom"
							class="fullscreen-button menu">
							<i class="glyphicon glyphicon-fullscreen"></i>
						</div>

					</div>

				</div>
			</div>
			<!-- vd_head-section -->

			<div class="vd_title-section clearfix">
				<div class="vd_panel-header">
					<h1>Payment Dashboard</h1>
					<small class="subtitle">Payment Dashboard</small>
					<div class="vd_panel-menu  hidden-xs">
						<div class="menu no-bg vd_red"
							data-original-title="Start Layout Tour Guide"
							data-toggle="tooltip" data-placement="bottom"
							onClick="javascript:introJs().setOption('showBullets', false).start();">
							<span class="menu-icon font-md"><i
								class="fa fa-question-circle"></i></span>
						</div>
						<!-- menu -->

						<div class="menu">
							<div data-action="click-trigger">
								<span class="menu-icon mgr-10"><i class="fa fa-filter"></i></span>Filter
								<i class="fa fa-angle-down"></i>
							</div>
							<div class="vd_mega-menu-content width-xs-2 left-xs"
								data-action="click-target">
								<div class="child-menu">
									<div class="content-list content-menu">
										<ul class="list-wrapper pd-lr-10">
											<li><a href="${contextPath}/home">
													<div class="menu-icon">
														<i class=" fa fa-user"></i>
													</div>
													<div class="menu-text">Default Home</div>
											</a></li>
											<li><a href="${contextPath}/payment-dashboard">
													<div class="menu-icon">
														<i class=" fa fa-user"></i>
													</div>
													<div class="menu-text">Payment Home</div>
											</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<!-- menu -->
					</div>
					<!-- vd_panel-menu -->
				</div>
				<!-- vd_panel-header -->
			</div>
			<!-- vd_title-section -->

			<div class="vd_content-section clearfix">
				<div class="row">
				<div class="col-lg-3 col-md-6 col-sm-6 mgbt-xs-15">
						<div class="vd_status-widget vd_bg-yellow widget">
							<div class="vd_panel-menu">
								<div data-action="refresh" data-original-title="Refresh"
									data-rel="tooltip" class=" menu entypo-icon smaller-font">
									<i class="icon-cycle"></i>
								</div>
							</div>
							<!-- vd_panel-menu -->

							<a class="panel-body" href="#">
								<div class="clearfix">
									<span class="menu-icon"> <i class="append-icon fa fa-fw fa-money"></i>
									</span> <span class="menu-value"> 10000 </span>
								</div>
								<div class="menu-text clearfix">Total Transactions</div>
							</a>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-6 mgbt-sm-15">
						<div class="vd_status-widget vd_bg-green widget">
							<div class="vd_panel-menu">
								<div data-action="refresh" data-original-title="Refresh"
									data-rel="tooltip" class=" menu entypo-icon smaller-font">
									<i class="icon-cycle"></i>
								</div>
							</div>
							<!-- vd_panel-menu -->

							<a class="panel-body" href="#">
								<div class="clearfix">
									<span class="menu-icon"> <i class="append-icon fa fa-fw fa-money"></i>
									</span> <span class="menu-value"> 5000</span>
								</div>
								<div class="menu-text clearfix">Total Cedit</div>
							</a>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-6 mgbt-sm-15">
						<div class="vd_status-widget vd_bg-red  widget">
							<div class="vd_panel-menu">
								<div data-action="refresh" data-original-title="Refresh"
									data-rel="tooltip" class=" menu entypo-icon smaller-font">
									<i class="icon-cycle"></i>
								</div>
							</div>
							<!-- vd_panel-menu -->

							<a class="panel-body" href="#">
								<div class="clearfix">
									<span class="menu-icon"> <i class="append-icon fa fa-fw fa-money"></i>
									</span> <span class="menu-value"> 3000 </span>
								</div>
								<div class="menu-text clearfix">Total Debit</div>
							</a>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-6 mgbt-xs-15">
						<div class="vd_status-widget vd_bg-blue widget">
							<div class="vd_panel-menu">
								<div data-action="refresh" data-original-title="Refresh"
									data-rel="tooltip" class=" menu entypo-icon smaller-font">
									<i class="icon-cycle"></i>
								</div>
							</div>
							<!-- vd_panel-menu -->

							<a class="panel-body" href="#">
								<div class="clearfix">
									<span class="menu-icon"> <i class="append-icon fa fa-fw fa-money"></i>
									</span> <span class="menu-value"> 2000 </span>
								</div>
								<div class="menu-text clearfix">Profit</div>
							</a>
						</div>
					</div>
					
				</div>
				<div class="row">
					<!-- col-md-8 -->
					<div class="col-md-4">
						<div class="panel vd_transaction-widget light-widget widget">
							<div class="panel-body">
								<div class="vd_panel-menu">
									<div data-action="refresh"
										class="menu entypo-icon smaller-font" data-placement="bottom"
										data-toggle="tooltip" data-original-title="Refresh">
										<i class="icon-cycle"></i>
									</div>
									<div class="menu entypo-icon smaller-font"
										data-placement="bottom" data-toggle="tooltip"
										data-original-title="Config">
										<div data-action="click-trigger" class="menu-trigger">
											<i class="icon-cog"></i>
										</div>
										<div class="vd_mega-menu-content  width-xs-2  left-xs"
											data-action="click-target">
											<div class="child-menu">
												<div class="content-list content-menu">
													<ul class="list-wrapper pd-lr-10">
														<li><a href="#">
																<div class="menu-icon">
																	<i class=" fa fa-user"></i>
																</div>
																<div class="menu-text">Panel Menu</div>
														</a></li>
													</ul>
												</div>
											</div>
										</div>
									</div>
									<div class="menu entypo-icon" data-placement="bottom"
										data-toggle="tooltip" data-original-title="Close"
										data-action="close">
										<i class="icon-cross"></i>
									</div>
								</div>
								<!-- vd_panel-menu -->
								<h5 class="mgbt-xs-20 mgtp-20">
									<span class="menu-icon append-icon"> <i class="icon-pie"></i>
									</span> <strong>Current Week Transaction</strong>
								</h5>
								<!-- <div id="pie-chart" class="pie-chart" style="height: 388px;"></div> -->
								<div id="chartContainerPiechart" style="height: 430px; width: 100%;"></div>
								<div class="vd_info brbr">
									<h5 class="text-right font-semibold">
										<strong>TOTAL TRANSACTION</strong>
									</h5>
									<h3 class="text-right  vd_red">
										<span class="append-icon"><i class="fa fa-usd"></i></span>546,456
									</h3>
								</div>
							</div>
						</div>
					</div>
					<!-- .col-md-4 -->
					<div class="col-md-8 mgbt-md-20 mgbt-lg-0">
						<div class="panel vd_interactive-widget light-widget widget">
							<div class="panel-body-list">
								<div class="vd_panel-menu">
									<div data-action="refresh"
										class="menu entypo-icon smaller-font" data-placement="bottom"
										data-toggle="tooltip" data-original-title="Refresh">
										<i class="icon-cycle"></i>
									</div>
									<div class="menu entypo-icon smaller-font"
										data-placement="bottom" data-toggle="tooltip"
										data-original-title="Config">
										<div data-action="click-trigger" class="menu-trigger">
											<i class="icon-cog"></i>
										</div>
										<div class="vd_mega-menu-content  width-xs-2  left-xs"
											data-action="click-target">
											<div class="child-menu">
												<div class="content-list content-menu">
													<ul class="list-wrapper pd-lr-10">
														<li><a href="#">
																<div class="menu-icon">
																	<i class=" fa fa-user"></i>
																</div>
																<div class="menu-text">Panel Menu</div>
														</a></li>
													</ul>
												</div>
											</div>
										</div>
									</div>
									<div class="menu entypo-icon" data-placement="bottom"
										data-toggle="tooltip" data-original-title="Close"
										data-action="close">
										<i class="icon-cross"></i>
									</div>
								</div>
								<!-- vd_panel-menu -->

								<div class="pd-20">
									<div class="panel-body-list pd-25">
                    <h2 class="mgtp--5"> <span class="font-semibold">Revinue</span> Chart </h2>
                    <select style="width:100px;margin-left: 90%;">
                    <option value="34">Abv</option>
                    </select>
                   <!--  <div id="bar-placeholder" class="bar-chart" style="height:318px;"></div> -->
                   <div id="chartContainer" style="height: 370px; width: 100%;"></div>
                  </div>
								</div>
							</div>
						</div>
						<!-- Panel Widget -->
					</div>
				
				</div>
				<div class="row">
					<div class="col-md-7">
						<div class="row">
							<div class="col-md-12">
								<div class="tabs widget">
									<ul class="nav nav-tabs widget">
										<li class="active"><a data-toggle="tab" href="#home-tab">
												<span class="menu-icon"><i class="fa fa-comments"></i></span>
												Recent Reviews <span class="menu-active"><i
													class="fa fa-caret-up"></i></span>
										</a></li>
										<li><a data-toggle="tab" href="#posts-tab"> <span
												class="menu-icon"><i class="fa fa-rocket"></i></span>
												Activities <span class="menu-active"><i
													class="fa fa-caret-up"></i></span>
										</a></li>
										<li><a data-toggle="tab" href="#list-tab"> <span
												class="menu-icon"><i class="fa fa-user"></i></span> New
												Users <span class="menu-active"><i
													class="fa fa-caret-up"></i></span>
										</a></li>
									</ul>
									<div class="tab-content">
										<div id="home-tab" class="tab-pane active">
											<div class="content-list content-image menu-action-right">
												<div data-rel="scroll" data-scrollheight="550">
													<ul class="list-wrapper pd-lr-15">
														<li>
															<div class="menu-icon">
																<a href="#"><img alt="example image"
																	src="${contextPath}/static/sFreelance/img/avatar/avatar.jpg"></a>
															</div>
															<div class="menu-text">This product is so good that
																i manage to buy it 1 for me and 3 for my families. Lorem
																ipsum dolor sit amet, consectetur adipisicing elit, sed
																do eiusmod tempor incidtation ullamco laboris nisi ut
																aliquip ex tris.</div>
															<div class="menu-text">
																<div class="menu-info">
																	in <a href="#">Samsung Galaxy S4</a> - <span
																		class="menu-date">12 Minutes Ago </span> - <span
																		class="menu-rating vd_yellow"><i
																		class="icon-star"></i><i class="icon-star"></i><i
																		class="icon-star"></i><i class="icon-star"></i><i
																		class="icon-star"></i></span>

																</div>
															</div>
															<div class="menu-action">
																<div class="menu-action-icon vd_green"
																	data-original-title="Approve" data-rel="tooltip-bottom">
																	<i class="fa fa-check"></i>
																</div>
																<div class="menu-action-icon vd_red"
																	data-original-title="Reject" data-rel="tooltip-bottom">
																	<i class="fa fa-times"></i>
																</div>
															</div>
														</li>
														<li>
															<div class="menu-icon">
																<img alt="example image" src="${contextPath}/static/sFreelance/img/avatar/avatar-2.jpg">
															</div>
															<div class="menu-text">Lorem ipsum dolor sit amet,
																consectetur adipisicing elit, sed do eiusmod tempor
																incidtation ullamco.</div>
															<div class="menu-text">
																<div class="menu-info">
																	in <a href="#">LG G2</a> - <span class="menu-date">12
																		Minutes Ago </span> - <span class="menu-rating vd_yellow"><i
																		class="icon-star"></i><i class="icon-star"></i><i
																		class="icon-star"></i><i class="icon-star"></i></span>

																</div>
															</div>
															<div class="menu-action">
																<div class="menu-action-icon vd_green"
																	data-original-title="Approve" data-rel="tooltip-bottom">
																	<i class="fa fa-check"></i>
																</div>
																<div class="menu-action-icon vd_red"
																	data-original-title="Reject" data-rel="tooltip-bottom">
																	<i class="fa fa-times"></i>
																</div>
															</div>
														</li>
														<li>
															<div class="menu-icon">
																<img alt="example image" src="${contextPath}/static/sFreelance/img/avatar/avatar-3.jpg">
															</div>
															<div class="menu-text">Lorem ipsum dolor sit amet,
																consectetur adipisicing elit, sed do eiusmod tempor
																incidtation ullamco. Consectetur adipisicing elit, sed
																do eiusmod. Lorem ipsum dolor sit amet, consectetur
																adipisicing elit, sed do eiusmod tempor incidtation
																ullamco.</div>
															<div class="menu-text">
																<div class="menu-info">
																	in <a href="#">Sony Experia Z10</a> - <span
																		class="menu-date">12 Minutes Ago </span> - <span
																		class="menu-rating vd_yellow"><i
																		class="icon-star"></i></span>

																</div>
															</div>
															<div class="menu-action">
																<div class="menu-action-icon vd_green"
																	data-original-title="Approve" data-rel="tooltip-bottom">
																	<i class="fa fa-check"></i>
																</div>
																<div class="menu-action-icon vd_red"
																	data-original-title="Reject" data-rel="tooltip-bottom">
																	<i class="fa fa-times"></i>
																</div>
															</div>
														</li>
														<li>
															<div class="menu-icon">
																<img alt="example image" src="${contextPath}/static/sFreelance/img/avatar/avatar-4.jpg">
															</div>
															<div class="menu-text">Lorem ipsum dolor sit amet,
																consectetur adipisicing elit, sed do eiusmod tempor
																incidtation ullamco. Ipsum dolor sit amet, consectetur
																adipisicing elit !!!</div>
															<div class="menu-text">
																<div class="menu-info">
																	in <a href="#">Nokia Lumia</a> - <span
																		class="menu-date">12 Minutes Ago </span> - <span
																		class="menu-rating vd_yellow"><i
																		class="icon-star"></i><i class="icon-star"></i><i
																		class="icon-star"></i><i class="icon-star"></i></span>

																</div>
															</div>
															<div class="menu-action">
																<div class="menu-action-icon vd_green"
																	data-original-title="Approve" data-rel="tooltip-bottom">
																	<i class="fa fa-check"></i>
																</div>
																<div class="menu-action-icon vd_red"
																	data-original-title="Reject" data-rel="tooltip-bottom">
																	<i class="fa fa-times"></i>
																</div>
															</div>
														</li>
														<li>
															<div class="menu-icon">
																<img alt="example image" src="${contextPath}/static/sFreelance/img/avatar/avatar-5.jpg">
															</div>
															<div class="menu-text">Lorem ipsum dolor sit amet,
																consectetur adipisicing elit.</div>
															<div class="menu-text">
																<div class="menu-info">
																	in <a href="#">Samsung Galaxy Note 8</a> - <span
																		class="menu-date">12 Minutes Ago </span> - <span
																		class="menu-rating vd_yellow"><i
																		class="icon-star"></i><i class="icon-star"></i></span>

																</div>
															</div>
															<div class="menu-action">
																<div class="menu-action-icon vd_green"
																	data-original-title="Approve" data-rel="tooltip-bottom">
																	<i class="fa fa-check"></i>
																</div>
																<div class="menu-action-icon vd_red"
																	data-original-title="Reject" data-rel="tooltip-bottom">
																	<i class="fa fa-times"></i>
																</div>
															</div>
														</li>
														<li>
															<div class="menu-icon">
																<img alt="example image" src="${contextPath}/static/sFreelance/img/avatar/avatar-6.jpg">
															</div>
															<div class="menu-text">Lorem ipsum dolor sit amet,
																consectetur adipisicing elit.</div>
															<div class="menu-text">
																<div class="menu-info">
																	in <a href="#">LG L3</a> - <span class="menu-date">12
																		Minutes Ago </span> - <span class="menu-rating vd_yellow"><i
																		class="icon-star"></i><i class="icon-star"></i></span>

																</div>
															</div>
															<div class="menu-action">
																<div class="menu-action-icon vd_green"
																	data-original-title="Approve" data-rel="tooltip-bottom">
																	<i class="fa fa-check"></i>
																</div>
																<div class="menu-action-icon vd_red"
																	data-original-title="Reject" data-rel="tooltip-bottom">
																	<i class="fa fa-times"></i>
																</div>
															</div>
														</li>
														<li>
															<div class="menu-icon">
																<img alt="example image" src="${contextPath}/static/sFreelance/img/avatar/avatar-7.jpg">
															</div>
															<div class="menu-text">Lorem ipsum dolor sit amet,
																consectetur adipisicing elit consectetur adipisicing
																elit xorem ipsum dolor sit amet, consectetur adipisicing
																elit.</div>
															<div class="menu-text">
																<div class="menu-info">
																	in <a href="#">Motorola Moto-X</a> - <span
																		class="menu-date">12 Minutes Ago </span> - <span
																		class="menu-rating vd_yellow"><i
																		class="icon-star"></i><i class="icon-star"></i></span>

																</div>
															</div>
															<div class="menu-action">
																<div class="menu-action-icon vd_green"
																	data-original-title="Approve" data-rel="tooltip-bottom">
																	<i class="fa fa-check"></i>
																</div>
																<div class="menu-action-icon vd_red"
																	data-original-title="Reject" data-rel="tooltip-bottom">
																	<i class="fa fa-times"></i>
																</div>
															</div>
														</li>
														<li>
															<div class="menu-icon">
																<img alt="example image" src="${contextPath}/static/sFreelance/img/avatar/avatar-8.jpg">
															</div>
															<div class="menu-text">Lorem ipsum dolor sit amet,
																consectetur adipisicing elit xorem ipsum dolor sit amet,
																consectetur adipisicing elit.</div>
															<div class="menu-text">
																<div class="menu-info">
																	in <a href="#">Monitor Asus</a> - <span
																		class="menu-date">12 Minutes Ago </span> - <span
																		class="menu-rating vd_yellow"><i
																		class="icon-star"></i><i class="icon-star"></i><i
																		class="icon-star"></i><i class="icon-star"></i></span>

																</div>
															</div>
															<div class="menu-action">
																<div class="menu-action-icon vd_green"
																	data-original-title="Approve" data-rel="tooltip-bottom">
																	<i class="fa fa-check"></i>
																</div>
																<div class="menu-action-icon vd_red"
																	data-original-title="Reject" data-rel="tooltip-bottom">
																	<i class="fa fa-times"></i>
																</div>
															</div>
														</li>
													</ul>
												</div>
												<div class="closing text-center">
													<a href="#">See All Recent Reviews <i
														class="fa fa-angle-double-right"></i></a>
												</div>
											</div>

										</div>
										<div id="posts-tab" class="tab-pane sidebar-widget">
											<div class="content-list">
												<div data-rel="scroll">
													<ul class="list-wrapper pd-lr-15">
														<li><a href="#">
																<div class="menu-icon vd_yellow">
																	<i class="fa fa-suitcase"></i>
																</div>
																<div class="menu-text">
																	Someone has give you a surprise
																	<div class="menu-info">
																		<span class="menu-date"> ~ 12 Minutes Ago</span>
																	</div>
																</div>
														</a></li>
														<li><a href="#">
																<div class="menu-icon vd_blue">
																	<i class=" fa fa-user"></i>
																</div>
																<div class="menu-text">
																	Change your user profile details
																	<div class="menu-info">
																		<span class="menu-date"> ~ 1 Hour 20 Minutes
																			Ago</span>
																	</div>
																</div>
														</a></li>
														<li><a href="#">
																<div class="menu-icon vd_red">
																	<i class=" fa fa-cogs"></i>
																</div>
																<div class="menu-text">
																	Your setting is updated
																	<div class="menu-info">
																		<span class="menu-date"> ~ 12 Days Ago</span>
																	</div>
																</div>
														</a></li>
														<li><a href="#">
																<div class="menu-icon vd_green">
																	<i class=" fa fa-book"></i>
																</div>
																<div class="menu-text">
																	Added new article
																	<div class="menu-info">
																		<span class="menu-date"> ~ 19 Days Ago</span>
																	</div>
																</div>
														</a></li>
														<li><a href="#">
																<div class="menu-icon vd_green">
																	<img alt="example image" src="${contextPath}/static/sFreelance/img/avatar/avatar.jpg">
																</div>
																<div class="menu-text">
																	Change Profile Pic
																	<div class="menu-info">
																		<span class="menu-date"> ~ 20 Days Ago</span>
																	</div>
																</div>
														</a></li>
														<li><a href="#">
																<div class="menu-icon vd_red">
																	<i class=" fa fa-cogs"></i>
																</div>
																<div class="menu-text">
																	Your setting is updated
																	<div class="menu-info">
																		<span class="menu-date"> ~ 12 Days Ago</span>
																	</div>
																</div>
														</a></li>
														<li><a href="#">
																<div class="menu-icon vd_green">
																	<i class=" fa fa-book"></i>
																</div>
																<div class="menu-text">
																	Added new article
																	<div class="menu-info">
																		<span class="menu-date"> ~ 19 Days Ago</span>
																	</div>
																</div>
														</a></li>
														<li><a href="#">
																<div class="menu-icon vd_green">
																	<img alt="example image" src="${contextPath}/static/sFreelance/img/avatar/avatar.jpg">
																</div>
																<div class="menu-text">
																	Change Profile Pic
																	<div class="menu-info">
																		<span class="menu-date"> ~ 20 Days Ago</span>
																	</div>
																</div>
														</a></li>

													</ul>
												</div>
												<div class="closing text-center" style="">
													<a href="#">See All Activities <i
														class="fa fa-angle-double-right"></i></a>
												</div>
											</div>
										</div>
										<div id="list-tab" class="tab-pane">
											<div class="content-grid column-xs-2 column-sm-6 height-xs-3">
												<div data-rel="scroll">
													<ul class="list-wrapper">
														<li><a href="#">
																<div class="menu-icon width-50">
																	<img alt="example image" src="${contextPath}/static/sFreelance/img/avatar/avatar.jpg">
																</div>
														</a>
															<div class="menu-text">
																Gabriella Montagna
																<div class="menu-info">
																	<div class="menu-date">San Diego</div>
																	<div class="menu-action">
																		<span class="menu-action-icon vd_green vd_bd-green"
																			data-original-title="Approve" data-toggle="tooltip"
																			data-placement="bottom"> <i
																			class="fa fa-check"></i>
																		</span> <span class="menu-action-icon vd_red vd_bd-red"
																			data-original-title="Reject" data-toggle="tooltip"
																			data-placement="bottom"> <i
																			class="fa fa-times"></i>
																		</span>
																	</div>
																</div>
															</div></li>
														<li><a href="#">
																<div class="menu-icon width-50">
																	<img alt="example image" src="${contextPath}/static/sFreelance/img/avatar/avatar-2.jpg">
																</div>
														</a>
															<div class="menu-text">
																Jonathan Fuzzy
																<div class="menu-info">
																	<div class="menu-date">Seattle</div>
																	<div class="menu-action">
																		<span class="menu-action-icon vd_green vd_bd-green"
																			data-original-title="Approve" data-toggle="tooltip"
																			data-placement="bottom"> <i
																			class="fa fa-check"></i>
																		</span> <span class="menu-action-icon vd_red vd_bd-red"
																			data-original-title="Reject" data-toggle="tooltip"
																			data-placement="bottom"> <i
																			class="fa fa-times"></i>
																		</span>
																	</div>
																</div>
															</div></li>
														<li><a href="#">
																<div class="menu-icon width-50">
																	<img alt="example image" src="${contextPath}/static/sFreelance/img/avatar/avatar-3.jpg">
																</div>
														</a>
															<div class="menu-text">
																Sakura Hinata
																<div class="menu-info">
																	<div class="menu-date">Hawaii</div>
																	<div class="menu-action">
																		<span class="menu-action-icon vd_green vd_bd-green"
																			data-original-title="Approve" data-toggle="tooltip"
																			data-placement="bottom"> <i
																			class="fa fa-check"></i>
																		</span> <span class="menu-action-icon vd_red vd_bd-red"
																			data-original-title="Reject" data-toggle="tooltip"
																			data-placement="bottom"> <i
																			class="fa fa-times"></i>
																		</span>
																	</div>
																</div>
															</div></li>
														<li><a href="#">
																<div class="menu-icon width-50">
																	<img alt="example image" src="${contextPath}/static/sFreelance/img/avatar/avatar-4.jpg">
																</div>
														</a>
															<div class="menu-text">
																Rikudou Sennin
																<div class="menu-info">
																	<div class="menu-date">Las Vegas</div>
																	<div class="menu-action">
																		<span class="menu-action-icon vd_green vd_bd-green"
																			data-original-title="Approve" data-toggle="tooltip"
																			data-placement="bottom"> <i
																			class="fa fa-check"></i>
																		</span> <span class="menu-action-icon vd_red vd_bd-red"
																			data-original-title="Reject" data-toggle="tooltip"
																			data-placement="bottom"> <i
																			class="fa fa-times"></i>
																		</span>
																	</div>
																</div>
															</div></li>
														<li><a href="#">
																<div class="menu-icon width-50">
																	<img alt="example image" src="img/avatar/avatar-5.jpg">
																</div>
														</a>
															<div class="menu-text">
																Kim Kardiosun
																<div class="menu-info">
																	<div class="menu-date">New York</div>
																	<div class="menu-action">
																		<span class="menu-action-icon vd_green vd_bd-green"
																			data-original-title="Approve" data-toggle="tooltip"
																			data-placement="bottom"> <i
																			class="fa fa-check"></i>
																		</span> <span class="menu-action-icon vd_red vd_bd-red"
																			data-original-title="Reject" data-toggle="tooltip"
																			data-placement="bottom"> <i
																			class="fa fa-times"></i>
																		</span>
																	</div>
																</div>
															</div></li>
														<li><a href="#">
																<div class="menu-icon width-50">
																	<img alt="example image" src="img/avatar/avatar-6.jpg">
																</div>
														</a>
															<div class="menu-text">
																Brad Pita
																<div class="menu-info">
																	<div class="menu-date">Seattle</div>
																	<div class="menu-action">
																		<span class="menu-action-icon vd_green vd_bd-green"
																			data-original-title="Approve" data-toggle="tooltip"
																			data-placement="bottom"> <i
																			class="fa fa-check"></i>
																		</span> <span class="menu-action-icon vd_red vd_bd-red"
																			data-original-title="Reject" data-toggle="tooltip"
																			data-placement="bottom"> <i
																			class="fa fa-times"></i>
																		</span>
																	</div>
																</div>
															</div></li>
														<li><a href="#">
																<div class="menu-icon width-50">
																	<img alt="example image" src="img/avatar/avatar-7.jpg">
																</div>
														</a>
															<div class="menu-text">
																Celline Dior
																<div class="menu-info">
																	<div class="menu-date">Los Angeles</div>
																	<div class="menu-action">
																		<span class="menu-action-icon vd_green vd_bd-green"
																			data-original-title="Approve" data-toggle="tooltip"
																			data-placement="bottom"> <i
																			class="fa fa-check"></i>
																		</span> <span class="menu-action-icon vd_red vd_bd-red"
																			data-original-title="Reject" data-toggle="tooltip"
																			data-placement="bottom"> <i
																			class="fa fa-times"></i>
																		</span>
																	</div>
																</div>
															</div></li>
														<li><a href="#">
																<div class="menu-icon width-50">
																	<img alt="example image" src="img/avatar/avatar-8.jpg">
																</div>
														</a>
															<div class="menu-text">
																Goerge Bruno Marz
																<div class="menu-info">
																	<div class="menu-date">Las Vegas</div>
																	<div class="menu-action">
																		<span class="menu-action-icon vd_green vd_bd-green"
																			data-original-title="Approve" data-toggle="tooltip"
																			data-placement="bottom"> <i
																			class="fa fa-check"></i>
																		</span> <span class="menu-action-icon vd_red vd_bd-red"
																			data-original-title="Reject" data-toggle="tooltip"
																			data-placement="bottom"> <i
																			class="fa fa-times"></i>
																		</span>
																	</div>
																</div>
															</div></li>

													</ul>
												</div>
												<div class="closing text-center">
													<a href="#">See All New Users <i
														class="fa fa-angle-double-right"></i></a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- tabs-widget -->
							</div>
							<!-- col-md-12 -->
						</div>
						<!-- row -->

					</div>
					<!-- col-md-6 -->
					<div class="col-md-5">
						<div class="panel widget">
							<div class="panel-body-list  table-responsive">
								<table class="table no-head-border table-striped">
									<thead class="vd_bg-blue vd_white">
										<tr>
											<th><i class="fa fa-search append-icon"></i>Last 5
												Search Terms</th>
											<th style="width: 20px">Results</th>
											<th style="width: 140px">Number of Uses</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Pan</td>
											<td class="text-center">4</td>
											<td class="text-center">20</td>
										</tr>
										<tr>
											<td>Fork</td>
											<td class="text-center">1</td>
											<td class="text-center">5</td>
										</tr>
										<tr>
											<td>Big Bowl</td>
											<td class="text-center">7</td>
											<td class="text-center">23</td>
										</tr>
										<tr>
											<td>Cooking Utensils</td>
											<td class="text-center">2</td>
											<td class="text-center">1</td>
										</tr>
										<tr>
											<td>Ketchup Bottle</td>
											<td class="text-center">3</td>
											<td class="text-center">12</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="panel widget">
							<div class="panel-body-list  table-responsive">
								<table class="table no-head-border table-striped">
									<thead class="vd_bg-blue vd_white">
										<tr>
											<th><i class="fa fa-search append-icon"></i>Top 5 Search
												Terms</th>
											<th style="width: 20px">Results</th>
											<th style="width: 140px">Number of Uses</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Pan</td>
											<td class="text-center">4</td>
											<td class="text-center">20</td>
										</tr>
										<tr>
											<td>Fork</td>
											<td class="text-center">1</td>
											<td class="text-center">5</td>
										</tr>
										<tr>
											<td>Big Bowl</td>
											<td class="text-center">7</td>
											<td class="text-center">23</td>
										</tr>
										<tr>
											<td>Cooking Utensils</td>
											<td class="text-center">2</td>
											<td class="text-center">1</td>
										</tr>
										<tr>
											<td>Ketchup Bottle</td>
											<td class="text-center">3</td>
											<td class="text-center">12</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- col-md-6 -->

				</div>
				<!-- .row -->
			</div>
			<!-- .vd_content-section -->



		</div>
		<!-- .vd_content -->
	</div>
	<!-- .vd_container -->
</div>
<!-- .vd_content-wrapper -->

<!-- Middle Content End -->

<!-- Specific Page Scripts END -->
<script type="text/javascript" src="${contextPath}/static/sFreelance/js/jquery.js"></script> 



<script type="text/javascript">


window.onload = function () {

var chart = new CanvasJS.Chart("chartContainer", {
	animationEnabled: true,
	title:{
		text: "Monthly Transaction, 2016"
	},	
	axisY: {
		title: "Billions of Barrels",
		titleFontColor: "#4F81BC",
		lineColor: "#4F81BC",
		labelFontColor: "#4F81BC",
		tickColor: "#4F81BC"
	},
	axisY2: {
		title: "Millions of Barrels/day",
		titleFontColor: "#C0504E",
		lineColor: "#C0504E",
		labelFontColor: "#C0504E",
		tickColor: "#C0504E"
	},	
	toolTip: {
		shared: true
	},
	legend: {
		cursor:"pointer",
		itemclick: toggleDataSeries
	},
	data: [{
		type: "column",
		name: "Credit",
		legendText: "Proven Oil Reserves",
		showInLegend: true, 
		dataPoints:[
			{ label: "1st", y: 100 },
			{ label: "2nd", y: 302.25 },
			{ label: "3rd", y: 157.20 },
			{ label: "4th", y: 148.77 },
		]
	},
	{
		type: "column",	
		name: "Debit",
		legendText: "Oil Production",
		axisYType: "secondary",
		showInLegend: true,
		dataPoints:[
			{ label: "1st", y: 80 },
			{ label: "2nd", y: 302.25 },
			{ label: "3rd", y: 157.20 },
			{ label: "4th", y: 148.77 },
		]
	}
	,
	{
		type: "column",	
		name: "Profit/Loss",
		legendText: "Oil Production",
		axisYType: "secondary",
		showInLegend: true,
		dataPoints:[
			{ label: "1st", y: 20 },
			{ label: "2nd", y: 302.25 },
			{ label: "3rd", y: 157.20 },
			{ label: "4th", y: 148.77 },
		]
	}
	]
});
chart.render();

function toggleDataSeries(e) {
	if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		e.dataSeries.visible = false;
	}
	else {
		e.dataSeries.visible = true;
	}
	chart.render();
}

var chartPiechart = new CanvasJS.Chart("chartContainerPiechart", {
	exportEnabled: true,
	animationEnabled: true,
	title:{
		text: "State Operating Funds"
	},
	legend:{
		cursor: "pointer",
		itemclick: explodePiechart
	},
	data: [{
		type: "pie",
		showInLegend: true,
		toolTipContent: "{name}: <strong>{y}%</strong>",
		indexLabel: "{name} - {y}%",
		dataPoints: [
			{ y: 26, name: "School Aid", exploded: true },
			{ y: 20, name: "Medical Aid" },
			{ y: 5, name: "Debt/Capital" },
			{ y: 3, name: "Elected Officials" },
			{ y: 7, name: "University" },
			{ y: 17, name: "Executive" },
			{ y: 22, name: "Other Local Assistance"}
		]
	}]
});
chartPiechart.render();

function explodePiechart (e) {
	if(typeof (e.dataSeries.dataPoints[e.dataPointIndex].exploded) === "undefined" || !e.dataSeries.dataPoints[e.dataPointIndex].exploded) {
		e.dataSeries.dataPoints[e.dataPointIndex].exploded = true;
	} else {
		e.dataSeries.dataPoints[e.dataPointIndex].exploded = false;
	}
	e.chartPiechart.render();

}

}


</script>

