<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<!-- Middle Content Start -->    
<div class="vd_content-wrapper">
<div class="vd_container">
<div class="vd_content clearfix">
   <div class="vd_head-section clearfix">
      <div class="vd_panel-header">
         <ul class="breadcrumb">
            <li><a href="${contextPath}/">Home</a> </li>
            <li><a href="${contextPath}/">Contract Board</a> </li>
            <li class="active"> ${not empty contract.getId() ?'Modify Contract':'Create Contract'}</li>
         </ul>
         <div class="vd_title-section clearfix">
            <div class="vd_panel-header">
               <h1> ${not empty contract.getId() ?'Modify Contract':'Create Contract'}</h1>
               <small class="subtitle">Fill the below form ('*' for Mandetory Field)</small>
            </div>
         </div>
         <!-- vd_title-section -->
         <div class="vd_content-section clearfix">
            <div class="row" id="advanced-input">
               <div class="col-md-12">
                  <div class="panel widget">
                     <div class="alert alert-danger vd_hidden">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-cross"></i></button>
                        <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Oh snap!</strong> Change a few things up and try submitting again. 
                     </div>
                     <div class="alert alert-success vd_hidden">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-cross"></i></button>
                        <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Well done!</strong>. 
                     </div>
                     <div class="panel-heading vd_bg-grey">
                        <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-bar-chart-o"></i> </span> Contract Details </h3>
                     </div>
                     <div class="panel-body">
                        <form class="form-horizontal"  action="${contextPath}/contract" id="register-form" method="post" modelAttribute="contract" enctype="multipart/form-data">
                           <input type="hidden" name="${_csrf.parameterName}"
                              value="${_csrf.token}" />
                           <input type="hidden" name="Id" value="${contract.getId()}" />
                           <div class="form-group">
                              <label class="col-sm-2 control-label">Contract Type <span class="vd_red">*</span> </label>
                              <div class="col-sm-3 controls">
                                 <select   class="form-control populate required" id="contractType_" >
                                    <option value="Upload">Upload</option>
                                 </select>
                              </div>
                           </div>
                           <c:choose>
                              <c:when test="${not empty contract.getId()}">
                                 <div class="form-group">
                                    <label class="col-sm-2 control-label">Project  <span class="vd_red">*</span> </label>
                                    <div class="col-sm-3 controls">
                                       <input type="hidden" sclass="form-control" name="contract.project.Id" id="contractprojectId" value="${contract.project.getId()}"
                                          maxlength="100">
                                       <label class="col-sm-2 control-label">${contract.project.projectName} (${contract.project.projectCode}) </label>
                                    </div>
                                 </div>
                              </c:when>
                              <c:otherwise>
                                 <div class="form-group">
                                    <label class="col-sm-2 control-label">Clients <span class="vd_red">*</span> </label>
                                    <div class="col-sm-3 controls">
                                       <select name="project.Id"  class="form-control populate required" onChange="selectOther(this.value);" ${not empty contract.getId() ?'readonly':''}>
                                       <option value="">-Select-</option>
                                       <option ${not empty contract.otherFfnotProject?'selected':''} value="99999">Other</option>
                                       <c:forEach items="${projectList}" var="data">
                                          <option value="${data.getId()}">${data.projectCode}-${data.projectName}</option>
                                       </c:forEach>
                                       </select>
                                    </div>
                                    <div class="col-sm-3 controls">
                                       <label  class="col-sm-2 control-label"></label>
                                       <input type="text" class="form-control" style="display:${not empty contract.otherFfnotProject?'block':'none'}" id="otherFfnotProject" name="otherFfnotProject" value="${contract.otherFfnotProject}" ${not empty contract.otherFfnotProject?'readonly':''} maxlength="20" >
                                    </div>
                                 </div>
                              </c:otherwise>
                           </c:choose>
                           <div class="form-group">
                              <label class="col-sm-2 control-label">Contract Code <span class="vd_red">*</span></label>
                              <div class="col-sm-3 controls">
                                 <label class="col-sm-2 control-label"></label>
                                 <input type="text" class="form-control required" id="contarctCode" name="contarctCode" value="${contract.contarctCode}" ${not empty contract.contarctCode?'readonly':''}  class="width-70" maxlength="500">
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-sm-2 control-label">Contract Name <span class="vd_red">*</span></label>
                              <div class="col-sm-3 controls">
                                 <label  class="col-sm-2 control-label"></label>
                                 <input type="text" class="form-control required" id="contarctName" name="contarctName" value="${contract.contarctName}" ${not empty contract.contarctName?'readonly':''} maxlength="20" >
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-sm-2 control-label">Contract Details <span class="vd_red">*</span> </label>
                              <div class="col-sm-7 controls">                       
                                 <input id="file" type="file" name="file" class="form-control required">	
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-sm-2 control-label">Remarks <span class="vd_red">*</span> </label>
                              <div class="col-sm-7 controls">
                                 <textarea  rows="2" cols="20" id="remarks" name="remarks" 
                                    tabindex="2" class="form-control required" maxlength="500">${contract.remarks}</textarea>
                              </div>
                           </div>
                           <div class="form-group form-actions">
                           <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSfIMOgftMR5jDDlws-WZ5EhyH0HK79dg1ly4mLsdfGVuOowHA/viewform?embedded=true" width="640" height="1404" frameborder="0" marginheight="0" marginwidth="0">Loading…</iframe>
                              <div class="col-sm-4"> </div>
                              <div class="col-sm-7">
                                 <button class="btn vd_btn vd_bg-green vd_white" id="btn_submit" type="submit"><i class="icon-ok"></i> ${not empty contract.getId() ?'Update':'Save'}</button>
                                 <a href="${contextPath}/contracts" class="btn vd_btn vd_bg-grey" type="button" >Back</a>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
                  <!-- Panel Widget --> 
               </div>
               <!-- col-md-12 --> 
            </div>
            <!-- row -->
         </div>
         <!-- .vd_content-section --> 
      </div>
      <!-- .vd_content --> 
   </div>
   <!-- .vd_container --> 
</div>
<!-- .vd_content-wrapper --> 
<!-- Middle Content End --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script type="text/javascript" src="${contextPath}/static/sFreelance/js/jquery.js"></script> 

<script type="text/javascript">
   $(document).ready(function() {
   		"use strict";	
   
           var form_register = $('#register-form');
           var error_register = $('.alert-danger', form_register);
           var success_register = $('.alert-success', form_register);
   
           form_register.validate({
               errorElement: 'div', //default input error message container
               errorClass: 'vd_red', // default input error message class
               focusInvalid: false, // do not focus the last invalid input
               ignore: "",
               rules: {
                   contarctCode: {
                       required: true,
                   }
                  
               },
   			
   
             	errorPlacement: function(error, element) {
   				if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
   					element.parent().append(error);
   				} else if (element.parent().hasClass("vd_input-wrapper")){
   					error.insertAfter(element.parent());
   				}else {
   					error.insertAfter(element);
   				}
   			}, 
   			
               invalidHandler: function (event, validator) { //display error alert on form submit              
   					success_register.fadeOut();
   					error_register.fadeIn();
   					scrollTo(form_register,-100);
   
               },
   
               highlight: function (element) { // hightlight error inputs
   		
   				$(element).addClass('vd_bd-red');
   				$(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');
   
               },
   
               unhighlight: function (element) { // revert the change dony by hightlight
                   $(element)
                       .closest('.control-group').removeClass('error'); // set error class to the control group
               },
   
               success: function (label, element) {
                   label
                       .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                   	.closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
   				$(element).removeClass('vd_bd-red');
   				
               },
   
               submitHandler: function (form) {
   					success_register.fadeIn();
   					error_register.fadeOut();
   					scrollTo(form_register,-100);	
   					$('#register-form').submit();				
               }
           });
   });
</script>
<script type="text/javascript">
   function selectOther(id){
   if(id=="99999"){
   $("#otherFfnotProject").addClass('required');
   $("#otherFfnotProject").show(); 
   		
   }
   else
   {
   $("#otherFfnotProject").removeClass('required');
   $("#otherFfnotProject").hide();
    
   }
   }	
   
   
</script>