<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript">
   $("#btnPrint").live("click", function () {
          $("#dv_btns").hide();
       var divContents = $("#dvContainer").html();
       // var id_header_ = $("#id_header").html();
       
     
       var printWindow = window.open('', '', 'height=500,width=1250');
       printWindow.document.write('<html>');
      // printWindow.document.write(id_header_);
       printWindow.document.write('<body onmouseover="window.print();window.close();" >');
       printWindow.document.write(divContents);
       printWindow.document.write('</body></html>');
       printWindow.document.close();
       console.log(printWindow);
       //printWindow.print();
       $("#dv_btns").show();
   });
</script>
<!-- Middle Content Start  contract contractContent contarctName contarctCode-->
<div class="vd_content-wrapper">
<div class="vd_container">
   <div class="vd_content clearfix">
      <div class="vd_head-section clearfix">
         <div class="vd_panel-header">
            <ul class="breadcrumb">
               <li><a href="index.html">Home</a> </li>
               <li><a href="pages-custom-product.html">Pages</a> </li>
               <li class="active">Invoice</li>
            </ul>
            <div class="vd_panel-menu hidden-sm hidden-xs" data-intro="<strong>Expand Control</strong><br/>To expand content page horizontally, vertically, or Both. If you just need one button just simply remove the other button code." data-step=5  data-position="left">
               <div data-action="remove-navbar" data-original-title="Remove Navigation Bar Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
               <div data-action="remove-header" data-original-title="Remove Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
               <div data-action="fullscreen" data-original-title="Remove Navigation Bar and Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>
            </div>
         </div>
      </div>
      <div class="vd_title-section clearfix">
         <div class="vd_panel-header no-subtitle">
            <h1>Contract</h1>
         </div>
      </div>
      <div class="vd_content-section clearfix">
         <div class="row" id="dvContainer">
            <div class="col-sm-9">
               <div class="panel widget light-widget">
                  <div class="panel-body" style="padding:40px;">
                     <div class="pull-right text-right">
                        <h3 class="font-semibold mgbt-xs-20">CONTRACT</h3>
                        <table class="table table-bordered">
                           <tr>
                              <th>Contract Code</th>
                              <th>Contarct Name</th>
                           </tr>
                           <tr>
                              <td>${contract.contarctCode}</td>
                              <td>${contract.contarctName}</td>
                           </tr>
                        </table>
                     </div>
                     <div class="mgbt-xs-20"><img alt="logo" src="${contextPath}/static/sFreelance/img/logo.png" /></div>
                     <hr/>
                     <br/>
                     <div class="pd-25">
                        <div class="row">
                           <div class="col-xs-12">
                              ${contract.contractContent}
                           </div>
                        </div>
                        <table class="table table-condensed table-striped">
                           <thead>
                           </thead>
                           <tbody>
                           </tbody>
                           <tfoot>
                              <tr>
                                 <th colspan="2">Thank you for your business. Please remit the total amount due within 30 days.</th>
                              </tr>
                           </tfoot>
                        </table>
                     </div>
                     <!-- panel-body --> 
                  </div>
                  <!-- Panel Widget --> 
               </div>
               <!-- col-sm-9-->
               <div class="col-sm-3" id="dv_btns">
                  <div class="mgbt-xs-5">
                     <button class="btn vd_btn vd_bg-grey" type="button" id="btnPrint"><i class="fa fa-print append-icon"></i>Print</button>
                  </div>
                  <div class="mgbt-xs-5">
                     <a href="${contextPath}/contracts" class="btn vd_btn" type="button" id="btnBack">Back</a>
                  </div>
               </div>
            </div>
            <!-- row --> 
         </div>
         <!-- .vd_content-section --> 
      </div>
      <!-- .vd_content --> 
   </div>
   <!-- .vd_container --> 
</div>
<!-- .vd_content-wrapper --> 
<!-- Middle Content End -->
