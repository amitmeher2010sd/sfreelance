<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<!-- Middle Content Start append-icon fa fa-fw fa-eye	-->
<div class="vd_content-wrapper">
   <div class="vd_container">
      <div class="vd_content clearfix">
         <div class="vd_head-section clearfix">
            <div class="vd_panel-header">
               <ul class="breadcrumb">
                  <li><a href="index.html">Home</a> </li>
                  <li><a href="${contextPath}/">Contract Board</a> </li>
                  <li class="active">Manage Roles</li>
               </ul>
            </div>
         </div>
         <div class="vd_title-section clearfix">
            <div class="vd_panel-header">
               <h1>Manage Contract</h1>
               <small class="subtitle">Look <a href="http://www.datatables.net">below</a> for more information</small> 
            </div>
         </div>
         <div class="vd_content-section clearfix">
            <div class="row">
               <div class="col-md-12">
                  <div class="panel widget">
                     <c:if test="${not empty successMessage}">
                        <div class="alert alert-success alert-dismissable">
                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cross"></i></button>
                           <i class="fa fa-check-circle append-icon"></i><strong>Well done!</strong> ${successMessage} 
                        </div>
                     </c:if>
                     <c:if test="${not empty failureMessage}">
                        <div class="alert alert-danger alert-dismissable alert-condensed">
                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="icon-cross"></i></button>
                           <i class="fa fa-exclamation-circle append-icon"></i>
                           <strong>Oh snap!</strong> ${failureMessage} 
                        </div>
                     </c:if>
                     <div class="panel-heading vd_bg-grey">
                        <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span>List Of Contracts</h3>
                     </div>
                     <div class="panel-body table-responsive">
                        <table class="table table-striped" id="data-tables">
                           <thead>
                              <tr>
                                  <th>#</th>
                                 <th>Project Details</th>
                                 <th>Contract Code</th>
                                 <th>Contract Name</th>
                                 <th>Sign1</th>
                                 <th>Sign2</th>
                                 <th>Final Contract</th>
                                 <th>Remarks</th>
                                 <th>Contract Status</th>
                                 <th>created On</th>
                                 <th>Updated On</th>
                                 <th>Status</th>
                                 <c:if test="${userRole.roleId.roleName eq 'ADMIN'}">
                                    <th>Action</th>
                                 </c:if>
                                 <th>Mail</th>
                              </tr>
                              
                           </thead>
                           <tbody id="tbd">
                              <c:forEach items="${responseList}" var="data" varStatus="serialNumber">
                                 <tr>
                                    <td>${serialNumber.count}</td>
                                    <td>${data.project.projectCode}${not empty data.project.projectName?'-:':''}${data.project.projectName} ${not empty data.otherFfnotProject?'Other:':''}  ${data.otherFfnotProject}</td>
                                    <td>${data.contarctCode}</td>
                                    <td>${data.contarctName}</td>
                                    <td>
                                       <c:choose>
                                          <c:when test="${empty data.sign1Upload  and  empty data.sign2Upload}">
                                             <i class="append-icon fa fa-fw fa-cloud-upload" onclick="openModal('${data.getId()}')"  style="font-size: 24px;color: green;cursor: pointer;" aria-hidden="true">
                                          </c:when>
                                          <c:otherwise>														
                                             <i class="append-icon fa fa-fw fa-check"  style="font-size: 24px;color: green;" aria-hidden="true">
                                          </c:otherwise>
                                       </c:choose>
                                    </td>
                                    <td>
                                       <c:choose>
                                          <c:when test="${empty data.sign1Upload  and  empty data.sign2Upload}">
                                             <i  class="append-icon icon-cross3"  style="font-size: 24px;color: red;" aria-hidden="true">	
                                          </c:when>
                                          <c:when test="${not empty data.sign1Upload  and  empty data.sign2Upload}">													
                                             <i class="append-icon fa fa-fw fa-cloud-upload" onclick="openModal('${data.getId()}')"  style="font-size: 24px;color: green;cursor: pointer;" aria-hidden="true">
                                          </c:when>
                                          <c:otherwise>														
                                             <i class="append-icon fa fa-fw fa-check"  style="font-size: 24px;color: green;" aria-hidden="true">									
                                          </c:otherwise>
                                       </c:choose>
                                    </td>
                                    <td>
                                       <a href="${contextPath}/contract/file/${data.contarctCode}" target="_blank">view</a>	 								
                                    </td>
                                    <td>${data.remarks}</td>
                                    <td>
                                       <c:choose>
                                          <c:when test="${data.contractStatus eq 'CANCELLED'}">
                                             <span class="label label-danger">${data.projectStatus}</span>
                                          </c:when>
                                          <c:when test="${data.contractStatus eq 'INPROGRESS'}">
                                             <span class="label label-primary">${data.contractStatus}</span>
                                          </c:when>
                                          <c:when test="${data.contractStatus eq 'COMPLETED'}">
                                             <span class="label label-success">${data.contractStatus}</span>
                                          </c:when>
                                          <c:otherwise>														
                                             <span class="label label-default">${data.contractStatus}</span>													
                                          </c:otherwise>
                                       </c:choose>
                                    </td>
                                    <td>
                                       <fmt:formatDate type = "both"   value = "${data.createdOn}" />&nbsp;${not empty data.createdBy?'By:':''} ${data.createdBy.userName}
                                    </td>
                                    <td>
                                       <fmt:formatDate type = "both"   value = "${data.updatedOn}" />&nbsp;${not empty data.updatedBy?'By:':''} ${data.updatedBy.userName}
                                    </td>
                                    <td>
                                       <c:choose>
                                          <c:when test="${data.status eq 'ACTIVE'}">
                                             <span class="label label-success">${data.status}</span>
                                          </c:when>
                                          <c:otherwise>														
                                             <span class="label label-danger" style="font-size: 14px;">${data.status}</span>													
                                          </c:otherwise>
                                       </c:choose>
                                    </td>
                                    <c:if test="${userRole.roleId.roleName eq 'ADMIN'}">
                                       <td>
                                          <c:choose>
                                             <c:when test="${data.status eq 'ACTIVE' and data.contractStatus eq 'CREATED'}">														
                                                <button class="btn btn-success btn-sm" 
                                                   onclick="editData('${data.getId()}')">
                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                                </button>
                                                <button class="btn btn-danger btn-sm"  onclick="deleteData('${data.getId()}')">
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                                </button>
                                             </c:when>
                                             <c:otherwise>														
                                             </c:otherwise>
                                          </c:choose>
                                       </td>
                                    </c:if>
                                    <td>
                                       <a href="#" class="btn btn-success  btn-sm" onclick="mailModal('${data.getId()}')">
                                       <i class="append-icon fa fa-envelope"></i> Mail
                                       </a>
                                    </td>
                                 </tr>
                              </c:forEach>
                           </tbody>
                        </table>
                     </div>
                  </div>
                  <!-- Panel Widget --> 
               </div>
               <!-- col-md-12 --> 
            </div>
            <!-- row --> 
         </div>
         <!-- .vd_content-section --> 
      </div>
      <!-- .vd_content --> 
   </div>
   <!-- .vd_container --> 
</div>
<!-- .vd_content-wrapper --> 
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header vd_bg-blue vd_white">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
            <h4 class="modal-title" id="myModalLabel">Project Status</h4>
         </div>
         <div class="modal-body">
            <form class="form-horizontal"  action="${contextPath}/upload-signed-contract" id="uploadForm" method="post" enctype="multipart/form-data">
               <input type="hidden" name="${_csrf.parameterName}"
                  value="${_csrf.token}" />
               <input type="hidden" name="cid" id="cid" required/>
               <div class="form-group">
                  <label class="col-sm-4 control-label">Signed Contract</label>
                  <div class="col-sm-7 controls">
                     <input id="file" type="file" name="file" required>
                  </div>
               </div>
            </form>
         </div>
         <div class="modal-footer background-login">
            <button type="button" class="btn vd_btn vd_bg-grey" data-dismiss="modal">Close</button>
            <button type="button" class="btn vd_btn vd_bg-green" onclick="uploadForm()">Upload</button>
         </div>
      </div>
      <!-- /.modal-content --> 
   </div>
   <!-- /.modal-dialog --> 
</div>
<!-- /.modal --> 
<!-- Modal -->
<div class="modal fade" id="mailModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
   <div class="modal-dialog">
      <form class="form-horizontal"  action="${contextPath}/mail-contract-send" id="register-form" method="post">
         <div class="modal-content">
            <div class="modal-header vd_bg-blue vd_white">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
               <h4 class="modal-title" id="myModalLabel1">Email Contract</h4>
            </div>
            <div class="modal-body">
               <input type="hidden" name="${_csrf.parameterName}"
                  value="${_csrf.token}" />
               <input type="hidden" name="contractId" id="contractId" />
               <div class="form-group">
                  <div class="col-sm-9 controls">
                     <table class="table table-striped">
                        <thead>
                           <tr>
                              <th>Receiver Mail</th>
                              <th><a href="#"  onclick="addRow()">Add New</a></th>
                           </tr>
                        </thead>
                        <tbody id="data-tr">
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
            <div class="modal-footer background-login">
               <button type="reset" class="btn vd_btn vd_bg-grey">Reset</button>                         
               <button type="submit" class="btn vd_btn vd_bg-green">Send Mail</button>
            </div>
         </div>
         <!-- /.modal-content --> 
      </form>
   </div>
   <!-- /.modal-dialog --> 
</div>
<!-- /.modal --> 
<form action="${contextPath}/edit-contract" method="POST" id="editForm">
   <input type="hidden" name="${_csrf.parameterName}"
      value="${_csrf.token}" />
   <input type="hidden" name="id_edit" id="id_edit" />
</form>
<form action="${contextPath}/del-contract" method="POST"
   id="deleteForm">
   <input type="hidden" name="${_csrf.parameterName}"
      value="${_csrf.token}" />
   <input type="hidden" name="id_del" id="id_del" />
</form>
<!-- Placed at the end of the document so the pages load faster --> 
<script type="text/javascript" src="${contextPath}/static/sFreelance/js/jquery.js"></script> 

<script>  
   function editData(id){
   	$("#id_edit").val(id);
   	bootbox.confirm("Do you want to edit?",
   	function(result) {
   		if (result == true) {
   		   $("#editForm").submit();
   		} 
   	});
   }	
   function deleteData(id){
   	$("#id_del").val(id);
   	bootbox.confirm("Do you want to delete?",
       function(result) {
           if (result == true) {
               $("#deleteForm").submit();
           } 
       });
       }
    
   function openModal(cid){
   	$("#cid").val(cid);
   $("#myModal").modal("show");
   } 
   function uploadForm(){
    $("#uploadForm").submit();
   } 
   	function mailModal(id){
   	$("#contractId").val(id);
   	$('#mailModal').modal('show');
   	
   	
   	 
       }
       var index=0;
       function addRow(){
      
   	var tr="<tr id='del"+index+"'><td><input type='text' class='form-control' name='toUser' placeholder='Receipent mail'></td><td><a href='#'  onclick='delRow("+index+");'>delete</a></td></tr>";
   	 $("#data-tr").append(tr);	
   	 index=index+1;
   } 
        function delRow(id){
      	$("#del"+id).remove();
       	} 					
       
</script>