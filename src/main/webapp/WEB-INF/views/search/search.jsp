<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
  <script src="https://cdn.canvasjs.com/canvasjs.min.js"></script>
<!-- Middle Content Start -->    
<div class="vd_content-wrapper">
<div class="vd_container">
<div class="vd_content clearfix">
   <div class="vd_head-section clearfix">
      <div class="vd_panel-header">
         <ul class="breadcrumb">
            <li><a href="${contextPath}/">Home</a> </li>
            <li><a href="${contextPath}/">Search & Reports</a> </li>
            <li class="active">Search</li>
         </ul>
         
         <!-- vd_title-section -->
         <div class="vd_content-section clearfix">
            <div class="row">
               <div class="col-md-12">
                  <div class="panel widget">
                     <div class="panel-heading vd_bg-grey">
                        <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-bar-chart-o"></i> </span> Search By </h3>
                     </div>
                     <div class="panel-body">
                        <form class="form-horizontal"  action="${contextPath}/search" id="register-form" method="post" modelAttribute="searchRequest">
                           <div class="modal-content">                            
                              <div class="modal-body">
                                 <input type="hidden" name="${_csrf.parameterName}"
                                    value="${_csrf.token}" />                                
                                 <div class="alert alert-danger ${not empty failureMessage?'':'vd_hidden'} ">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-cross"></i></button>
                                    <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Oh snap!</strong> Change a few things up and try submitting again. 
                                 </div>
                                 <div class="form-group">
                                     <div class="form-group">
	                                  <label class="col-sm-2 control-label">Any Search<span class="vd_red">*</span> </label>                               
	                        				<div class="col-sm-3 controls">
	                           					<input type="text" class="form-control" id="txtsearch" name="anysearch" value="${searchRequest.anysearch}" >
	                        					</div>
	                                   </div>
                                 </div>
                                <hr>
                                 <div class="form-group">
                                    <label class="col-sm-2 control-label">By User <span class="vd_red">*</span></label>
                                    <div class="col-sm-1 controls">
                                       <input type="radio" class="form-control" ${empty searchRequest.rdSearchBy?'checked':''}  ${searchRequest.rdSearchBy eq 'USER'?'checked':''} value="USER" name="rdSearchBy" id="USER" onchange="showMySection(this.value)">
                                    </div>                                   
                                    <label class="col-sm-1 control-label">By Payment: <span class="vd_red">*</span></label>
                                    <div class="col-sm-1 controls">
                                       <input type="radio" class="form-control" name="rdSearchBy" ${searchRequest.rdSearchBy eq 'PAYMENT'?'checked':''}  value="PAYMENT" id="PAYMENT" onchange="showMySection(this.value)">
                                    </div>
	                                  <label class="col-sm-2 control-label">By Project: <span class="vd_red">*</span></label>
	                                    <div class="col-sm-1 controls">
	                                       <input type="radio" class="form-control" name="rdSearchBy" ${searchRequest.rdSearchBy eq 'PROJECT'?'checked':''} value="PROJECT"  id="PROJECT" onchange="showMySection(this.value)">
	                                  </div>
                                 </div>
                                <hr>
                               
                                 <div class="form-group" id="userSection">
                                     <label class="col-sm-2 control-label">User <span class="vd_red">*</span> </label>
                        				<div class="col-sm-3 controls">
                           					<select class="form-control form-control-sm required" id="user" name="user" >   
	                          					 <option value="">---select---</option> 
	                          					 <c:forEach items="${searchResponse.userstatusList}" var="data">
														<option  value="${data}" ${searchRequest.user eq data?'selected':''}>${data}</option> 
													</c:forEach>
							               		</select> 
                       				 </div>
                       				 <label class="col-sm-2 control-label">Skills <span class="vd_red">*</span></label>
                        					<div class="col-sm-3 controls">
                           						<select class="form-control form-control-sm required" id="skills" name="skills" >  
                           						<option  value="">---All Skill----</option>  
						               				<c:forEach items="${searchResponse.skillList}" var="data">
														<option  value="${data.skillName}"  ${searchRequest.skills eq data.skillName?'selected':''}>${data.skillName}</option> 
													</c:forEach>
						   						</select> 
                        					</div>
                                 </div>  
                                 <div id="paySection"  style="display:none;">  
	                                 
	                                   <div class="form-group">
	                                    <label class="col-sm-1 control-label">Payment Status <span class="vd_red">*</span></label>
	                        				<div class="col-sm-2 controls">
	                           					<select class="form-control form-control-sm required" id="paystatus" name="paystatus" > 
	                           						<option  value="ALL">---ALL----</option>  
							               				<c:forEach items="${searchResponse.paymentStatusList}" var="data">
															<option  value="${data}" ${searchRequest.paystatus eq data?'selected':''}>${data}</option> 
														</c:forEach>
							   						</select> 
							   						<span class="vd_green">(Used By Any Search Also)</span>
	                        				 </div>
	                        				  <label class="col-sm-1 control-label">Status <span class="vd_red">*</span></label>
	                        				<div class="col-sm-2 controls">
	                           					<select class="form-control form-control-sm required" id="projectstatus" name="projectstatus" > 
	                           						<option  value="ALL">---ALL----</option>  
							               				<c:forEach items="${searchResponse.projectStatusList}" var="data">
															<option  value="${data}" ${searchRequest.projectstatus eq data?'selected':''}>${data}</option> 
														</c:forEach>
							   						</select> 
							   						<span class="vd_green">(Used By Any Search Also)</span>
	                        				 </div>
	                        				<label class="col-sm-1 control-label">Type <span class="vd_red">*</span> </label>
	                        				<div class="col-sm-2 controls">
	                           					<select class="form-control form-control-sm required" id="payType" name="payType" > 
	                           						<option  value="ALL">---ALL----</option>  
							               				<c:forEach items="${searchResponse.paymentTypeList}" var="data">
															<option  value="${data}" ${searchRequest.payType eq data?'selected':''}>${data}</option> 
														</c:forEach>
							   						</select> 
	                        					</div>
	                                    </div>
                                   </div>
                                 
                                     
                                   <div class="form-group" id="dateSection" style="display:none;">
                                        <label class="col-sm-1 control-label"> Start Date <span class="vd_red">*</span></label>
                                    <div class="col-sm-2 controls">
                                       <input type="text" class="form-control  ignoreValidation date" id="startDateStr" value="${searchRequest.startDateStr}"
                                          name="startDateStr"  >
                                    </div>
                                
                                    <label class="col-sm-1 control-label"> End Date <span class="vd_red">*</span></label>
                                    <div class="col-sm-2 controls">
                                       <input type="text" class="form-control  ignoreValidation date" id="endDateStr" value="${searchRequest.endDateStr}"
                                          name="endDateStr"  >
                                    </div>
                                 </div>
                                 
                             
                              <div class="modal-footer background-login">
                                 <a type="button" href="${contextPath}/search" class="btn vd_btn vd_bg-grey" data-dismiss="modal">Reset</a>
                                 <button type="submit" class="btn vd_btn vd_bg-green">Search</button>
                              </div>
                           </div>
                           <!-- /.modal-content --> 
                        </form>
                     </div>
                  </div>
                  <!-- Panel Widget --> 
                  </div>
                  </div>
                  
                   <c:if test="${searchRequest.rdSearchBy eq 'PAYMENT'}">
                           
              <div class="row" id="advanced-input">
               <div class="col-md-12">
                  <div class="panel widget">
                     <div class="panel-heading vd_bg-grey">
                        <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span>List Of Payment History</h3>
                     </div>
                     <div class="panel-body table-responsive ">                  
                        <c:set var = "credit" value = "${0}"/>
                        <c:set var = "debit" value = "${0}"/>
                        <c:set var = "currency" value = "INR"/>
                        <c:set var = "usdInInr" value = "${searchResponse.USD_TO_INR}"/>
                        <table class="table table-striped" id="data-tables_payment">
                           <thead>
                              <tr>
                                 <th>#</th>
                                 <th>Payment for</th>
                                 <th>Project Details</th>
                                 <th>Freelancer Details</th>
                                 <th>Payment Date</th>
                                 <th>Start Date</th>
                                 <th>End Date</th>
                                 <th>Payment Amount</th>
                                 <th>Payment Status</th>
                                 <th>Remarks</th>
                               
                                 <th>Created On</th>
                                 <th>Updated On</th>
                                 <th>Status</th>
                                
                              </tr>
                           </thead>
                           <tbody id="tbd">
                              <c:forEach items="${searchResponse.paymentsList}" var="data" varStatus="serialNumber">
                               
                                 <tr style="color:black">
                                    <td>${serialNumber.count} </td>
                                    <td style="font-weight: bold;color: ${data.paymentfor eq 'CREDIT'?'green':'red'};">${data.paymentfor eq 'CREDIT'?'Payment Received':'Payment Released'}</td>
                                    <td>
                                       ${data.projectAllocation.project.projectName} (${data.projectAllocation.project.projectCode})
                                    Allocation Status: ${data.projectAllocation.projectStatus}
                                    </td>
                                    <td>${data.projectAllocation.userFreelancer.name}(${data.projectAllocation.userFreelancer.mobileNo})</td>
                                    <td>
                                       <fmt:formatDate type = "DATE" pattern="dd-MMM-yyyy"    value = "${data.paymentDate}" />
                                    </td>
                                    <td>
                                       <fmt:formatDate type = "DATE" pattern="dd-MMM-yyyy"   value = "${data.startDate}" />
                                    </td>
                                    <td>
                                       <fmt:formatDate type = "DATE"   value = "${data.endDate}" />
                                    </td>
                                    <td><span class="lbl_green">${data.paymentAmount}&nbsp;&nbsp;${data.currency} 
                                    <c:if test="${data.currency eq 'USD'}">
                                          => ${(data.paymentAmount)*(usdInInr)} INR
                                       </c:if>
                                    </span>
                                    
                                    </td>
                                    <td><span class="label label-${data.paymentStatus eq 'PAID'?'success':'danger'}">${data.paymentStatus}</span></td>
                                    <td>${data.remarks}</td>
                                   
                                    <td>
                                       <fmt:formatDate type = "both"   value = "${data.createdOn}" />&nbsp;${not empty data.createdBy?'By:':''} ${data.createdBy.userName}
                                    </td>
                                    <td>
                                       <fmt:formatDate type = "both"   value = "${data.updatedOn}" />&nbsp;${not empty data.updatedBy?'By:':''} ${data.updatedBy.userName}
                                    </td>
                                    
                                       <td>
                                       <span class="label label-${data.status eq 'ACTIVE'?'success':'danger'}">${data.status}</span>
                                    
                                       <c:if test="${data.paymentfor eq 'CREDIT'}">                                      
                                        <c:if test="${data.currency eq 'USD'}">
                                         <c:set var = "inr" value = "${(data.paymentAmount)*(usdInInr)}"/>  
                                        <c:set var = "credit" value = "${credit+inr}"/>
                                        </c:if>
                                         <c:if test="${data.currency eq 'INR'}">
                                          <c:set var = "credit" value = "${credit+data.paymentAmount}"/>
                                       </c:if>
                                     
                                      <c:set var = "inr" value = "${0}"/>  
                                        </c:if>
                                       <c:if test="${data.paymentfor eq 'DEBIT'}">
                                        <c:if test="${data.currency eq 'USD'}">     
                                         <c:set var = "inr" value = "${(data.paymentAmount)*(usdInInr)}"/>                                  
                                        <c:set var = "debit" value = "${debit+inr}"/>
                                        </c:if>
                                         <c:if test="${data.currency eq 'INR'}">
                                          <c:set var = "debit" value = "${debit+data.paymentAmount}"/>
                                       </c:if>
                                          
                                           <c:set var = "inr" value = "${0}"/>  
                                       </c:if>
                                  
                                    </td>
                                 </tr>
                              </c:forEach>
                           </tbody>
                        </table>
                        <p style="font-weight: bold;color: green;"><strong>CREDIT: <fmt:formatNumber value="${credit}" maxFractionDigits="2"/></strong>&nbsp;${currency} </p>
                        <p style="font-weight: bold;color: red;"><strong>DEBIT: <fmt:formatNumber value="${debit}" maxFractionDigits="2"/></strong>&nbsp;${currency} </p>
                        </hr>------------------------------</hr>
                        <p style="font-weight: bold;color: blue;"><strong> ${credit-debit>0?'PROFIT':'LOSS'}: <fmt:formatNumber value="${credit-debit}" maxFractionDigits="2"/>&nbsp;${currency}</strong> </p>
                     
                  
                   </div>
                  </div>
                  <!-- Panel Widget --> 
                  
                   <div class="col-md-12">
                <div class="panel widget">
                  <div class="panel-body-list  table-responsive">
                    <table class="table no-head-border table-striped">
                      <thead class="vd_bg-blue vd_white">
                        <tr>
                          <th style="width:12.5%"><i class="fa fa-search append-icon"></i>Payment Analysis</th>
                          <th style="width:12.5%"></th>
                           <th style="width:12.5%"></th>
                            <th style="width:12.5%"></th>
                             <th style="width:12.5%"></th>
                              <th style="width:12.5%"></th>
                               <th style="width:12.5%"></th>
                                <th style="width:12.5%"></th>
                        </tr>
                         <tr>                         
                          <th style="width:12.5%" style="font-size: medium;">Name</th>
                          <th style="width:12.5%"> Client Total</th>
                           <th style="width:12.5%">Client-PAID</th>
                            <th style="width:12.5%">Client-PENDING</th>
                             <th style="width:12.5%">Freelancer Total</th>
                           <th style="width:12.5%">Freelancer -PAID</th>
                            <th style="width:12.5%">Freelancer-PENDING</th>
                            <th style="width:12.5%">Total Profit</th>
                        </tr>
                      </thead>
                      <tbody>
                       
                     <c:forEach items="${searchResponse.paymentResponseSearchDTOs}" var="data" varStatus="serialNumber">
                    <c:if test="${data.name eq 'TOTAL'}">
                    <tr>
                     
                          <td colspan="8"><hr></td>
                                   </tr>
                    </c:if>
                     <tr>
                     
                          <td ><span class="badge vd_bg-blue" style="font-size: medium;">${data.name}</span></td>
                          <td class="text-center"><span class="badge vd_bg-green" style="font-size: medium;">${data.clientTotal}</span></td>
                          <td class="text-center"><span class="badge vd_bg-green" style="font-size: medium;">${data.clientPaid}</span></td>
                          <td class="text-center"><span class="badge vd_bg-red" style="font-size: medium;">${data.clientPending}</span></td>
                          <td class="text-center"> <span class="badge vd_bg-red" style="font-size: medium;">${data.freelancerTotal}</span></td>
                          <td class="text-center"> <span class="badge vd_bg-green" style="font-size: medium;">${data.freelancerPaid}</span></td>
                           <td class="text-center"> <span class="badge vd_bg-red" style="font-size: medium;">${data.freelancerPending}</span></td>
                          <td class="text-center"><span class="badge vd_bg-blue" style="font-size: medium;">${data.totalProfit}</span></td>
                        </tr>
                     </c:forEach>
                    
                      
                      </tbody>
                    </table>
                  </div>
                </div>
              
              </div>
                  
               </div>
               <!-- col-md-12 --> 
            </div>
            <!-- row -->
           
            </c:if>
            
                  
                  <c:if test="${searchRequest.rdSearchBy eq 'USER'}">
                           
              <div class="row" id="advanced-input1">
               <div class="col-md-12">
                  <div class="panel widget">
                     <div class="panel-heading vd_bg-grey">
                        <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-bar-chart-o"></i> </span> Search Result </h3>
                     </div>
                     <div class="panel-body table-responsive ">
                  <table class="table table-striped  clearfix" id="data-tables1">
                     <thead>
									<tr>
										<th>#</th>																		
										<th>Name</th>										
										<th>Mobile Number</th>
										<th>Email Id</th>
										<th>Skills</th>
										<th>Experience</th>
										<th>created On</th>
										<th>updated On</th>
										<th>Status</th>										
																												
									</tr>
								</thead>
                    
                      <tbody id="tbd">
									<c:forEach items="${searchResponse.userInfoList}" var="data" varStatus="serialNumber">
										 <tr style="background-color:${data.user.status eq 'ACTIVE'?'':'#e6393957'};">
											<td>${serialNumber.count}</td>
											
											<td>
													<span  class="txt_field_black">${data.user.name} </span> 
													<c:if test="${rtype eq 'f'}">
														${data.userType}											
													</c:if>
													<c:if test="${not empty data.contractors.name}"><br>
														<i class="append-icon fa fa-fw fa-users"></i><strong><u>Contractors Details</strong></u>
														${data.contractors.name}-${data.contractors.mobile}-${data.contractors.email}
													</c:if> 
											
											</td>
											
											<td>
											<c:set var = "mdata" value = "${fn:split(data.user.mobileNo, '-')}" />											
											<span  class="txt_field_black">${mdata[0]}${mdata[1]}</span> 
											</td>
											<td>${data.user.email}</td>
											<td><span  class="txt_field_green">${data.skills}</span> <br><strong><u>${not empty data.skillsOther?'Others':''}</u></strong>
											${data.skillsOther}										
											
											</td>
											<td>${data.experience} ${empty data.experience?'--':'Yrs'}</td>											
											 <td>
                                       <fmt:formatDate type = "both"   value = "${data.createdOn}" />&nbsp;${not empty data.createdBy?'By:':''} ${data.createdBy.userName}
                                    </td>
                                    <td>
                                       <fmt:formatDate type = "both"   value = "${data.updatedOn}" />&nbsp;${not empty data.updatedBy?'By:':''} ${data.updatedBy.userName}
                                    </td>
											
											<td><span class="label label-success">${data.user.status}</span> </td>
											
										</tr>
									</c:forEach>
								</tbody>
                    </table>
                   </div>
                  </div>
                  <!-- Panel Widget -->                  
                  
                  
               </div>
               <!-- col-md-12 --> 
            </div>
            <!-- row -->
           
            </c:if>
              
            
             
         
            
         </div>
         <!-- .vd_content-section --> 
      </div>
      <!-- .vd_content --> 
   </div>
   <!-- .vd_container --> 
</div>
<!-- .vd_content-wrapper --> 

</div>
</div>
<!-- Middle Content End --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script type="text/javascript" src="${contextPath}/static/sFreelance/js/jquery.js"></script> 

<script>

var rdSearchBy_="${searchRequest.rdSearchBy}";
if(rdSearchBy_!=""){
showMySection(rdSearchBy_);
}


function showMySection(data){
$("#dateSection").hide();
$("#userSection").hide();
$("#paySection").hide();

if(data==="USER"){
$("#userSection").show();
$("#advanced-input").hide();
$("#advanced-input1").show();


}
else if(data==="PROJECT"){
}
else if(data==="PAYMENT"){
$("#dateSection").show();
$("#paySection").show();
$("#advanced-input").show();
$("#advanced-input1").hide();

}
}

</script>
