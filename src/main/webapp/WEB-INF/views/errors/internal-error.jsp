<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<c:set var="contextPath" value="${pageContext.request.contextPath}"/> 

<style>
footer .vd_bottom {
    background-color: #252525;
    color: #f5f5f5;
    padding-top: 10px;
    padding-bottom: 10px;
    margin-top: 60px;
}
</style>

    
     <!-- Middle Content Start -->
    
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <div class="vd_content-section clearfix">
            <div class="vd_register-page">
              <div class="heading clearfix">
                <div class="logo">
                 
                </div>
              </div>
              <div class="panel widget">
                <div class="panel-body">
                  <div class="login-icon"> <i class="fa fa-cog"></i> </div>
                  <h1 class="font-semibold text-center" style="font-size:52px">${error}</h1>
                  <form class="form-horizontal" action="#" role="form">
                    <div class="form-group">
                      <div class="col-md-12">
                        <h4 class="text-center mgbt-xs-20">Your requested page could not be found or it is currently unavailable.</h4>
                        <p class="text-center"> Please <a href="${contextPath}/">click here</a> to go back to our home page or use the search form below</p>
                       
                      </div>
                    </div>
                    <div id="vd_login-error" class="alert alert-danger hidden"><i class="fa fa-exclamation-circle fa-fw"></i> Please fill the necessary field </div>
                  </form>
                </div>
              </div>
              <!-- Panel Widget -->
              <div class="register-panel text-center font-semibold"> <a href="${contextPath}/">Home</a> </div>
            </div>
            <!-- vd_login-page --> 
            
          </div>
          <!-- .vd_content-section --> 
          
        </div>
        <!-- .vd_content --> 
      </div>
      <!-- .vd_container --> 
    </div>
    <!-- .vd_content-wrapper --> 
    
    <!-- Middle Content End --> 


