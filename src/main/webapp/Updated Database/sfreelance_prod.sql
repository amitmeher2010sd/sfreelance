/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 8.0.25 : Database - sfreelance_prod
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sfreelance_prod` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `sfreelance_prod`;

/*Table structure for table `applyclient` */

DROP TABLE IF EXISTS `applyclient`;

CREATE TABLE `applyclient` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `gender` varchar(100) DEFAULT NULL,
  `mobile` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_by` bigint DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `status` enum('PENDING','CLOSED','CANCELED','INACTIVE') DEFAULT 'PENDING',
  `address` text,
  `requirments_details` text,
  `applycode` text NOT NULL,
  `meetDate` varchar(100) DEFAULT NULL,
  `meettime` varchar(100) DEFAULT NULL,
  `freelancer_details` text,
  `skills` varchar(200) DEFAULT NULL,
  `demostatus` enum('PENDING','WAITING_SCHEDULE','SCHEDULED','MEETING_DONE','HOLD','SELECTED','REJECTED','SCHEDULE_MEETING_IN_FUTURE') DEFAULT 'PENDING',
  `freelancer_user_id` bigint DEFAULT NULL,
  `remarks` text,
  `whatsappgroup` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `applyclient` */

insert  into `applyclient`(`id`,`name`,`gender`,`mobile`,`email`,`created_by`,`created_on`,`updated_by`,`updated_on`,`status`,`address`,`requirments_details`,`applycode`,`meetDate`,`meettime`,`freelancer_details`,`skills`,`demostatus`,`freelancer_user_id`,`remarks`,`whatsappgroup`) values 
(2,'Nelly','FEMALE','+14025162294','Nelly@GMAIL.COM',2,'2022-09-06 23:16:47',2,'2022-09-13 11:33:11','CLOSED','USA','github actions jenkins datadog new relic bash terraform kubernetes ansible helm gangway openstack aws gcp\r\n--------------------------------------------------------\r\nRequirements:\r\nTypically has 3 to 5 years of related DevOps experience\r\nDeep Knowledge of commonly used DevOps tooling concepts, hybrid-cloud environment\r\nAble to code in advance levels of Bash/Python with Golang a major plus\r\n2-4 years of experience in any of the IaC technology such as Terraform, Ansible, Pulumi\r\nWorking knowledge of CI/CD and related concepts\r\nWorks collaboratively and seeks input and feedback from co-workers\r\nCan work independently without too much direction after a task is assigned\r\nAble to contribute individually to complex areas of the tech stack with little to no support from Senior Engineers\r\n1 to 2 years of experience working with Kubernetes, Serverless, and one or more of 3 major cloud providers AWS, GCP and/or Azure','IuNKlvthZt9RktFJ2UFeTrR6lY00TY','07-09-2022','8:00 PM','Nitin Devops\r\n+919041030122','AWS','SELECTED',105,'$650/months from freelancer and to deveoper 26k/month',''),
(3,'Krupa','MALE','+19022339222','k@gmail.com',2,'2022-09-06 23:22:32',2,'2022-09-21 22:37:29','CANCELED','USA','Python and proxy','wPPxZ3QJUWvqPWshctQ1lDPPLCVxMG','26-09-2022','10:45 PM','','PYTHON','REJECTED',NULL,'',''),
(4,'Kvr Murthy','MALE','+919603613708','k@gmail.com',2,'2022-09-06 23:25:19',2,'2022-09-21 22:29:20','CANCELED','INDIA','Mobile automation testing wuth quantum  framework\r\n','00vXPkJMlIselOWNjfIm6tpODJJ8x0','','10:30 PM','FREELANCER: \r\n\r\nRequired : job support for Mobile automation testing. \r\nFramework  is quantum framework\r\n\r\n\r\n\r\nCHANDRU  UI TESTOR :+917305753272','JAVA','REJECTED',NULL,'client: 28k/month\r\n\r\nfreelancer:\r\n22k/month\r\n2hr/day','devfnd-mobileUi-QA-jsupport'),
(5,'Raj  Sandeep','MALE','+16828125256','s@gmail.com',2,'2022-09-06 23:44:25',2,'2022-09-10 08:19:07','PENDING','USA','java','CR49U42L4mfnXWkyohS9NbdahCOl7M','','8:30 AM','sayantan	+918961308286','JAVA','REJECTED',78,'dome done but client not confirmed\r\n============================\r\nCLIENT NOT RESPONDING SO MAKING THIS CANCELED',''),
(6,'Anoop Bhasi','MALE','+919995222873','a@gmail.com',2,'2022-09-07 09:51:20',2,'2022-09-21 22:39:26','CANCELED','na','AWS DEVOPS REQUIRMENT','9o5KRaMXcDruzlrJgU9FrpE5xPK5SI','','8:30 AM','NOT ALLOCATE\r\nMay be comming week ','AWS DEVOPS','REJECTED',NULL,'',''),
(7,'Rohit Sharma','MALE','+14164201775','',2,'2022-09-07 09:56:47',2,'2022-09-14 14:35:03','CLOSED','','JAVA OT ANGULAR\r\nNAHED FRIEND','SosJKh7VqNorXPsCFDvX6VVh6V3w3i','08-09-2022','10:30 AM','veer\'s developer','JAVA','SELECTED',79,'$600/months from freelancer and to developer 26k/month',''),
(8,'Vinod kumar','MALE','+19292355643','',2,'2022-09-07 10:01:27',2,'2022-09-21 22:25:35','CLOSED','USA','JAVA,J2EE,SPRING,OAUTH 2.0,OKTA,JWT,REACT AND AWS\r\n=========================\r\nCOMMING WEEK','Q0WT29wn7eoWBfSAGzzRZwElSpox6M','','10:30 PM','','JAVA','HOLD',NULL,'',''),
(9,'MK','MALE','+14096000707','',2,'2022-09-07 11:01:28',2,'2022-09-10 08:17:36','PENDING','','k8s aws EKs,help charts,helm release,prometheus,alet managerand flux','27udQdmwXZlvj7iXiqdT5YVbx8Behw','08-09-2022','9:00 PM','Bhagi','AWS','REJECTED',103,'Domo done but client not confirmed\r\n========================\r\nCLIENT NOT RESPONDING',''),
(10,'Mounica','FEMALE','+16476737273','',2,'2022-09-08 09:52:37',2,'2022-09-21 22:30:43','PENDING','Toronto ,Canada','','fuLXTU4eKXMml71oObtnGRpUyuvu72','25-09-2022','10:30 PM','','AWS DEVOPS','SCHEDULE_MEETING_IN_FUTURE',NULL,'',''),
(11,'Raj Subedi','MALE','+17143887225','',2,'2022-09-08 12:24:48',2,'2022-09-21 22:40:22','CANCELED','USA','fullstack with node js','tBlxH7A5ZOGWr8YI0Tnf6ZFFTB4O7U','','10:45 PM','','FULLSTACK DEVELOPER','REJECTED',NULL,'NEED REQUIRMENT CLARIFICATION',''),
(12,'Sai','MALE','+16479693694','',2,'2022-09-09 09:58:28',2,'2022-09-20 20:18:15','CLOSED','USA','java,angular,react','0EefgA2tGphHCrb9kcllFQBFWshotx','','8:30 PM','','JAVA','SELECTED',NULL,'18CAD CLIENT SAYING',''),
(13,'Ron Williams','MALE','+14734031985','ron@gmail.com',2,'2022-09-09 13:07:51',2,'2022-09-21 22:35:22','CANCELED','USA','','GWdniPmMUNCfqVtgDY2OzZmIY5uDOd','09-09-2022','10:45 PM','','SALESFORCE ADMIN','REJECTED',83,'today evening need to schedule','devfnd-Ron-salesforceadmin'),
(14,'Gangadhar','MALE','+918500518969','gan@gmail.com',2,'2022-09-09 14:25:32',2,'2022-09-21 22:38:24','CANCELED','','','raPr0b0KZqZ7CcrtENkM29AhFXyJZ8','16-09-2022','10:45 PM','','JAVA','REJECTED',NULL,'26k/month client',''),
(15,'Prashanth','MALE','+15105098896','Prashanth@gmail.com',2,'2022-09-13 13:16:01',2,'2022-09-21 21:43:14','CANCELED','USA','fastlane Jenkins sonarqube ','jodTm1AwkmbRfFKSu6QerS35MzXC2w','13-09-2022','6:00 PM','Devops:\r\nFastlane, Jenkins and sonarqube\r\n28k/month\r\nLet me know if any','DEVOPS','REJECTED',NULL,'below are some points :\r\n$600usd/month\r\nWeekly payment should be release. \r\nWeekly/$150usd\r\n2hr/day\r\nMonday to Friday\r\nBefore start please release the initial amount at least 1week payment..',''),
(16,'Bharat Gowda','MALE','+917337714227','7337714227@gmail.com',2,'2022-09-13 18:10:41',2,'2022-09-21 21:19:30','CLOSED','INDIA','Java\r\nJquery\r\nBackend java\r\n Spring\r\nSqlyog','wJzj8myHTsiL6dzVOtsFnu6TudDn7u','13-09-2022','8:30 PM','20k/month weeklypay','JAVA','SELECTED',NULL,'TODAY SCHEDULE','devfnd-java-promo-jsuppor'),
(17,'Prabhu Kiran Prathipati','MALE','+18049979006','',2,'2022-09-15 21:37:15',2,'2022-09-15 21:44:26','PENDING','USA -CST','I am looking for DevOps Job support.\r\nTechnology looking for: Typescript, Pulumi, YAML, Kubernetes, Docker, CICD Pipeline Development exp.\r\nThank You,','jIZkasoVhrtGxD9vMEW8uFLVvXnvjI','16-09-2022','7:00 AM','40k/month\r\n4hrs/day','DEVOPS','SCHEDULED',133,'below are some points :\r\n$900usd/month\r\nWeekly payment should be release. \r\n225$/week\r\n4hr/day\r\nMonday to Friday\r\nBefore start please release the initial amount at least 1week payment...','devfnd-Devops-kiran-jsupp'),
(18,'Seattle','FEMALE','+1 4019965922','',2,'2022-09-15 22:55:03',2,'2022-09-20 18:08:18','PENDING','','Java, AWS lambda, API gateway ','ibWP5hGL1mfEa8ElUbJeQPgoUk1yDZ','20-09-2022','8:00 PM','(30k/month,2hrs dailly)','JAVA','SCHEDULED',117,'2-2.5hrs/day\r\nMonday to Friday\r\nPayment : 550usd/month\r\nWeekly will be 138usd/weekly.\r\nAlways payment should be release in weekly basis. \r\n6yrs+ experience person will  provide support\r\nOnce demo session complete if you want to proceeds with our job support then you need to pay 1 week payment advance..','devfnd-java-aws-Seattl-ju'),
(19,'Kalpana ','FEMALE','+16058581273','',2,'2022-09-15 23:18:24',2,'2022-09-20 18:11:25','PENDING','','java','KhIJZA7dkLpuMqMDJY52dlhpqSRbGO','20-09-2022','9:15 PM','27k/month','JAVA','SCHEDULED',78,'Aruni friend\r\n2-2.5hrs/day\r\nMonday to Friday\r\nPayment : 600usd/month\r\nWeekly will be 150usd/weekly.\r\nAlways payment should be release in weekly basis. \r\n6yrs+ experience personal will  provide support',''),
(20,'Meghna','FEMALE','+1 (402) 889-91','',2,'2022-09-17 21:13:05',NULL,NULL,'PENDING','','looking for api automation\r\nTestng, Java , postaman','T7ruMOFVTCqrIs7TYjYF25qErDWyzf','18-09-2022','','26.5k/month','QA AUTOMATION','WAITING_SCHEDULE',NULL,'Below are some points regarding the support. \r\n\r\n2hrs/day   , 3day/week\r\nMonday to Friday\r\nPayment : 113usd/month\r\nWeekly will be 113usd/weekly.\r\nAlways payment should be release in weekly basis. \r\n\r\nOnce demo session complete if you want to proceeds with our job support then you need to pay 1 week payment advance..',''),
(21,'Nihar','MALE','+1 (267) 476-29','',2,'2022-09-17 21:18:04',NULL,NULL,'PENDING','','','4pKa1Xqvm2KUOSSokFCgPBn1b9HXKx','15-10-2022','9:30 PM','30k/month','SALESFORCE ADMIN','WAITING_SCHEDULE',NULL,'$500usd/month',''),
(22,'SaiReact','MALE','+1 (437) 224-04','',2,'2022-09-20 18:14:36',2,'2022-09-21 21:45:33','CANCELED','',' react, material ui and storybook','aHFRGeZN2kQHPBSUcvci6SKQv1NQj0','20-09-2022','8:00 PM','400usd\r\n+91 89495 34195\r\n~King','REACT JS','REJECTED',NULL,'$600usd/month\r\nWeekly payment should be release. \r\n2hr/day\r\nMonday to Friday\r\nAfter completion demo if u like to proceeds then \r\nBefore start u need release the initial amount at least 1week payment...\r\n\r\n================================\r\nWaiting for client confirmation','devfnd-reactjs-sai-jbsupp'),
(23,'Chinni','MALE','+91 91606 49165','',2,'2022-09-21 16:18:59',NULL,NULL,'PENDING','banglore','AWS DevOps support','9APnXyv12604VlMicgEOlBuenDvsFQ','','','','DEVOPS','PENDING',NULL,'28k/month',''),
(24,'Aman Singh','MALE','+91 95702 10158','',2,'2022-09-21 16:21:01',NULL,NULL,'PENDING','','AWS cloud (codebuild & codepipeline), Terraform & terragrunt for infra, GitHub actions for cicd','79YW9Gbx20RlsIBeXhiFANcIfl44KS','','','','DEVOPS','PENDING',NULL,'28k/month',''),
(25,'Murali','MALE','+916303292155','',2,'2022-09-22 19:57:10',NULL,NULL,'PENDING','','aws devops  ','x9MAm66KQMecr5tsevCkrjFpskror7','22-09-2022','9:00 PM','22k/mnth\r\nafter 1 month 23k','DEVOPS','SCHEDULED',NULL,'30k/month from client','devfnd-awsdevops-Murali');

/*Table structure for table `bank_details` */

DROP TABLE IF EXISTS `bank_details`;

CREATE TABLE `bank_details` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `bank_name` varchar(50) NOT NULL,
  `account_no` varchar(30) NOT NULL,
  `ifsc_code` varchar(30) NOT NULL,
  `account_holder_name` varchar(30) NOT NULL,
  `account_holder_address` varchar(100) DEFAULT NULL,
  `account_holder_contact_no` varchar(20) NOT NULL,
  `created_by` bigint NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_by` bigint DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') NOT NULL DEFAULT 'ACTIVE',
  `google_pay` varbinary(30) DEFAULT NULL,
  `any_upi` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `bank_details` */

/*Table structure for table `contact` */

DROP TABLE IF EXISTS `contact`;

CREATE TABLE `contact` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `email` varchar(30) NOT NULL,
  `message` text NOT NULL,
  `created_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `contact` */

insert  into `contact`(`id`,`name`,`email`,`message`,`created_on`) values 
(1,'aaa','a@gmail.com','sssms','2022-08-26 17:11:20'),
(2,'RQWR','a@gmail.com','SFSAF','2022-08-26 17:18:06'),
(3,'EQE','amit@gmail.com','EWQE','2022-08-26 17:19:19'),
(4,'EWRWE','amit@gmail.com','EREW','2022-08-26 17:20:28'),
(5,'fqrq','a@gmail.com','wrwr','2022-08-26 17:24:23'),
(6,'e','amit@gmail.com','2424','2022-08-26 17:24:41'),
(7,'rer','a@gmail.com','erer','2022-08-26 17:25:24'),
(8,'dadad','a@gmail.com','eqwrwqr','2022-08-28 16:57:52');

/*Table structure for table `contractors` */

DROP TABLE IF EXISTS `contractors`;

CREATE TABLE `contractors` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `created_by` bigint NOT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_by` bigint DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') NOT NULL DEFAULT 'ACTIVE',
  `address` text NOT NULL,
  `consultancy_details` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `contractors` */

insert  into `contractors`(`id`,`name`,`gender`,`mobile`,`email`,`created_by`,`created_on`,`updated_by`,`updated_on`,`status`,`address`,`consultancy_details`) values 
(10,'Veer','MALE','+91-9408180059','no@gmail.com',2,'2022-08-27 15:02:13',2,'2022-08-27 18:39:33','ACTIVE','BANGLORE','Friends Group'),
(11,'Sheetal','FEMALE','+91-9866207461','no@gmail.com',2,'2022-08-27 15:04:05',2,'2022-08-27 18:40:09','ACTIVE','BANGLORE','From Any consultancy\r\n(Client provider for job support )'),
(12,'Srinibash','MALE','+91-9293008156','no@gmail.com',2,'2022-08-27 15:09:20',2,'2022-08-27 18:40:44','ACTIVE','BANGLORE','java client provider with 25k payment fixed'),
(13,'Ray or Renu','FEMALE','+91-7499847960','no@gmail.com',2,'2022-08-27 15:11:42',2,'2022-08-27 18:41:05','ACTIVE','NA','Java Trainer and java proxy person\r\nalso rovide client for job support'),
(14,'Eissa li khan','MALE','+91-8247251960','no@gmail.com',2,'2022-08-27 15:14:30',2,'2022-08-27 18:41:45','ACTIVE','Hidrabad\r\n','Consultancy for job support (client provider)\r\n\r\nUS job client proveder'),
(15,'Harse vatia','MALE','+91-8905003200','na@gmail.com',2,'2022-08-31 18:38:54',2,'2022-09-14 17:22:38','ACTIVE','na','group people\r\nNOT HONESTY\r\nDONT DEALWITH HIM'),
(16,'Vinayak','MALE','+60-1126304653','Vinayak@gmail.com',2,'2022-09-03 14:28:08',NULL,NULL,'ACTIVE','NA','He have multiple developer'),
(17,'Karun B','MALE','+91-9963736390','na@gmail.com',2,'2022-09-14 17:21:26',NULL,NULL,'ACTIVE','HYD BUT WORK FOR USCOMPANY','job marketting/jon support/proxysupport/BGC foreign client\r\n\r\n200$ for BGC\r\n500$ -CLIENT FOR JOB'),
(18,'Pooja Sanga','FEMALE','+91-9309816067','9309816067@gmail.com',2,'2022-09-21 11:54:52',NULL,NULL,'ACTIVE','Pune,India','Developers:\r\n1. .net microservices\r\n2. QA - Automation+ manual\r\n3. Service now \r\n4. Java backend/microservices/AWS/azure\r\n5. SQL\r\n6. Data engineer- tableau, python\r\n7. Python / pyspark/ AWS\r\n8. Python /django/ webservices\r\n9. Angular/ react');

/*Table structure for table `contracts_info` */

DROP TABLE IF EXISTS `contracts_info`;

CREATE TABLE `contracts_info` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `project_id` bigint DEFAULT NULL COMMENT 'It is mandetory if project specific',
  `contarct_name` varchar(50) CHARACTER SET latin1 COLLATE latin1_german2_ci NOT NULL,
  `contarct_code` varchar(50) CHARACTER SET latin1 COLLATE latin1_german2_ci NOT NULL,
  `sign1_upload` text CHARACTER SET latin1 COLLATE latin1_swedish_ci COMMENT 'First User Signed contract upload',
  `sign2_upload` text CHARACTER SET latin1 COLLATE latin1_swedish_ci COMMENT 'Second User Signed contract upload',
  `contract_upload` text CHARACTER SET latin1 COLLATE latin1_german2_ci,
  `remarks` longtext CHARACTER SET latin1 COLLATE latin1_german2_ci NOT NULL COMMENT 'Comments for each steps',
  `created_by` bigint NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_by` bigint DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `contract_status` enum('CREATED','COMPLETED','CANCELLED','INPROGRESS') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'CREATED' COMMENT 'CREATED- Add/edit contract ,INITIATED- sent for sign,COMPLETED- done with signed,CANCELED- not had any conteact',
  `status` enum('ACTIVE','INACTIVE') CHARACTER SET latin1 COLLATE latin1_german2_ci NOT NULL DEFAULT 'ACTIVE',
  `client_id` bigint DEFAULT NULL,
  `freelancer_id` bigint DEFAULT NULL,
  `other_ifnot_project` varchar(255) CHARACTER SET latin1 COLLATE latin1_german2_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1 COLLATE=latin1_german2_ci;

/*Data for the table `contracts_info` */

/*Table structure for table `notification` */

DROP TABLE IF EXISTS `notification`;

CREATE TABLE `notification` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `subject` text,
  `descriptions` text,
  `created_by` datetime NOT NULL,
  `created_on` bigint NOT NULL,
  `updated_by` datetime DEFAULT NULL,
  `updated_on` bigint DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') NOT NULL DEFAULT 'ACTIVE',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `notification` */

/*Table structure for table `payment_history` */

DROP TABLE IF EXISTS `payment_history`;

CREATE TABLE `payment_history` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `project_allocation_id` bigint NOT NULL COMMENT 'Project assigned to bot client and freelancer',
  `payment_date` date NOT NULL,
  `payment_amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT 'Freelancer Received payment or client given payment',
  `start_date` date DEFAULT NULL COMMENT 'Payment For started work',
  `end_date` date DEFAULT NULL COMMENT 'Payment For ended work',
  `remarks` tinytext NOT NULL COMMENT 'Descriptions for which payment for',
  `created_by` bigint NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_by` bigint DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') NOT NULL DEFAULT 'ACTIVE',
  `payment_status` enum('PAID','PENDING','CANCELLED','WAITING_PAYMENT') NOT NULL DEFAULT 'PENDING',
  `currency` varchar(20) DEFAULT NULL,
  `paymentfor` enum('DEBIT','CREDIT') NOT NULL DEFAULT 'DEBIT',
  `hourlyOrProjectWise` tinyint DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=169 DEFAULT CHARSET=latin1;

/*Data for the table `payment_history` */

insert  into `payment_history`(`id`,`project_allocation_id`,`payment_date`,`payment_amount`,`start_date`,`end_date`,`remarks`,`created_by`,`created_on`,`updated_by`,`updated_on`,`status`,`payment_status`,`currency`,`paymentfor`,`hourlyOrProjectWise`) values 
(71,60,'2022-08-22',22500.00,'2022-08-08','2022-08-22','ADVANCE FOR THIS TIME || Paid',2,'2022-08-29 21:44:30',2,'2022-08-29 21:44:49','ACTIVE','PAID','INR','CREDIT',0),
(72,60,'2022-08-22',13500.00,'2022-08-08','2022-08-22','Paid to freelancer || paid',2,'2022-08-29 21:45:37',2,'2022-08-29 21:45:46','ACTIVE','PAID','INR','DEBIT',0),
(73,61,'2022-09-10',300.00,'2022-08-26','2022-09-10','Waiting for payment--Payment initiated || paid || 2paid || Received',2,'2022-08-31 17:22:24',2,'2022-09-15 14:33:08','ACTIVE','PAID','USD','CREDIT',0),
(74,62,'2022-08-25',20000.00,'2022-08-10','2022-08-25','Paid  || paid',2,'2022-08-31 17:31:25',2,'2022-08-31 17:31:36','ACTIVE','PAID','INR','CREDIT',0),
(75,62,'2022-08-25',14000.00,'2022-08-10','2022-08-25','paid to freelancer || paid to freelancer',2,'2022-08-31 17:32:29',2,'2022-08-31 17:32:37','ACTIVE','PAID','INR','DEBIT',0),
(76,60,'2022-09-02',11500.00,'2022-08-23','2022-09-02','1 weekly payment || paid',2,'2022-09-02 22:28:18',2,'2022-09-02 22:28:34','ACTIVE','PAID','INR','CREDIT',0),
(77,60,'2022-09-02',5500.00,'2022-08-23','2022-09-02','paif to client || paid ok done tata baba',2,'2022-09-02 22:29:15',2,'2022-09-02 22:29:33','ACTIVE','PAID','INR','DEBIT',0),
(78,64,'2022-09-03',8639.40,'2022-09-10','2022-09-17','paid by client || paid',2,'2022-09-03 14:22:52',2,'2022-09-03 14:23:05','ACTIVE','PAID','INR','CREDIT',0),
(79,64,'2022-09-03',2500.00,'2022-09-10','2022-09-17','paid to client Nitin Freelancer Engineer || paid',2,'2022-09-03 14:24:11',2,'2022-09-03 14:24:19','ACTIVE','PAID','INR','DEBIT',0),
(80,65,'2022-09-03',10000.00,'2022-08-23','2022-09-02','paid by client || ok',2,'2022-09-03 14:56:41',2,'2022-09-03 14:56:50','ACTIVE','PAID','INR','CREDIT',0),
(81,65,'2022-09-10',0.00,'2022-09-03','2022-09-10','Waiting for payment || As no worked done',2,'2022-09-03 14:57:52',2,'2022-09-10 00:32:34','ACTIVE','PAID','INR','CREDIT',0),
(82,65,'2022-09-03',14000.00,'2022-08-23','2022-09-03','PAID to FREELANCER VINAYAK || PAID',2,'2022-09-03 15:01:20',2,'2022-09-03 15:05:05','ACTIVE','PAID','INR','DEBIT',0),
(83,66,'2022-08-19',12256.00,'2022-08-12','2022-08-19','paid by client || paid',2,'2022-09-03 15:49:08',2,'2022-09-03 15:50:30','ACTIVE','PAID','INR','CREDIT',0),
(84,66,'2022-08-27',10383.60,'2022-08-20','2022-08-27','paid by client || paid',2,'2022-09-03 15:50:09',2,'2022-09-03 15:50:18','ACTIVE','PAID','INR','CREDIT',0),
(85,66,'2022-08-19',6250.00,'2022-08-12','2022-08-19','paid to developer || paid',2,'2022-09-03 15:51:36',2,'2022-09-03 15:52:25','ACTIVE','PAID','INR','DEBIT',0),
(86,66,'2022-08-27',6250.00,'2022-08-20','2022-08-27','paid || paid',2,'2022-09-03 15:52:12',2,'2022-09-03 15:53:05','ACTIVE','PAID','INR','DEBIT',0),
(87,63,'2022-09-03',7000.00,'2022-08-26','2022-09-03','WAITING PAYMENT FROM CIENT || received from client',2,'2022-09-03 15:58:11',2,'2022-09-04 08:41:00','ACTIVE','PAID','INR','CREDIT',0),
(88,67,'2022-08-27',20000.00,'2022-08-27','2022-09-03','paid by cient || paid by client',2,'2022-09-03 16:24:29',2,'2022-09-03 16:25:43','ACTIVE','PAID','INR','CREDIT',0),
(89,67,'2022-09-03',7500.00,'2022-08-27','2022-09-03','Paid to deveoper || paid',2,'2022-09-03 16:30:16',2,'2022-09-03 16:30:31','ACTIVE','PAID','INR','DEBIT',0),
(90,68,'2022-09-15',0.00,'2022-09-01','2022-09-15','NA || project not assigned',2,'2022-09-03 16:55:42',2,'2022-09-10 09:03:42','ACTIVE','CANCELLED','INR','CREDIT',0),
(91,63,'2022-09-03',4800.00,'2022-08-26','2022-09-03','TO FREELANCER || paid',2,'2022-09-04 08:30:46',2,'2022-09-04 08:37:47','ACTIVE','PAID','INR','DEBIT',0),
(92,67,'2022-09-10',20000.00,'2022-09-04','2022-09-10','waiting for client payment || PAID & CONFIRMED BY ME',2,'2022-09-06 00:06:23',2,'2022-09-09 16:50:11','ACTIVE','PAID','INR','CREDIT',0),
(93,67,'2022-09-10',7500.00,'2022-09-04','2022-09-10','need to pay developer || PAID & CONFIRMEDBY DEVELOPER',2,'2022-09-06 00:07:13',2,'2022-09-09 16:49:25','ACTIVE','PAID','INR','DEBIT',0),
(94,63,'2022-09-10',7000.00,'2022-09-04','2022-09-10','waiting for client payment || RECEIVED BY GOKUL',2,'2022-09-06 00:13:08',2,'2022-09-11 00:04:22','ACTIVE','PAID','INR','CREDIT',0),
(95,63,'2022-09-10',4800.00,'2022-09-04','2022-09-10','waiting for freelancer payment || reveived by gokul',2,'2022-09-06 00:14:27',2,'2022-09-11 00:04:55','ACTIVE','PAID','INR','DEBIT',0),
(96,62,'2022-09-10',6000.00,'2022-08-25','2022-09-10','waiting from client payment',2,'2022-09-06 00:16:13',2,'2022-09-14 14:46:55','ACTIVE','PENDING','INR','CREDIT',0),
(99,69,'2022-09-10',0.00,'2022-09-03','2022-09-10','advance paid by client || No payment',2,'2022-09-07 18:23:09',2,NULL,'ACTIVE','CANCELLED','USD','CREDIT',0),
(100,69,'2022-09-10',0.00,'2022-09-03','2022-09-10','PER DAY 1500 INR IF HE CONNECTED  || Nopayno work',2,'2022-09-07 18:24:20',2,'2022-09-10 15:33:06','ACTIVE','CANCELLED','INR','DEBIT',0),
(101,70,'2022-09-14',0.00,'2022-09-07','2022-09-14','ADVANCE PAYMENT ASKING',2,'2022-09-07 21:10:36',NULL,NULL,'ACTIVE','CANCELLED','USD','CREDIT',0),
(102,70,'2022-09-14',0.00,'2022-09-07','2022-09-14','pAYMENT FOR CLIENT',2,'2022-09-07 21:12:03',NULL,NULL,'ACTIVE','CANCELLED','INR','DEBIT',0),
(103,61,'2022-09-10',11000.00,'2022-08-26','2022-09-15','Waiting for payment--Payment initiated || Paid to Freelancer (NB: Very few day developer & client connected)',2,'2022-09-08 19:46:52',2,'2022-09-15 14:34:42','ACTIVE','PAID','INR','DEBIT',0),
(104,72,'2022-09-09',12756.25,'2022-09-09','2022-09-15','ADVANCE PAID BY NELLY || PAID& CONFIRMED',2,'2022-09-09 16:53:54',2,'2022-09-09 16:54:18','ACTIVE','PAID','INR','CREDIT',0),
(105,72,'2022-09-15',6500.00,'2022-09-09','2022-09-15','after 1 week need to pay to developer || paid',2,'2022-09-09 16:56:29',2,'2022-09-15 16:00:33','ACTIVE','PAID','INR','DEBIT',0),
(106,64,'2022-09-10',8639.40,'2022-09-03','2022-09-10','waiting for payment || leave',2,'2022-09-09 17:07:05',2,'2022-09-10 00:13:44','ACTIVE','PAID','INR','CREDIT',0),
(107,64,'2022-09-10',2500.00,'2022-09-03','2022-09-10','waiting to pay || Leave',2,'2022-09-09 17:08:26',2,'2022-09-10 00:16:01','ACTIVE','PAID','INR','DEBIT',0),
(108,71,'2022-09-05',175.00,'2022-09-02','2022-09-09','advance paid || confirmed ',2,'2022-09-09 17:22:33',2,'2022-09-09 17:23:31','ACTIVE','PAID','USD','CREDIT',0),
(109,71,'2022-09-10',3000.00,'2022-09-02','2022-09-09','WAITING TO GIVE PAYMENT BY CALCULATING THE PER DAY AS :  1500inr/DAY || 1500*2 =3000INR PAID FOR 2 DAYS CONNECT',2,'2022-09-09 17:25:07',2,'2022-09-10 15:34:11','ACTIVE','PAID','INR','DEBIT',0),
(110,71,'2022-09-15',175.00,'2022-09-09','2022-09-16','waiting for advance payment || PAYMENT TRANSFERRED INITIATED BUT IT WILL BE CREDITED AT SEPT 15',2,'2022-09-09 17:26:27',2,'2022-09-13 11:10:05','ACTIVE','PAID','USD','CREDIT',0),
(111,71,'2022-09-16',1400.00,'2022-09-09','2022-09-16','WAITING TO GIVE PAYMENT BY CALCULATING THE PER DAY AS : 1500inr/DAY || 1400INR PAID FOR 1 DAY CONNECT',2,'2022-09-09 17:27:25',2,'2022-09-16 13:24:25','ACTIVE','PAID','INR','DEBIT',0),
(112,65,'2022-09-10',0.00,'2022-09-03','2022-09-10','Waiting to pat once client paid || NO WORK NO PAY',2,'2022-09-09 20:14:55',2,'2022-09-10 09:10:46','ACTIVE','PAID','INR','DEBIT',0),
(113,73,'2022-09-13',11700.00,'2022-09-12','2022-09-18','Advance paid by client',2,'2022-09-13 20:39:03',NULL,NULL,'ACTIVE','PAID','INR','CREDIT',0),
(114,73,'2022-09-13',6700.00,'2022-09-12','2022-09-18','ADVANCE PAID',2,'2022-09-13 20:40:26',NULL,NULL,'ACTIVE','PAID','INR','DEBIT',0),
(115,60,'2022-09-16',0.00,'2022-09-03','2022-09-17','just created || No Pay as no work',2,'2022-09-13 20:55:13',2,'2022-09-16 21:18:01','ACTIVE','PAID','INR','CREDIT',0),
(116,60,'2022-09-16',0.00,'2022-09-03','2022-09-17','just created || MADE TO 11500 || No Pay as no work',2,'2022-09-13 20:55:55',2,'2022-09-16 21:18:39','ACTIVE','PAID','INR','DEBIT',0),
(117,61,'2022-09-20',0.00,'2022-09-11','2022-09-25','just created || Making 0 as closing the project',2,'2022-09-13 20:58:04',2,'2022-09-20 23:49:03','ACTIVE','CANCELLED','USD','CREDIT',0),
(118,61,'2022-09-20',0.00,'2022-09-11','2022-09-25','just created || Making 0 as closing the project',2,'2022-09-13 20:58:51',2,'2022-09-20 23:49:34','ACTIVE','CANCELLED','INR','DEBIT',0),
(119,62,'2022-09-25',6000.00,'2022-09-11','2022-09-25','just created',2,'2022-09-13 21:00:05',NULL,NULL,'ACTIVE','PENDING','INR','CREDIT',0),
(120,63,'2022-09-17',7000.00,'2022-09-11','2022-09-17','just created || received',2,'2022-09-13 21:05:59',2,'2022-09-18 11:21:09','ACTIVE','PAID','INR','CREDIT',0),
(121,63,'2022-09-17',4800.00,'2022-09-11','2022-09-17','just created || paid',2,'2022-09-13 21:06:32',2,'2022-09-18 11:21:38','ACTIVE','PAID','INR','DEBIT',0),
(122,65,'2022-09-16',0.00,'2022-09-11','2022-09-17','just created || cancelled',2,'2022-09-13 21:10:42',2,'2022-09-16 13:53:53','ACTIVE','CANCELLED','INR','CREDIT',0),
(123,67,'2022-09-17',20000.00,'2022-09-11','2022-09-17','just created || Received',2,'2022-09-13 21:17:14',2,'2022-09-17 07:33:24','ACTIVE','PAID','INR','CREDIT',0),
(124,67,'2022-09-17',7500.00,'2022-09-11','2022-09-17','just created || paid',2,'2022-09-13 21:17:42',2,'2022-09-17 07:34:23','ACTIVE','PAID','INR','DEBIT',0),
(125,72,'2022-09-22',0.00,'2022-09-16','2022-09-22','just created || No connect',2,'2022-09-13 21:30:23',2,'2022-09-22 23:12:07','ACTIVE','PAID','INR','CREDIT',0),
(126,72,'2022-09-22',0.00,'2022-09-16','2022-09-22','just created || no connect',2,'2022-09-13 21:30:59',2,'2022-09-22 23:12:41','ACTIVE','PAID','INR','DEBIT',0),
(127,74,'2022-09-13',11740.00,'2022-09-13','2022-09-20','advance paid || advanced paid',2,'2022-09-14 14:42:04',2,'2022-09-14 14:44:11','ACTIVE','PAID','INR','CREDIT',0),
(128,74,'2022-09-20',2360.00,'2022-09-13','2022-09-20','just created || 2.36 paid for 2 days connect',2,'2022-09-14 14:43:43',2,'2022-09-20 22:16:26','ACTIVE','PAID','INR','DEBIT',0),
(129,60,'2022-10-02',0.00,'2022-09-18','2022-10-02','just created || no connect',2,'2022-09-15 16:32:51',2,'2022-09-23 20:23:00','ACTIVE','PAID','INR','CREDIT',0),
(130,60,'2022-10-02',0.00,'2022-09-18','2022-10-02','just created || no connect',2,'2022-09-15 16:35:25',2,'2022-09-23 20:23:13','ACTIVE','PAID','INR','DEBIT',0),
(131,63,'2022-09-24',7000.00,'2022-09-18','2022-09-24','just created || paid',2,'2022-09-15 16:39:29',2,'2022-09-24 08:38:04','ACTIVE','PAID','INR','CREDIT',0),
(132,63,'2022-09-30',7500.00,'2022-09-25','2022-09-30','just created || 1',2,'2022-09-15 16:40:14',NULL,NULL,'ACTIVE','PENDING','INR','CREDIT',0),
(133,63,'2022-09-24',4800.00,'2022-09-18','2022-09-24','just created || paid',2,'2022-09-15 16:41:09',2,'2022-09-24 08:38:15','ACTIVE','PAID','INR','DEBIT',0),
(134,75,'2022-09-17',10000.00,'2022-09-11','2022-09-17','just created || Received',2,'2022-09-16 13:55:21',2,'2022-09-18 22:23:44','ACTIVE','PAID','INR','CREDIT',0),
(135,75,'2022-09-17',6625.00,'2022-09-11','2022-09-17','just created || paid',2,'2022-09-16 13:56:38',2,'2022-09-18 11:55:35','ACTIVE','PAID','INR','DEBIT',0),
(136,75,'2022-09-03',10000.00,'2022-08-16','2022-08-22','started support and paid',2,'2022-09-16 13:59:13',NULL,NULL,'ACTIVE','PAID','INR','CREDIT',0),
(137,73,'2022-09-22',0.00,'2022-09-18','2022-09-24','just created || project closed',2,'2022-09-16 14:02:55',2,'2022-09-22 17:47:28','ACTIVE','CANCELLED','INR','CREDIT',0),
(138,73,'2022-09-22',0.00,'2022-09-24','2022-09-30','just created || project closed',2,'2022-09-16 14:03:48',2,'2022-09-22 17:46:19','ACTIVE','CANCELLED','INR','CREDIT',0),
(139,73,'2022-09-22',0.00,'2022-09-18','2022-09-24','just created || project closed',2,'2022-09-16 14:04:29',2,'2022-09-22 17:47:54','ACTIVE','CANCELLED','INR','DEBIT',0),
(140,73,'2022-09-30',0.00,'2022-09-24','2022-09-30','just created || project closed',2,'2022-09-16 14:05:35',2,'2022-09-22 17:46:53','ACTIVE','CANCELLED','INR','DEBIT',0),
(141,74,'2022-09-21',11740.00,'2022-09-21','2022-09-27','just created || received',2,'2022-09-16 14:10:33',2,'2022-09-21 12:58:33','ACTIVE','PAID','INR','CREDIT',0),
(142,74,'2022-09-27',6500.00,'2022-09-21','2022-09-27','just created',2,'2022-09-16 14:12:08',NULL,NULL,'ACTIVE','PENDING','INR','DEBIT',0),
(143,72,'2022-09-30',0.00,'2022-09-23','2022-09-30','just created || no connect',2,'2022-09-16 14:16:02',2,'2022-09-22 23:13:15','ACTIVE','PAID','INR','CREDIT',0),
(144,72,'2022-09-30',0.00,'2022-09-23','2022-09-30','just created || no connect',2,'2022-09-16 14:16:37',2,'2022-09-22 23:13:34','ACTIVE','PAID','INR','DEBIT',0),
(145,71,'2022-09-23',175.00,'2022-09-17','2022-09-23','just created || Received',2,'2022-09-16 14:18:15',2,'2022-09-23 20:16:09','ACTIVE','PAID','USD','CREDIT',0),
(146,71,'2022-09-23',1500.00,'2022-09-17','2022-09-23','DAY AS : 1500inr/DAY  || 1 day connect only i.e 1500inr',2,'2022-09-16 14:20:06',2,'2022-09-23 20:16:51','ACTIVE','PAID','INR','DEBIT',0),
(147,71,'2022-09-30',175.00,'2022-09-23','2022-09-30','just created',2,'2022-09-16 14:21:02',NULL,NULL,'ACTIVE','PENDING','USD','CREDIT',0),
(148,71,'2022-09-30',7500.00,'2022-09-23','2022-09-30','just created',2,'2022-09-16 14:22:02',NULL,NULL,'ACTIVE','PENDING','INR','DEBIT',0),
(149,67,'2022-09-24',20000.00,'2022-09-18','2022-09-24','just created || paid',2,'2022-09-16 14:26:04',2,'2022-09-24 08:36:33','ACTIVE','PAID','INR','CREDIT',0),
(150,67,'2022-09-24',7500.00,'2022-09-18','2022-09-24','just created || paid',2,'2022-09-16 14:26:41',2,'2022-09-24 08:36:48','ACTIVE','PAID','INR','DEBIT',0),
(151,67,'2022-09-30',20000.00,'2022-09-25','2022-09-30','just created',2,'2022-09-16 14:27:36',NULL,NULL,'ACTIVE','PENDING','INR','CREDIT',0),
(152,67,'2022-09-30',7500.00,'2022-09-25','2022-09-30','just created ',2,'2022-09-16 14:28:56',2,NULL,'ACTIVE','PENDING','INR','DEBIT',0),
(153,75,'2022-09-20',0.00,'2022-09-18','2022-09-24','just created || Project closed',2,'2022-09-16 14:36:17',2,'2022-09-20 11:14:49','ACTIVE','CANCELLED','INR','CREDIT',0),
(154,75,'2022-09-20',0.00,'2022-09-18','2022-09-24','just created || Project closed',2,'2022-09-16 14:36:55',2,'2022-09-20 11:15:07','ACTIVE','CANCELLED','INR','DEBIT',0),
(155,75,'2022-09-30',0.00,'2022-09-25','2022-09-30','just created || Making 0 as closing the project',2,'2022-09-16 15:20:01',NULL,NULL,'ACTIVE','CANCELLED','INR','CREDIT',0),
(156,75,'2022-09-30',0.00,'2022-09-25','2022-09-30','just created  ||  Making 0 as closing the project',2,'2022-09-16 15:20:56',NULL,NULL,'ACTIVE','CANCELLED','INR','DEBIT',0),
(157,64,'2022-09-16',0.00,'2022-09-11','2022-09-17','just created || duplicate entry',2,'2022-09-16 15:22:20',2,'2022-09-16 15:24:01','ACTIVE','CANCELLED','INR','CREDIT',0),
(158,64,'2022-09-24',2500.00,'2022-09-18','2022-09-24','just created',2,'2022-09-16 15:23:03',NULL,NULL,'ACTIVE','PENDING','INR','DEBIT',0),
(159,64,'2022-09-24',8639.40,'2022-09-18','2022-09-24','just created || Received',2,'2022-09-16 15:24:45',2,'2022-09-24 08:47:27','ACTIVE','PAID','INR','CREDIT',0),
(160,64,'2022-09-30',8639.40,'2022-09-25','2022-09-30','just created',2,'2022-09-16 15:26:07',NULL,NULL,'ACTIVE','PENDING','INR','CREDIT',0),
(161,64,'2022-09-30',2500.00,'2022-09-25','2022-09-30','just created',2,'2022-09-16 15:26:58',NULL,NULL,'ACTIVE','PENDING','INR','DEBIT',0),
(162,63,'2022-09-30',4800.00,'2022-09-25','2022-09-30','just created || 1',2,'2022-09-16 15:36:12',NULL,NULL,'ACTIVE','PENDING','INR','DEBIT',0),
(163,76,'2022-09-30',6000.00,'2022-09-15','2022-09-30','Only received payment',2,'2022-09-16 22:03:42',NULL,NULL,'ACTIVE','PENDING','INR','CREDIT',0),
(164,77,'2022-09-20',3500.00,'2022-09-20','2022-09-26','ADVANCE 3500 PAID OUR OF 7K || RECEIVED',2,'2022-09-20 11:54:58',2,'2022-09-20 11:55:19','ACTIVE','PAID','INR','CREDIT',0),
(165,77,'2022-09-20',-3500.00,'2022-09-20','2022-09-26','REST PAYMENT FOR 3500  || refund to client as project cancelled',2,'2022-09-20 11:56:12',2,'2022-09-22 15:41:28','ACTIVE','PAID','INR','CREDIT',0),
(166,77,'2022-09-20',0.00,'2022-09-20','2022-09-26','just created || project cancelled',2,'2022-09-20 11:56:53',2,'2022-09-22 15:41:43','ACTIVE','CANCELLED','INR','DEBIT',0),
(167,78,'2022-09-24',10700.00,'2022-09-24','2022-09-24','just created',2,'2022-09-24 16:09:06',NULL,NULL,'ACTIVE','PENDING','INR','CREDIT',1),
(168,78,'2022-09-24',8000.00,'2022-09-24','2022-09-24','just created',2,'2022-09-24 16:09:49',NULL,NULL,'ACTIVE','PENDING','INR','DEBIT',1);

/*Table structure for table `post` */

DROP TABLE IF EXISTS `post`;

CREATE TABLE `post` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `posteddate` datetime NOT NULL,
  `skills` text NOT NULL,
  `description` text NOT NULL,
  `status` enum('ACTIVE','INACTIVE') NOT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_by` bigint DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `posted` tinyint DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `post` */

insert  into `post`(`id`,`title`,`posteddate`,`skills`,`description`,`status`,`created_by`,`created_on`,`updated_by`,`updated_on`,`posted`) values 
(5,'NEED JAVA DEVELOPER','2022-08-26 00:00:00','JAVA ,NODE,REACT JS','This product is so good that i manage to buy it 1 for me and 3 for my families. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incidtation ullamco laboris nisi ut aliquip ex tris.','ACTIVE',3,'2022-08-26 16:38:18',NULL,'2022-08-26 16:38:26',1),
(6,'NEED PYTHON DEVELOPER','2022-08-31 00:00:00','PYTHON','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incidtation ullamco. Consectetur adipisicing elit, sed do eiusmod. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incidtation ullamco.','ACTIVE',3,'2022-08-26 16:41:50',NULL,'2022-08-26 16:41:55',1);

/*Table structure for table `project` */

DROP TABLE IF EXISTS `project`;

CREATE TABLE `project` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `project_code` varchar(100) DEFAULT NULL,
  `project_name` varchar(100) DEFAULT NULL,
  `project_desc` text,
  `required_skils` text,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `project_cost` varchar(100) DEFAULT NULL,
  `project_status` enum('PENDING','INPROGRESS','COMPLETED','CANCELLED') NOT NULL,
  `created_by` bigint NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_by` bigint DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') NOT NULL DEFAULT 'ACTIVE',
  `agent_user_role_id` bigint NOT NULL,
  `project_allocation_id` bigint DEFAULT NULL,
  `project_code_internal` text NOT NULL,
  `client_user_id` bigint NOT NULL,
  `currency` varchar(20) DEFAULT NULL,
  `freequency` enum('WEEKLY','BI-WEEKLY','TRI-WEEKLY','MONTHLY','HOURLY','TASK WISE','PROJECT WISE') NOT NULL DEFAULT 'WEEKLY',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_project_tagging` (`project_code`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=latin1;

/*Data for the table `project` */

insert  into `project`(`id`,`project_code`,`project_name`,`project_desc`,`required_skils`,`start_date`,`end_date`,`project_cost`,`project_status`,`created_by`,`created_on`,`updated_by`,`updated_on`,`status`,`agent_user_role_id`,`project_allocation_id`,`project_code_internal`,`client_user_id`,`currency`,`freequency`) values 
(58,'PC0001','JAVA-NAHED-JB-SUPPOR','JAVA SUPPORT','JAVA','2022-08-22','2022-09-22','45000.0','COMPLETED',2,'2022-08-29 20:50:23',2,'2022-08-29 21:01:07','ACTIVE',2,60,'519733912b6ZmFuN1AL7AGda3anXOICsLq5wgFd',77,'INR','MONTHLY'),
(59,'PC0002','Nitin Java project','JAVA PROJECT','JAVA','2022-08-26','2022-10-26','600.0','COMPLETED',2,'2022-08-31 15:07:26',2,'2022-08-31 15:08:22','ACTIVE',2,61,'g5mmUX3UditjZq91PnWFFjjYJLt568',95,'USD','MONTHLY'),
(60,'PC0003','Manjari-Sayantan Java Pro','JAVA PROJECT','JAVA','2022-08-26','2022-10-26','40000.0','INPROGRESS',2,'2022-08-31 16:01:43',NULL,NULL,'ACTIVE',2,62,'FbelOi0nRw2uZ0Hi0zPFfwXhlWpMUB',96,'INR','MONTHLY'),
(61,'PC0004','Python-Sampath-suppo','PYTHON PROJECT','PYTHON ,SQL','2022-08-26','2022-10-26','28000.0','INPROGRESS',2,'2022-08-31 16:42:19',NULL,NULL,'ACTIVE',2,63,'KEbXDJQNGwqial6JgwFIPCs3JyH99M',97,'INR','MONTHLY'),
(62,'PC0005','Adanwa Lady-devops-p','DEVOPS SUPPORT PROJECT','TERAFORM,AKS,ECS,AWS,GITHUB.SERVICE NOW','2022-09-02','2022-10-02','110.0','INPROGRESS',2,'2022-09-02 23:25:36',NULL,NULL,'ACTIVE',2,64,'RB1IGCtXuKH5cVCvnStnWkn9UBaOo5',104,'USD','MONTHLY'),
(63,'PC0006','Dimpi-Python Project','PYTHON PROJECT','PYTHON','2022-09-09','2022-10-28','700.0','COMPLETED',2,'2022-09-03 11:36:24',2,'2022-09-03 11:37:22','ACTIVE',2,66,'uQheuH9pSHsBE67KLTX5eDuczWNnxs',106,'CAD','MONTHLY'),
(64,'PC0007','QA-RAJ-CYPRES PROJEC','Testing job support','API TESTING,QA,SELENIUM','2022-08-23','2022-09-23','40000.0','COMPLETED',2,'2022-09-03 14:51:22',NULL,NULL,'ACTIVE',2,75,'H77OpzZrNOKFZugkMk1saxQKGgvexy',112,'INR','MONTHLY'),
(65,'PC0008','sankar-pythonproject','python project','python','2022-08-27','2022-11-27','1000.0','INPROGRESS',2,'2022-09-03 16:21:07',NULL,NULL,'ACTIVE',2,67,'WpO8OHhDfXzrZjxcGHdxuJhg0t1tFn',113,'USD','MONTHLY'),
(66,'PC0009','Sheetal-saurav-javap','java project','java','2022-09-01','2022-10-01','40000.0','CANCELLED',2,'2022-09-03 16:52:36',NULL,NULL,'ACTIVE',2,68,'ZlXrQfHCSiDuAqBcRmpkbj4uSvrLBp',116,'INR','MONTHLY'),
(67,'PC00010','Alero-devops-jobsupp','devops job support','AWS, GCP\r\n\r\nGradle, Maven, \r\n\r\nJenkins, Kubernetes(EKS, GKE), ArgoCD, Docker, ECR, \r\n\r\nJfrog, Sonarqube\r\n\r\nPrometheus, Grafana, Kibana, \r\n\r\nTerraform \r\n\r\nMySQL, MongoDB\r\nLanguage - Go, Python, Java','2022-09-04','2022-09-04','700.0','INPROGRESS',2,'2022-09-07 18:14:51',NULL,NULL,'ACTIVE',2,71,'9Bms5S0wfbXaCe3FMAe6GYRt9GpVvz',119,'USD','MONTHLY'),
(68,'PC00011','devfnd-JavaSpring-Sa','NA','JAVA\r\nOthers Migration Struts Dao to Spring MVC Store Procedure, SQL, Java8, Struts','2022-09-07','2022-10-07','550.0','CANCELLED',2,'2022-09-07 21:05:56',NULL,NULL,'ACTIVE',2,70,'7XCmaYAxKWlWObSEfNIFSvsG6ZVrKP',120,'USD','MONTHLY'),
(69,'PC00012','dev-Nelly-devops-jss','AWS','github actions jenkins datadog new relic bash terraform kubernetes ansible helm gangway openstack aws gcp -------------------------------------------------------- Requirements: Typically has 3 to 5 years of related DevOps experience Deep Knowledge of commonly used DevOps tooling concepts, hybrid-cloud environment Able to code in advance levels of Bash/Python with Golang a major plus 2-4 years of experience in any of the IaC technology such as Terraform, Ansible, Pulumi Working knowledge of CI/CD','2022-09-09','2022-10-09','650.0','INPROGRESS',2,'2022-09-09 14:35:02',NULL,NULL,'ACTIVE',2,72,'p5oWfJyfgMiU0PlTalL2QxrsnkPG5U',123,'USD','MONTHLY'),
(70,'PC00013','devfnd-rohit-javaFul','java fiullsract','java fiullsract','2022-09-13','2022-10-13','600.0','INPROGRESS',2,'2022-09-13 11:39:36',NULL,NULL,'ACTIVE',2,74,'LY3fxEzKYU5vsJOs7kml2PKyBMKrUe',126,'USD','MONTHLY'),
(71,'PC00014','Aru Need Java Job Su','NA','java 8, microservices, rest api, mariya db, snowflake, ocp','2022-09-12','2022-10-12','600.0','COMPLETED',2,'2022-09-13 20:27:57',NULL,NULL,'ACTIVE',2,73,'4ntX4cBhbSYZoHl5pcQsE9gpFPeJPD',127,'USD','MONTHLY'),
(72,'PC00015','angularBiswaSheetal','ang','ang','2022-09-15','2022-10-15','40000.0','INPROGRESS',2,'2022-09-16 21:58:44',NULL,NULL,'ACTIVE',2,76,'DP78Og9cR6ToSMbq890Zd5jKnJirXH',134,'INR','MONTHLY'),
(73,'PC00016','devfnd-java-promo-jsuppor','java support','java support','2022-09-20','2022-10-20','28000.0','CANCELLED',2,'2022-09-20 11:40:26',NULL,NULL,'ACTIVE',2,77,'oH8BFPKUCbsC827fynX6YmIQZC5T1N',137,'INR','MONTHLY'),
(74,'PC00017','devfnd-Sai-javaReact','JAVA','JAVA','2022-09-17','2022-09-30','1070.0','INPROGRESS',2,'2022-09-24 16:01:07',NULL,NULL,'ACTIVE',2,78,'pAJm2nUky1CL1tLoyPhJiE4o0o4nZN',142,'INR','HOURLY');

/*Table structure for table `project_allocation` */

DROP TABLE IF EXISTS `project_allocation`;

CREATE TABLE `project_allocation` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `project_id` bigint NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `freelancer_user_id` bigint NOT NULL,
  `freelancer_cost` varchar(100) NOT NULL,
  `project_status` enum('PENDING','INPROGRESS','COMPLETED','CANCELLED','CLOSED') NOT NULL,
  `created_by` bigint NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_by` bigint DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') NOT NULL DEFAULT 'ACTIVE',
  `revise_end_date` varchar(100) DEFAULT NULL,
  `payment_frequency_debit` enum('Monthly','Weekly','Bi-Weekly','Hourly') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Weekly',
  `currency` varchar(20) DEFAULT NULL,
  `payment_frequency_payment` varchar(50) DEFAULT NULL,
  `whatsapp_group` varchar(100) DEFAULT NULL,
  `client_pay_advance` tinyint NOT NULL DEFAULT '0',
  `freelancer_pay_advance` tinyint NOT NULL DEFAULT '0',
  `payment_frequency_credit` enum('Monthly','Weekly','Bi-Weekly','Hourly') CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT 'Weekly',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=latin1;

/*Data for the table `project_allocation` */

insert  into `project_allocation`(`id`,`project_id`,`start_date`,`end_date`,`freelancer_user_id`,`freelancer_cost`,`project_status`,`created_by`,`created_on`,`updated_by`,`updated_on`,`status`,`revise_end_date`,`payment_frequency_debit`,`currency`,`payment_frequency_payment`,`whatsapp_group`,`client_pay_advance`,`freelancer_pay_advance`,`payment_frequency_credit`) values 
(60,58,'2022-08-22','2022-09-22',79,'27000.0','COMPLETED',2,'2022-08-29 21:02:36',NULL,NULL,'ACTIVE','2022-09-23 22:42:23.957','Bi-Weekly','INR','13500','NA',0,0,'Bi-Weekly'),
(61,59,'2022-08-26','2022-10-26',79,'22000.0','COMPLETED',2,'2022-08-31 15:13:10',NULL,NULL,'ACTIVE','2022-09-20 23:50:14.008','Bi-Weekly','INR','11000','devfnd-Nitin-jbsuport',0,0,'Bi-Weekly'),
(62,60,'2022-08-26','2022-10-26',78,'28000.0','INPROGRESS',2,'2022-08-31 16:05:26',NULL,NULL,'ACTIVE','2022-10-24 00:00:00','Bi-Weekly','INR','14000','',0,0,'Bi-Weekly'),
(63,61,'2022-08-26','2022-10-26',84,'16000.0','INPROGRESS',2,'2022-08-31 16:47:54',NULL,NULL,'ACTIVE','2022-10-26 00:00:00','Weekly','INR','4000','devfnd-Sampath-python-sup',0,0,'Weekly'),
(64,62,'2022-09-02','2022-10-02',105,'10000.0','INPROGRESS',2,'2022-09-02 23:34:11',NULL,NULL,'ACTIVE','2022-10-02 00:00:00','Weekly','INR','2500','devops-jobSupport-Ad',1,0,'Weekly'),
(65,64,'2022-08-23','2022-10-23',89,'28000.0','CLOSED',2,'2022-09-03 14:54:37',NULL,NULL,'ACTIVE','2022-09-14 16:08:17.077','Weekly','INR','14000','devfnd-QA-raj-cypress',1,0,'Weekly'),
(66,63,'2022-08-12','2022-11-12',80,'25000.0','COMPLETED',2,'2022-09-03 15:47:13',NULL,NULL,'ACTIVE','2022-09-14 15:31:01.239','Weekly','INR','6250','devfnd-Dimpi-Pythonjobsup',0,0,'Weekly'),
(67,65,'2022-08-27','2022-10-27',80,'30000.0','INPROGRESS',2,'2022-09-03 16:22:37',NULL,NULL,'ACTIVE','2022-10-27 00:00:00','Weekly','INR','7500','devfnd-sankar-python-jobs',0,0,'Weekly'),
(68,66,'2022-09-01','2022-10-01',114,'30000.0','CANCELLED',2,'2022-09-03 16:53:58',NULL,NULL,'ACTIVE','2022-09-17 15:30:08.198','Bi-Weekly','INR','15000','no group',0,0,'Weekly'),
(69,67,'2022-09-04','2022-10-29',103,'28000.0','CLOSED',2,'2022-09-07 18:19:20',NULL,NULL,'ACTIVE','2022-09-07 21:21:40.452','Weekly','INR','7000','devfnd-olero-devops',1,0,'Weekly'),
(70,68,'2022-09-07','2022-10-07',78,'26500.0','CANCELLED',2,'2022-09-07 21:07:42',NULL,NULL,'ACTIVE','2022-09-13 12:14:44.773','Weekly','INR','6625','devfnd-JavaSpring-Sandeep',0,0,'Weekly'),
(71,67,'2022-09-07','2022-10-07',81,'33000.0','INPROGRESS',2,'2022-09-07 21:21:40',NULL,NULL,'ACTIVE','2022-10-07','Weekly','INR','8250','Devfnd-AleroFran-jsupport',0,0,'Weekly'),
(72,69,'2022-09-09','2022-10-09',105,'26000.0','INPROGRESS',2,'2022-09-09 14:39:54',NULL,NULL,'ACTIVE','2022-10-09 00:00:00','Weekly','INR','6500','dev-Nelly-devops-jss',1,0,'Weekly'),
(73,71,'2022-09-12','2022-10-12',78,'27000.0','COMPLETED',2,'2022-09-13 20:31:08',NULL,NULL,'ACTIVE','2022-09-23 20:04:39.788','Weekly','INR','6750','Aru Need Java Job Su',1,0,'Weekly'),
(74,70,'2022-09-13','2022-10-13',79,'26000.0','INPROGRESS',2,'2022-09-14 14:38:46',NULL,NULL,'ACTIVE','2022-10-13 00:00:00','Weekly','INR','6500','devfnd-rohit-javaFull-Jsu',1,0,'Weekly'),
(75,64,'2022-09-12','2022-10-12',132,'26500.0','COMPLETED',2,'2022-09-14 16:08:17',NULL,NULL,'ACTIVE','2022-09-20 11:16:02.451','Weekly','INR','6625','devfnd-Raj-UICypress-supp',0,0,'Weekly'),
(76,72,'2022-09-15','2022-10-15',135,'28000.0','INPROGRESS',2,'2022-09-16 22:00:18',NULL,NULL,'ACTIVE','2022-10-15 00:00:00','Bi-Weekly','INR','14000','no group',0,0,'Bi-Weekly'),
(77,73,'2022-09-20','2022-10-20',136,'20000.0','CANCELLED',2,'2022-09-20 11:51:50',NULL,NULL,'ACTIVE','2022-09-22 15:42:49.668','Weekly','INR','5000','devfnd-java-promo-jsuppor',1,0,'Weekly'),
(78,74,'2022-09-18','2022-09-30',143,'800.0','INPROGRESS',2,'2022-09-24 16:02:26',NULL,NULL,'ACTIVE','2022-09-30 00:00:00','Hourly','INR','800','devfnd-Sai-javaReact',1,0,'Hourly');

/*Table structure for table `resource_allocation` */

DROP TABLE IF EXISTS `resource_allocation`;

CREATE TABLE `resource_allocation` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `project_id` bigint NOT NULL,
  `start_date` varchar(100) NOT NULL,
  `end_date` varchar(100) NOT NULL,
  `project_status` enum('PENDING','INPROGRESS','CANCELED','COMPLETED') NOT NULL,
  `created_by` bigint NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_by` bigint DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `address` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `resource_allocation` */

/*Table structure for table `role` */

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `role_id` bigint NOT NULL AUTO_INCREMENT,
  `role_code` varchar(50) NOT NULL,
  `role_display_name` varchar(50) NOT NULL,
  `created_by` bigint NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_by` bigint DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') NOT NULL DEFAULT 'ACTIVE',
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `UC_role_name` (`role_code`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb3;

/*Data for the table `role` */

insert  into `role`(`role_id`,`role_code`,`role_display_name`,`created_by`,`created_on`,`updated_by`,`updated_on`,`status`) values 
(1,'ADMIN','Company',1,'2019-07-27 00:00:00',NULL,NULL,'ACTIVE'),
(2,'CONSULTANCY','Consultancy',1,'2019-07-27 00:00:00',NULL,NULL,'ACTIVE'),
(3,'CLIENT','Client',1,'2019-07-27 00:00:00',NULL,NULL,'ACTIVE'),
(4,'FREELANCER','Freelancer',1,'2019-07-27 00:00:00',NULL,NULL,'ACTIVE'),
(5,'AGENT','Agent',1,'2022-08-24 19:48:05',NULL,NULL,'ACTIVE'),
(6,'OPERATOR','OPERATOR',1,'2022-08-26 16:31:54',NULL,NULL,'ACTIVE');

/*Table structure for table `skill` */

DROP TABLE IF EXISTS `skill`;

CREATE TABLE `skill` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_by` bigint DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') DEFAULT 'ACTIVE',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `skill` */

insert  into `skill`(`id`,`name`,`description`,`created_by`,`created_on`,`updated_by`,`updated_on`,`status`) values 
(1,'JAVA','JAVA',NULL,'2022-08-27 18:57:37',NULL,'2022-07-10 11:24:59','ACTIVE'),
(2,'.NET','.NET RELATED SKILL',NULL,'2022-07-10 11:05:51',NULL,NULL,'ACTIVE'),
(3,'PYTHON','Python',NULL,'2022-08-27 18:57:35',NULL,'2022-07-10 11:24:05','ACTIVE'),
(4,'DEVOPS','3434',NULL,'2022-07-10 11:28:27',NULL,NULL,'ACTIVE'),
(5,'AWS','AWS',NULL,'2022-08-27 18:57:33',NULL,'2022-07-14 18:33:49','ACTIVE'),
(6,'SALESFORCE DEVELOPER','SALESFORCE DEVELOPER',NULL,'2022-08-27 18:57:09',NULL,NULL,'ACTIVE'),
(7,'SALESFORCE ADMIN','SALESFORCE ADMIN',NULL,'2022-08-27 18:57:30',NULL,NULL,'ACTIVE'),
(8,'DATABASE','DATABASE',NULL,'2022-08-27 18:57:55',NULL,NULL,'ACTIVE'),
(9,'SERVICE NOW','SERVICE NOW',NULL,'2022-08-27 18:58:07',NULL,NULL,'ACTIVE'),
(10,'QA AUTOMATION','QA AUTOMATION',NULL,'2022-08-27 19:46:11',NULL,NULL,'ACTIVE'),
(11,'QA MANUAL','QA MANUAL',NULL,'2022-08-27 20:13:25',NULL,NULL,'ACTIVE'),
(12,'API TESTING','API TESTING',NULL,'2022-08-27 20:23:10',NULL,NULL,'ACTIVE'),
(13,'SELENIUM','SELENIUM',NULL,'2022-08-27 20:27:28',NULL,NULL,'ACTIVE'),
(14,'AZURE DATA FACTORY','AZURE DATA FACTORY',NULL,'2022-08-31 18:49:38',NULL,NULL,'ACTIVE'),
(15,'AWS DATA ENGINEERING','AWS DATA ENGINEERING',NULL,'2022-08-31 18:49:57',NULL,NULL,'ACTIVE'),
(16,'ANGULAR','ANGULAR',NULL,'2022-09-03 12:08:50',NULL,NULL,'ACTIVE'),
(17,'REACT JS','REACT JS',NULL,'2022-09-07 10:16:55',NULL,NULL,'ACTIVE'),
(18,'AWS DEVOPS','AWS DEVOPS',NULL,'2022-09-07 10:20:19',NULL,NULL,'ACTIVE'),
(19,'FULLSTACK DEVELOPER','FULLSTACK DEVELOPER',NULL,'2022-09-08 12:21:49',NULL,NULL,'ACTIVE'),
(20,'JAVA FULLSTACK DEVELOPER','JAVA FULLSTACK DEVELOPER',NULL,'2022-09-08 12:22:06',NULL,NULL,'ACTIVE'),
(21,'PYTHON FULLSTACK DEVELOPER','PYTHON FULLSTACK DEVELOPER',NULL,'2022-09-08 12:22:40',NULL,NULL,'ACTIVE'),
(22,'UI DEVELOPER','UI DEVELOPER',NULL,'2022-09-08 12:23:06',NULL,NULL,'ACTIVE'),
(23,'MEAN STACK DEVELOPER','MEAN STACK DEVELOPER',NULL,'2022-09-08 12:23:09',NULL,NULL,'ACTIVE');

/*Table structure for table `systemtbl` */

DROP TABLE IF EXISTS `systemtbl`;

CREATE TABLE `systemtbl` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `fnm` varchar(255) NOT NULL,
  `tnm` varchar(255) DEFAULT NULL,
  `feml` varchar(255) DEFAULT NULL,
  `teml` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `dt` datetime DEFAULT NULL,
  `sub` longtext,
  `sms` longtext,
  `pic` longtext,
  `image` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

/*Data for the table `systemtbl` */

/*Table structure for table `template` */

DROP TABLE IF EXISTS `template`;

CREATE TABLE `template` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `template_name` varchar(100) NOT NULL,
  `template_content` longtext NOT NULL,
  `remarks` text,
  `created_by` bigint NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_by` bigint DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') NOT NULL DEFAULT 'ACTIVE',
  `type` enum('DEFAULT','CUSTOM') NOT NULL DEFAULT 'DEFAULT',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `template` */

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `user_id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `gender` enum('MALE','FEMALE','OTHER') NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile_no` varchar(20) NOT NULL,
  `user_name` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `remark` text,
  `otp` varchar(10) DEFAULT NULL,
  `isblocked` tinyint NOT NULL,
  `created_by` bigint NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_by` bigint DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') NOT NULL,
  `usercode` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `UC_user_mobile_no` (`mobile_no`),
  UNIQUE KEY `UC_user_email` (`email`),
  UNIQUE KEY `UC_user_user_name` (`user_name`),
  UNIQUE KEY `USER_UNIQUE_CONSTRAIN` (`mobile_no`,`user_name`,`usercode`)
) ENGINE=InnoDB AUTO_INCREMENT=144 DEFAULT CHARSET=utf8mb3 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Data for the table `user` */

insert  into `user`(`user_id`,`name`,`gender`,`email`,`mobile_no`,`user_name`,`password`,`address`,`remark`,`otp`,`isblocked`,`created_by`,`created_on`,`updated_by`,`updated_on`,`status`,`usercode`) values 
(1,'Admin','MALE','admin@gmail.com','9348999171','admin','$2a$10$11hWpxRKydREQIYLzxZmBu8hIeRKbsViiNExmW8ZPLaXFhlKh2gp2','Patia, Bhubaneswar, Odisha, India','',NULL,0,1,'2022-06-29 18:38:24',NULL,NULL,'ACTIVE','yrty566rgry'),
(2,'Consultancy','MALE','consultancy@gmail.com','9846474747','consultancy','$2a$10$U.jyjolWO91wFHesqEmBRepAyCUmkAkEDePS6zAAjzGL.Vm3nA1v6','NA',NULL,NULL,0,1,'2022-07-29 14:18:42',NULL,NULL,'ACTIVE','IoCvpSk7uopolm3op0caMGuQwdZpZf1227145848'),
(3,'Operator','MALE','operator@gmail.cm','9235252523','operator','$2a$10$pALIyaFv5IAG4k0JAupXluPeLqzoisnVek6jnzG0FY6KCWh8Z0LX2','Bhubaneswar, Odisha, India1',NULL,NULL,0,1,'2022-08-24 20:02:17',3,'2022-08-27 11:32:12','ACTIVE','8I70FDwBDRolm6pgz1e9mDD04Yt9Bo-800806417'),
(77,'Nahed','MALE','nahed@gmail.com','+1-4372190666','nahed','$2a$10$wi679ZZF9HVhTJFXaN0D4uuA/z4X8euDPAy3bM/ZumA2rEmMwfYhS','CANEDA',NULL,NULL,0,2,'2022-08-27 15:19:30',NULL,NULL,'ACTIVE','8VOsQePi6n7XSvGoyTH59wlFAgkaib-558573501'),
(78,'Sayantan','MALE','sayantan@gmail.com','+91-8961308286','sayantan','$2a$10$QMUSR7EIX4HE04JWlPzlcubY14IcJEhbad643Tnx3lF3RkMdCQAcO','KOLKATA BUT STAY IN BANGLORE',NULL,NULL,0,2,'2022-08-27 18:49:04',NULL,NULL,'ACTIVE','NVsdhqnbSSTMV5ms4Qk2Ny1Wc1sFSc-545999150'),
(79,'Nirmalya Roy','MALE','nirmalya@gmail.com','+91-9674528152','nirmalya','$2a$10$Kv/.D9Iw6EmKslhaxpADnOsnmCq9v7CDQ7siN3LGy1YTraihXyN6q','KOLKATA',NULL,NULL,0,2,'2022-08-27 18:51:20',NULL,NULL,'ACTIVE','sIJMR6K69r4oXsgSpYUWL9q95u82qV-545863272'),
(80,'Akash','MALE','akash@gmail.com','+91-7337673065','akash','$2a$10$04L4keeKQMReqMxP37gLxe7h4ARX1va4yti8zwkhPL7V5rbdWLrQq','KOLKATA',NULL,NULL,0,2,'2022-08-27 18:52:59',NULL,NULL,'ACTIVE','kBkbM0vZJ3rDwNSvvdJRHBZOg4L7x3-545764538'),
(81,'Mithilesh','MALE','mithilesh@gmail.com','+91-6290892459','mithilesh','$2a$10$Z2sMfd48TuwRDlXFBVF54u9dbDw.Sn8A1Ds01ratWCcKIcjVI5nya','NA',NULL,NULL,0,2,'2022-08-27 18:56:27',NULL,NULL,'ACTIVE','7r11aKM2OWk9zBsqBfRnlyFSNMfVOV-545556842'),
(82,'Mahi','MALE','mahi@gmail.com','+91-7088447225','mahi','$2a$10$3bf7hajxHL9Nle6qZ4LhfOYjFPOhqdZ3XChMKAbmgNGXoz.S.OouW','NA',NULL,NULL,0,2,'2022-08-27 18:59:35',NULL,NULL,'ACTIVE','dbDiDUqMguRM1XqCIurnnf4RY64TBX-545368538'),
(83,'Prashanta','MALE','prashanta@gmail.com','+91-9900877553','prashanta','$2a$10$Ju7xYrNHH4QhJGawW4lBpuVyX.sTTqM.ikc52o4SQ0mj88yB2MdlW','na',NULL,NULL,0,2,'2022-08-27 19:01:06',NULL,NULL,'ACTIVE','jLvDEqDfXndlptaPWbVWA74r8eJqgA-545278379'),
(84,'Saya','MALE','saya@gmail.com','+91-9000454344','saya','$2a$10$LcHJqtMs.ugjwr6tJKMWau8nGzXdCCBSMKg6CQybK.sxp40CrUGTi','NA',NULL,NULL,0,2,'2022-08-27 19:04:50',NULL,NULL,'ACTIVE','9DedZgdEVy7M5bAkzbeandhj6EPHvd-545052680'),
(85,'Ayush','MALE','ayush@gmail.com','+91-9427636230','ayush','$2a$10$IfJvLMgNkR5gwYmfsVEDB.X71wX4eHE.dvST19n73FIzVPleettle','NA',NULL,NULL,0,2,'2022-08-27 19:06:36',NULL,NULL,'ACTIVE','beUy1upB5CtoOMEItrkbpXqwp0aXUt-544947287'),
(86,'Swapnika','MALE','swapnika@gmail.com','+91-9642539636','swapnika','$2a$10$9P6Dv6WPlX97id6xv8ODo.jTCBDCm6FguD8Uzg.WA6q6AXMnd0pjm','NA',NULL,NULL,0,2,'2022-08-27 20:07:07',NULL,NULL,'ACTIVE','nZmoUxbDpjjUV7BP6mg9S6KmW1nlCR-541316980'),
(87,'Ashish','MALE','ashish@gmail.com','+91-7411003184','ashish','$2a$10$ZMxeDViXxrqQWVdUHkq4.eMhQKL6ODJsNMimg.CGk4YMy4VO//yYe','NA',NULL,NULL,0,2,'2022-08-27 20:16:29',NULL,NULL,'ACTIVE','rGbahG06GBHRnx27iYq6msN2zaDVEU-540754765'),
(88,'YN','MALE','yn@gmail.com','+91-7414911524','yn','$2a$10$quEKOQ0Pw1.HYBrPOeW1LuwvXO2pslKTerslcPtCgJm9nfwBZQan2','NA',NULL,NULL,0,2,'2022-08-27 20:26:22',NULL,NULL,'ACTIVE','QNJX4Hd1T1cc9HPrMygFRYEDfPeXIZ-540161493'),
(89,'Harsh Bhatia','MALE','harsh@gmail.com','+91-8905003200','harsh','$2a$10$b5lGI9gvka5yZJmqc1.Or.uGyeNsNiI9dH5eMc60wK7eXKF9MFXsK','NA',NULL,NULL,0,2,'2022-08-27 20:31:03',NULL,NULL,'ACTIVE','mbJeZt79yIRa0L9j8qNVjGRkITWxOw-539880532'),
(90,'Arul','MALE','arul@gmail.com','+91-9003868909','arul','$2a$10$I8IrBq8P8MgcBL0UfB3tLuXNjPz7CoY0W5fMGSd2qI05OiiZ3.q8q','NA',NULL,NULL,0,2,'2022-08-27 20:33:58',NULL,NULL,'ACTIVE','Hxvir1KZ5xCb5xeHuRvleczPxMooR5-539706280'),
(91,'Virendra','MALE','virendra@gmail.com','+91-9716228838','virendra','$2a$10$iwpXoImKEENLfGs84zfHw.C1WbOdBoiD5sJiYw9jHo7cChNkV7cXm','NA',NULL,NULL,0,2,'2022-08-27 20:37:53',NULL,NULL,'ACTIVE','rR9q6ozbZVNUDHwA7i710x8XPNZ5an-539470359'),
(92,'Maruti','MALE','maruti@gmail.com','+91-9741766649','maruti','$2a$10$sxJ0CJyvIv0QU7Dd4hpRYeI1yxnewJeY5fZdSuVNPK0IMtLgZZCIu','NA',NULL,NULL,0,2,'2022-08-27 20:40:18',NULL,NULL,'ACTIVE','DEMzdTqDTWGpdBstR9hzBAG8Exa6UY-539325824'),
(93,'Rani','FEMALE','rani@gmail.com','+91-9820260827','rani','$2a$10$xlPBYxHzu.2tU8c6/ms6vOeT4fXYIC8m/lwRoHxf/VcucEKFnWmzG','MUMBAI',NULL,NULL,0,2,'2022-08-27 20:50:19',NULL,NULL,'ACTIVE','a7IsU5L7u5PU5rWboBSqxKZMYD2Sda-538724400'),
(94,'Srikant','MALE','srikant@gmail.com','+91-8296647516','srikant','$2a$10$Y1xOcVhXaihXlKtPP4ZgYu1BHq0A2e8a/Sk73/uFvaM7Ep0ilZcF2','Banglore',NULL,NULL,0,2,'2022-08-27 20:53:32',NULL,NULL,'ACTIVE','7HvtqZspQ8bR5JlQKgDgRaD9ZIznyv-538531027'),
(95,'Nitin','MALE','nitinclient@gmail.com','+1-9014468136','nitinclient','$2a$10$zs7GiIZ078w2wD8V4CMvPuso1jQtGxjI32fdOYAW4Ju8ympomPRE.','USA',NULL,NULL,0,2,'2022-08-31 14:34:12',NULL,NULL,'ACTIVE','daBqtO4NQs37Dx3YXYmAWoycSdKdjt-215691442'),
(96,'Manjari','FEMALE','lclient@gmail.com','00-0000000000','lclient','$2a$10$Zz0nHPeZVOePgmRDb9t/0uR.l4Y1FSM4fEsIXo9xulGoyzplCBiHa','USA MQAY BE',NULL,NULL,0,2,'2022-08-31 15:58:42',NULL,NULL,'ACTIVE','8TVfvOc15FGq9V9Rw6UrciHJN3o7yC-210621569'),
(97,'Sampath','MALE','sampath@gmail.com','+91-9912230888','sampath','$2a$10$.IkrSFQa/K0mKzt4oiiKbedQJLLTRm411uPfffBmbqi7Pe8SSzkiG','NA',NULL,NULL,0,2,'2022-08-31 16:38:47',NULL,NULL,'ACTIVE','gqblmfs36rhjCWbkjwpNYtcBxvQjYX-208216367'),
(98,'Mayank Gupta','MALE','mayank@gmail.com','+91-6260049003','mayank','$2a$10$DVEh0Gr7I8IjhF19G/Vy5.jX7VcPXnvIfHpZti/vJ9UWPSY6s.j4K','NA',NULL,NULL,0,2,'2022-08-31 18:33:31',NULL,NULL,'ACTIVE','XryyjkiKV5MfSYndWN8U2w2pbBQ8SY-201332256'),
(99,'Soujanya','MALE','Soujanya@gmail.com','+91-9502265763','Soujanya','$2a$10$jIGDQBqlHvFfLpFd4YACfeTtwmuf/auEfqb3D8.IrSJLqJvKSSsIy','NA',NULL,NULL,0,2,'2022-08-31 18:35:26',NULL,NULL,'ACTIVE','W8v5PJSAon5lyGkAc9z5INT7npalxF-201217580'),
(100,'Vinav Mevada','MALE','Vinav@gmail.com','+91-8866655719','Vinav','$2a$10$9lKlqfP.ej4dBYCl9BjgF.yRkwcIOVK1jhzO83WBD8UMQb2/V/Y1K','na',NULL,NULL,0,2,'2022-08-31 18:37:41',NULL,NULL,'ACTIVE','feMYVivAdYnBefB4al2NqUcx7ltWM1-201083271'),
(102,'Sampath Freelancer','MALE','sampfree@gmail.com','+91-09912230888','sampfree','$2a$10$f3NbpdA.QHbW6vsPgUP5Cue3R4hjdeI24YTdiqX7xHvbrf7oTfI6m','na',NULL,NULL,0,2,'2022-08-31 18:51:15',NULL,NULL,'ACTIVE','NMWrYwNg5Xz9NlFZkJP6iS9afq0zgp-200268714'),
(103,'Bhargavi','FEMALE','bhargavi@gmail.com','+91-9705218153','bhargavi','$2a$10$myaawtrpH5n2/5q43ffreOLKavVYj/Lmd/f9GzId.EHLpPoGsSGVS','Banglore',NULL,NULL,0,2,'2022-08-31 22:54:23',NULL,NULL,'ACTIVE','uwfmvPqb0wsj17bEKTvA8bpy0D6lyh-185680693'),
(104,'Adanwa','FEMALE','Adanwa@gmail.com','+1-8322452018','Adanwa','$2a$10$URjJ618SzOBXkwbhxYNefeV/AZSxU86g4s6uKookbjxnStMp.hYP.','USA',NULL,NULL,1,2,'2022-09-02 23:22:17',NULL,NULL,'ACTIVE','mw3CRVe4XI4nsYxjjkHepHRL299nrQ-11207086'),
(105,'Nitin Freelancer Engineer','MALE','nitinengineer@gmail.com','+91-9041030122','nitinengineer','$2a$10$bZfkRnS/1PZnPSo7N44xmeabvUStv2nR59EU1qaQy5bH.rRKpKnU.','NA',NULL,NULL,1,2,'2022-09-02 23:29:57',NULL,NULL,'ACTIVE','R9uCMKg7A2Lv7nvdGr3qSFDIWdZogn-10746391'),
(106,'Dimpi','FEMALE','dimpi@gmail.com','+1-4166597534','dimpi','$2a$10$8/0/MT9qXeoSquAk5.avb.OuHbK6xa//mVFql8qJgbBVHf9Ssfd7.','CANADA',NULL,NULL,1,2,'2022-09-03 11:29:25',NULL,NULL,'INACTIVE','n6qIJF2c5K9ws1UXNwYvaNGZsqxY2b32421692'),
(107,'Sunil Kumar','MALE','sunilkumar@gmail.com','+91-7417793087','sunilkumar','$2a$10$DPgvireQeNTcz7nhhteFA.C05VwLpP75XMyfV7VLM7IJaXu9lbjrS','NA',NULL,NULL,1,2,'2022-09-03 12:11:01',NULL,NULL,'ACTIVE','o3Ju1QNru9B9vcZeD6PUWsuWXSusVm34917414'),
(108,'Vignesh','MALE','Vignesh@gmail.com','+91-7598059677','Vignesh','$2a$10$BdRGkuQLS/AYHsAgrMs9OOtdcip0S3HJn6KQLmWuOo2jkmjBQtBH.','TAMILNADU',NULL,NULL,1,2,'2022-09-03 12:20:14',NULL,NULL,'ACTIVE','If5eoZyvKcWNDaCFssKrwzz8hjrzOg35469939'),
(109,'MR Pradip Savaliya','MALE','pradip@gmail.com','+91-8980467050','pradip','$2a$10$WFeVrhpvND0RY9Lttw5JHeyBaz30C8tiaFVpHbtzpETPd/YAic5B2','Surat,Gujurat',NULL,NULL,1,2,'2022-09-03 12:22:45',NULL,NULL,'ACTIVE','Kxen6rN2GchAZOefo1QQ8M1aqSHXwn35620946'),
(111,'Harse vatia Freelancer','MALE','harsevatiaf@gmail.com','+91--8905003200','harsevatiaf','$2a$10$NMmD5hbpE/nCF.r1u/6Ji.HI1abO4DD1tkwUFjtwdUF3AX0.TICNa','NA',NULL,NULL,1,2,'2022-09-03 14:36:46',NULL,NULL,'ACTIVE','3apXFzUq6NllrHYTIiVuF4NpdvI7hF43661938'),
(112,'Rajsekhar Abduls friend','MALE','rajsekhar@gmail.com','+1-5193189952','rajsekhar','$2a$10$empO5TGC0UYUl28b6VpzLeJCJeDDYOV6SZwvRogyMx1vUCS85Cnne','CANADA',NULL,NULL,1,2,'2022-09-03 14:40:15',NULL,NULL,'ACTIVE','H9EB1TQGQt4oFEaiEonVdyd3JGyclg43871447'),
(113,'Sankar','MALE','sankar@gmail.com','+1-4165613419','sankar','$2a$10$QN73HU0.agx6OJpKgbAutuK7Uq2uhJlhCTuAJEJ1xiYLI9J4iBCzK','USA I THINK',NULL,NULL,1,2,'2022-09-03 16:17:10',NULL,NULL,'ACTIVE','GvMNVTvFOBavrEPOjZPgoNtT6X9YiK49685609'),
(114,'Saurav','MALE','saurav@gmail.com','+91-7008679497','saurav','$2a$10$XV3A3MchnjE2WkfP7nhly.mUzpwZ/.QLil9MRM1yzKVsVPyL/N6IS','NA',NULL,NULL,1,2,'2022-09-03 16:45:21',NULL,NULL,'ACTIVE','ZXFGfChvxzqOLESNlbMlLXytKkftVa51377654'),
(116,'client','MALE','client@gmail.com','00-0000000001','client1','$2a$10$KCxZttKmkY8XG2vPZnRrNeyfe/uDOfMOPTXTCfniUSd0EpBPTaR42','NA',NULL,NULL,1,2,'2022-09-03 16:48:47',NULL,NULL,'ACTIVE','nliPqlJCJxNK8C3FWSKYVPrr9z1AQO51583393'),
(117,'p kUMAR','MALE','pkumar@gmail.com','+91-9309816067','pkumar','$2a$10$aVNoHS4T.fEIBkrJOzhctuqSa7/W63qMhl68ud2ENop3BBhpKaq9a','Hydrabad',NULL,NULL,1,2,'2022-09-06 23:31:19',NULL,NULL,'ACTIVE','9ZGmYZhQopm3sksqBXIJ7BdEB9Veub334935850'),
(118,'sHRIKANT','MALE','skant@gmail.com','+91-0829664751','SKANT','$2a$10$2oE6YLqna3zePU8jYeT58u1njQD91cpwO20CGjRT2fyMrpD7d1oBi','na',NULL,NULL,1,2,'2022-09-06 23:48:52',NULL,NULL,'ACTIVE','nI43Tr3r5zqIFJ8LdruHBOshO8ISbu335987761'),
(119,'Alero Francess','FEMALE','aleroFrancess@gmail.com','+1-4707366963','aleroFrancess','$2a$10$7Sv93RvO4gTWR8reKKJpReDryaFJy62NkUSz6DcVfqCwE8m7FJSmy','USA',NULL,NULL,1,2,'2022-09-07 18:11:02',NULL,NULL,'ACTIVE','GWsAjALRfaSSPbgCRR6BoKOWf8GK5v402117754'),
(120,'Raj Sandeep','MALE','s@gmail.com','+1-6828125256','rajsandeep','$2a$10$aCPaPWNcS8KOrSXOMN53iujiOwW1wsIGsaCPd/05bbCFIymboy.8e','USA',NULL,NULL,1,2,'2022-09-07 21:03:42',NULL,NULL,'ACTIVE','Uh0lDEcPtcoGJWnurxMwtSnYV8aF1S412478068'),
(121,'Vamsi','MALE','vamsi@gmail.com','+91-9640321589','vamsi','$2a$10$gcbeXqoW7G.XG85HA7VC0e6o1DxHK13G4CBU3kzRlYA9kiyus5ZWe','Hydrabad',NULL,NULL,1,2,'2022-09-09 10:45:35',NULL,NULL,'ACTIVE','XBmO7EmpgmEdU6NkS1ljwfJNNM9S4u548191704'),
(122,'Pratap','MALE','pratap@gmail.com','+91-9491456510','pratap','$2a$10$CqsQE7zNb7wznUYL1H0Kee6HpOPIxPnC4mO4Nc1W0cVH8ddI4x1Eu','Hydrabad',NULL,NULL,1,2,'2022-09-09 12:25:50',NULL,NULL,'ACTIVE','ujicN2pZShauS4ujpuuDpQRj3ezu2I554206539'),
(123,'Nelly','FEMALE','nelly@gmail.com','+1-4025162294','nelly','$2a$10$r.7Fg09ugZpgLk6YQytMZ.UH8KSvwY8IEz9HeKW95yPGOQtIyYacq','USA',NULL,NULL,1,2,'2022-09-09 14:31:39',NULL,NULL,'ACTIVE','ZWv0m03ZHEKLj7dsxORqZ5Fs70VGOQ561754690'),
(124,'Kaustav Karmakar','MALE','kaustavkarmakar2@gmail.com','+91-9674774916','kaustav','$2a$10$FWsTaK/fTm3e.Mdu0IGrbOjQCpwRM4WJd41S5IVfaTbRGx1yQkkUS','Kolkata,WB,700078',NULL,NULL,1,2,'2022-09-10 08:49:06',NULL,NULL,'INACTIVE','ZBQKLvHtgIm21MmjJN6bbbIrYUxRIL627602260'),
(125,'Chhotu Sow','MALE','chhotukumarsow@gmail.com','+91-8210118348','chhotu','$2a$10$axvX31pAEwqJUaamp/XkyOIu8Mf/1UJpQWvg7z9gofS/krJykfqRe','BIhar',NULL,NULL,1,2,'2022-09-10 09:19:21',NULL,NULL,'ACTIVE','oefncQnDp9OgkwNcVNWKCKwMuZkFPT629417648'),
(126,'Rohit Sharma','MALE','rohitsharma@gmail.com','+1-4164201775','rohitsharma','$2a$10$WeplwNiCYc3gx5cR5AJeUuiQn9sekPQLdDXv7CUq6rwN65.3k8LcC','USA',NULL,NULL,1,2,'2022-09-13 11:35:49',NULL,NULL,'ACTIVE','whW9exeEcQ4zgxsyYZqqJWrcKQYFmu896807823'),
(127,'arunima robert','FEMALE','na@gmail.com','+1-2018441294','arunimarobert','$2a$10$HqwIgt0rqQZlJvNtKpTfjeTpFxhW89vNnLVB07VYCMLrBx0iEi8p.','newjersey',NULL,NULL,1,2,'2022-09-13 20:22:50',NULL,NULL,'ACTIVE','MqRITYgYs3eaND3kcxw1IEb8VUtv3J928426343'),
(128,'Raj Happy','MALE','7028157588@gmail.com','+91-7028157588','rajhappy','$2a$10$vdH.pKoENDtb/Y1qLwgpleLfMjC88M36zRP8eTDZWK8cI5uHu2DI6','NA',NULL,NULL,1,2,'2022-09-14 15:37:16',NULL,NULL,'ACTIVE','P3sxH1MmxVngZXZMcGIYp5kc4pnJ1w997692620'),
(129,'Kartik','MALE','6300257389@gmail.com','+91-6300257389','kartik','$2a$10$OEHAkAqKN5NxWNwcPBtizeGdgy8X2U3N00XVnwLRTMOhsfdMwBqhi','NA',NULL,NULL,1,2,'2022-09-14 15:40:39',NULL,NULL,'ACTIVE','YNIx8uS4efXkpbZpgVCiW6nkNHMz1w997895459'),
(130,'Mahesh babu','MALE','9491046309@gmail.com','+91-9491046309','mahesh','$2a$10$jJvr20M5NwqoHeFyL37J9u4S7b3.YiErr4AEdjapBxqwNytXQ6i8u','NA',NULL,NULL,1,2,'2022-09-14 15:52:49',NULL,NULL,'ACTIVE','6AIJFhdhr4qkqziVdiVv5yeZ5YubEU998624798'),
(131,'Mallikarjun','MALE','8125307094@gmail.com','+91-8125307094','mallikarjun','$2a$10$/LukcLYrvAm04833Konkme9o2VX/L7ojHB66BncHeVvTOr8aoggcK','NA',NULL,NULL,1,2,'2022-09-14 15:56:06',NULL,NULL,'ACTIVE','oWKkOKwmBp3qoeW9DWgsd9jGtC4syQ998822144'),
(132,'Chandra Sekhar','MALE','9553090030@gmail.com','+91-9553090030','chandrasekhar','$2a$10$YQlqjmLOBypWgMsPKUsF..OzZ42.kf.Yn7HAkgqxgy8y9f8CqnUvG','NA',NULL,NULL,1,2,'2022-09-14 16:04:09',NULL,NULL,'ACTIVE','WWNoXOQLggAxy6Ompoj28CfJQSuriC999304878'),
(133,'Sujay Kamal Madisetty','MALE','7702822114@gmail.com','+91-7702822114','SujayKamalMadisett','$2a$10$z/Z7bLScrScjAWT5SQjzJuzdWXX16XnEfZUN16f8.cKroGIUuTI46','Melbourne',NULL,NULL,1,2,'2022-09-15 21:43:52',NULL,NULL,'ACTIVE','NP8puddYYMgJPq5SAv6FcflhKrarJG1106087948'),
(134,'client3','MALE','client3@gmail.com','+1-0000000002','client3','$2a$10$imotbniQGbRbBUFygAmKoOuW5N1NErvhpDzTLNd3RD8s3laY5DUme','USA',NULL,NULL,1,2,'2022-09-16 21:54:58',NULL,NULL,'ACTIVE','bZ9bGFJcBCcpeydOPCEdcfjN4CLhSo1193154553'),
(135,'Biswajit Rout','MALE','9853832028@GMAIL.COM','+91-9853832028','9853832028','$2a$10$0TVq7yEeKcEqCSkFVuECf.0fAPlMoNsykolWA/8ZP7aTNTqQRXx/u','BBSR',NULL,NULL,1,2,'2022-09-16 21:57:12',NULL,NULL,'ACTIVE','wCg092zbZc59Y8N8nSLVOwt4zjSSy21193288478'),
(136,'Promodini','MALE','7873263683@gmail.com','+91-7873263683','promo','$2a$10$sIP3VRQMwoWi0eS7kWmglOyXgvZHtDbi3GNWCQeOaTC8Nq5tQ6mDu','RAULKELA,ODISHA',NULL,NULL,1,2,'2022-09-20 11:20:37',NULL,NULL,'ACTIVE','PSzhJscL9pYsph9OkvS3KxbaqF2UHD1500693195'),
(137,'BHARATH DS','MALE','9391206429@gmail.com','+91-9391206429','BHARATHDS','$2a$10$a02ao7D9JpR7QlLE2zN7ZulhzUhnzYCfR3IYrfkFqXSxlvZcuzs7K','BANGALORE ',NULL,NULL,1,2,'2022-09-20 11:36:10',NULL,NULL,'ACTIVE','5p43InKZmzvncQlRm6wi1hspgUvltp1501626442'),
(138,'Venkatesh Reddy','MALE','9110613157@gmail.com','+91-9110613157','9110613157','$2a$10$AqR//kI42PU0yY22FyMB8.XvKmAPlaE/6N5sQLkEHpi.34yd0YgDu','bangalore',NULL,NULL,1,2,'2022-09-20 12:38:03',NULL,NULL,'ACTIVE','tqY9r2bJfQMHTvpwzxpHzGXX1RUJky1505339484'),
(139,'Potnuru Dilish','MALE','9014396312@gmail.com','+91-9014396312','9014396312','$2a$10$yusK2YNzdsLqhGzFupvzbOau4x5uXFYR/.iTYodLyBSVnu5wlYjoC','Visakhapatnam ',NULL,NULL,1,2,'2022-09-20 22:39:57',NULL,NULL,'ACTIVE','2Ie9d0o3peILZFYZt8FVPp7JdklLVO1541453432'),
(140,'Rushikesh Shrikrishna Jadhav','MALE',' rjadhav9396@gmail.com','+91-9604018888','9604018888','$2a$10$y838v7wNG.7qm4vjS7VDju7QBKe05v.UhRsJTi5ueDnjBvOGnDA.S','Kharadi,Pune',NULL,NULL,1,2,'2022-09-20 22:48:38',NULL,NULL,'ACTIVE','OpE95BYlxwLqXLrlpGSpry5q1cUQWz1541974367'),
(141,'Raveendra Gollaprolu','MALE','raveendragollaprolu@gmail.com','+91-9550763436','9550763436','$2a$10$vzsZNzzZaXq8euwmgKIXP.tgYY1Fs9bdSGeJcB6O/nzkUWiUw1nGi','KR Puram, Bangalore',NULL,NULL,1,2,'2022-09-21 12:03:02',NULL,NULL,'ACTIVE','tFWZWGvmAPsxJDkAvgo6Ioc1I58fIZ1589637873'),
(142,'Sai','MALE','6479693694@gmail.com','+1-6479693694','Saiuname','$2a$10$dTHt9SgsFz7FR3LbqxPBw.oM2tVUYJFHt9NBgBjsMPjn9cbWARmFK','CANADA',NULL,NULL,1,2,'2022-09-24 15:54:15',NULL,NULL,'ACTIVE','64WZ8Qjhamkj2POgw4qQoO05omSfDS1862711353'),
(143,'Arun','MALE','7010197976@gmail.com','+91-7010197976','7010197976','$2a$10$kO0uptutXc4uNTEPrFmIZelmZRTHZPtjEOQhAImXZqS3jqOUnzwoi','na',NULL,NULL,1,2,'2022-09-24 15:57:57',NULL,NULL,'ACTIVE','b6g3yPMLjE75YXuTgiIHsGbtCQB4o21862933436');

/*Table structure for table `user_info` */

DROP TABLE IF EXISTS `user_info`;

CREATE TABLE `user_info` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` bigint NOT NULL,
  `profile_pic` varchar(255) DEFAULT NULL,
  `profession` varchar(100) DEFAULT NULL,
  `organization_name` varchar(100) DEFAULT NULL,
  `organization_details` text,
  `logo` varchar(255) DEFAULT NULL,
  `id_proof_name` varchar(100) DEFAULT NULL,
  `id_proof_number` varchar(100) DEFAULT NULL,
  `id_proof_document` tinytext,
  `fb_link` tinytext,
  `website_link` tinytext,
  `insta_link` tinytext,
  `linkdean_link` text,
  `standardPayment` varchar(100) DEFAULT '0.00',
  `support_type` enum('ONLINE','OFFLINE','ONLINE & OFFLINE') DEFAULT 'ONLINE',
  `created_by` bigint NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_by` bigint DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') NOT NULL DEFAULT 'ACTIVE',
  `user_type` enum('EXTERNAL','INTERNAL') DEFAULT 'EXTERNAL',
  `currency` varchar(20) DEFAULT NULL,
  `contractor_id` bigint DEFAULT NULL,
  `skills` text,
  `experience` varchar(50) DEFAULT NULL,
  `skills_other` text,
  `user_bank` bigint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=latin1;

/*Data for the table `user_info` */

insert  into `user_info`(`id`,`user_id`,`profile_pic`,`profession`,`organization_name`,`organization_details`,`logo`,`id_proof_name`,`id_proof_number`,`id_proof_document`,`fb_link`,`website_link`,`insta_link`,`linkdean_link`,`standardPayment`,`support_type`,`created_by`,`created_on`,`updated_by`,`updated_on`,`status`,`user_type`,`currency`,`contractor_id`,`skills`,`experience`,`skills_other`,`user_bank`) values 
(1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'fb Link','46','instaLink','linkdeanLink','233.00','ONLINE & OFFLINE',1,'2022-07-02 16:47:57',NULL,'2022-07-02 17:39:38','ACTIVE','EXTERNAL',NULL,NULL,NULL,NULL,NULL,NULL),
(2,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','',NULL,'OFFLINE',1,'2022-07-29 15:15:06',NULL,'2022-08-14 11:19:53','ACTIVE',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(3,3,NULL,'ENGINEER1',NULL,NULL,NULL,'adsad','sdsad',NULL,'https://www.facebook.com/','https://www.google.com/',NULL,'https://www.linkedin.com/feed/',NULL,'ONLINE',2,'2022-08-24 20:02:17',3,'2022-08-26 16:48:30','ACTIVE',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(41,77,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','',NULL,'ONLINE',2,'2022-08-27 15:19:30',2,'2022-08-27 19:53:36','ACTIVE',NULL,NULL,NULL,'JAVA','5',NULL,NULL),
(42,78,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','0.00','ONLINE',2,'2022-08-27 18:49:05',2,'2022-09-16 12:17:58','ACTIVE','EXTERNAL','INR',NULL,'JAVA,AWS','7+','MS',NULL),
(43,79,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','0.00','ONLINE',2,'2022-08-27 18:51:20',2,'2022-08-27 20:01:35','ACTIVE','EXTERNAL','INR',NULL,'JAVA','7',NULL,NULL),
(44,80,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','0.00','ONLINE',2,'2022-08-27 18:52:59',2,'2022-08-27 20:04:06','ACTIVE','EXTERNAL','INR',NULL,'PYTHON','0',NULL,NULL),
(45,81,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','0.00','ONLINE',2,'2022-08-27 18:56:27',NULL,NULL,'ACTIVE','EXTERNAL','INR',NULL,'DEVOPS,AWS',NULL,NULL,NULL),
(46,82,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','0.00','ONLINE',2,'2022-08-27 18:59:36',NULL,NULL,'ACTIVE','EXTERNAL','INR',NULL,'SALESFORCE DEVELOPER',NULL,NULL,NULL),
(47,83,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','0.00','ONLINE',2,'2022-08-27 19:01:06',NULL,NULL,'ACTIVE','EXTERNAL','INR',NULL,'SALESFORCE ADMIN',NULL,NULL,NULL),
(48,84,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','0.00','ONLINE',2,'2022-08-27 19:04:51',NULL,NULL,'ACTIVE','EXTERNAL','INR',NULL,'PYTHON',NULL,NULL,NULL),
(49,85,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','0.00','ONLINE',2,'2022-08-27 19:06:36',NULL,NULL,'ACTIVE','EXTERNAL','INR',NULL,'PYTHON,AWS',NULL,NULL,NULL),
(50,86,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','0.00','ONLINE',2,'2022-08-27 20:07:07',NULL,NULL,'ACTIVE','EXTERNAL','INR',NULL,'PYTHON,AWS','7+',NULL,NULL),
(51,87,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','0.00','ONLINE',2,'2022-08-27 20:16:29',NULL,NULL,'ACTIVE','EXTERNAL','INR',NULL,'QA MANUAL','6+',NULL,NULL),
(52,88,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','0.00','ONLINE',2,'2022-08-27 20:26:23',2,'2022-08-27 20:28:09','ACTIVE','EXTERNAL','INR',NULL,'QA AUTOMATION,API TESTING,SELENIUM','5+',NULL,NULL),
(53,89,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','0.00','ONLINE',2,'2022-08-27 20:31:03',NULL,NULL,'ACTIVE','EXTERNAL','INR',NULL,'QA AUTOMATION,API TESTING,SELENIUM','0',NULL,NULL),
(54,90,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','0.00','ONLINE',2,'2022-08-27 20:33:58',NULL,NULL,'ACTIVE','EXTERNAL','INR',NULL,'JAVA','9+',NULL,NULL),
(55,91,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','0.00','ONLINE',2,'2022-08-27 20:37:53',NULL,NULL,'ACTIVE','EXTERNAL','INR',NULL,'JAVA','0',NULL,NULL),
(56,92,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','0.00','ONLINE',2,'2022-08-27 20:40:18',NULL,NULL,'ACTIVE','EXTERNAL','INR',NULL,'JAVA','8+',NULL,NULL),
(57,93,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','0.00','ONLINE',2,'2022-08-27 20:50:19',NULL,NULL,'ACTIVE','EXTERNAL','INR',NULL,'JAVA','6+',NULL,NULL),
(58,94,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','0.00','ONLINE',2,'2022-08-27 20:53:32',NULL,NULL,'ACTIVE','EXTERNAL','INR',NULL,'JAVA,.NET','9+',NULL,NULL),
(59,95,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','',NULL,'ONLINE',2,'2022-08-31 14:34:12',NULL,NULL,'ACTIVE',NULL,NULL,NULL,'JAVA','5+',NULL,NULL),
(60,96,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','',NULL,'ONLINE',2,'2022-08-31 15:58:42',NULL,NULL,'ACTIVE',NULL,NULL,11,'JAVA','4+',NULL,NULL),
(61,97,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','',NULL,'ONLINE',2,'2022-08-31 16:38:47',NULL,NULL,'ACTIVE',NULL,NULL,NULL,'PYTHON','5+',NULL,NULL),
(62,98,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','0.00','ONLINE',2,'2022-08-31 18:33:31',NULL,NULL,'ACTIVE','EXTERNAL','INR',NULL,'DEVOPS','1.3+',NULL,NULL),
(63,99,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','0.00','ONLINE',2,'2022-08-31 18:35:26',NULL,NULL,'ACTIVE','EXTERNAL','INR',NULL,'DEVOPS','6+',NULL,NULL),
(64,100,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','0.00','ONLINE',2,'2022-08-31 18:37:41',2,'2022-08-31 18:39:51','ACTIVE','EXTERNAL','INR',15,'JAVA,API TESTING,SELENIUM','7',NULL,NULL),
(65,102,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','0.00','ONLINE',2,'2022-08-31 18:51:15',2,'2022-08-31 18:52:09','ACTIVE','EXTERNAL','INR',NULL,'AWS,AZURE DATA FACTORY,AWS DATA ENGINEERING','5+',NULL,NULL),
(66,103,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','0.00','ONLINE',2,'2022-08-31 22:54:23',2,'2022-08-31 23:06:39','ACTIVE','EXTERNAL','INR',NULL,'DEVOPS,AWS','4+','Kubenetes,Jenkins,Ansible,docker,aws',NULL),
(67,104,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','',NULL,'ONLINE',2,'2022-09-02 23:22:17',NULL,NULL,'ACTIVE',NULL,NULL,NULL,'DEVOPS,AWS','5+','others',NULL),
(68,105,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','0.00','ONLINE',2,'2022-09-02 23:29:57',NULL,NULL,'ACTIVE','EXTERNAL','INR',NULL,'DEVOPS,AWS,QA MANUAL','8+','NA',NULL),
(69,106,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','',NULL,'ONLINE',2,'2022-09-03 11:29:26',NULL,'2022-09-14 17:23:22','INACTIVE',NULL,NULL,NULL,'PYTHON','4+','NA',NULL),
(70,107,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','800.00','ONLINE',2,'2022-09-03 12:11:02',2,'2022-09-03 12:11:40','ACTIVE','EXTERNAL','INR',NULL,'ANGULAR','9+','Total 9+yrs but in angular 5yrs+',NULL),
(71,108,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','0.00','ONLINE',2,'2022-09-03 12:20:14',NULL,NULL,'ACTIVE','EXTERNAL','INR',NULL,'ANGULAR','3.5+','Angular UI Developer',NULL),
(72,109,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','0.00','ONLINE',2,'2022-09-03 12:22:45',NULL,NULL,'ACTIVE','EXTERNAL','INR',NULL,'ANGULAR','5+','UI developer with 2+yrs in Angular',NULL),
(73,111,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','NA','ONLINE',2,'2022-09-03 14:36:46',NULL,NULL,'ACTIVE','EXTERNAL','INR',16,'QA MANUAL,API TESTING,SELENIUM','5+','NA',NULL),
(74,112,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','',NULL,'ONLINE',2,'2022-09-03 14:40:15',NULL,NULL,'ACTIVE',NULL,NULL,NULL,'QA MANUAL,API TESTING,SELENIUM','4+','Testing',NULL),
(75,113,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','',NULL,'ONLINE',2,'2022-09-03 16:17:10',NULL,NULL,'ACTIVE',NULL,NULL,NULL,'PYTHON','0','NA',NULL),
(76,114,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','0','ONLINE',2,'2022-09-03 16:45:21',NULL,NULL,'ACTIVE','EXTERNAL','INR',10,'JAVA','5+','NA',NULL),
(77,116,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','',NULL,'ONLINE',2,'2022-09-03 16:48:47',NULL,NULL,'ACTIVE',NULL,NULL,11,'JAVA','0','NA',NULL),
(78,117,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','0','ONLINE',2,'2022-09-06 23:31:20',NULL,NULL,'ACTIVE','EXTERNAL','INR',NULL,'JAVA,AWS','0','Java, springboot, mysql, docker, kubernetes\r\n Microservices\r\nAws',NULL),
(79,118,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','0','ONLINE',2,'2022-09-06 23:48:52',NULL,NULL,'ACTIVE','EXTERNAL','INR',NULL,'JAVA,.NET,PYTHON','6+','na',NULL),
(80,119,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','',NULL,'ONLINE',2,'2022-09-07 18:11:02',NULL,NULL,'ACTIVE',NULL,NULL,NULL,'DEVOPS,AWS','0','AWS, GCP\r\n\r\nGradle, Maven, \r\n\r\nJenkins, Kubernetes(EKS, GKE), ArgoCD, Docker, ECR, \r\n\r\nJfrog, Sonarqube\r\n\r\nPrometheus, Grafana, Kibana, \r\n\r\nTerraform \r\n\r\nMySQL, MongoDB\r\nLanguage - Go, Python, Java',NULL),
(81,120,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','',NULL,'ONLINE',2,'2022-09-07 21:03:42',NULL,NULL,'ACTIVE',NULL,NULL,NULL,'JAVA','0','Migration Struts Dao to Spring MVC\r\n\r\nStore Procedure, SQL, Java8, Struts',NULL),
(82,121,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','NA','ONLINE',2,'2022-09-09 10:45:35',NULL,NULL,'ACTIVE','EXTERNAL','INR',NULL,'.NET,AZURE DATA FACTORY','5+','.net fullstack,frontend,Azure',NULL),
(83,122,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','NA','ONLINE',2,'2022-09-09 12:25:50',NULL,NULL,'ACTIVE','EXTERNAL','INR',NULL,'JAVA','10','J2ee,servelet,jsp,spring,hibernate',NULL),
(84,123,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','',NULL,'ONLINE',2,'2022-09-09 14:31:39',NULL,NULL,'ACTIVE',NULL,NULL,NULL,'AWS','0','github actions jenkins datadog new relic bash terraform kubernetes\r\n-------------- Requirements: Typically has 3 to 5 years of related DevOps experience Deep Knowledge of commonly used DevOps tooling concepts, hybrid-cloud environment Able to code in advance levels of Bash/Python with Golang a major plus 2-4 years of experience in any of the IaC technology such as Terraform, Ansible, Pulumi Working knowledge of CI/CD and related concepts Works collaboratively and seeks input and feedback from co-',NULL),
(85,124,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','NA','ONLINE',2,'2022-09-10 08:49:06',NULL,'2022-09-21 12:05:56','INACTIVE','EXTERNAL','INR',NULL,'REACT JS,UI DEVELOPER,MEAN STACK DEVELOPER','5','Node js,html,css,js',NULL),
(86,125,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','',NULL,'ONLINE',2,'2022-09-10 09:19:21',NULL,NULL,'ACTIVE',NULL,NULL,NULL,'ANGULAR,REACT JS','2.5','git,htnl,css,js',NULL),
(87,126,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','',NULL,'ONLINE',2,'2022-09-13 11:35:51',NULL,NULL,'ACTIVE',NULL,NULL,NULL,'JAVA,ANGULAR,JAVA FULLSTACK DEVELOPER','0','java,angular,react',NULL),
(88,127,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','',NULL,'ONLINE',2,'2022-09-13 20:22:50',NULL,NULL,'ACTIVE',NULL,NULL,NULL,'JAVA,AWS','0','java 8, microservices, rest api, mariya db, snowflake, ocp',NULL),
(89,128,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','NA','ONLINE',2,'2022-09-14 15:37:16',NULL,NULL,'ACTIVE','EXTERNAL','INR',16,'DEVOPS,AWS DEVOPS','8+','Fastlane,jenkin,sonaruibe',NULL),
(90,129,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','NA','ONLINE',2,'2022-09-14 15:40:39',NULL,NULL,'ACTIVE','EXTERNAL','INR',16,'ANGULAR','5+','ADA,CICD,JS,UI',NULL),
(91,130,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','NA','ONLINE',2,'2022-09-14 15:52:49',NULL,NULL,'ACTIVE','EXTERNAL','INR',16,'JAVA,JAVA FULLSTACK DEVELOPER','5+','NA',NULL),
(92,131,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','NA','ONLINE',2,'2022-09-14 15:56:06',NULL,NULL,'ACTIVE','EXTERNAL','INR',16,'DEVOPS','5+','NA',NULL),
(93,132,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','NA','ONLINE',2,'2022-09-14 16:04:09',NULL,NULL,'ACTIVE','EXTERNAL','INR',NULL,'QA AUTOMATION,API TESTING','4+','cypress,Api test,UI Automation',NULL),
(94,133,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','NA','ONLINE',2,'2022-09-15 21:43:53',NULL,NULL,'ACTIVE','EXTERNAL','INR',NULL,'DEVOPS,AWS','5+',' 4+ exp in DevOps\r\nMasters in Data Engineering + Data Science \r\nCurrently working in Capgemini had 4+ exp',NULL),
(95,134,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','',NULL,'ONLINE',2,'2022-09-16 21:54:58',NULL,NULL,'ACTIVE',NULL,NULL,11,'ANGULAR','0','NA',NULL),
(96,135,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','NA','ONLINE',2,'2022-09-16 21:57:12',NULL,NULL,'ACTIVE','EXTERNAL','INR',NULL,'ANGULAR','6+','na',NULL),
(97,136,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','NA','ONLINE',2,'2022-09-20 11:20:37',NULL,NULL,'ACTIVE','EXTERNAL','INR',NULL,'JAVA','7+','MS,JSP,SRING,SPRINGBOOT,JPA,HIBERNATE,JS',NULL),
(98,137,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','',NULL,'ONLINE',2,'2022-09-20 11:36:10',NULL,NULL,'ACTIVE',NULL,NULL,NULL,'JAVA','0','Spring \r\nJavaScript \r\nJquery\r\nSqlyog \r\nJson',NULL),
(99,138,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','600INR/HRS & 35K/MONTH/2HRS','ONLINE',2,'2022-09-20 12:38:03',NULL,NULL,'ACTIVE','EXTERNAL','INR',NULL,'DEVOPS,AWS','9+','Aws,linux, Jenkins,  git , shall, yaml, ansible, gitlab, bit bucket, docker, kubernetes,terraform, cloudformation,json,\r\nCICD pipeline',NULL),
(100,139,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','30k/month','ONLINE',2,'2022-09-20 22:39:58',NULL,NULL,'ACTIVE','EXTERNAL','INR',NULL,'SALESFORCE DEVELOPER','3.5+','Salesforce Devlopment, LWC, Aura, configuration and customization',NULL),
(101,140,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','NA','ONLINE',2,'2022-09-20 22:48:38',NULL,NULL,'ACTIVE','EXTERNAL','INR',NULL,'ANGULAR','2.5+','HTML , CSS , JavaScrip,HTML , CSS , JavaScrip,Github',NULL),
(102,141,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','28k/month , 2hrs job support','ONLINE',2,'2022-09-21 12:03:02',NULL,NULL,'ACTIVE','EXTERNAL','INR',NULL,'DEVOPS,AWS','4+','Operating Systems\r\nRed Hat Linux, CentOS, ubuntu, windows\r\nApplication Server\r\nTomcat, Nginx\r\nVersion Control Tool\r\nSVN, GitHub\r\nContinuous Integration Tool\r\nJenkins\r\nTicketing Tool\r\nService now\r\nBuild Tool\r\nMaven\r\nScripting\r\n Shell scripting, groovy\r\nMonitoring Tools\r\nNagios, cloud watch, Kibana\r\nCloud Environment\r\nAWS, Azure\r\nDevOps Tools\r\nTerraform,  Kubernetes, GitHub, Maven, Jenkins, Docker\r\n\r\n',NULL),
(103,142,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','',NULL,'ONLINE',2,'2022-09-24 15:54:15',NULL,NULL,'ACTIVE',NULL,NULL,NULL,'JAVA FULLSTACK DEVELOPER','0','Front end React , Angular and backend java',NULL),
(104,143,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','NA','ONLINE',2,'2022-09-24 15:57:57',2,'2022-09-24 16:03:34','ACTIVE','EXTERNAL','INR',10,'JAVA','5+','na ',NULL);

/*Table structure for table `user_requests` */

DROP TABLE IF EXISTS `user_requests`;

CREATE TABLE `user_requests` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `address` text,
  `organization_name` varchar(100) DEFAULT NULL,
  `created_by` bigint DEFAULT NULL,
  `created_on` datetime NOT NULL,
  `updated_by` bigint DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') DEFAULT NULL,
  `user_type` enum('FREELANCER','CLIENT','CONSULTANCY') DEFAULT NULL,
  `remark` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `user_requests` */

insert  into `user_requests`(`id`,`name`,`email`,`mobile`,`address`,`organization_name`,`created_by`,`created_on`,`updated_by`,`updated_on`,`status`,`user_type`,`remark`) values 
(8,'AMIT MEHER MEHER','amit@gmail.com','+917878919981','NA','NONE',NULL,'2022-08-26 16:28:23',NULL,NULL,'ACTIVE','CLIENT','NA'),
(9,'eee','44@gmail.com','42423434','qwrwqr','NONE',NULL,'2022-08-28 15:35:15',NULL,NULL,'ACTIVE','FREELANCER','wrwqr');

/*Table structure for table `user_role` */

DROP TABLE IF EXISTS `user_role`;

CREATE TABLE `user_role` (
  `user_role_id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` bigint NOT NULL,
  `role_id` bigint NOT NULL,
  `admin_id` bigint DEFAULT NULL,
  `agent_id` bigint DEFAULT NULL,
  `created_by` bigint NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_by` bigint DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') NOT NULL DEFAULT 'ACTIVE',
  PRIMARY KEY (`user_role_id`),
  UNIQUE KEY `UC_user_role_agent_admin` (`user_id`,`role_id`,`admin_id`,`agent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8mb3 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Data for the table `user_role` */

insert  into `user_role`(`user_role_id`,`user_id`,`role_id`,`admin_id`,`agent_id`,`created_by`,`created_on`,`updated_by`,`updated_on`,`status`) values 
(1,1,1,1,NULL,1,'2022-06-29 18:38:24',NULL,NULL,'ACTIVE'),
(2,2,2,1,NULL,1,'2022-07-29 14:18:43',NULL,NULL,'ACTIVE'),
(3,3,6,1,NULL,2,'2022-08-24 20:02:17',NULL,NULL,'ACTIVE'),
(64,77,3,1,2,2,'2022-08-27 15:19:30',NULL,NULL,'ACTIVE'),
(65,78,4,1,2,2,'2022-08-27 18:49:05',NULL,NULL,'ACTIVE'),
(66,79,4,1,2,2,'2022-08-27 18:51:20',NULL,NULL,'ACTIVE'),
(67,80,4,1,2,2,'2022-08-27 18:52:59',NULL,NULL,'ACTIVE'),
(68,81,4,1,2,2,'2022-08-27 18:56:27',NULL,NULL,'ACTIVE'),
(69,82,4,1,2,2,'2022-08-27 18:59:36',NULL,NULL,'ACTIVE'),
(70,83,4,1,2,2,'2022-08-27 19:01:06',NULL,NULL,'ACTIVE'),
(71,84,4,1,2,2,'2022-08-27 19:04:51',NULL,NULL,'ACTIVE'),
(72,85,4,1,2,2,'2022-08-27 19:06:36',NULL,NULL,'ACTIVE'),
(73,86,4,1,2,2,'2022-08-27 20:07:07',NULL,NULL,'ACTIVE'),
(74,87,4,1,2,2,'2022-08-27 20:16:29',NULL,NULL,'ACTIVE'),
(75,88,4,1,2,2,'2022-08-27 20:26:23',NULL,NULL,'ACTIVE'),
(76,89,4,1,2,2,'2022-08-27 20:31:03',NULL,NULL,'ACTIVE'),
(77,90,4,1,2,2,'2022-08-27 20:33:58',NULL,NULL,'ACTIVE'),
(78,91,4,1,2,2,'2022-08-27 20:37:53',NULL,NULL,'ACTIVE'),
(79,92,4,1,2,2,'2022-08-27 20:40:18',NULL,NULL,'ACTIVE'),
(80,93,4,1,2,2,'2022-08-27 20:50:19',NULL,NULL,'ACTIVE'),
(81,94,4,1,2,2,'2022-08-27 20:53:32',NULL,NULL,'ACTIVE'),
(82,95,3,1,2,2,'2022-08-31 14:34:12',NULL,NULL,'ACTIVE'),
(83,96,3,1,2,2,'2022-08-31 15:58:42',NULL,NULL,'ACTIVE'),
(84,97,3,1,2,2,'2022-08-31 16:38:47',NULL,NULL,'ACTIVE'),
(85,98,4,1,2,2,'2022-08-31 18:33:31',NULL,NULL,'ACTIVE'),
(86,99,4,1,2,2,'2022-08-31 18:35:26',NULL,NULL,'ACTIVE'),
(87,100,4,1,2,2,'2022-08-31 18:37:41',NULL,NULL,'ACTIVE'),
(88,102,4,1,2,2,'2022-08-31 18:51:15',NULL,NULL,'ACTIVE'),
(89,103,4,1,2,2,'2022-08-31 22:54:23',NULL,NULL,'ACTIVE'),
(90,104,3,1,2,2,'2022-09-02 23:22:17',NULL,NULL,'ACTIVE'),
(91,105,4,1,2,2,'2022-09-02 23:29:57',NULL,NULL,'ACTIVE'),
(92,106,3,1,2,2,'2022-09-03 11:29:26',NULL,NULL,'INACTIVE'),
(93,107,4,1,2,2,'2022-09-03 12:11:02',NULL,NULL,'ACTIVE'),
(94,108,4,1,2,2,'2022-09-03 12:20:14',NULL,NULL,'ACTIVE'),
(95,109,4,1,2,2,'2022-09-03 12:22:45',NULL,NULL,'ACTIVE'),
(96,111,4,1,2,2,'2022-09-03 14:36:46',NULL,NULL,'ACTIVE'),
(97,112,3,1,2,2,'2022-09-03 14:40:15',NULL,NULL,'ACTIVE'),
(98,113,3,1,2,2,'2022-09-03 16:17:10',NULL,NULL,'ACTIVE'),
(99,114,4,1,2,2,'2022-09-03 16:45:21',NULL,NULL,'ACTIVE'),
(100,116,3,1,2,2,'2022-09-03 16:48:47',NULL,NULL,'ACTIVE'),
(101,117,4,1,2,2,'2022-09-06 23:31:19',NULL,NULL,'ACTIVE'),
(102,118,4,1,2,2,'2022-09-06 23:48:52',NULL,NULL,'ACTIVE'),
(103,119,3,1,2,2,'2022-09-07 18:11:02',NULL,NULL,'ACTIVE'),
(104,120,3,1,2,2,'2022-09-07 21:03:42',NULL,NULL,'ACTIVE'),
(105,121,4,1,2,2,'2022-09-09 10:45:35',NULL,NULL,'ACTIVE'),
(106,122,4,1,2,2,'2022-09-09 12:25:50',NULL,NULL,'ACTIVE'),
(107,123,3,1,2,2,'2022-09-09 14:31:39',NULL,NULL,'ACTIVE'),
(108,124,4,1,2,2,'2022-09-10 08:49:06',NULL,NULL,'INACTIVE'),
(109,125,3,1,2,2,'2022-09-10 09:19:21',NULL,NULL,'ACTIVE'),
(110,126,3,1,2,2,'2022-09-13 11:35:51',NULL,NULL,'ACTIVE'),
(111,127,3,1,2,2,'2022-09-13 20:22:50',NULL,NULL,'ACTIVE'),
(112,128,4,1,2,2,'2022-09-14 15:37:16',NULL,NULL,'ACTIVE'),
(113,129,4,1,2,2,'2022-09-14 15:40:39',NULL,NULL,'ACTIVE'),
(114,130,4,1,2,2,'2022-09-14 15:52:49',NULL,NULL,'ACTIVE'),
(115,131,4,1,2,2,'2022-09-14 15:56:06',NULL,NULL,'ACTIVE'),
(116,132,4,1,2,2,'2022-09-14 16:04:09',NULL,NULL,'ACTIVE'),
(117,133,4,1,2,2,'2022-09-15 21:43:53',NULL,NULL,'ACTIVE'),
(118,134,3,1,2,2,'2022-09-16 21:54:58',NULL,NULL,'ACTIVE'),
(119,135,4,1,2,2,'2022-09-16 21:57:12',NULL,NULL,'ACTIVE'),
(120,136,4,1,2,2,'2022-09-20 11:20:37',NULL,NULL,'ACTIVE'),
(121,137,3,1,2,2,'2022-09-20 11:36:10',NULL,NULL,'ACTIVE'),
(122,138,4,1,2,2,'2022-09-20 12:38:03',NULL,NULL,'ACTIVE'),
(123,139,4,1,2,2,'2022-09-20 22:39:58',NULL,NULL,'ACTIVE'),
(124,140,4,1,2,2,'2022-09-20 22:48:38',NULL,NULL,'ACTIVE'),
(125,141,4,1,2,2,'2022-09-21 12:03:02',NULL,NULL,'ACTIVE'),
(126,142,3,1,2,2,'2022-09-24 15:54:15',NULL,NULL,'ACTIVE'),
(127,143,4,1,2,2,'2022-09-24 15:57:57',NULL,NULL,'ACTIVE');

/*Table structure for table `user_skils` */

DROP TABLE IF EXISTS `user_skils`;

CREATE TABLE `user_skils` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` bigint NOT NULL,
  `skils_id` bigint NOT NULL,
  `rating` enum('1','2','3','4','5') DEFAULT '4',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `user_skils` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
